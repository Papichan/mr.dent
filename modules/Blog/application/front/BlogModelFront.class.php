<?php

/**
 * Модель для работы с модулем Blog во фронт-енде
 *
 * @version 2.0
 */
class BlogModelFront {

    public $rootAlias = 'blog';

    public function __construct($rootAlias = null)
    {
        if ($rootAlias) {
            $this->rootAlias = $rootAlias;
        }
    }

    public function countNews($sectionID){
        $query = 'SELECT count(nl.`newsID`)
                    FROM `' . Blog::$newsTableName . '` nl
                    LEFT JOIN `' . Blog::$sectionsTableName . '` s ON s.`sectionID` = nl.`sectionID`
                    LEFT JOIN `' . TablesNames::$IMAGES_TABLE_NAME . '` im ON im.`imageID` = nl.`imageID`
                    LEFT JOIN `greeny_blog_colors` col ON col.`colorID` = nl.`colorID`
                    WHERE nl.`isPublished` = 1 AND nl.`isDeleted` = 0 AND s.`sectionID` = '.$sectionID.'
                   ';
        return DB::QueryOneValue($query);
    }

    public function GetNews($sect = null, $start = 0, $limit = 100)
    {
        $sectCond = "";
        if ($sect == 'unsort') {
            $sectCond = 'nl.`sectionID` IS NULL AND';
        }
        elseif ($sect) {
            $sectCond = 'nl.`sectionID` = ' . $sect . " AND";
        }
        $query = 'SELECT SQL_CALC_FOUND_ROWS
                    nl.`newsID`, nl.`size`, nl.`title` AS title, nl.`summary` AS summary, nl.`publicationDate`, col.`bgColor`, col.`textColor`,
                    concat(' . DB::Quote("/" . $this->rootAlias) . ', IF (nl.sectionID IS NOT NULL, concat("/",s.alias), "" ), "/",nl.newsID,"/") as fullAlias, s.*, IF (nl.body = "", 1 , 0) as isBody,im.src
                    FROM `' . Blog::$newsTableName . '` nl
                    LEFT JOIN `' . Blog::$sectionsTableName . '` s ON s.`sectionID` = nl.`sectionID`
                    LEFT JOIN `' . TablesNames::$IMAGES_TABLE_NAME . '` im ON im.`imageID` = nl.`imageID`
                    LEFT JOIN `greeny_blog_colors` col ON col.`colorID` = nl.`colorID`
                    WHERE ' . $sectCond . ' nl.`isPublished` = 1 AND nl.`isDeleted` = 0
                    ORDER BY nl.`publicationDate` DESC
                    LIMIT ' . $start . ', ' . $limit . '
                ';
        return DB::QueryToArray($query);
    }

    public function GetNewsItem($newsID, $sectionID = null)
    {
        $section = $sectionID ? "nl.sectionID = " . $sectionID : "nl.sectionID IS NULL";
        $query = '
			SELECT
				nl.`newsID`,
				nl.`summary`,
				nl.`title` AS title,
				nl.`body` AS body,
				nl.`publicationDate`,
                                a.*,
				s.alias,
				im.src
			FROM `' . Blog::$newsTableName . '` nl
			LEFT JOIN `' . Blog::$sectionsTableName . '` s ON s.`sectionID` = nl.`sectionID`
			LEFT JOIN `' . TablesNames::$IMAGES_TABLE_NAME . '` im ON im.`imageID` = nl.`imageID`
			LEFT JOIN `greeny_blog_author` a ON a.`authorID` = nl.`authorID`
			WHERE nl.`newsID`=' . $newsID . ' AND  ' . $section . '
			LIMIT 0, 1
		';
        $result = DB::QueryOneRecordToArray($query);
        if (!empty($result)) {
            $result['gallery'] = $this->GetNewsGallery($result['newsID']);
        }
        return $result;
    }

    public function GetFoundRows()
    {
        $query = 'SELECT FOUND_ROWS()';
        return DB::QueryOneValue($query);
    }

    public function GetSectionID($alias)
    {
        $query = "SELECT sectionID FROM " . Blog::$sectionsTableName . " WHERE alias=" . DB::Quote($alias);
        return DB::QueryOneValue($query);
    }

    private function GetNewsGallery($newsID)
    {
        $query = "SELECT * FROM `" . Blog::$galleryTableName . "` g INNER JOIN `" . TablesNames::$IMAGES_TABLE_NAME . "` im ON im.imageID=g.imageID WHERE g.newsID=" . $newsID;
        return DB::QueryToArray($query);
    }

}

?>