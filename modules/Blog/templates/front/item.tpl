<!-- wrapper  start -->
<div id="wrapper" class="page-article">
    <!-- header  start -->
    <header id="header">
        <div class="wrap">
            <nav class="head-construction">
                <div class="holder">
                    <strong class="logo"><a href="/">MitLabs</a></strong>
                    <div class="head-contacts">
                        <strong class="phone"><a href="tel:{$SETTINGS.front.phone1.SMALL}">{$SETTINGS.front.phone1.ccode} ({$SETTINGS.front.phone1.tcode}) {$SETTINGS.front.phone1.body}</a></strong>
                        <a class="link-mail" href="mailto:{$SETTINGS.front.email}">{$SETTINGS.front.email}</a>
                    </div>
                </div>
                <input id="open-menu" class="open-check" type="checkbox">
                <div class="menu-holder">
                    <!--<input class="open-inp" type="checkbox" id="blog-list" name="tabs" /-->
                    <ul class="panel">
                        {foreach from=$SUBMENU item=section key=key}
                            {if ($section.count_news > 0)}
                                <li {if ($NEWS.alias == $section.alias)}class="active"{/if}><a href="{$section.fullAlias}"><span>{$section.caption}</span></a></li>
                            {/if}
                        {/foreach}
                    </ul>
                    <label for="open-menu" class="close-menu">
                        <span class="t"></span>
                        <span class="b"></span>
                    </label>
                </div>
                <label for="open-menu" class="toogle-menu">
                    <span class="t"></span>
                    <span class="c"></span>
                    <span class="b"></span>
                </label>
                <label for="open-menu" class="nav-overlay">&nbsp;</label>
            </nav>
        </div>
    </header>            

    <!-- header  end 
         main  start -->
    <div id="main" class="start-anim">
        <div class="wrap">
            <div class="top-btn">
                <a href="/{if $PAGE.alias !== null}{$PAGE.alias}/{/if}" class="btn btn-back">на главную</a>
            </div>
            {if !empty($NEWS)}
                <div class="content">
                    <div class="img-main">
                        <div class="img-shadow">
                            {if !empty($NEWS.src)}
                                <img src="{$NEWS.src}"  width="1170" height="607" />
                            {else}
                                <img src="{$IMAGES_ALIAS}noimage.jpg" alt="image"  width="1170" height="607" />
                            {/if}
                        </div>
                        <div class="img-text">
                            <div class="text-center">
                                <div class="text-inner">
                                    <div class="text-last">
                                        <h1>{$NEWS.title}</h1>
                                        <div class="img-inner">
                                            <p>
                                                {if !empty($NEWS.summary)}
                                                    {$NEWS.summary}
                                                {else}
                                                    Эта статья стоит того, чтобы потратить время на чтение
                                                {/if}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="lh-low">
                        <div class="holder-text">
                            {if !empty($NEWS.body)}
                                {$NEWS.body}
                            {else}
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tellus ipsum, ullamcorper non urna in, faucibus eleifend arcu. Morbi ultricies velit quis aliquam pellentesque. Integer sit amet purus non magna convallis iaculis ut vitae enim. Maecenas justo massa, pretium id aliquam vel, fermentum nec orci. Nulla ac congue leo, volutpat vehicula est. Sed id sapien ac nunc interdum porttitor. Praesent lobortis fringilla risus, in molestie diam pharetra vitae. Integer euismod sagittis facilisis. Nam eleifend tellus a elit dignissim luctus. Donec et risus tortor. 
                                    Maecenas pulvinar ultricies orci, at condimentum velit hendrerit quis. Aenean ultricies diam laoreet dui elementum porta id a libero. Curabitur blandit in leo id viverra. Nulla non diam sit amet lacus ullamcorper congue sed vitae nisl. Suspendisse cursus mauris iaculis purus molestie varius. Nulla volutpat lectus id risus ornare fermentum. Nunc blandit eros ut felis imperdiet finibus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Fusce nisl mauris, placerat non euismod auctor, ultricies aliquet magna.</p>
                                {/if}
                            <div class="author">
                                <a href="http://mitlabs.ru/feedback/">
                                    <span class="author-img">
                                        {if (!empty($NEWS.imageID))}
                                            <img src="/getimage/{$NEWS.imageID}/" alt="image" width="95" height="95"/>
                                        {else}
                                            <img src="{$IMAGES_ALIAS}ava.jpg" alt="image" width="95" height="95"/>
                                        {/if}
                                    </span>
                                    <strong class="author-name">
                                        {$NEWS.authorName}
                                        <span class="author-rank">{$NEWS.authorPosition}</span>
                                    </strong>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            {else}
                <h2 style="text-align: center; padding: 30px;">Новость не существует</h2>
            {/if}

            <div class="main-foot">
                <div class="col">
                    <a href="/{if $PAGE.alias !== null}{$PAGE.alias}/{/if}" class="btn btn-back">обратно к списку статей</a>
                </div>
                <div class="col">
                    <ul class="list-net">
                        <li><a href="{$SETTINGS.front.instagram}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                        <!--<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>-->
                        <li><a href="{$SETTINGS.front.facebook}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        <li><a href="{$SETTINGS.front.vk}"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
                        <!--<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>-->
                    </ul>
                </div>
            </div>
        </div>

        {if !empty($MORE_NEWS)}                
            <div class="hold-blog foot-gallery">
                <div class="wrap"><span class="title">Статьи этого раздела</span></div>
                <div class="slide">
                    <div class="hold">
                        <ul>
                            {foreach from=$MORE_NEWS item=item key=key}
                                <li>
                                    <div class="item {if ($item.textColor == 'white')}item-text-white{else}item-text-black{/if}">
                                        <a href="{$item.fullAlias}" class="inner-item" style="background-color: {$item.bgColor}">
                                            <span class="item-img">
                                                {if !empty($item.src)}
                                                    <img src="{$item.src}" width="370" height="242" />
                                                {else}
                                                    <img src="{$IMAGES_ALIAS}noimage.jpg" alt="image" width="370" height="242" />
                                                {/if}
                                            </span>
                                            <strong class="title-item">{$item.title}</strong>
                                            {$item.summary}
                                            <span class="btn-item"></span>
                                        </a>
                                        <a href="/blog/{$item.alias}" class="btn-item btn-section"><span>{$item.caption}</span></a>
                                    </div>
                                </li>
                            {/foreach}
                        </ul>
                    </div>
                    {if $MORE_NEWS|@count > 3}
                        <span class="prev">&lArr;</span>
                        <span class="next">&rArr;</span>
                    {/if}
                </div>
            </div>
        {/if}
    </div>    
</div>
