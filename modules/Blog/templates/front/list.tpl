{foreach from=$NEWS item=item key=key}
{if ($item.size == 'large')}
    <div class="item item-large">
        <a href="{$item.fullAlias}" class="inner-item">
            <span class="item-img">
                {if !empty($item.src)}
                    <img src="{$item.src}" width="769" height="480" />
                {else}
                    <img src="{$IMAGES_ALIAS}noimage.jpg" alt="image" width="769" height="480" />
                {/if}
                <strong class="title-item">{$item.title}</strong>
            </span>
            <span class="col">
                <span class="text-item">{$item.summary}</span>
                <span class="btn-item"></span>
            </span>
        </a>
        <a href="#" class="btn-item btn-section"><span>раздел</span></a>
    </div>                           
{elseif ($item.size == 'middle')}
    <div class="item item-middle">
        <a href="{$item.fullAlias}" class="inner-item">
            <span class="item-img">
                {if !empty($item.src)}
                    <img src="{$item.src}" width="769" height="480" />
                {else}
                    <img src="{$IMAGES_ALIAS}noimage.jpg" alt="image" width="769" height="480" />
                {/if}
                <strong class="title-item">{$item.title}</strong>
            </span>
            <strong class="title-item">{$item.title}</strong>
            <span class="btn-item"></span>
        </a>
        <a href="#" class="btn-item btn-section"><span>раздел</span></a>
    </div>
{else}
    <div class="item {if ($item.textColor == 'white')}item-text-white{else}item-text-black{/if}">
        <a href="{$item.fullAlias}" class="inner-item" style="background-color: {$item.bgColor}">
            <span class="item-img">
                {if !empty($item.src)}
                    <img src="{$item.src}" width="370" height="242" />
                {else}
                    <img src="{$IMAGES_ALIAS}noimage.jpg" alt="image" width="370" height="242" />
                {/if}
            </span>
            <strong class="title-item">{$item.title}</strong>
            {$item.summary}
            <span class="btn-item"></span>
        </a>
        <a href="#" class="btn-item btn-section"><span>раздел</span></a>
    </div>
{/if}
{/foreach}
