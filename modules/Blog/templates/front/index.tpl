<!-- wrapper  start -->
<div id="wrapper">
    <!-- header  start -->
    <header id="header">
        <div class="wrap">
            <div class="head-construction">
                <div class="construction-inner">
                    <div class="holder">
                        <strong class="logo"><a href="/">MitLabs</a></strong>
                        <div class="head-contacts">
                            <strong class="phone"><a href="tel:{$SETTINGS.front.phone1.SMALL}">{$SETTINGS.front.phone1.ccode} ({$SETTINGS.front.phone1.tcode}) {$SETTINGS.front.phone1.body}</a></strong>
                            <a class="link-mail" href="mailto:{$SETTINGS.front.email}">{$SETTINGS.front.email}</a>
                        </div>
                    </div>
                    <div class="construction-foot">
                        <div class="row">
                            <div class="col">
                                <a href="/" class="btn btn-back"><span class="hidden-sm">обратно</span> на главную</a>
                            </div>
                            <div class="col">
                                <span class="text-net">Следите за нами в соцсетях</span>
                                <ul class="list-net">
                                    <li><a href="{$SETTINGS.front.instagram}"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                    <!--<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>-->
                                    <li><a href="{$SETTINGS.front.facebook}"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                    <li><a href="{$SETTINGS.front.vk}"><i class="fa fa-vk" aria-hidden="true"></i></a></li>
                                    <!--<li><a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="head-blue">
                    <input class="open-inp" type="checkbox" id="blog-list" name="tabs" />
                    <label class="title-head" for="blog-list">Рубрики <i class="fa fa-angle-down" aria-hidden="true"></i></label>
                    <ul class="panel">
                        {foreach from=$SUBMENU item=section key=key}
                            {if ($section.count_news > 0)}
                                <li {if ($SECTION == $section.alias)}class="active"{/if}><a href="{$section.fullAlias}"><span>{$section.caption}</span></a></li>
                            {/if}
                        {/foreach}
                    </ul>
                </div>
            </div>
        </div>
    </header>
    <!-- header  end 
         main  start -->
    <div id="main" class="start-anim">
        <div class="wrap">
            <div class="hold-blog">
                {if !empty($NEWS)}
                    <div class="row-blog">
                        {foreach from=$NEWS item=item key=key}
                            {if ($item.size == 'large')}
                                <div class="item item-large">
                                    <a href="{$item.fullAlias}" class="inner-item">
                                        <span class="item-img">
                                            {if !empty($item.src)}
                                                <img src="{$item.src}" width="769" height="480" />
                                            {else}
                                                <img src="{$IMAGES_ALIAS}noimage.jpg" alt="image" width="769" height="480" />
                                            {/if}
                                            <strong class="title-item">{$item.title}</strong>
                                        </span>
                                        <span class="col">
                                            <span class="text-item">{$item.summary}</span>
                                            <span class="btn-item"></span>
                                        </span>
                                    </a>
                                    <a href="/blog/{$item.alias}" class="btn-item btn-section"><span>{$item.caption}</span></a>
                                </div>                           
                            {elseif ($item.size == 'middle')}
                                <div class="item item-middle">
                                    <a href="{$item.fullAlias}" class="inner-item">
                                        <span class="item-img">
                                            {if !empty($item.src)}
                                                <img src="{$item.src}" width="769" height="480" />
                                            {else}
                                                <img src="{$IMAGES_ALIAS}noimage.jpg" alt="image" width="769" height="480" />
                                            {/if}
                                            <strong class="title-item">{$item.title}</strong>
                                        </span>
                                        <strong class="title-item">{$item.title}</strong>
                                        <span class="btn-item"></span>
                                    </a>
                                    <a href="/blog/{$item.alias}" class="btn-item btn-section"><span>{$item.caption}</span></a>
                                </div>
                            {else}
                                <div class="item {if ($item.textColor == 'white')}item-text-white{else}item-text-black{/if}">
                                    <a href="{$item.fullAlias}" class="inner-item" style="background-color: {$item.bgColor}">
                                        <span class="item-img">
                                            {if !empty($item.src)}
                                                <img src="{$item.src}" width="370" height="242" />
                                            {else}
                                                <img src="{$IMAGES_ALIAS}noimage.jpg" alt="image" width="370" height="242" />
                                            {/if}
                                        </span>
                                        <strong class="title-item">{$item.title}</strong>
                                        {$item.summary}
                                        <span class="btn-item"></span>
                                    </a>
                                    <a href="/blog/{$item.alias}" class="btn-item btn-section"><span>{$item.caption}</span></a>
                                </div>
                            {/if}
                        {/foreach}

                        {include file="paginator.tpl"}
                    </div>
                {/if}
            </div>
            <div class="foot-btn">
                <a href="/" class="btn btn-back">обратно на главную</a>
            </div>
        </div>
        <div class="foot-picture">
            <span class="picture">&nbsp;</span>
        </div>
    </div>
    <!-- main  end -->
</div>