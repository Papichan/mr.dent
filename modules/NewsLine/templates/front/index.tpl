
<!-- СТАРОЕ
<div class="block-hold">
	{if !empty($news)}
		<ul class="list-news">
			{foreach from=$news item=item key=key}
			<li>
				<a {if !$item.isBody}href="{$item.fullAlias}"{/if} class="figure">
					{if !empty($item.srcSmall)}
						<img src="{$item.srcSmall}" width="220" />
					{else}
						<img src="{$IMAGES_ALIAS}noimage.jpg" alt="image" width="220" height="165" />
					{/if}
				</a>
					<span class="date">{$item.publicationDate|date_format:"%e %m %Y":"":"rus"}</span>
					<a class="title-news" {if !$item.isBody}href="{$item.fullAlias}"{/if}>{$item.title}</a>
					<p>{$item.summary}</p>
				{if !$item.isBody}<a href="{$item.fullAlias}">Подробнее</a>{/if}
			</li>
			{/foreach}
		</ul>
		{include file="paginator.tpl"}
	{/if}
</div>
-->

<main>


	<!-- Хлеб Крошки -->
	<section class="up  up__news">
		<div class=" content-wrapper up__article">

			<ul class="breadcrumbs up__breadcrubms">
				<li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
				<li><a href="/{$PAGE.alias}/">Статьи</a></li>

			</ul>

		</div>
	</section>




	<!-- ВЫВОД ВСЕХ НОВОСТЕЙ -->
	{if !empty($news)}
	<section class="news-list content-wrapper">
		<h2 class="news-list__title">Статьи</h2>

		<div class="news-list__wrapper">

			{foreach from=$news item=item key=key}
			<div class="news-list__item">
				<div class="news-list__item-img">
					<img width="382" height="260" src="{$item.src}" alt="">
				</div>

				<div class="news-list__item-text">
					<h3 class="news-list__item-title">{$item.title}</h3>
					<p>{$item.summary}</p>
					<div class="news-list__item-button">
						<a href="{$item.fullAlias}" class="button button--transparent">Подробнее <span>>></span></a>
					</div>
				</div>
			</div>
			{/foreach}

		</div>
	</section>
	{/if}



	
<!-- ПАГИНАЦИЯ -->
  <div class="news__pages">
    <!--
    <ul>
      <li class="page-current"><a href="">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
    </ul>
  -->
  {include file='paginator.tpl'}
  </div>
  



	<!-- ФОРМА ОТПРАВКИ НИЗ-->
	<section id="appointment" class="form">
		<div class=" content-wrapper">
                    <img src="{$IMAGES_ALIAS}form-image.png" alt="">
                    <div class="form-wrapper">
		        <!-- Загрузка формы отправки "Записаться на прием" -->
                        {include file='form/form-appointment.tpl'}
                    </div>
		</div>
	</section>

</main>