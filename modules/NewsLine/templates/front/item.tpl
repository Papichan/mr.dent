
<!--=========================================================== ======================================================-->
<!--
<div class="group-info">
	{if !empty($news.src)}
		<figure class="baner-hold">
			<img src="{$news.src}" alt="image" width="480" height="320" />
		</figure>
	{/if}
	{$news.body}
</div>
{$news.publicationDate|date_format:"%e %m %Y":"":"rus"}
<br/><br/>
{if !empty($news.gallery)}
	<div class="block-hold">
		<h2>ФОТО</h2>
		<ul class="photo">
			{foreach from=$news.gallery item=photo}
				<li>
					<a data-rel="1" href="{$photo.src}" class="link-photo fancybox">
						<span class="text-center">
							<span class="text-inner">
								<span class="text-last">
									<img src="{$photo.srcSmall}" alt="image" width="90" height="59" />
								</span>
							</span>
						</span>
					</a>
				</li>
			{/foreach}
			<li class="last"></li>
		</ul>
	</div>
{/if}

<a href="/{$ROOT_ALIAS}/{if $news.alias !== null}{$news.alias}/{/if}">Вернуться в раздел</a>
-->
<!--=========================================================== ======================================================-->

<main>

	<h1 class="visually-hidden"> Центр стоматологии «Mr.Dent»</h1>
	<!-- ХЛЕБ.КРОШКИ -->
	<div class="content-wrapper up__article">
		<ul class="breadcrumbs up__breadcrubms">
			<li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
			<li><a href="{$BREADCRUMBS[0].alias}">Статьи<span style="color: #42c6d1;">></span></a></li>
			<li><a class="current-link">{$news.title}</a></li>
		</ul>
	</div>

                
	<!-- НАЧАЛО КАРТИНКА(БЛОШАЯ) -->
	<article class="article  content-wrapper ">
		<div class="content-wrapper article__background">
			<img width="100%" height="auto" src="{$news.bigImage}" alt="">
                        <!--<img width="100%" height="auto" src="/getimage/{$news.bigImageID}/315x215/" alt="">-->
		</div>






		<!-- СТАТЬЯ ЗАГОЛОВОК -->
		<!--
		<section class="article__part1 ">
			<h2 class="article__title">{$news.title}</h2>
			<p>
				Среди самых частых проблем, с которыми сталкиваются современные люди, можно выделить:
			</p>
		</section>
-->

		<!-- СТАТЬЯ -->
		<section class="article__part2 ">
			<h2 class="article__title">{$news.title}</h2>
			<div ><img src="{$news.smallImage}" width="534" height="377" alt="" class="article__left-img">
<!--
				<div class="article__part2-wrap">Среди самых частых проблем, с которыми сталкиваются современные люди, можно выделить:
					<ul class="article__part2-list">
						<li>Чувствительность зубов;</li>
						<li>Потемнение эмали, цветной налет;</li>
						<li>Кариес;</li>
						<li>Склонность к воспалениям десен.</li>
					</ul>
					{$news.body}
				</div>
-->
				{$news.body}
			</div>
		</section>


		<!-- БЛОКИ СТАТЬИ -->
		<section class="article__part3">
			{if !empty($news.blocks) }
				{foreach from=$news.blocks item='block'}
					<div class="article__block">
						<h3 class="article__block-title">{$block.title}</h3>
						<p>{$block.text}</p>
					</div>
				{/foreach}
			{/if}
		</section>



                <!-- JS СОЦ.СЕТИ и Переход по страницам -->
		<section class="article__down">
                        <!-- JavaScript СОЦ СЕТИ -->
			<div class="article__down-left">Поделиться:   <script type="text/javascript">
					{literal}
						(function() {
						if (window.pluso)if (typeof window.pluso.start == "function") return;
						if (window.ifpluso==undefined) { window.ifpluso = 1;
							var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
							s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
							s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
							var h=d[g]('body')[0];
							h.appendChild(s);
						}})();</script>
					{/literal}
                            <div class="pluso" data-background="transparent"
                                 data-options="medium,round,line,horizontal,nocounter,theme=04"
                                 data-services="vkontakte,google,facebook">
                            </div>
                        </div>
			<!-- Переход по страницам -->
                        <div class="article__down-right">
                            
                            {include file='paginator.tpl'}
                            <!--
				<a href="" class="article__prev"><img width="62" height="62" src="{$IMAGES_ALIAS}slide-left.png" alt=""></a>
				<a href="" class="article__next"><img width="62" height="62" src="{$IMAGES_ALIAS}slide-right.png" alt=""></a>
			
                            </div>
		</section>
	</article>





        
        
	<!-- ФОРМА ОТПРАВКИ НИЗ -->
	<section id="appointment" class="form">
		<div class=" content-wrapper">
			<img src="{$IMAGES_ALIAS}form-image.png" alt="">
			<div class="form-wrapper">
				 <!-- Загрузка формы отправки "Записаться на прием" -->
                                {include file='form/form-appointment.tpl'}
			</div>
		</div>
	</section>



	<!-- НОВОСТИ -->
	{if isset($TEN_NEWS)}
		<section class="news">
			<h2 class="news__title">Новости</h2>
			<div  class="news__wrapper news__slider content-wrapper">

				{foreach from=$TEN_NEWS item='new'}
					<div>
						<div class="news__item">
							<a href="{$new.fullAlias}">
								<div class="news__item-img">
									<img src="{$new.srcSmall}" alt="">
								</div>
								<div class="news__item-title">
									<a href="{$new.fullAlias}">{$new.summary}</a></div>
								<div class="news__item-data">{$news.publicationDate|date_format:"%e.%m.%Y"}</div>
							</a>
						</div>
					</div>
				{/foreach}

			</div>
			<div class="news__button">
				<a href="{$BREADCRUMBS[0].alias}" class="button button--pink">Все новости <span>>></span></a>
			</div>
		</section>
	{/if}

</main>