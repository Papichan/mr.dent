<?php
/**
 * Представление для работы с модулем NewsLine во фронт-енде
 * 
 * @version 2.0
 *
 */
class NewsLineViewAdmin extends AdminView
{
    public function __construct()
    {
        $this -> moduleName = 'NewsLine';
        parent::__construct();
        
        $this -> tpl -> menuTpl = $this -> tplsPath .'menu.tpl';
    }
    
    public function NewsList()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'list.tpl';
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
    }
    
    public function Sections()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'sections.tpl';     
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
    }
     public function Recycled()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'recycled.tpl';     
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
    }
    
     public function Menu()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'menu.tpl';     
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
    }
    
    public function Form()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'form.tpl';     
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
    }
}
?>