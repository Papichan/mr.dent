<?php

/**
 * Модель для работы с модулем NewsLine во фронт-енде
 *
 * @version 2.0
 */
class NewsLineModelFront
{
    public $rootAlias = 'newsline';

    public function __construct($rootAlias = null)
    {
        if ($rootAlias)
        {
            $this->rootAlias = $rootAlias;
        }
    }



    /** Извлекает связанные блоки DependendTables для страницы отдельной новости
     * @return array
     */
    public function getNewsBlocks($newsID)
    {
        $blocks = DB::QueryToArray(
            'SELECT nb.title, nb.text
                    FROM  `news_blocks` as nb 
                    INNER JOIN `greeny_nl_news` as d ON nb.newsID = d.newsID
                    WHERE nb.newsID = '.$newsID
        );

        return $blocks;
    }



    public function GetNews($sect = null, $start = 0, $limit = 100)
    {
		$sectCond = "";
		if ($sect == 'unsort')
		{
			$sectCond = 'nl.`sectionID` IS NULL AND';
		}
		elseif ($sect)
		{
			$sectCond = 'nl.`sectionID` = ' . $sect . " AND";
		}
		$query = '
			SELECT SQL_CALC_FOUND_ROWS
				nl.`newsID`, nl.`title` AS title, nl.`summary` AS summary, nl.`publicationDate`, nl.`bigImageID`,
                concat(' . DB::Quote("/" . $this->rootAlias) . ', IF (nl.sectionID IS NOT NULL, concat("/",s.alias), "" ), "/",nl.newsID,"/") as fullAlias, s.*, IF (nl.body = "", 1 , 0) as isBody,im.srcSmall, im.src
			FROM
				`' . NewsLine::$newsTableName . '` nl
			LEFT JOIN `' . NewsLine::$sectionsTableName . '` s ON s.`sectionID` = nl.`sectionID`
			LEFT JOIN `' . TablesNames::$IMAGES_TABLE_NAME . '` im ON im.`imageID` = nl.`smallImageID`
			WHERE
				' . $sectCond . ' nl.`isPublished` = 1 AND nl.`isDeleted` = 0
			ORDER BY nl.`publicationDate` DESC
			LIMIT ' . $start . ', ' . $limit . '
		';
        return DB::QueryToArray($query);
    }

	public function GetNewsItem($newsID, $sectionID = null)
    {
        $section = $sectionID ? "nl.sectionID = " . $sectionID : "nl.sectionID IS NULL";
        $query = '
			SELECT
				nl.`newsID`,
				nl.`title` AS title,
				nl.`body` AS body,
				nl.`publicationDate`,
				nl.`smallImageID`,
				nl.`bigImageID`,
				s.alias
			FROM
				`' . NewsLine::$newsTableName . '` nl
			LEFT JOIN `' . NewsLine::$sectionsTableName . '` s ON s.`sectionID` = nl.`sectionID`
			WHERE
				nl.`newsID`=' . $newsID . ' AND  ' . $section . '
			LIMIT 0, 1
		';
        $result  = DB::QueryOneRecordToArray($query);
        if (!empty($result))
        {
            $result['gallery'] = $this->GetNewsGallery($result['newsID']);
        }
        return $result;
	}


    /**ДОБАВЛЕН ! ПОПКОВ СЕРГЕЙ !
     * Получить картинку для формы отправки, подходит для поиска пути только
     * одной картинки, без связующей таблицы
     * @param int это id изображения для поиска полного пути
     * @return string полный путь к картике из greeny_images
     */
    public function getImg($imageID)
    {
        $image = DB::QueryOneValue('
            SELECT src as image 
            FROM `greeny_images` 
            WHERE imageID = ' . $imageID
        );

        return $image;
    }


    public function GetFoundRows()
	{
		$query = 'SELECT FOUND_ROWS()';
		return DB::QueryOneValue($query);
	}

	public function GetSectionID($alias)
	{
		$query = "SELECT sectionID FROM " . NewsLine::$sectionsTableName . " WHERE alias=" . DB::Quote($alias);
		return DB::QueryOneValue($query);
	}

	private function GetNewsGallery($newsID)
	{
        $query = "SELECT * FROM `" . NewsLine::$galleryTableName . "` g INNER JOIN `" . TablesNames::$IMAGES_TABLE_NAME . "` im ON im.imageID=g.imageID WHERE g.newsID=" . $newsID;
        return DB::QueryToArray($query);
	}

}

?>