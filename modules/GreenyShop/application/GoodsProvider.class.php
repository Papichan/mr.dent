<?php

/**
 * Класс для работы с поставщиками интернет-магазина
 *
 * @version 1.0
 */
class GoodsProvider extends Singleton 
{
    public function __construct()
    {
        
    }
    
    /**
     * Синглтоновский метод
     *
     * @return GoodsProvider
     */
    public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }
    
    /**
     * Менеджер общей таблицы товаров
     *
     * @var DBTableManager
     */
    public $goodsTableManager = null;
    
    /**
     * Массив с типами товаров.
     * В качестве ключа - служебная константа
     *
     * @var array
     */
    public $goodsTypes = null;
    
    public function GetGoods($filter, $withInfo = false)
    {
        
    }
}

?>