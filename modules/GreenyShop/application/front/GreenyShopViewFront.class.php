<?php
/**
 * Представление для работы с модулем GreenyShop во фронт-енде
 * 
 * @version 2.0
 *
 */
class GreenyShopViewFront extends FrontView
{
    public function __construct()
    {
        $this -> moduleName = 'GreenyShop';
        parent::__construct();
    }
    
    public function Index()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'index.tpl';
        
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/style.css');
    }
}
?>