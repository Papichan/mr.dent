<?php

/**
 * Класс для работы с товарами интернет-магазина
 *
 * @version 1.0
 */
class GoodsManager extends Singleton 
{
    public function __construct()
    {
        
    }
    
    /**
     * Синглтоновский метод
     *
     * @return GoodsManager
     */
    public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }
    
    /**
     * Менеджер общей таблицы товаров
     *
     * @var DBTableManager
     */
    public $goodsTableManager = null;
    
    public function GetGoods()
    {
        
    }
}

?>