<?php
/**
 * Представление для работы с модулем GreenyShop во фронт-енде
 * 
 * @version 2.0
 *
 */
class GreenyShopViewAdmin extends AdminView
{
    public function __construct()
    {
        $this -> moduleName = 'GreenyShop';
        parent::__construct();
        
        $this -> tpl -> menuTpl = $this -> tplsPath .'menu.tpl';
    }
    
    public function Index()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'index.tpl';
        
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/style.css');
    }
    
    public function Orders()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'orders.tpl';
        
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/list.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/style.css');
    }
    
    public function Order()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'order.tpl';
        
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/form.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/style.css');
    }
}
?>