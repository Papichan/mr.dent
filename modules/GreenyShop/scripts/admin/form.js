$(document).ready( function() {
	$("#changeCustomer").click(ChangeCustomer);
	$("#cancelChangeCustomer").click(CnacelChangeCustomer);
	$("#changeCustomerForm form").ajaxForm({beforeSubmit: standartAJAXBeforeSubmit, success: ChangeUserResponseHandler});
	$("#userFIO").change(function(event)
		{
			$("#userID").val("");
		})
		.autocomplete(MODULE_ALIAS+"customercomplete/", {minChars:3, max:100, width:300}).
		result(function(event, data, formatted)
		{
			if (data) $("#userID").val(data[1]);
		});
	$("#deliveryDate").mask("99.99.9999");
	$("#orderForm").validate({submitHandler: function(form) {
				jQuery(form).ajaxSubmit({beforeSubmit: standartAJAXBeforeSubmit, success: OrderUpdateResponseHandler});
				}});
	$("#deleteGoodsBtn, #changeOrderBtn").click(function() {
		$(document).data("submitClicked", $(this).name);
		if ($("#" + $(this).attr("name") + "_").length == 0)
			$("#orderForm").append($("<input type='hidden' id='"+ $(this).attr("name")+ "_' name='"+ $(this).attr("name")+ "' value='1'/>"));
	});
	$("#printLink").click(function(event){
		$("#sidebar, #header, #nav, #footer, :submit").slideToggle(1000);
		$("#content").animate({margin: "0px"}, 1000);
		$("body").css("background-image", "none");		
		$.jGrowl("Обновите страницу (F5), чтобы всё стало как было");
	});
});

function ChangeCustomer(event)
{
	event.preventDefault();
	$("#customerinfo, #changeCustomerForm").slideToggle();
}

function CnacelChangeCustomer(event)
{
	$("#customerinfo, #changeCustomerForm").slideToggle();
}

function ChangeUserResponseHandler(data, status)
{
	data = eval('(' + data + ')');
	if (!standartAJAXResponseHandler(data, status)) return;
	$.jGrowl(data.msg);
	setTimeout("window.location.reload()", 3000);
}

function OrderUpdateResponseHandler(data, status)
{
	data = eval('(' + data + ')');
	if (!standartAJAXResponseHandler(data, status)) return;
	$.jGrowl(data.msg);
	
	// пересчитаем корзину
	if ($(document).data("submitClicked") == "doDeleteGoods")
	{
		$("input:checkbox:checked[name*=goodsToDelete]").each(function(){
			$(this).parent().parent().remove();
		});
	}

	$(document).data("total", 0);
	$(".goodAmount").each(function() {
		var that = $(this);
		if (that.val() == 0)
			that.parent().parent().remove();
		else if (!isNaN(that.val()))
		{
			var price = that.parent().prev().text();
			price = price.substring(0, price.length-3);
			var summ = price * that.val();
			$(document).data("total", $(document).data("total") + summ);
			that.parent().next().text(summ + " р.");
		}
	});
	$("#totalCost").text($(document).data("total") + " р.");
}