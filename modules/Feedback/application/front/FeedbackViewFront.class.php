<?php
/**
 * Представление для формы запроса
 * 
 * @version 1.0
 *
 */
class FeedbackViewFront extends FrontView
{
    public function __construct()
    {
        $this -> moduleName = 'Feedback';
        parent::__construct();
        
        $this -> tplsPath = IncPaths::$MODULES_PATH.$this->moduleName.'/templates/front/';
    }
    
    public function Index()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'contacts_form.tpl';
        
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/contacts.js');
    }
}
?>