<?php

/**
 * Администраторская часть модуля обратной связи
 * 
 * @version 1.2
 *
 */
class FeedbackControllerAdmin extends AdminController 
{
	/**
	 * Объект класса
	 *
	 * @var Feedback
	 */
	private $feedback = null;
	
        private $fullAliases = null;
        
	/**
	 * Путь у шаблонам админской панели
	 *
	 * @var string
	 */
	private $moduleTplsPath = null;
	
	private $moduleAlias = null;
	
	private $modulePath = null;
	
	public function __construct()
	{
                parent::__construct();
                $this -> fullAliases = Request::GetAliases();
            
		// Указываем относительный путь к админским шаблонам модуля
		$this -> moduleTplsPath = IncPaths::$MODULES_PATH.'Feedback/admintpls/';
		$this -> moduleAlias = AppSettings::$ADMIN_ALIAS.'Feedback/';
		$this -> modulePath = '/modules/Feedback/';
                
                $this -> view = new FeedbackViewAdmin();
                // Инициализируем объект класса
                $this -> feedback = new Feedback();
                
                ViewData::Assign('ROOT_ALIAS', AppSettings::$ADMIN_ALIAS);
		ViewData::Assign('MODULE_ALIAS', $this -> moduleAlias);
                ViewData::Assign('MODULE_PATH', $this -> modulePath);
	}

	public function Index($args = null)
	{           
                //сохраняем модерацию отзыва
		if (isset($_POST['do']) && ($_POST['do'] == 'Сохранить') && isset($_POST['messageID'])){
                    if (isset($_POST['isModeration'])) $isModeration = 1;
                    else $isModeration = 0;

                    if (!$this->feedback->updateIsModeration($_POST['messageID'], $isModeration)) 
                        $this -> Error('Ошибка сохранения!', $this -> moduleAlias . '/list/');
		}

		if (isset($this -> fullAliases[2]))
		{
			switch ($this -> fullAliases[2])
			{
				case 'list':
					return $this -> MessagesList();
					break;
				case 'message':
					return $this -> Message();
					break;
                                case 'appointment':
                                        return $this -> Appointment();
                                        break;
                                case 'question':
                                        return $this -> Question();
                                    break;
                                case 'consultation':
                                    return $this -> Consultation();
                                    break;
                                /*case 'free':
                                    return $this -> FreeConsultation();
                                    break;*/
				default:
                                    return $this -> MessagesList();
                                    break;
			}
		}
		else 
			return $this -> MessagesList();
                
	}

	private function MessagesList()
	{
                if (isset(Request::$GET['page'])  && is_numeric(Request::$GET['page'])) $pageNum = Request::$GET['page'];
                else $pageNum = 1;

		$messagesOnPage = $this -> feedback -> settings['MessagesOnPage'];
		$messagesNumber = $this -> feedback -> CountMessages();
                
		if ($messagesNumber > $messagesOnPage)
		  $this -> setPaginator($messagesOnPage, $messagesNumber, $pageNum);

		$messages = $this -> feedback -> GetMessages( array(), $messagesOnPage, ($pageNum - 1)*$messagesOnPage );
		
                ViewData::Assign('MESSAGES', $messages);
                $this -> view -> Index();
		return $this -> view;
	}

	private function Message()
	{
		if (!isset($this -> fullAliases[3]) || !is_numeric($this -> fullAliases[3]))
                    return $this -> Error('Не указан ID сообщения', $this -> moduleAlias . 'list/');
			
                $messageID = $this -> fullAliases[3];
		$message = $this -> feedback -> GetMessage( array('messageID' => $messageID) );
               
                if (!$message) return $this -> Error('Не правильно указан ID сообщения', $this -> moduleAlias . 'list/');
		
		$message['title'] = htmlspecialchars($message['title']);
		$message['text'] = htmlspecialchars($message['text']);
		$message['text'] = str_replace("\r\n", '<br />', $message['text']);
                
                ViewData::Assign('MESSAGE', $message);
                $this -> view -> Item();
		return $this -> view;
	}

    private function Appointment()
    {
        if (isset(Request::$GET['page'])  && is_numeric(Request::$GET['page'])) $pageNum = Request::$GET['page'];
        else $pageNum = 1;

        $messagesOnPage = $this -> feedback -> settings['MessagesOnPage'];
        $messagesNumber = $this -> feedback -> CountMessages(['type' => 'appointment']);

        if ($messagesNumber > $messagesOnPage)
            $this -> setPaginator($messagesOnPage, $messagesNumber, $pageNum);

        $messages = $this -> feedback -> GetMessages( ['type' => 'appointment'], $messagesOnPage, ($pageNum - 1)*$messagesOnPage );

        ViewData::Assign('MESSAGES', $messages);
        $this -> view -> Appointment();
        return $this -> view;
    }

    private function Question()
    {
        if (isset(Request::$GET['page'])  && is_numeric(Request::$GET['page'])) $pageNum = Request::$GET['page'];
        else $pageNum = 1;

        $messagesOnPage = $this -> feedback -> settings['MessagesOnPage'];
        $messagesNumber = $this -> feedback -> CountMessages( ['type' => 'question'] );

        if ($messagesNumber > $messagesOnPage)
            $this -> setPaginator($messagesOnPage, $messagesNumber, $pageNum);

        $messages = $this -> feedback -> GetMessages( ['type' => 'question'], $messagesOnPage, ($pageNum - 1)*$messagesOnPage );

        ViewData::Assign('MESSAGES', $messages);
        $this -> view -> Question();
        return $this -> view;
    }

    private function Consultation()
    {
        if (isset(Request::$GET['page'])  && is_numeric(Request::$GET['page'])) $pageNum = Request::$GET['page'];
        else $pageNum = 1;

        $messagesOnPage = $this -> feedback -> settings['MessagesOnPage'];
        $messagesNumber = $this -> feedback -> CountMessages(['type' => 'consultation']);

        if ($messagesNumber > $messagesOnPage)
            $this -> setPaginator($messagesOnPage, $messagesNumber, $pageNum);

        $messages = $this -> feedback -> GetMessages( ['type' => 'consultation'], $messagesOnPage, ($pageNum - 1)*$messagesOnPage );

        ViewData::Assign('MESSAGES', $messages);
        $this -> view -> Consultation();
        return $this -> view;
    }

    private function FreeConsultation()
    {
        if (isset(Request::$GET['page'])  && is_numeric(Request::$GET['page'])) $pageNum = Request::$GET['page'];
        else $pageNum = 1;

        $messagesOnPage = $this -> feedback -> settings['MessagesOnPage'];
        $messagesNumber = $this -> feedback -> CountMessages(['type' => 'freeConsultation']);

        if ($messagesNumber > $messagesOnPage)
            $this -> setPaginator($messagesOnPage, $messagesNumber, $pageNum);

        $messages = $this -> feedback -> GetMessages( ['type' => 'freeConsultation'], $messagesOnPage, ($pageNum - 1)*$messagesOnPage );

        
         ViewData::Assign('MESSAGES', $messages);
        $this -> view -> Question();
        return $this -> view;
        
        ViewData::Assign('MESSAGES', $messages);
        $this -> view -> FreeConsultation();
        return $this -> view;
    }
}
?>

