<h1>Полученное сообщение</h1>

<form action="{$MODULE_ALIAS}" method="post">
    <ul>
            <li>Заголовок сообщения
                    <br />
                    <div class="feedback_messageItem">{$MESSAGE.title}</div>
            </li>

            {if ($MESSAGE.doctor != "none")}
            <li>Доктор
                <br />
                <div class="feedback_messageItem">{$MESSAGE.doctor}</div>
            </li>
            {/if}

            <li>Пользователь
                    <br />
                    <div class="feedback_messageItem">
                            <strong>Имя</strong>: {$MESSAGE.userName} 
                            <br />
                            <strong>E-mail</strong>: <a href="mailto:{$MESSAGE.userEmail}">{$MESSAGE.userEmail}</a>
                    </div>
            </li>
            
            {if ($MESSAGE.userPhone != "none")}
            <li>Телефон
                <br />
                <div class="feedback_messageItem">{$MESSAGE.userPhone}</div>
            </li>
            {/if}

            <li>Дата отправки
                    <br />
                    <div class="feedback_messageItem">{$MESSAGE.sendDate}</div>
            </li>

            <li>Текст сообщения
                    <br />
                    <div class="feedback_messageItem">{$MESSAGE.text}</div>
            </li>
    </ul>
    <input type="checkbox" name="isModeration" value="1" style="cursor:pointer" alt="" {if $MESSAGE.isModeration}checked{/if}>
    <input type="hidden" name="messageID" value="{$MESSAGE.messageID}" alt="">
    <label title="Установите, если хотите опубликовать отзыв" for="page_isActive">Публиковать</label>
    <div><br/></div>
    <input class="btn" type="submit" name="do" value="Сохранить" alt="">
</form>