<?php
/**
 * Представление для формы запроса
 * 
 * @version 1.0
 *
 */
class FotobankViewFront extends FrontView
{
    public function __construct()
    {
        $this -> moduleName = 'Fotobank';
        parent::__construct();
        
        $this -> tplsPath = IncPaths::$MODULES_PATH.$this->moduleName.'/templates/front/';
    }
    
    public function Index()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath.'index.tpl';
        
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/style.css');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/script.js');
    }
}
?>