<?php
/**
 * Представление для работы с модулем Фотобанк в бек-енде
 * 
 * @version 1.0
 *
 */
class FotobankViewAdmin extends AdminView
{
    public function __construct()
    {
        $this -> moduleName = 'Fotobank';
        parent::__construct();
        
        $this -> tpl -> menuTpl = $this -> tplsPath . 'menu.tpl';
    }
    
    public function Index()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'users_images.tpl';
        
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/jquery.collapse/collabse.css');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.collapse/jquery.collapse.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.collapse/jquery.collapse_storage.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/script.js');
    }
    
    public function Uploads($tagID = null)
    {
        ViewData::Append('STYLE_FILE', '/css/admin/form_bootstrap.css');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/jquery.fileupload/bootstrap.min.css');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/jquery.fileupload/style.css');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/jquery.fileupload/bootstrap-responsive.min.css');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/jquery.fileupload/bootstrap-image-gallery.min.css');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/jquery.fileupload/jquery.fileupload-ui.css');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/admin/style.css');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.fileupload/jquery.ui.widget.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.fileupload/tmpl.min.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.fileupload/load-image.min.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.fileupload/canvas-to-blob.min.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.fileupload/bootstrap.min.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.fileupload/bootstrap-image-gallery.min.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.fileupload/jquery.iframe-transport.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.fileupload/jquery.fileupload.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.fileupload/jquery.fileupload-process.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.fileupload/jquery.fileupload-resize.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.fileupload/jquery.fileupload-validate.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.fileupload/jquery.fileupload-ui.js');
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/admin/jquery.fileupload/main.js');
        ViewData::Append('JS_VARS', array('tagID' => $tagID));
        
        $this -> tpl -> contentTpl = $this -> tplsPath . 'uploads.tpl';
    }
    
}
?>