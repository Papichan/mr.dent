        <ul>
            <li>
                <a href="{$MODULE_ALIAS}sections/" title="Редактор категорий">Категории</a>
            </li>
            {if isset($SECTIONS)}
            <ul class="sections">
                {foreach from=$SECTIONS item=SECTION}
                <li>
                    <a href="{$MODULE_ALIAS}list/{$SECTION.asectionID}" title="Показать статьи из категории «{$SECTION.caption}»">
                        <span style="{if (isset($CUR_SECTION) && ($CUR_SECTION == $SECTION.asectionID))}font-weight: bold;{/if}">
                            {$SECTION.caption}
                        </span>
                    </a>
                </li>
                {/foreach}
            </ul>
            {/if}
            <li>
                <a href="{$MODULE_ALIAS}" title="Показать новости из всех категорий">Все статьи</a>
            </li>
            <li>
                <a href="{$MODULE_ALIAS}recycled/" title="Список удаленных статей">Корзина ({$DELETED_ARTICLES_NUMBER})</a>
            </li>
        </ul>