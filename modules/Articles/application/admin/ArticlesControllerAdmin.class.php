<?php

/**
 * Администраторская часть модуля статей
 * 
 * @version 3.1
 *
 */
class ArticlesControllerAdmin extends AdminController {

    /**
     * Объект класса статей
     *
     * @var Articles
     */
    private $articles = null;

    /**
     * Путь к шаблонам модуля
     *
     * @var string
     */
    private $moduleTplsPath = null;

    /**
     * Алиас данного модуля
     *
     * @var string
     */
    private $moduleAlias = null;
    private $fullAliases = null;

    /**
     * Физический путь к модулю
     *
     * @var string
     */
    private $modulePath = null;

    public function __construct()
    {
        parent::__construct();
        $this->fullAliases = Request::GetAliases();

        // Указываем относительный путь к админским шаблонам модуля
        $this->moduleTplsPath = IncPaths::$MODULES_PATH . 'Articles/templates/';
        $this->moduleAlias = AppSettings::$ADMIN_ALIAS . 'Articles/';
        $this->modulePath = IncPaths::$MODULES_PATH . 'Articles/';
        $this->view = new ArticlesViewAdmin();
        $this->articles = new Articles();

        ViewData::Assign('MODULE_TPLS_PATH', $this->moduleTplsPath);
        ViewData::Assign('MODULE_ALIAS', $this->moduleAlias);
        ViewData::Assign('DELETED_ARTICLES_NUMBER', $this->articles->ArticlesNumber(array('isDeleted' => 1)));
        ViewData::Assign('JS_VARS', array('MODULE_ALIAS' => $this->moduleAlias));
    }

    /**
     * Возвращает HTML код страницы
     *
     * @return string
     */
    public function Index($args = null)
    {
        //получаем список категорий
        $sections = $this->articles->GetSections();

        ViewData::Assign('SECTIONS', $sections);

        if (isset($this->fullAliases[2])) {
            switch ($this->fullAliases[2]) {
                // Просмотр списка статей
                case 'list':
                    return $this->ArticlesList(isset($this->fullAliases[3]) ? $this->fullAliases[3] : null);
                    break;

                // Добавление статьи
                case 'add':
                    return $this->AddArticles();
                    break;

                // Редактирование статьи
                case 'edit':
                    return $this->EditArticles();
                    break;

                // Удаление статьи
                case 'delete':
                    return $this->DeleteArticles();
                    break;

                // Удаление нескольких статей
                case 'deletes':
                    return $this->DeleteManyArticles();
                    break;

                // Работа с корзиной статей
                case 'recycled':
                    return $this->Recycled();
                    break;

                // Категории статей 
                case 'sections':
                    return $this->Sections();
                    break;

                //  Путеводитель
                case 'guide':
                    return $this->Guide();
                    break;

                default:
                    return $this->ArticlesList();
                    break;
            }
        }
        else {
            return $this->ArticlesList();
        }
    }

    /**
     * Показывает список всех категорий с возможностью редактирования.
     *
     */
    private function Sections()
    {
        if (isset($_POST['doAdd']) && isset($_POST['section'])) {
            $sections = $this->articles->AddSections(array('caption' => $_POST['section']));
        }
        else if (isset($_POST['doDel']) && isset($_POST['sections'])) {
            $arraySectionID = $_POST['sections'];
            $sections = $this->articles->WipeSections($arraySectionID);
        }
        else if (isset($_POST['doSave']) && isset($_POST['sections'][0]) && isset($_POST['section'])) {
            $this->articles->UpdateSections($_POST['sections'][0], $_POST['section']);
        }

        //получаем список категорий
        $sections = $this->articles->GetSections();
        ViewData::Assign('SECTIONS', $sections);
        $this->view->Sections();
        return $this->view;
    }

    /**
     * Показывает список всех статей.
     * При этом они разбиваются на страницы.
     *
     */
    private function ArticlesList($section = null)
    {
        $articlesOnPageNumber = $this->articles->settings['MessagesOnPageAdmin'];
        //trace($articlesOnPageNumber);
        // Определяем номер страницы
        if (isset(Request::$GET['page']) && is_numeric(Request::$GET['page']))
            $pageNumber = Request::$GET['page'];
        else
            $pageNumber = 1;

        $attrs = array('isDeleted' => 0);
        if (!is_null($section)) {
            $attrs['asectionID'] = $section;
            ViewData::Assign('CUR_SECTION', $section);
        }

        $articlesArray = $this->articles->GetArticles($attrs, $articlesOnPageNumber, ($pageNumber - 1) * $articlesOnPageNumber);
        ViewData::Assign('ARTICLES_LIST', $articlesArray);

        $allArticlesNumber = $this->articles->ArticlesNumber($attrs);

        $hrefPrefix = $this->moduleAlias . 'list/' . (!is_null($section) ? 'section/' . $section . '/' : '') . 'page/';
        $this->setPaginator($articlesOnPageNumber, $allArticlesNumber, $pageNumber);

        $this->view->ArticlesList();
        return $this->view;
    }

    /**
     * Добавление статьи
     *
     */
    private function AddArticles()
    {
        if (isset($_POST['doSave'])) {
            $contentManager = new ContentManager();
            
            try {
                $articleID = $contentManager->FlushData(Articles::$articlesTableName, $_POST);

                if (isset($_POST['async'])){
                    return new JsonActionResult(array("msg" => "ok", "articleID" => $articleID, "refresh" => ($contentManager->formRefreshRequired ? true : false )));
                }
                else{
                    $this->Success('Данные сохранены', $this->moduleAlias . 'edit/' . $articleID . '/');             
                }
            } catch (Exception $e) {
                if (isset($_POST['async']))
                    die('{"error":"' . $e->getMessage() . '"}');
                else
                    $this->Error($e->getMessage(), $this->moduleAlias . 'add/');
            }
        }

        return $this->SetForm(Articles::$articlesTableName, 'Создание статьи');
    }

    /**
     * Редактирование статьи
     *
     */
    private function EditArticles()
    {
        $articleID = $this->fullAliases[3];

        if (is_null($articleID) || !is_numeric($articleID)) {
            if (Request::IsAsync()) {
                return new JsonActionResult(array('error' => "Возникла ошибка при редактировании статьи: не указан ID статьи"));
            }
            else {
                $this->Error('Возникла ошибка при редактировании статьи: не указан ID статьи', $this->moduleAlias);
            }
        }

        if (isset($_POST['doSave'])) {
            $contentManager = new ContentManager();
            try {
                $contentManager->FlushData(Articles::$articlesTableName, $_POST);

                if (Request::IsAsync()) {
                    return new JsonActionResult(array("msg" => "ok", "articleID" => $articleID, "refresh" => ($contentManager->formRefreshRequired ? 'true' : 'false')));
                }
                else {
                    $this->Success('Данные сохранены', $this->moduleAlias . 'edit/' . $articleID . '/');
                }
            } catch (Exception $e) {
                if (Request::IsAsync()) {
                    return new JsonActionResult(array("error" => $e->getMessage()));
                }
                else
                    $this->Error($e->getMessage(), $this->moduleAlias . 'edit/' . $articleID . '/');
            }
        }

        $delete = array(
            'url' => $this->moduleAlias . 'delete/',
            'name' => 'articleID',
            'value' => $articleID
        );
        return $this->SetForm(Articles::$articlesTableName, 'Редактирование статьи', $articleID, $delete);
    }

    /**
     * Удаление статьи
     *
     */
    private function DeleteArticles()
    {
        if (isset($_POST['doDelete']) || isset($_POST['doDelete_x'])) {
            if (!isset($_POST['articleID']) || (!is_numeric($_POST['articleID'])))
                $this->Error('Возникла ошибка при удалении новости: не указан id статьи', $this->moduleAlias);
            else
                return $articleID = $_POST['articleID'];
            try {
                $this->articles->DeleteArticles($articleID);
                return $this->Success('Статья перемещена в корзину', $this->moduleAlias);
            } catch (Exception $e) {
                return $this->Error('Возникла ошибка при удалении статьи: ' . $e->getMessage(), $this->moduleAlias . 'edit/' . $articleID . '/');
            }
        }
    }

    private function DeleteManyArticles()
    {
        if (isset($_POST['doDelete']) || isset($_POST['doDelete_x'])) {
            if (!isset($_POST['toDelete']) || empty($_POST['toDelete']))
                $this->Error('Не отмечены статьи для удаления');

            $toDelete = $_POST['toDelete'];

            try {
                foreach ($toDelete as $articleID) {
                    $this->articles->DeleteArticles($articleID);
                }
                return $this->Success('Отмеченные статьи перемещены в корзину', $this->moduleAlias);
            } catch (Exception $e) {
                $this->Error('Возникла ошибка при удалении статьи: ' . $e->getMessage(), $this->moduleAlias);
            }
        }
    }

    /**
     * Работа с корзиной
     *
     */
    private function Recycled()
    {
        if (isset($_POST['doRestore'])) {
            $toEdit = (isset($_POST['toEdit'])) ? $_POST['toEdit'] : array();

            foreach ($toEdit as $articleID) {
                try {
                    $this->articles->RestoreArticles($articleID);
                } catch (Exception $e) {
                    $this->Error('Возникла ошибка при восстановлении статьи: ' . $e->getMessage(), $this->moduleAlias . 'recycled/');
                }
            }
            $this->Success('Отмеченные статьи были восстановлены', $this->moduleAlias);
        }

        if (isset($_POST['doWipe'])) {
            $toEdit = (isset($_POST['toEdit'])) ? $_POST['toEdit'] : array();

            foreach ($toEdit as $articleID) {
                try {
                    $this->articles->WipeArticles($articleID);
                } catch (Exception $e) {
                    $this->Error('Возникла ошибка при удалении статьи: ' . $e->getMessage(), $this->moduleAlias . 'recycled/');
                }
            }

            $this->Success('Отмеченные статьи были удалены', $this->moduleAlias . 'recycled/');
        }
        $this->view->Recycled();
        $articlesArray = $this->articles->GetArticles(array('isDeleted' => 1));
        ViewData::Assign('ARTICLES_LIST', $articlesArray);
        return $this->view;
    }

    protected function SetForm($tableName, $title, $rowID = 0, $delete = null)
    {
        $this->view->AssignFormToTpl($tableName, $rowID);
        $this->view->Form();
        ViewData::Assign('TITLE', $title);
        if (!is_null($delete))
            ViewData::Assign('DELETE', $delete);
        return $this->view;
    }

    /**
     * Путеводитель :)
     *
     */
    private function Guide()
    {
        if (isset(parent::$RequestedAlias[2]) && (parent::$RequestedAlias[2] == 'articles')) {
            header('Location: ' . $this->moduleAlias);
        }
        else {
            $this->Error('Ошибка перенаправления', $_SERVER['HTTP_REFERER']);
        }
    }

}

?>