<?php
/**
 * Представление для работы с модулем Articles во фронт-енде
 * 
 * @version 2.0
 *
 */
class ArticlesViewFront extends FrontView
{
    /*
     * Название модуля
     */
    protected $moduleName = 'Articles';
    
    /*
     * Папка модуля
     */
    protected $publicFilesPath = null;
    
    public function __construct()
    {
        parent::__construct();
        $this -> publicFilesPath = AppSettings::$MODULES_ALIAS.$this->moduleName.'/';
        $this -> tplsPath = IncPaths::$MODULES_PATH.$this->moduleName.'/templates/front/';
    }
    
    public function Index()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'index.tpl';
        
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/style.css');
    }
    
    public function Item()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath . 'item.tpl';
        
        ViewData::Append('SCRIPT_FILE', $this -> publicFilesPath.'scripts/script.js');
        ViewData::Append('STYLE_FILE', $this -> publicFilesPath.'css/style.css');
    }
}
?>