<?php

/**
 * Контроллер для работы с модулем Articles во фронт-енде
 *
 * @version 2.0
 */
class ArticlesControllerFront extends FrontController
{
    protected $model;

	
        public function __construct()
        {
            parent::__construct();
             $settings = CommonModule::LoadSettings('Articles');
            $rootAlias = $settings['rootAlias'];
            //trace($rootAlias);
            // Получаем страницу из базы
            $this -> page = new Page($rootAlias);
            $this -> view = new ArticlesViewFront();
            $this -> model = new ArticlesModelFront();
            
        }

        public function Index($args = null)
        {       
            $this -> articles = new Articles();
            
            if (!empty($args)&& is_numeric($args[0]))
            {
                return $this->Item($args);                    
            }
            elseif (!empty($args)&& !is_numeric($args[0])) 
            {            
                throw new PageNotFoundException('Error');
            }	
            
            $page = 1;	
            if (isset(Request::$GET['page']) && is_numeric(Request::$GET['page'])) $page = Request::$GET['page'];		
            $limit = $this -> articles -> settings['MessagesOnPageFront'];
            $start = ($page - 1) * $limit;
            
            $sections = $this -> articles -> GetSections();
            foreach ($sections as $key => &$articles1)
            {                      
                $articles1['article']=  $this-> model ->GetArticles($articles1['asectionID'], $start, $limit);
            }  
            //получаем также новости без категории
            $ar = array();
            if ($res = $this-> model -> GetArticles(NULL, $start, $limit)){
                $ar['caption'] = 'без категории';
                $ar['article'] = $res;
                $sections[] = $ar;
            }
            
            //пагинация
            $countRows = $this -> model -> GetFoundRows();
            $this -> setPaginator($limit, $countRows, $page);
           // trace($sections);
            ViewData::Assign('articles', $sections);    		
            $this -> view -> Index();
            return $this -> view;
        }
        
        public function Item($args)
	{           
            if (!is_numeric($args[0]))
               throw PageNotFoundException('Error');
            $article = $this->model->GetArticle($args[0]);
            
            ViewData::Assign('article', $article); 
            $this->view->Item();
            return $this->view;
        }
	
}
?>