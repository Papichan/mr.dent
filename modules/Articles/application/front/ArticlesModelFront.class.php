<?php

/**
 * Модель для работы с модулем Articles во фронт-енде
 *
 * @version 2.0
 */
class ArticlesModelFront
{
        public function GetArticles($sect, $start, $limit)
	{
                if (is_null($sect)) $sect = ' IS NULL';
                else $sect = ' = '.$sect;
        
		$query = '
			SELECT SQL_CALC_FOUND_ROWS
				a.`articleID`,				
				a.`title` AS title,
				a.`summary` AS summary,
				a.`publicationDate`
			FROM
				`'.  Articles::$articlesTableName.'` a 			
			WHERE
				a.`asectionID`'.$sect.' AND
				a.`isPublished` = 1 AND
				a.`isDeleted` = 0
			ORDER BY a.`publicationDate` ASC
			LIMIT ' . $start . ', ' . $limit . '
		';

		return DB::QueryToArray($query);
	}

	public function GetArticle($articleID)
	{
		$query = '
			SELECT 
				a.`articleID`,				
				a.`title` AS title,
				a.`body` AS body,
				a.`publicationDate`
			FROM
				`'.  Articles::$articlesTableName.'` a 			
			WHERE
				a.`articleID`='.$articleID.' 
			LIMIT 0, 1
		';

		$result = DB::QueryToArray($query);
		return $result;
	}

	public function GetFoundRows()
	{
		$query = 'SELECT FOUND_ROWS()';
		return DB::QueryOneValue($query);
	}
    
}

?>