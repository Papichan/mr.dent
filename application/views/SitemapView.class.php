<?php

class SitemapView extends FrontView
{
    public function __construct($tplName = 'main.tpl')
    {
        parent::__construct($tplName);
        $this -> tpl -> contentTpl = $this -> tplsPath . 'sitemap/sitemap.tpl';
    }
}