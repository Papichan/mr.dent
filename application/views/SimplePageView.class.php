<?php
/**
 * Представление для обычных страниц
 * 
 * @version 1.0
 *
 */
class SimplePageView extends FrontView
{
    private $page = null;
    
    public function __construct($page)
    {
        $this -> page = $page;
        // Получаем контент
        $pageContent = $this -> page -> Content();
        ViewData::Assign('CONTENT', $pageContent['pageContent']);
        
        parent::__construct();
    }
}
?>