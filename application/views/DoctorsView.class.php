<?php
/**
 * Представление для главной страницы сайта
 *
 * @version 1.0
 *
 */
class DoctorsView extends FrontView
{
    public function __construct($path)
    {
        parent::__construct();
        $this->tplsPath .= $path.'/';
    }

    /**
     * Отобразить всех докторов
     * @return $this
     */
    public function Index()
    {
        $this->tpl->contentTpl = $this->tplsPath.'index.tpl';
        return $this;
    }

    /**
     * Показать одного доктора
     * @return $this
     */
    public function OneDoctor()
    {
        $this->tpl->contentTpl = $this->tplsPath.'doctor.tpl';
        return $this;
    }
}

?>