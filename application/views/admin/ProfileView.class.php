<?php
/**
 * 
 * Представление для профиля в Админке
 * 
 * @version 1.5
 * 
 */
class ProfileView extends AdminView
{
    public function __construct($tplName = 'main.tpl')
    {
        parent::__construct($tplName);

        $this -> tplsPath = $this -> tplsPath.'profile/';
        $this -> tpl -> menuTpl = $this -> tplsPath .'menu.tpl';
    }

    public function View()
    {
        $this->tpl->contentTpl = $this->tplsPath . 'profile.tpl';
    }
    
    public function Edit()
    {
        $this->tpl->contentTpl = $this->tplsPath . 'edit.tpl';
    }
    
    public function PassChange()
    {
        $this->tpl->contentTpl = $this->tplsPath . 'pass.tpl';
    }
}
