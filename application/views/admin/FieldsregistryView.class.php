<?php
/**
 *
 * Представление для fieldsregistry в Админке
 *
 * @version 1.1
 *
 */
class FieldsregistryView extends AdminView
{
    public function __construct($mainTpl = null)
	{
		if (!$mainTpl && AppSettings::$ADMIN_TEMPLATE_COL == 1)
		{
			$mainTpl = 'single_column.tpl';
		}
		elseif (!$mainTpl)
		{
			$mainTpl = 'main.tpl';
		}
		parent::__construct($mainTpl);
	}

    public function Index()
    {
        $this->tpl->contentTpl = $this->tplsPath . 'for_fields_registry.tpl';
    }
}
