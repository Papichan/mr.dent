<?php

class AnyEditorView extends AdminView
{
    public function __construct($mainTpl = null)
	{
		if (!$mainTpl && AppSettings::$ADMIN_TEMPLATE_COL == 1)
		{
			$mainTpl = 'single_column.tpl';
		}
		elseif (!$mainTpl)
		{
			$mainTpl = 'main.tpl';
		}

		parent::__construct($mainTpl);

        // Адрес к шаблонам раздела
        $this -> tplsPath	 = $this -> tplsPath . 'anyeditor/';
		$this -> tpl -> menuTpl	 = $this -> tplsPath . "menu.tpl";
	}

	public function Index()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath .'index.tpl';
    }

    public function Items()
    {
        $this -> tpl -> contentTpl = $this -> tplsPath .'items.tpl';
        ViewData::Assign('INCLUDE_JQMODAL', 1);
    }

    public function Add($tableName)
    {
        $this -> SetForm($tableName);

        ViewData::Assign('CONTENT_HEADER', 'Добавление записи');
        ViewData::Assign('FORM_SUBMIT_NAME', 'doAdd');
    }

    public function Edit($tableName, $rowID)
    {
        $this -> SetForm($tableName, $rowID);

        ViewData::Assign('CONTENT_HEADER', 'Редактирование записи');
        ViewData::Assign('FORM_SUBMIT_NAME', 'doSave');
        ViewData::Assign('EDIT_MODE', 1);
    }

    private function SetForm($tableName, $rowID = null)
    {
        $this -> tpl -> contentTpl = $this -> tplsPath .'form.tpl';

        ViewData::Append('SCRIPT_FILE', AppSettings::$ADMIN_SCRIPTS . 'anyeditor.js');
        ViewData::Assign('TABLE_NAME', $tableName);

        $this -> AssignFormToTpl($tableName, $rowID);
    }
}

?>