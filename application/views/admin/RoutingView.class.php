<?php

class RoutingView extends AdminView
{
	public function __construct($tplName = 'main.tpl')
	{
		parent::__construct($tplName);

		$this->tplsPath = $this->tplsPath . 'routing/';
		ViewData::Append('STYLE_FILE', AppSettings::$ADMIN_STYLES . 'routing.css');
		ViewData::Append('SCRIPT_FILE', AppSettings::$ADMIN_SCRIPTS . 'routing.js');
	}

	public function Tree($tree)
	{
		ViewData::Assign('ROUTING_TREE', $tree);
		$this-> tpl->contentTpl = $this->tplsPath . 'tree.tpl';

		return $this;
	}

	public function EditRoute($route, $linearTree)
	{
		ViewData::Assign('mode', 'edit');
		ViewData::Assign('LINEAR_TREE', $linearTree);
		ViewData::Assign('ROUTE', $route);
		$this->tpl->contentTpl = $this->tplsPath . 'edit.tpl';
		return $this;
	}

	public function AddRoute($linearTree, $after = null)
	{
		ViewData::Assign('mode', 'add');
		ViewData::Assign('LINEAR_TREE', $linearTree);
		if (!is_null($after))
		{
			ViewData::Assign('ROUTE', array('parentID' => $after));
		}

		$this->tpl->contentTpl = $this->tplsPath . 'edit.tpl';
		return $this;
	}
}
