<?php
/**
 * Представление для формы авторизации
 * 
 * @version 1.0
 *
 */
class AuthorisationView extends AdminView 
{

    public function __construct()
    {
        parent::__construct('auth.tpl');
    }
}
?>