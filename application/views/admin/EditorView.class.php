<?php
class EditorView extends AdminView
{
    public function __construct()
    {
        parent::__construct();
		$this->tplsPath .= "editor/";
		$this->tpl->menuTpl = $this->tplsPath . 'menu.tpl';
		ViewData::Append('SCRIPT_FILE', AppSettings::$SCRIPTS_ALIAS . 'jquery.form.js');
		ViewData::Append('SCRIPT_FILE', AppSettings::$ADMIN_SCRIPTS .'robots.js');
    }

	public function Other()
	{
		$this->tpl->contentTpl = $this->tplsPath . 'file.tpl';
	}

	public function Robots()
	{
		$this->tpl->contentTpl = $this->tplsPath . 'file.tpl';
	}

	public function Htaccess()
	{
		$this->tpl->contentTpl = $this->tplsPath . 'file.tpl';
	}

	public function Index()
	{
		$this->tpl->contentTpl = $this->tplsPath . 'index.tpl';
	}

}