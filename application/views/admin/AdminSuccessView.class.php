<?php
/**
 * Представление успеха в админке
 *
 * @version 1.0
 *
 */
class AdminSuccessView extends AdminView
{
    public function __construct($msg, $link = null, $params = null)
    {
        parent::__construct();
		if (AppSettings::$ADMIN_TEMPLATE_COL == 1)
		{
			$this -> tpl -> tplPath = $this -> tplsPath . 'single_column.tpl';
		}
		else
		{
			$this -> tpl -> tplPath = $this -> tplsPath . 'main.tpl';
		}
		$this -> tpl -> contentTpl = $this -> tplsPath . 'success.tpl';
		ViewData::Assign('SUCCESS_MSG', $msg);
		ViewData::Assign('REDIRECT_URL', $link);
		ViewData::Assign('PAGE_TITLE', 'Успех!');
    }
}
?>