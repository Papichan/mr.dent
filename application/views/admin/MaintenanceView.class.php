<?php
class MaintenanceView extends AdminView
{
    public function __construct($tplName = 'main.tpl')
    {
        parent::__construct($tplName);
        $this->tplsPath .= 'maintenance/';
        
        ViewData::Assign('INCLUDE_FCK', true);
        $this->tpl->contentTpl = $this->tplsPath . 'maintenance.tpl';
    }
}
