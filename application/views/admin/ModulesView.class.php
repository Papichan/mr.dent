<?php
/**
 * 
 * Представление для модулей в Админке
 * 
 * @version 2.2
 * 
 */
class ModulesView extends AdminView
{
    public function __construct($tplName = 'main.tpl')
    {
        parent::__construct($tplName);

        $this -> tplsPath = $this -> tplsPath.'modules/';
        $this -> tpl -> menuTpl = $this -> tplsPath .'menu.tpl';
    }

    public function Index()
    {
        $this-> tpl->contentTpl = $this->tplsPath . 'welcome.tpl';
    }

    public function Edit()
    {
        $this->tpl->contentTpl = $this->tplsPath . 'edit.tpl';
    }
    
    public function Install()
    {
        $this->tpl->contentTpl = $this->tplsPath . 'install.tpl';
    }
}
