<?php

/**
 * Description of ReviewsView
 *
 * @author grom
 */
class ReviewsAdminView extends AdminView
{
	public function __construct()
	{
		parent::__construct();
		// Адрес к шаблонам раздела
        $this -> tplsPath = $this -> tplsPath . 'reviews/';

        // Шаблон с меню
        $this -> tpl -> menuTpl = $this -> tplsPath . 'menu.tpl';
	}

	public function ShowList()
    {
		$this -> tpl -> contentTpl = $this -> tplsPath . 'list.tpl';
	}

	public function Unapproved($itemsOnPage, $itemsNumber, $currentPage, $prefixUrl)
	{
		if ($itemsNumber > $itemsOnPage)
		{
			$this -> SetJQPaginator($itemsOnPage, $itemsNumber, $currentPage, $prefixUrl);
		}

		$this -> tpl -> contentTpl = $this -> tplsPath . 'unapproved.tpl';
	}

	public function Edit()
	{
		$this -> tpl -> contentTpl = $this -> tplsPath . 'edit.tpl';
	}
}
