<?php
/**
 * Представление для страницы услуг сайта
 *
 * @version 1.0
 *
 */
class ServicesView extends FrontView
{
    public function __construct($path)
    {
        parent::__construct();
        $this->tplsPath .= $path.'/';
    }

    public function Index()
    {
        $this->tpl->contentTpl = $this->tplsPath.'index.tpl';
        return $this;
    }
    
    public function Ortoped()
    {
        $this->tpl->contentTpl = $this->tplsPath.'lechenie_zubov.tpl';
        return $this;
    }
}

?>