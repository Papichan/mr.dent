<?php
/**
 * Представление для главной страницы сайта
 *
 * @version 1.0
 *
 */
class LechenieZubovView extends FrontView
{
    public function __construct($path)
    {
        parent::__construct();
        $this->tplsPath .= $path.'/';
    }

    public function Index()
    {
        $this->tpl->contentTpl = $this->tplsPath.'index.tpl';
        return $this;
    }
}

?>