<?php
/**
 * Description of ReviewView
 *
 * @author grom
 */
class ReviewView extends FrontView
{
	public function __construct()
	{
		parent::__construct();
		$this->tplsPath .= "reviews/";
		ViewData::Append('SCRIPT_FILE', AppSettings::$SCRIPTS_ALIAS .'reviews.js');
		$this -> tpl -> contentTpl = $this -> tplsPath . 'reviews.tpl';
	}
}