<?php
/**
 * Представление для фронт-энда
 *
 * @version 1.0
 *
 */
class FrontView extends ViewResult
{
    public function __construct($tplName = 'main.tpl')
    {
		AppSettings::$SECTION_ID = 'front';
		$this -> tplsPath			 = '';

		parent::__construct($this -> tplsPath . $tplName);

        //записываем пути к шаблонам и подключаемым файлам
        if (!is_null($this -> moduleName)){
            $this -> publicFilesPath = AppSettings::$MODULES_ALIAS.$this -> moduleName.'/';
            $this -> tplsPath = IncPaths::$MODULES_PATH.$this -> moduleName.'/templates/front/';
            ViewData::Assign('IMAGES_MODULE_ALIAS', $this -> publicFilesPath.'images/');
        }

        ViewData::Assign('STYLES_ALIAS', AppSettings::$STYLES_ALIAS);
        ViewData::Assign('LIBS_ALIAS', AppSettings::$LIBS_ALIAS);
        ViewData::Assign('SCRIPTS_ALIAS',AppSettings::$SCRIPTS_ALIAS);
        ViewData::Assign('IMAGES_ALIAS', AppSettings::$IMAGES_ALIAS);
		}

}
