<?php
/**
 * Представление для страницы Контакты
 *
 * @version 1.0
 *
 */
class ContactsView extends FrontView
{
    public function __construct($path)
    {
        parent::__construct();
        $this->tplsPath .= $path.'/';
    }

    public function Index()
    {
        $this->tpl->contentTpl = $this->tplsPath.'index.tpl';
        return $this;
    }
}

?>