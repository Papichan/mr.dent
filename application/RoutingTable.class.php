<?php
/**
 * Таблица маршрутизации
 *
 */
class RoutingTable
{
    private $routingTable;

    public function __construct()
    {
        $this -> routingTable = array();
         /////////////// zero level
        $this -> routingTable[] = array(
            'pattern'           => '*', // host
            'controller'        => 0,
            'action'            => 1,
            'defaultController' => 'DefaultController',
            'defaultAction'     => 'Index'
        );

        ////////////// first level
        $this -> routingTable[0]['children'] = array();

        $this -> routingTable[0]['children'][] = array(
            'pattern'              => 'admin',
            'action'               => 'Index',
            'controller'           => 'AdminController',

            'children'  => array(
					array(
                        'pattern'   => 'upload',
                        'action'    => 'Index',
                        'controller'=> 'UploadAdminController'
                    ),
					array(
                        'pattern'   => 'cropthumbnail',
                        'controller'=> 'CropThumbnailAdminController',
                        'action'    => 'Index'
                    ),
                    array(
                        'pattern'   => 'structure',
                        'action'    => 2,
                        'controller'=> 'StructureController'
                    ),
                    array(
                        'pattern'   => 'anyeditor',
                        'action'    => 2,
                        'controller'=> 'AnyEditorController'
                    ),
                    array(
                        'pattern'   => 'pwdrecovery',
                        'action'    => 2,
                        'controller'=> 'PasswordRecovery'
                    ),
                    array(
                        'pattern'   => 'maintenance',
                        'action'    => 'Index',
                        'controller'=> 'MaintenanceController'
                    ),
                    array(
                        'pattern'   => '*',
                        'action'    => 'Index',
                        'controller'=> 'OldAdminController'
                    )
                )
        );

        $this -> routingTable[0]['children'][] = array(
            'pattern'           => 'ajax',
            'controller'        => 'AjaxController',
            'action'			=> 1
        );

        $this -> routingTable[0]['children'][] = array(
            'pattern'           => 'logout',
            'controller'        => 'FrontController',
            'action'            => 'Logout'
        );

        $this -> routingTable[0]['children'][] = array(
            'pattern'           => 'getcaptcha',
            'controller'        => 'CaptchaGoogleController',
            'action'            => 'GetCaptcha'
        );

        $this -> routingTable[0]['children'][] = array(
            'pattern'           => 'sitemap',
            'controller'        => 'SitemapController',
            'action'            => 'Index'
        );

        $this -> routingTable[0]['children'][] = array(
            'pattern'           => '*',
            'controller'        => 'SimplePageController',
            'action'            => 'Index',
//            'defaultController' => 'FrontController',
            'defaultAction'     => 'Index'
        );

        $this -> routingTable[0]['children'][] = array(
            'pattern'           => '',
            'controller'        => 'IndexController',
            'defaultController' => 'FrontController',
            'defaultAction'     => 'Index'
        );

        ///////////////// second level
    }

    public function &GetRoutingTable()
    {
        return $this -> routingTable;
    }
}
?>