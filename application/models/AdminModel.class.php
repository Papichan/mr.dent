<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 06.04.15
 * Time: 17:13
 */

class AdminModel extends FrontModel {
    public function GetInstalledModules(){
        $query = "SELECT * FROM greeny_modules WHERE isVisible=1";
        $modules = DB::QueryToArray($query);
        return $modules;
    }
}