<?php
/**
 * Моедль для отзывов
 *
 * @author grom
 */
class ReviewModel
{
	const REVIEWS_TABLE_NAME = 'reviews';

	public function GetReviews($limit = null, $start = null)
	{
		$lim	 = (!empty($limit) && is_numeric($limit)) ? (' LIMIT ' .
				((!is_numeric($start)) ? 0 : $start) .
			', ' . $limit) : '';
		$query	 = 'SELECT SQL_CALC_FOUND_ROWS r.*, im.src AS rf, im.srcSmall AS rfs FROM `reviews` r LEFT JOIN greeny_images im ON im.imageID=r.imageID ' . $lim;
//		FB::log($query);
		return DB::QueryToArray($query);
	}

	public function GetApproved($limit = null, $start = null, $lang)
	{
		$langQ	 = !empty($lang) ? " AND lang=" . DB::Quote($lang) : " AND lang IS NULL ";
		$lim = (!empty($limit) && is_numeric($limit)) ? (' LIMIT ' .
			((!is_numeric($start)) ? 0 : $start) .
			', ' . $limit) : '';
		$query	 = 'SELECT SQL_CALC_FOUND_ROWS r.*, im.src AS rf, im.srcSmall AS rfs FROM `reviews` r LEFT JOIN greeny_images im ON im.imageID=r.imageID WHERE isApproved = 1 ' . $langQ . ' ORDER BY dateAdd DESC' . $lim;
		return DB::QueryToArray($query);
	}

	public function GetLastApproved($limit = 2)
	{
		$query = 'SELECT * FROM `reviews` WHERE isApproved=1 ORDER BY dateAdd DESC LIMIT ' . (int)$limit;
		return DB::QueryToArray($query);
	}

	public function GetUnapproved($limit = null, $start = null)
	{
		$lim = (!empty($limit) && is_numeric($limit)) ? (' LIMIT ' .
			((!is_numeric($start)) ? 0 : $start) .
			', ' . $limit) : '';
		$query = 'SELECT SQL_CALC_FOUND_ROWS * FROM `reviews` WHERE isApproved = 0 ORDER BY dateAdd DESC' . $lim;
		return DB::QueryToArray($query);
	}

	public function GetFoundRowsCount()
	{
		return DB::QueryOneValue('SELECT FOUND_ROWS()');
	}

	public function AddReview($review)
	{
		$this -> Validate($review);

		$query = '
			INSERT INTO `reviews` (name, email, review, userIp, isApproved)
			VALUES (
				' . DB::Quote($review['name']  ) . ',
				' . DB::Quote($review['email'] ) . ',
				' . DB::Quote($review['review']) . ',
				' . ((empty($review['userIp'])) ? 'NULL' : DB::Quote($review['userIp'])) . ',
				0
			)
		';
		return DB::Query($query)-> rowCount() > 0;
	}

	public function SaveReview($review)
	{
		if (empty($review['reviewID']))
			throw new Exception('Не найден идентификатор записи');
		$this -> Validate($review);
		$query = "
			UPDATE `reviews`
			SET
				name = " . DB::Quote($review['name']  ) . ",
				email = " . DB::Quote($review['email']  ) . ",
				review = " . DB::Quote($review['review']  ) . ",
				userIp = " . DB::Quote($review['userIp']  ) . ",
				dateAdd = STR_TO_DATE('".$review['dateAdd']."', '%d-%m-%Y %H:%i:%s')
			WHERE
				reviewID = " . (int)$review['reviewID'] . "
		";
		return DB::Query($query) -> rowCount() > 0;
	}

	public function SetApproved($review)
	{
		if (empty($review['reviewID']))
			throw new Exception('Не найден идентификатор записи');
		if (!isset($review['isApproved']))
			$review['isApproved'] = true;
		return DB::Query('
			UPDATE `reviews`
			SET isApproved = ' . (($review['isApproved']) ? 1 : 0) . '
			WHERE reviewID = ' . (int)$review['reviewID'] . '
		') -> rowCount() > 0;
	}

	public function DeleteReview($review)
	{
		if (empty($review['reviewID']))
			throw new Exception('Не найден идентификатор записи');
		return DB::Query('
			DELETE FROM `reviews`
			WHERE reviewID = ' . (int)$review['reviewID'] . '
			LIMIT 1
		');
	}

	public function Validate($data, $dic)
	{
		$fileTypes	 = array(
			'image/jpeg',
			'image/png',
			'image/gif'
		);
		$errorFields = array();
		if (!isset($data['captcha']) || !Captcha::CheckCaptcha($data['captcha']))
		{
			$errorFields['captcha'] = $dic['errorCapthca'];
		}
		if (empty($data['title']))
		{
			$errorFields['title'] = $dic['errorTitle'];
		}
		if (empty($data['name']) || !preg_match('/[a-zа-яА-Я]+/i', $data['name']))
		{
			$errorFields['name'] = $dic['errorName'];
		}
		if (empty($data['review']) || UTF8::strlen($data['review']) < 5)
		{
			$errorFields['review'] = $dic['errorReview'];
		}
		if (empty($data['age']) || !is_numeric($data['age']))
		{
			$errorFields['age'] = $dic['errorAge'];
		}
		if (isset($_FILES['file']))
		{
			if ($_FILES['file']['size'] > 5 * 1024 * 1024) // > 5 Mb
				$errorFields['file'] = $dic['errorImageFileSize'];

			if (!in_array($_FILES['file']['type'], $fileTypes))
				$errorFields['file'] = $dic['errorImageFileType'];
		}
		if (!empty($errorFields))
		{
			throw new ValidationException("error", 1000, $errorFields);
		}
//		if (!Validator::EmailValidator($review['email']))
//			throw new Exception('E-mail адрес указан неверно');
	}

    public function AddFileRow($fileurl){
        $query = "INSERT INTO greeny_files SET src='".$fileurl."'";
        DB::Query($query);
        $id = DB::LastInsertID();
        return $id;
    }

    public function AddNewReview($data)
	{
		if (isset($_FILES['file']))
		{
			$fileName	 = ImageManager::ImageUpload("file", 'review/');
			$fileNameSmall		 = ImageManager::ImageCopy($fileName, 'postfix', '_small');
			ImageManager::StrongImageResize($fileNameSmall, 222, 254);
			$tm			 = new DBTableManager(TablesNames::$IMAGES_TABLE_NAME);
			$data['imageID'] = $tm->Insert(array('src' => $fileName, 'srcSmall' => $fileNameSmall));
		}

		$tm				 = new DBTableManager("reviews");
		return $tm->Insert($data);
	}
}