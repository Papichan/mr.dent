<?php

/**
 * Небольшое дополнение для работы с контентом страницы
 *
 * @version 1.0
 */
class ContactsModel extends FrontModel
{
    public function Index($login)
    {
        $query = 'SELECT 
                    COUNT(*) 
                 FROM `Users` usr
                 WHERE 
                    usr.`login` = "'.$login.'"';

        return DB::QueryOneValue($query);
    }






    /**
     * Получить путь к файлу
     * @param  ID файла для поиска полного пути
     * @return полный путь к файлу из greeny_files
     */
    public function  getFile($fileID)
    {
        $file = DB::QueryOneValue('
        SELECT src as path FROM
         `greeny_files` 
         WHERE fileID = '. $fileID
        );

        return $file;
    }



    /**
     * Получить пути к файлам
     * @param  ID файла для поиска полного пути
     * @return полный путь к файлу из greeny_files
     */
    public function  getFiles($contactsID)
    {
        $files = DB::QueryToArray(
            'SELECT t3.src as path, t2.name as name
                    FROM `contacts` as t1 
                    INNER JOIN `contact_files` as t2 ON t2.contactsID = t1.contactsID  
                    INNER JOIN `greeny_files` as t3 ON t3.fileID =  t2.fileID  
                    '
        );

        return $files;
    }


}

?>