<?php

/**
 * Небольшое дополнение для работы с контентом страницы
 *
 * @version 1.0
 */
class IndexModel extends FrontModel
{
    public function Index($login)
    {
       $query = 'SELECT 
                    COUNT(*) 
                 FROM `Users` usr
                 WHERE 
                    usr.`login` = "'.$login.'"';
       
       return DB::QueryOneValue($query);
    }


    /**
     * Получить все данные для блока слайдера
     *//*
    public function getSlider()
    {
        $slider = DB::QueryToArray(
            'SELECT t1.sliderID, t3.src as img, t1.titleOne, t1.titleTwo, t1.buttonLink, t1.buttonTitle 
                    FROM `slider` as t1 
                    INNER JOIN `sliderImages` as t2 ON t2.sliderID =  t1.sliderID  
                    INNER JOIN `greeny_images` as t3 ON t3.imageID =  t2.imageID  
                    '
        );

        return $slider;
    }
*/

    /**
     * Получить все данные для блока Клиника в цифрах
     */
    public function getStatistic()
    {
        $statistic = DB::QueryToArray(
            'SELECT t1.statisticID, t2.src as img, t1.statisticNumber as nubmer, t1.statisticTitle as title
                    FROM `statistic` as t1 
                    INNER JOIN `greeny_images` as t2  ON t1.imageID =  t2.imageID'
        );

        return $statistic;
    }


    /**
     * Получить все данные для блока Отзывы клиентов
     * @param  Количество отзывов для выборки, по дефолту 5 записей
     */
    public function getReviews($maxReview = 5)
    {
        $review = DB::QueryToArray(
            'SELECT r.name, r.review, g.src as image, r.dateAdd as dateAdd
                    FROM `reviews` as r 
                    INNER JOIN `greeny_images` as g ON r.imageID = g.imageID
                    WHERE r.isApproved = 1  LIMIT 0, '.$maxReview
        );
        return $review;
    }

    
    
    
    
    /** !!!!! ПОМЕНЯТЬ АРГУМЕНТЫ  !!!!!
     * Получить все картинки для блоков Услуги компании
     * @param  Количество отзывов для выборки, по дефолту 5 записей
     */
    public function getImgesFromMenu($menuID = 2, $parentID = 18)
    {    
        
        $images = DB::QueryToArray(
            'SELECT gm.src as smallImage, m.name, m.alias
                    FROM `Menu` as m 
                    INNER JOIN `greeny_images` as gm ON m.smallImage = gm.imageID
                    WHERE m.menuListID ='.$menuID . ' AND parentID = '.$parentID
                );
                
        return $images;
    }
    
    
    /**
     * Получить картинку для формы отправки
     * @param  ID изображения для поиска полного пути
     * @return полный путь к картике из greeny_images
    */
    public function  getImg($imageID)
    {
        $image = DB::QueryOneValue('
        SELECT src as image FROM
         `greeny_images` 
         WHERE imageID = '. $imageID
        );

        return $image;
    }


}

?>