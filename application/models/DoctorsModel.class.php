<?php

/**
 * Небольшое дополнение для работы с контентом страницы
 *
 * @version 1.0
 */
class DoctorsModel extends FrontModel
{
    public function Index($login)
    {
        $query = 'SELECT 
                    COUNT(*) 
                 FROM `Users` usr
                 WHERE 
                    usr.`login` = "'.$login.'"';

        return DB::QueryOneValue($query);
    }


    /** Извлекает связанные блоки по ID страницы
     * @return array
     */
    public function getAdvantage($pageID)
    {
        $blocks = DB::QueryToArray(
            "SELECT a.title, a.text, gi.src as image
                    FROM `advantage` as a 
                    INNER JOIN `greeny_images` as gi ON a.imageID = gi.imageID
                    WHERE a.pageID = $pageID AND a.isApproved = 1"
        );

        return $blocks;
    }



    /**
     * Получить все данные для одного врача
     */
    public function getDoctor($int)
    {
        $doctor = DB::QueryToArray(
            'SELECT p.name, p.profession, g.src as imageFull, p.description, p.category, p.position, p.reviews, p.certificate, p.experience
                    FROM `personal` as p 
                    INNER JOIN `greeny_images` as g ON p.imageFull = g.imageID
                    WHERE p.isActive = 1 AND p.personalID = ' . $int
        );

        return $doctor;
    }


    /**
     * Получить все картинки для слайдера на страницу с Врачами
     */
    public function getSlider($doctorID)
    {
        $slider = DB::QueryToArray(
            'SELECT g.src as image
                    FROM `doctor_images` as di 
                    INNER JOIN `greeny_images` as g ON di.imageID = g.imageID
                    WHERE di.doctorID = ' . $doctorID
        );

        return $slider;
    }

    /**
     * Получить картинку для формы отправки
     * @param  ID изображения для поиска полного пути
     * @return полный путь к картике из greeny_images
     */
    public function  getImg($imageID)
    {
        $image = DB::QueryOneValue('
        SELECT src as image FROM
         `greeny_images` 
         WHERE imageID = '. $imageID
        );

        return $image;
    }
}

?>