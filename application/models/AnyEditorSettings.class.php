<?php
class AnyEditorSettings
{
    public static $EditableTables = array(

        /* Все табы по Страницам - связь со страницами по pageID */
        'tabs'    => array(
            'tableName'     => 'tabs',
            'ruName'        => 'Блоки по страницам /*',
            'ruItem'        => 'Блоки по страницам /*',
            'listFields'    => array(
                'tabsID' => array(
                    'width'         => 10,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'pageID' => array(
                    'width'         => 10,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),/*
                'sliderID' => array(
                    'width'         => 20,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),*/
                'title' => array(
                    'width'         => 20,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'text' => array(
                    'width'         => 60,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'isActive' => array(
                    'width'         => 5,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
            ),
            'filter'            => null,
            'limitOnPage'       => 20,
            'allowAddition'     => 1,
            'allowRemove'       => 1
        ),
        
        /*
        'country_id' => array(
                    'width' => 300,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true,
                    'linkTable' => 'country_sender',
                    'linkField' => 'name',
                    'linkKey' => 'id'
                ),
                'cargo_package_id' => array(
                    'width' => 300,
                    'align' => 'left',
                    'sortable' => true,
                    'searchable' => true,
                    'linkTable' => 'cargo_package_parameters',
                    'linkField' => 'name',
                    'linkKey' => 'id'
                ),
        */
        
        /* ВСЕ СЛАЙДЕРЫ (МОГУТ БЫТЬ ПРИВЯЗАНЫ К РАЗНЫМ СТРАНИЦАМ) */
        'sliders'    => array(
            'tableName'     => 'sliders',
            'ruName'        => 'Слайдеры по страницам /*',
            'ruItem'        => 'Слайдеры по страницам  /*',
            'listFields'    => array(
                'slidersID' => array(
                    'width'         => 10,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'pageID' => array(
                    'width'         => 10,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),/*
                'sliderID' => array(
                    'width'         => 20,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),*/
                'title' => array(
                    'width'         => 30,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'text' => array(
                    'width'         => 50,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'isActive' => array(
                    'width'         => 5,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
            ),
            'filter'            => null,
            'limitOnPage'       => 20,
            'allowAddition'     => 1,
            'allowRemove'       => 1
        ),
      

        /* Блок статистики */
        /* Для блока статистика в цифрах */
        'statistic'    => array(
            'tableName'     => 'statistic',
            'ruName'        => 'Статистика /Главная, О клинике',
            'ruItem'        => 'Статистика /Главная, О клинике',
            'listFields'    => array(
                'statisticID' => array(
                    'width'         => 20,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'statisticNumber' => array(
                    'width'         => 50,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'statisticTitle' => array(
                    'width'         => 50,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => false
                ),

            ),
            'filter'            => null,
            'limitOnPage'       => 20,
            'allowAddition'     => 1,
            'allowRemove'       => 1
        ),



        /* Для блока: Наше преимущество */
        'advantage'    => array(
            'tableName'     => 'advantage',
            'ruName'        => 'Наше преимущество /*',
            'ruItem'        => 'Наше преимущество /*',
            'listFields'    => array(
                'advantageID' => array(
                    'width'         => 5,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'pageID' => array(
                    'width'         => 7,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'title' => array(
                    'width'         => 20,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'text' => array(
                    'width'         => 70,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'isApproved' => array(
                    'width'         => 5,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
            ),
            'filter'            => null,
            'limitOnPage'       => 20,
            'allowAddition'     => 1,
            'allowRemove'       => 1
        ),




        /* Блок Слайдер с сертификатами */
        'certificate'    => array(
            'tableName'     => 'certificate',
            'ruName'        => 'Наши сертификаты /О клинике',
            'ruItem'        => 'Наши сертификаты /О клинике',
            'listFields'    => array(
                'certificateID' => array(
                    'width'         => 5,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'name' => array(
                    'width'         => 50,
                    'align'         => 'self',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'summary' => array(
                    'width'         => 50,
                    'align'         => 'self',
                    'sortable'      => true,
                    'searchable'    => false
                ),/*
                'imageID' => array(
                    'width'         => 20,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),*/
                'isApproved' => array(
                    'width'         => 5,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
            ),
            'filter'            => null,
            'limitOnPage'       => 20,
            'allowAddition'     => 1,
            'allowRemove'       => 1
        ),


        /* Блок  с ценами цна Услуги клиники */
        'prices'    => array(
            'tableName'     => 'prices',
            'ruName'        => 'Цены на услуги /Цены',
            'ruItem'        => 'Цены на услуги /Цены',
            'listFields'    => array(
                'pricesID' => array(
                    'width'         => 5,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'title' => array(
                    'width'         => 30,
                    'align'         => 'self',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'description' => array(
                    'width'         => 70,
                    'align'         => 'self',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'cost' => array(
                    'width'         => 10,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'isApproved' => array(
                    'width'         => 5,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
            ),
            'filter'            => null,
            'limitOnPage'       => 20,
            'allowAddition'     => 1,
            'allowRemove'       => 1
        ),












        /* Причины появлениякариеса lechenie_zubov */
        'reason_cariesa'    => array(
            'tableName'     => 'reason_cariesa',
            'ruName'        => 'Причины /Лечение кариеса',
            'ruItem'        => 'Причины /Лечение кариеса',
            'listFields'    => array(
                'reason_cariesaID' => array(
                    'width'         => 5,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'description' => array(
                    'width'         => 20,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'isActive' => array(
                    'width'         => 5,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
            ),
            'filter'            => null,
            'limitOnPage'       => 20,
            'allowAddition'     => 1,
            'allowRemove'       => 1
        ),


        /* Для блока Наш Персонал */
        'personal'    => array(
            'tableName'     => 'personal',
            'ruName'        => 'Наши специалисты /Врачи',
            'ruItem'        => 'Наши специалисты /Врачи',
            'listFields'    => array(
                'personalID' => array(
                    'width'         => 2,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'name' => array(
                    'width'         => 10,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'profession' => array(
                    'width'         => 10,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'experience' => array(
                     'width'         => 3,
                     'align'         => 'left',
                     'sortable'      => true,
                     'searchable'    => true
                 ),
                'certificate' => array(
                    'width'         => 3,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'reviews' => array(
                    'width'         => 3,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'position' => array(
                    'width'         => 5,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'category' => array(
                    'width'         => 5,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'description' => array(
                    'width'         => 40,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),/*
                'imageAva' => array(
                    'width'         => 10,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'imageFull' => array(
                    'width'         => 10,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),*/
                'isActive' => array(
                    'width'         => 3,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
            ),
            'filter'            => null,
            'limitOnPage'       => 20,
            'allowAddition'     => 1,
            'allowRemove'       => 1
        ),








        /* Голубые блоки blue_blocks */
        'blue_blocks'    => array(
            'tableName'     => 'blue_blocks',
            'ruName'        => 'Голубые блоки /*',
            'ruItem'        => 'Голубые блоки /*',
            'listFields'    => array(
                'blue_blocksID' => array(
                    'width'         => 5,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'parentPage' => array(
                    'width'         => 5,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                 'DependendDoctor' => array(
                    'width'         => 5,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'textTop' => array(
                    'width'         => 20,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),/*
                'imageIconID' => array(
                    'width'         => 5,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),*/
                'imageIconTitle' => array(
                    'width'         => 17,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'imageAvaTitle' => array(
                    'width'         => 18,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),/*
                'imageAvaID' => array(
                    'width'         => 5,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),*/
                'buttonTitle' => array(
                    'width'         => 10,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'buttonLink' => array(
                    'width'         => 15,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'isActive' => array(
                    'width'         => 7,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
            ),
            'filter'            => null,
            'limitOnPage'       => 20,
            'allowAddition'     => 1,
            'allowRemove'       => 1
        ),































        // lowercase key!
        /*
        'news'      => array(
            'tableName'     => 'News',
            'ruName'        => 'Новости',
            'ruItem'        => 'Новость',
            'listFields'    => array(
                'newsID' => array(
                    'width'         => 40,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => false
                ),
                'title' => array(
                    'width'         => 500,
                    'align'         => 'left',
                    'sortable'      => true,
                    'searchable'    => true
                ),
                'creationDate' => array(
                    'width'         => 200,
                    'align'         => 'right',
                    'sortable'      => true,
                    'searchable'    => false
                )
            ),
            'filter'            => null,
            'orderBy'           => '`newsID`',
            'limitOnPage'       => 20,
            'allowAddition'     => 0,
            'allowRemove'       => 0
        ),
		'videos'	=>	array(
			'tableName'			=> 'Videos',
			'ruName'			=> 'Видео-файлы',
			'ruItem'			=> 'Видео',
			'listFields'		=> array('title', 'description', 'creationDate', 'fileSize'),
			'filter'			=> null,
			'orderBy'			=> '`creationDate` DESC',
			'limitOnPage'		=> 50,
			'allowAddition'		=> 1,
			'allowRemove'		=> 1,
			'eventListeners'	=> array(
									'AnyEditorEntryAddPreFlush'
										=> array(
											array(
												'class' => 'VideosEventListener',
												'method' => 'AddPreFlushHandler'
											)
										   ),
									'AnyEditorEntryEditPreFlush'
										=> array(
											array(
												'class' => 'VideosEventListener',
												'method' => 'EditPreFlushHandler'
											)
										   )
									)
		)*/

//        'greeny_nl_news'    => array(
//            'tableName'     => 'greeny_nl_news',
//            'ruName'        => 'Новости',
//            'ruItem'        => 'Новости',
//            'listFields'    => array(
//                'newsID' => array(
//                    'width'         => 40,
//                    'align'         => 'right',
//                    'sortable'      => true,
//                    'searchable'    => false
//                ),
//                'title' => array(
//                    'width'         => 500,
//                    'align'         => 'left',
//                    'sortable'      => true,
//                    'searchable'    => true
//                ),
//                'publicationDate' => array(
//                    'width'         => 200,
//                    'align'         => 'center',
//                    'sortable'      => true,
//                    'searchable'    => false
//                )
//            ),
//            'filter'            => null,
//            'orderBy'           => '`messageID`',
//            'limitOnPage'       => 20,
//            'allowAddition'     => 0,
//            'allowRemove'       => 0
//        ),
//
//        'fb_typetags'    => array(
//            'tableName'     => 'fb_typeTags',
//            'ruName'        => 'Типы тегов',
//            'ruItem'        => 'Типы тегов',
//            'listFields'    => array(
//                'typeTagID' => array(
//                    'width'         => 40,
//                    'align'         => 'right',
//                    'sortable'      => true,
//                    'searchable'    => false
//                ),
//                'caption' => array(
//                    'width'         => 500,
//                    'align'         => 'left',
//                    'sortable'      => true,
//                    'searchable'    => true
//                ),
//            ),
//            'filter'            => null,
//            'orderBy'           => '`typeTagID`',
//            'limitOnPage'       => 20,
//            'allowAddition'     => 1,
//            'allowRemove'       => 1
//        ),
//
//        'fb_tags'    => array(
//            'tableName'     => 'fb_Tags',
//            'ruName'        => 'Теги',
//            'ruItem'        => 'Теги',
//            'listFields'    => array(
//                'tagID' => array(
//                    'width'         => 40,
//                    'align'         => 'right',
//                    'sortable'      => true,
//                    'searchable'    => false
//                ),
//                'caption' => array(
//                    'width'         => 500,
//                    'align'         => 'left',
//                    'sortable'      => true,
//                    'searchable'    => true
//                ),
//                'typeTagID' => array(
//                    'width'         => 500,
//                    'align'         => 'left',
//                    'sortable'      => true,
//                    'searchable'    => true,
//                    'linkTable'     => 'fb_typeTags',
//                    'linkField'     => 'caption'
//                ),
//            ),
//            'filter'            => null,
//            'orderBy'           => '`tagID`',
//            'limitOnPage'       => 20,
//            'allowAddition'     => 1,
//            'allowRemove'       => 1
//        ),
    );
}
?>