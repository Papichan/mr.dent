<?php

/**
 * Небольшое дополнение для работы с контентом страницы
 *
 * @version 1.0
 */
class AboutModel extends FrontModel
{
    public function Index($login)
    {
        $query = 'SELECT 
                    COUNT(*) 
                 FROM `Users` usr
                 WHERE 
                    usr.`login` = "' . $login . '"';

        return DB::QueryOneValue($query);
    }

    /**
     * Получить все данные для блока Клиника в цифрах
     */
    public function getStatistic()
    {
        $statistic = DB::QueryToArray(
            'SELECT t1.statisticID, t2.src as img, t1.statisticNumber as nubmer, t1.statisticTitle as title
                    FROM `statistic` as t1 
                    INNER JOIN `greeny_images` as t2  ON t1.imageID =  t2.imageID'
        );
        return $statistic;
    }


    /**
     * Получить все данные для блока Клиника в цифрах
     */
    public function getAdvantage($pageID)
    {

        $advantage = DB::QueryToArray(
            "SELECT a.title, a.text, g.src as image
                    FROM `advantage` as a 
                    INNER JOIN `greeny_images` as g ON a.imageID = g.imageID
                    WHERE a.pageID = $pageID AND a.isApproved = 1"
        );

        return $advantage;
    }


    /**
     * Получить все данные для блока Клиника в цифрах
     */
    public function getCertificate()
    {

        $advantage = DB::QueryToArray(
            'SELECT c.name, c.summary, g.src as image
                    FROM `certificate` as c 
                    INNER JOIN `greeny_images` as g ON c.imageID = g.imageID
                    WHERE c.isApproved = 1'
        );

        return $advantage;
    }


    /** Находит и возвращает массив картинок для слайдера по id страницы
     * @param int номер страницы для которой надо получить слайдер
     * @return array изображения для слайдера
     */
    public function getSlider($about_companyID)
    {
        $slider = DB::QueryToArray(
            "SELECT gm.src as sliderImage, sa.title, sa.link 
                    FROM `about_company` as ac 
                    INNER JOIN `sliderAbout` as sa ON ac.about_companyID = sa.about_companyID
                    INNER JOIN `greeny_images` gm  ON sa.imageID = gm.imageID
                    WHERE ac.about_companyID = $about_companyID"
        );

        return $slider;
    }

}

?>