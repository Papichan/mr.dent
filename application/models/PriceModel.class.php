<?php

/**
 * Небольшое дополнение для работы с контентом страницы
 *
 * @version 1.0
 */
class PriceModel
{
    public function Index($login)
    {
        $query = 'SELECT 
                    COUNT(*) 
                 FROM `Users` usr
                 WHERE 
                    usr.`login` = "'.$login.'"';

        return DB::QueryOneValue($query);
    }

    /**
     * Получить все данные для блока Цены на Услуги
     */
    public function getPrices()
    {
        $prices = DB::QueryToArray(
            'SELECT title, description, cost FROM `prices` 
                    WHERE isApproved = 1'
        );
        return $prices;
    }

    /**
     * Получить картинку для формы отправки
     * @param  ID изображения для поиска полного пути
     * @return полный путь к картике из greeny_images
     */
    public function  getImg($imageID)
    {
        $image = DB::QueryOneValue('
        SELECT src as image FROM
         `greeny_images` 
         WHERE imageID = '. $imageID
        );

        return $image;
    }

}

?>