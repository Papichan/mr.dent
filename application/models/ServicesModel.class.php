<?php

/**
 * Небольшое дополнение для работы с контентом страницы
 *
 * @version 1.0
 */
class ServicesModel extends FrontModel
{
    public function Index($login)
    {
        $query = 'SELECT 
                    COUNT(*) 
                 FROM `Users` usr
                 WHERE 
                    usr.`login` = "'.$login.'"';

        return DB::QueryOneValue($query);
    }
    
   
    
     /** Извлекает и возвращает Выпадающие блоки
     * в месте со слайдером для них если он есть
     * @param  int   ID страницы из greeny_pageStructure по нему изымаются все
     * связанные с ним изображения.
     * @return array Массив картинок с заголовками иссылками
     */
    public function getTabs($pageID)
    {
        $tab = DB::QueryToArray('
                SELECT t.tabsID, t.title, t.text 
                FROM `tabs` as t
                WHERE t.pageID = ' . $pageID    
        );

        $images = DB::QueryToArray("
                SELECT 
                    t.tabsID, 
                    gi.src as image, 
                    ts.title, 
                    ts.link
                FROM   `greeny_pageStructure` as ps
                INNER  JOIN `tabs` as t ON ps.pageID = t.pageID
                INNER  JOIN `tab_sliders` as ts ON t.tabsID = ts.tabsID
                INNER  JOIN `greeny_images` as gi ON ts.imageID = gi.imageID
                WHERE  ps.isDeleted = 0 AND ps.isActive = 1 AND ps.pageID = $pageID"
        );
        
        return [$tab, $images];
    }
    
    /**
     * Получить все данные для блока Причины появления кариеса
     */
    public function getReasons()
    {
        $prices = DB::QueryToArray(
            'SELECT description FROM `lechenie_reason` 
                WHERE isActive = 1'
        );
        return $prices;
    }

 
    
    /**
     * Получить все данные для блока Цены на Услуги
     */
    public function getPrices($limitRows = 5)
    {
        $prices = DB::QueryToArray(
            "SELECT title, description, cost FROM `prices` 
                    WHERE isApproved = 1 LIMIT 0, $limitRows"
        );
        return $prices;
    }

    /**
     * Получить все данные для блока Клиника в цифрах
     */
    public function getStatistic()
    {
        $statistic = DB::QueryToArray(
            'SELECT t1.statisticID, t2.src as img, t1.statisticNumber as nubmer, t1.statisticTitle
                    FROM `statistic` as t1 
                    INNER JOIN `greeny_images` as t2  ON t1.imageID =  t2.imageID'
        );
        return $statistic;
    }
    
    
    /**
     * Получить все данные для блока Отзывы клиентов
     */
    public function getReviews()
    {
        $review = DB::QueryToArray(
            'SELECT 
                r.name, 
                r.review, 
                g.src as image, 
                r.dateAdd as dateAdd
            FROM `reviews` as r 
            INNER JOIN `greeny_images` as g ON r.imageID = g.imageID
            WHERE r.isApproved = 1'
        );
        return $review;
    }
    
    
    
    /**
     * Получить все данные для блока Отзывы клиентов
     */
    public function getAll()
    {
        $personal = DB::QueryToArray(
            'SELECT 
                p.name, 
                p.profession, 
                g.src as image, 
                p.signUpLink, 
                p.signUpTitle, 
                p.informationLink, 
                p.informationAbout
            FROM `personal` as p 
            INNER JOIN `greeny_images` as g ON p.imageID = g.imageID
            WHERE p.isApproved = 1'
        );
        return $personal;
    }

    
    

    /**
     * Получить картинку для формы отправки
     * @param  ID изображения для поиска полного пути
     * @return полный путь к картике из greeny_images
     */
    public function  getImg($imageID)
    {
        $image = DB::QueryOneValue('
        SELECT src as image FROM
         `greeny_images` 
         WHERE imageID = '. $imageID
        );

        return $image;
    }


     /** Извлекает связанные блоки DependendTables для отдельной страницы
     * @return array
     */
    public function getLechenieBocks($lechenieID)
    {
        $blocks = DB::QueryToArray(
            'SELECT lb.title, lb.text
                    FROM  `lechenieBlocks` as lb 
                    WHERE lb.lechenie_zubovID = '.$lechenieID
        );

        return $blocks;
    }
    
}

?>