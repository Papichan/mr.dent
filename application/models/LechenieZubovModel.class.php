<?php

/**
 * Небольшое дополнение для работы с контентом страницы
 *
 * @version 1.0
 */
class LechenieZubovModel extends FrontModel
{
    public function Index($login)
    {
        $query = 'SELECT 
                    COUNT(*) 
                 FROM `Users` usr
                 WHERE 
                    usr.`login` = "'.$login.'"';

        return DB::QueryOneValue($query);
    }



    /**
     * Получить все данные для блока Клиника в цифрах
     */
    public function getStatistic()
    {
        $statistic = DB::QueryToArray(
            'SELECT t1.statisticID, t2.src as img, t1.statisticNumber as nubmer, t1.statisticTitle
                    FROM `statistic` as t1 
                    INNER JOIN `greeny_images` as t2  ON t1.imageID =  t2.imageID'
        );
        return $statistic;
    }


    /**
     * Получить все данные для блока Отзывы клиентов
     */
    public function getReviews()
    {
        $review = DB::QueryToArray(
            'SELECT r.name, r.review, g.src as image, r.dateAdd as dateAdd
                    FROM `reviews` as r 
                    INNER JOIN `greeny_images` as g ON r.imageID = g.imageID
                    WHERE r.isApproved = 1'
        );
        return $review;
    }



    /**
     * Получить все данные для блока Цены на Услуги
     */
    public function getPrices($limitRows = 5)
    {
        $prices = DB::QueryToArray(
            "SELECT title, description, cost FROM `prices` 
                    WHERE isApproved = 1 LIMIT 0, $limitRows"
        );
        return $prices;
    }



    /**
     * Получить все данные для блока Лечение кариеса
     */
    public function getLechenie()
    {
        $prices = DB::QueryToArray(
            'SELECT title, description FROM `lechenie_cariesa` 
                    WHERE isActive = 1'
        );
        return $prices;
    }


    /**
     * Получить все данные для блока Причины появления кариеса
     */
    public function getReasons()
    {
        $prices = DB::QueryToArray(
            'SELECT description FROM `reason_cariesa` 
                    WHERE isActive = 1'
        );
        return $prices;
    }


}

?>