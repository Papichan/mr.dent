<?php

/**
 * Небольшое дополнение для работы с контентом страницы
 *
 * @version 1.0
 */
class FrontModel
{

    /**
     * Вернуть Меню для футера
     */
    public static function getMainMenu()
    {
        $query = 'SELECT name, alias
                 FROM `Menu`
                 WHERE  parentID = 1 AND isActive = 1';

        return DB::QueryToArray($query);
    }

    /** Извлекает и возвращает изображения для слайдера по ID страницы
     * 
     * @param  int   ID страницы из greeny_pageStructure по нему изымаются все
     * связанные с ним изображения.
     * @return array Массив картинок с заголовками иссылками
     */
    public function getSlider($pageID)
    {
        $images = DB::QueryToArray(
            'SELECT 
                    gi.src as image, 
                    si.title,
                    si.text, 
                    si.buttonTitle, 
                    si.link
                FROM  `greeny_pageStructure` as ps
                INNER JOIN `sliders` as s ON ps.pageID = s.pageID
                INNER JOIN `slidersImages` as si ON si.slidersID = s.slidersID
                INNER JOIN `greeny_images` as gi ON si.imageID = gi.imageID
                WHERE  ps.isDeleted = 0 AND ps.isActive = 1 AND ps.pageID = '. $pageID
        );
        return $images;
    }
    
    public function checkLoginUser($login)
    {
       $query = 'SELECT 
                    COUNT(*) 
                 FROM `'.TablesNames::$USERS_TABLE_NAME.'` usr
                 WHERE 
                    usr.`login` = "'.$login.'"';
       
       return DB::QueryOneValue($query);
    }
    
    public function checkEmailUser($email)
    {
       $query = 'SELECT 
                    COUNT(*) 
                 FROM `'.TablesNames::$USERS_TABLE_NAME.'` usr
                 WHERE 
                    usr.`email` = "'.$email.'"';
       
       return DB::QueryOneValue($query);
    }
    
    public function saveUser($user)
    {
       $query = 'INSERT INTO `'.TablesNames::$USERS_TABLE_NAME.'` (`login`,`password`,`email`,`FIO`,`role`,`isActive`,`registrationDate`)
                 VALUES ("'.$user['login'].'","'.$user['password'].'","'.$user['email'].'","'.$user['FIO'].'","user",0,NOW())';
       
       return DB::Query($query);
    }
    
    public function getLoginPageAliases()
    {
       $query = 'SELECT
                    ps.`pageID`
                 FROM `'.TablesNames::$PAGE_STRUCTURE_TABLE_NAME.'` ps
                 WHERE 
                    ps.`isLogin` = 1';

       $res = DB::QueryToArray($query);
       if (is_array($res)){
           $ar = array();
           foreach($res as $item){
               $ar[] = $item['pageID'];
           }
           
           return $ar;
       }else
           return false;
    }



    /** ИЗМЕНЕН ДОБАВЛЕН ! ПОПКОВ СЕРГЕЙ !
     * Получить все данные для блока Отзывы клиентов
     * @param  int   количество запичей которое надо получить,
     * @param  bool  обрезать название до "Имя Фамилия"
     * @return array массив с врачами клиники
     */
    public function getPersonal($numbers = 100, $cutName = true)
    {
        $personal = DB::QueryToArray(
            "SELECT p.personalID, p.name, p.profession, g.src as imageAva
                    FROM `personal` as p 
                    INNER JOIN `greeny_images` as g ON p.imageAva = g.imageID
                    WHERE p.isActive = 1 LIMIT 0, $numbers"
        );

        // Если true то обрезать название, оставить только "Имя Фамилия"
        if ($cutName)
        {
            $count = count($personal);
            for ($i=0; $i < $count ;$i++)
            {
                $result = explode(' ', $personal[$i]['name']);
                $personal[$i]['name'] = $result[0] . ' ' . $result[1];
            }
        }
        return $personal;
    }


    /** ИЗМЕНЕН ДОБАВЛЕН ! ПОПКОВ СЕРГЕЙ !
     * Получить картинку для формы отправки, подходит для поиска пути только
     * одной картинки, без связующей таблицы
     * @param int это id изображения для поиска полного пути
     * @return string полный путь к картике из greeny_images
     */
    public function getImg($imageID)
    {
        $image = DB::QueryOneValue('
            SELECT src as image 
            FROM `greeny_images` 
            WHERE imageID = ' . $imageID
        );

        return $image;
    }


    /** ИЗМЕНЕН ДОБАВЛЕН ! ПОПКОВ СЕРГЕЙ !
     * Получить информацию о докторе связанную через DependendTable с таблицей blue_blocks
     * Получает ( Путь к картинке, Имя доктора, Должность )
     * @param int это id изображения для поиска полного пути
     * @return string полный путь к картике из greeny_images
     */
    public function getDependendDoctor($dependendDoctorID)
    {
        $dependendDoctor = DB::QueryToArray("
            SELECT gm.src as imageAva, p.name, p.position
            FROM  `blue_blocks` as bb
            INNER JOIN `personal` as p ON bb.dependendDoctor = p.personalID
            INNER JOIN `greeny_images` as gm ON p.imageAva = gm.imageID
            WHERE bb.dependendDoctor = $dependendDoctorID"
        );

        return $dependendDoctor;
    }
    

    /** ИЗМЕНЕН ДОБАВЛЕН ! ПОПКОВ СЕРГЕЙ !
     * ВНИМАНИЕ: Можно использ-ть как pageID так и parentID из Массива  $this -> page->pageAttrs
     * Тоесть можно выбирать как вид голубого блока как для одной отдельной страницы
     * так и указав parentID для всех потомков этой страницы.
     * Получет pageID страницы и находит для него всю информацию, включая все картинки.
     * @param int это номер страницы для которой будут выбраны все голубые блоки для этой страницы
     * @return array массив со всеми голубыми блоками для данной страницы
     */
    public function GetBlueBlocks($pageID = null)
    {
        if($pageID == null){
            return [];
        }

        $blueBlocks = DB::QueryToArray(
            "SELECT * 
            FROM `blue_blocks`
            WHERE parentPage = $pageID AND isActive = 1"
        );

        $countBlocks = count($blueBlocks); // Количество голубых блоков для данной страницы

        for ($i=0; $i < $countBlocks; $i++)
        {
            if( !empty($blueBlocks[$i]['imageAvaID']) ) {
                $blueBlocks[$i]['imageAva']  = $this->getImg($blueBlocks[$i]['imageAvaID']);
            }
            if( !empty($blueBlocks[$i]['imageIconID']) ) {
                $blueBlocks[$i]['imageIcon'] = $this->getImg($blueBlocks[$i]['imageIconID']);
            }
            if( !empty($blueBlocks[$i]['dependendDoctor']) ) {
                $blueBlocks[$i]['dependendDoctor'] = $this->getDependendDoctor($blueBlocks[$i]['dependendDoctor']);
            }
        }


        return $blueBlocks;
    }
}

?>