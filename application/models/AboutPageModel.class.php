<?php

class AboutPageModel extends SimplePageModel 
{
    const ABOUT_PAGE_TABLE = 'AboutPages';
    const ABOUT_PAGE_IMAGES_TABLE = 'AboutPageImages';
    
    /**
     * Контент страницы
     *
     * @return array
     */
    public function Content()
    {
        // Получим контент из основной таблицы
        $content = $this -> page -> Content();

        // Получим изображения
        $query = '
            SELECT  a.*, i.*
            FROM    `'. self::ABOUT_PAGE_IMAGES_TABLE .'` a
            JOIN    `'. TablesNames::$IMAGES_TABLE_NAME .'` i
                ON  a.`imageID` = i.`imageID`
            WHERE   a.`aboutPageID` = '. DB::Quote($this -> page -> contentID) .'
            ORDER BY a.`priority` DESC
        ';
        
        // Объединим в один массив
        $content['images'] = DB::QueryToArray($query);
        
        return $content;
    }
}

?>