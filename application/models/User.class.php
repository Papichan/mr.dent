<?php
/**
 * Пользователь системы. 
 * 
 * @version 1.1
 *
 */
class User
{
    private $userID;
    private $login;
    private $email;
    private $FIO;
    private $role;
    private $authorised = false;

    /**
     * Создаёт пользователя и загружает его из сессии
     *
     */
    public function __construct()
    {
        if (isset($_SESSION['userID']))
        {
                $userManager = UsersManager::getInstance();
                $user = $userManager -> GetUser(array('userID' => $_SESSION['userID']));
                if (!empty($user)){
                    if ($user['isActive']) 
                        $this -> InitUser($user);
                    else
                        $this -> UnsetUser();
                }
        }
    }

    public function InitUser($userAttrs)
    {
        $this -> userID = $userAttrs['userID'];
        $this -> login = $userAttrs['login'];
        $this -> email = $userAttrs['email'];
        $this -> FIO = $userAttrs['FIO'];
        $this -> role = $userAttrs['role'];
        $this -> authorised = true;
        $_SESSION['userID'] = $this -> userID;
        $_SESSION['userInfo'] = serialize($userAttrs);
    }
    
    public function UnsetUser()
    {
        $this -> userID = null;
        $this -> login = null;
        $this -> email = null;
        $this -> FIO = null;
        $this -> role = null;
        $this -> authorised = false;
        if (isset($_SESSION['userID'])) unset($_SESSION['userID']);
	if (isset($_SESSION['userInfo'])) unset($_SESSION['userInfo']);
    }

    public function GetUserID()
    {
        return $this -> userID;
    }
    
    public function GetLogin()
    {
        return $this -> login;
    }

    public function GetEmail()
    {
        return $this -> email;
    }
    
    public function GetFio()
    {
        return $this -> FIO;
    }
    
    public function GetRole()
    {
        return $this -> role;
    }
    
    public function IsAuthorised()
    {
        return $this -> authorised;
    }
    
    public function ToArray()
    {
        return array(
            'userID'    => $this -> userID,
            'login'     => $this -> login,
            'email'     => $this -> email,
            'FIO'       => $this -> FIO,
            'role'      => $this -> role,
            'authorised'=> $this -> authorised
        );
    }
}
?>