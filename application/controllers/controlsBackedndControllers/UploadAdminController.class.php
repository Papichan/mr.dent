<?php
/**
 * Контроллер загрузки файлов. Работает только для авторизованных пользователей
 * @version 1.0
 */
class UploadAdminController extends AdminController
{
	/**
     *
     */
    public function __construct()
    {
        // Вызываем конструктор родительского класса
        parent::__construct();
    }

    /**
     * ACTION INDEX
     *
     */
    public function Index($args = null)
    {
		// Check post_max_size (http://us3.php.net/manual/en/features.file-upload.php#73762)
		$POST_MAX_SIZE = ini_get('post_max_size');
		$unit = strtoupper(substr($POST_MAX_SIZE, -1));
		$multiplier = ($unit == 'M' ? 1048576 : ($unit == 'K' ? 1024 : ($unit == 'G' ? 1073741824 : 1)));

		if (!empty($_SERVER['CONTENT_LENGTH']) && (int)$_SERVER['CONTENT_LENGTH'] > $multiplier*(int)$POST_MAX_SIZE && $POST_MAX_SIZE)
		{
			header("HTTP/1.1 500 Internal Server Error"); // This will trigger an uploadError event in SWFUpload
			return new ContentActionResult('POST exceeded maximum allowed size.');
		}

		// Settings
		$save_path = IncPaths::$ROOT_PATH . 'uploaded/tmp/';
		if (!file_exists($save_path))
		{
			if (!is_writable(IncPaths::$ROOT_PATH . 'uploaded/'))
				return new ContentActionResult('No permissions to write to /uploaded. '.$save_path );
			$newDir = mkdir(IncPaths::$ROOT_PATH . 'uploaded/tmp');
			if ($newDir === false)
			{
				return $this -> HandleError('Can not create tmp directory. '.$save_path );
			}
		}
		$upload_name = "Filedata";
		$max_file_size_in_bytes = 2147483647;				// 2GB in bytes

		// We have to get Allowed extensions from fields regstry
		if (!empty($_GET['frid']) && is_numeric($_GET['frid']))
		{
			$provider = new DBControlsRegistryProvider();
			$controlParams = $provider -> GetControlParamsByID($_GET['frid']);
			if (empty($controlParams) || empty($controlParams['controlSettings']) || empty($controlParams['controlSettings']['allowedExtensions']) )
				$extension_whitelist = array("jpg", "jpeg", "gif", "png");
			else
			{
				$allowedExt = $controlParams['controlSettings']['allowedExtensions'];
				$allowedExt = str_replace(array(' ', '.', '*'), '', $allowedExt);
				$extension_whitelist = explode(';', $allowedExt);
			}
		}
		else
		{
			return $this -> HandleError('Field ID has not been passed. It must be passed in GET frid');
		}
			// Allowed file extensions
		$valid_chars_regex = '.A-Z0-9_-';				// Characters allowed in the file name (in a Regular Expression format)

		// Other variables
		$MAX_FILENAME_LENGTH = 260;
		$file_name = "";
		$file_extension = "";
		$uploadErrors = array(
			0=>"There is no error, the file uploaded with success",
			1=>"The uploaded file exceeds the upload_max_filesize directive in php.ini",
			2=>"The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form",
			3=>"The uploaded file was only partially uploaded",
			4=>"No file was uploaded",
			6=>"Missing a temporary folder"
		);


		// Validate the upload
		if (!isset($_FILES[$upload_name]))
			return $this -> HandleError("No upload found in \$_FILES for " . $upload_name);

		else if (isset($_FILES[$upload_name]["error"]) && $_FILES[$upload_name]["error"] != 0)
			return $this -> HandleError($uploadErrors[$_FILES[$upload_name]["error"]]);

		else if (!isset($_FILES[$upload_name]["tmp_name"]) || !@is_uploaded_file($_FILES[$upload_name]["tmp_name"]))
			return $this -> HandleError("Upload failed is_uploaded_file test.");

		else if (!isset($_FILES[$upload_name]['name']))
			return $this -> HandleError("File has no name.");

		// Validate the file size (Warning: the largest files supported by this code is 2GB)
		$file_size = @filesize($_FILES[$upload_name]["tmp_name"]);
		if (!$file_size || $file_size > $max_file_size_in_bytes)
			return $this -> HandleError("File exceeds the maximum allowed size");

		if ($file_size <= 0)
			return $this -> HandleError("File size outside allowed lower bound");



		// Validate file extension
		$path_info = pathinfo($_FILES[$upload_name]['name']);
		$file_extension = $path_info["extension"];
		$is_valid_extension = false;
		foreach ($extension_whitelist as $extension) {
			if (strcasecmp($file_extension, $extension) == 0) {
				$is_valid_extension = true;
				break;
			}
		}
		if (!$is_valid_extension)
		{
			return $this -> HandleError("Invalid file extension");
		}

		// Validate file name (for our purposes we'll just remove invalid characters)
		//$file_name = preg_replace('/[^'.$valid_chars_regex.']|\.+$/i', "", basename($_FILES[$upload_name]['name']));
//		$fname = $path_info['filename'];
//		$fname = Transliter::TranslitToStrictName($fname);

        $fname = preg_replace('/[^'.$valid_chars_regex.']|\.+$/i', "", $path_info['filename']);
		 if (strlen($fname) == 0)
                {
                    $fname = Transliter::TranslitToStrictName($path_info['filename']);
                }
		// Change filename, if such name exists
		$fext = $path_info['extension'];
		while (file_exists($save_path . $fname .'.'. $fext))
		{
			$fname = $fname .'_'. rand(0, 1000);
		}
		if (strlen($fname) == 0 || strlen($fname) > $MAX_FILENAME_LENGTH)
		{
			return $this -> HandleError("Invalid file name");
		}

		// New file name
		$file_name = $fname .'.'. $fext;

		// Validate file contents (extension and mime-type can't be trusted)
		/*
			Validating the file contents is OS and web server configuration dependant.  Also, it may not be reliable.
			See the comments on this page: http://us2.php.net/fileinfo

			Also see http://72.14.253.104/search?q=cache:3YGZfcnKDrYJ:www.scanit.be/uploads/php-file-upload.pdf+php+file+command&hl=en&ct=clnk&cd=8&gl=us&client=firefox-a
			 which describes how a PHP script can be embedded within a GIF image file.

			Therefore, no sample code will be provided here.  Research the issue, decide how much security is
			 needed, and implement a solution that meets the needs.
		*/


		// Process the file
		/*
			At this point we are ready to process the valid file. This sample code shows how to save the file. Other tasks
			 could be done such as creating an entry in a database or generating a thumbnail.

			Depending on your server OS and needs you may need to set the Security Permissions on the file after it has
			been saved.
		*/
		if (!@move_uploaded_file($_FILES[$upload_name]["tmp_name"], $save_path.$file_name)) {
			return $this -> HandleError("File could not be saved.");
		}

		//exit(0);
		exit('{"state" : "success", "filePath" : "'. str_replace(str_replace('\\', '/', getcwd()), '', $save_path).$file_name .'"}');

	}

  /* Handles the error output. This error message will be sent to the uploadSuccess event handler.  The event handler
will have to check for any error messages and react as needed. */
	function HandleError($message)
	{
		//header("HTTP/1.1 500 Internal Server Error");
		$errorMsg = '{"state" : "error", "msg" : "'. $message .'"}';
		return new JsonActionResult($errorMsg);
	}
}
?>