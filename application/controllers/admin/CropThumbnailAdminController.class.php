<?php
/**
 * Контроллер для обрезки картинок. Работает только для авторизованных пользователей
 * @version 1.0
 */
class CropThumbnailAdminController extends AdminController
{
	/**
     *
     */
    public function __construct()
    {
        // Вызываем конструктор родительского класса
        parent::__construct();
    }

    /**
     * ACTION INDEX
     *
     */
    public function Index($args = null)
    {
        if (
            !isset(Request::$POST['x1']) || !is_numeric(Request::$POST['x1']) ||
            !isset(Request::$POST['x2']) || !is_numeric(Request::$POST['x2']) ||
            !isset(Request::$POST['y1']) || !is_numeric(Request::$POST['y1']) ||
            !isset(Request::$POST['y2']) || !is_numeric(Request::$POST['y2']) ||
            !isset(Request::$POST['thumbWidth']) || !is_numeric(Request::$POST['thumbWidth']) ||
            !isset(Request::$POST['thumbHeight']) || !is_numeric(Request::$POST['thumbHeight']) ||
            Request::$POST['x1'] >= Request::$POST['x2'] ||
            Request::$POST['y1'] >= Request::$POST['y2'] ||
            empty(Request::$POST['thumb']) ||
            empty(Request::$POST['img'])
        )
            return new JsonActionResult(array('error' => 'Неверные параметры'));

        $thumb = Request::$POST['thumb'];

        $quest = UTF8::strpos($thumb, '?');
        if ($quest !== false)
        {
            $thumb = UTF8::substr($thumb, 0, $quest);
            Request::$POST['thumb'] = $thumb;
        }

        $imageFilePath = IncPaths::$ROOT_PATH . Request::$POST['img'];
        $imageFilePath = str_replace('//', '/', $imageFilePath);
        if (!file_exists($imageFilePath))
            return new JsonActionResult(array('error' => 'Не найден файл картинки '));

        $thumb = IncPaths::$ROOT_PATH . $thumb;
        $thumb = str_replace('//', '/', $thumb);

        if (file_exists($thumb))
        {
            if (!is_writable($thumb))
                return new JsonActionResult(array('error' => 'Не имею права переписать файл уменьшенной копии'));
        }

        if (!($imageInfo = @getimagesize($imageFilePath)))
            return new JsonActionResult(array('error' => 'Файл не является картинкой'));

        $imageWidth = $imageInfo[0];
        $imageHeight = $imageInfo[1];
        $imageType = $imageInfo[2];

        switch ($imageType)
        {
            case IMAGETYPE_GIF:
                $image = imagecreatefromgif($imageFilePath);
                break;

            case IMAGETYPE_JPEG:
                $image = imagecreatefromjpeg($imageFilePath);
                break;

            case IMAGETYPE_PNG:
                $image = imagecreatefrompng($imageFilePath);
                break;

            default:
                return new JsonActionResult(array('error' => 'Данный тип не поддерживается'));
                break;
        }

        $targ_w = Request::$POST['thumbWidth'];
        $targ_h = Request::$POST['thumbHeight'];
        $x1 = Request::$POST['x1'];
        $x2 = Request::$POST['x2'];
        $y1 = Request::$POST['y1'];
        $y2 = Request::$POST['y2'];
        $w = $x2 - $x1;
        $h = $y2 - $y1;
        $jpeg_quality = 90;

        $dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

        if($imageType == IMAGETYPE_PNG or $imageType == IMAGETYPE_GIF){
            imagecolortransparent($dst_r, imagecolorallocate($dst_r, 0, 0, 0));
            imagealphablending($dst_r, false);
            imagesavealpha($dst_r, true);
        }

        imagecopyresampled($dst_r,
                           $image,
                           0,
                           0,
                           $x1,
                           $y1,
                           $targ_w,
                           $targ_h,
                           $w,
                           $h
                           );

        switch ($imageType)
        {
            case IMAGETYPE_GIF:
                imagegif($dst_r, $thumb);
                break;

            case IMAGETYPE_JPEG:
                imagejpeg($dst_r, $thumb, $jpeg_quality);
                break;

            case IMAGETYPE_PNG:
                imagepng($dst_r, $thumb);
                break;

            default:
                break;
        }
        return new JsonActionResult(array('thumb' => Request::$POST['thumb']));
	}
}
?>