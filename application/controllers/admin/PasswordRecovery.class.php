<?php
/**
 * Восстановление пароля
 * 
 * @version 1.0
 *
 */
class PasswordRecovery extends SuperPowerController 
{
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Action
     *
     * @return ActionResult
     */
    public function Index($args = null)
    {
       return new EmptyActionResult();
    }

    /**
     * AJAX Action
     *
     * @return ActionResult
     */
    public function SendToken()
    {
        if (isset(Request::$POST['doSend']))
        {
            if (!Validator::EmailValidator(Request::$POST['email']))
                return new JsonActionResult(array('error' => 'Некорректный e-mail',"errno" => 101));

            $userMngr = new UsersManager();
            $usr = $userMngr -> GetUser(array('email' => Request::$POST['email']));
            if (empty($usr))
                    return new JsonActionResult(array('error' => 'Пользователь с таким адресом не найден',"errno" => 102));    

            $pwd = new Password();
            $token = $pwd -> CreateNewPaswordToken($usr['userID']);

            $app = Application::GetApplication();
            $currentRoute = $app -> GetRoute();
            $controllerURL = $app -> GetRouter() -> GetURL($currentRoute -> GetControllerName());

            $mailer = new BitPlatformMailer();
            $mailer -> MsgHTML('Для восстановления пароля переидите по ссылке: <br />http://'.$_SERVER['HTTP_HOST'].$controllerURL.'checktoken/'.$token);
            $mailer -> Subject = 'Восстановление пароля';
            $mailer -> AddAddress($usr['email'], $usr['FIO']);
            if (!$mailer -> Send())   
            { 
                if ($mailer -> IsError())
                    return new JsonActionResult(array('error' => 'Письмо не отправлено '.$mailer -> ErrorInfo));
            }
            else return new JsonActionResult(array('msg'=>'email','text' => 'Письмо с ключом отправлено на ваш ящик'));
        }
        else return new JsonActionResult(array('error' => 'Wrong request'));
    }

    /**
     * Action
     *
     * @return ActionResult
     */
    public function CheckToken($args = null)
    {
        $pwd = new Password();
        if (empty($args) || !$pwd -> CheckToken($args[0]))
            die('Ключ неверен или срок его действия уже истёк.');

        if (isset(Request::$POST['doChange']))
        {
            if (!$pwd -> ChangePassword($args[0], Request::$POST['password']))
               return new JsonActionResult(array('error' => 'Пароль изменить не удалось'));
            else 
               return new JsonActionResult(array('msg'=>'ok', 'text' => 'Пароль успешно изменён'));
        }
        return new CheckTokenView();
    }
}
?>