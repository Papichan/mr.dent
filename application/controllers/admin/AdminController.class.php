<?php
/**
 * Контроллер админки
 *
 * @version 1.1
 *
 */
class AdminController extends SuperPowerController
{
    /**
     * Пользователь
     *
     * @var User
     */
    protected $user = null;

    public function __construct()
    {
        @session_start();

        /**
         * костыль для свфаплоада при создании новой сессии
         */
        if (
        	isset($_POST['PHPSESSID']) and
        	isset($_POST['userpass']) and
        	isset($_POST['username']) and
        	isset($_POST['Upload']) and
        	$_POST['Upload'] == 'Submit Query'
        )
        {
	        $_COOKIE['username'] = $_POST['username'];
	        $_COOKIE['userpass'] = $_POST['userpass'];
	        unset($_POST['userpass']);
	        unset($_POST['username']);
        }

        PluginsSubscriber::SubscribeAll(EventListenersList::$ListenersList);

        parent::__construct();

        $this -> user = new User();
        // Если сессия истекла, то попробуем восстановить
        // Попытка авторизации через куки
        if (!$this -> user -> IsAuthorised())
        {
            $this -> TryAuthorise();
        }

        ViewData::Assign('ACTIVE_MODULES', Modules::getActiveModules());
        ViewData::Assign('USER', $this -> user -> ToArray());
        ViewData::Append('SETTINGS', $this -> settingsManager -> GetSettingsAssoc('admin'));

        // Получаем объекты из AnyEditor для генерации меню
        //require_once(IncPaths::$MODULES_PATH .'AnyEditor/AnyEditor.class.php');
        $anyEditorObjects = AnyEditor::GetEditingObjects('anyeditor');
		ViewData::Assign('ANYEDITOR_OBJECTS', $anyEditorObjects);
    }

    /**
     *  Авторизация через куки или восстановление из сессии
     *
     */
    public function TryAuthorise()
    {
        $res = Authorisation::CookieAuthorisation();
        if (is_array($res))
        {
            if ($res['role'] !== 'user'){
                $this -> user -> InitUser($res);
            }
        }
    }

    /**
     * Переопределяем инициализирующий экшн
     *
     */
    public function Init()
    {
        $menumodel = new AdminModel();
        $modules = $menumodel->GetInstalledModules();
        ViewData::Assign('MODULES', $modules);
        if (!$this -> user -> IsAuthorised())
            return $this -> Authorisation();
        else
        {
            if ($this->user->GetRole() == 'user')
                return $this -> Authorisation();

            //return new EmptyActionResult();
            if (Request::IsAsync() and isset(Request::$POST['username']))
			{
                throw new Exception('{"msg":"ok","text":"welcome"}');
			}
			else {
                return new EmptyActionResult();
            }
        }
    }

    /**
     * Action
     * Форма авторизации
     *
     * @return ActionResult
     */
    public function Authorisation()
    {
        $res = Authorisation::PostAuthorisation();
        if (is_array($res)){
            if ($res['role'] !== 'user'){
                $this -> user -> InitUser($res);
                throw new Exception('{"msg":"ok","text":"welcome"}');
            }else throw new Exception('{"error":"ошибка","errno":8}');
        }elseif(is_numeric($res) && ($res != 1)){
            throw new Exception('{"error":"ошибка","errno":'.$res.'}');

        }

  /*      $res = Authorisation::PostAuthorisation();
        if (is_array($res)){
            if ($res['role'] !== 'user'){
                $this -> user -> InitUser($res);
                return new JsonActionResult(array("msg" => "ok", "text" => "welcome"));
			}else ViewData::Assign('ERROR', "Ошибка при авторизации: 8"); //throw new Exception('{"error":"ошибка","errno":8}');
        }elseif(is_numeric($res) && ($res != 1)){
            ViewData::Assign('ERROR', "Ошибка при авторизации: ".$res);
            //throw new Exception('{"error":"ошибка","errno":'.$res.'}');
        }
*/
        $this -> view = new AuthorisationView();
        $this -> view -> StopExecution();
        ViewData::Assign('PANEL_ADMIN', '1');
        return $this -> view;
    }

    /**
     * Успешное выполнение действия.
     *
     * @var array $params Дополнительные параметры, передаваемые в JSON (ассинхронный запрос)
     * @var array $params Массив вида ("msg" => 'текст', "link" => 'ссылка')
     * @return JsonActionResult
     */
    public function Success($msg, $link = null, $params = null)
    {
        // Запрос ассинхронный, возвращаем ответ в формате JSON
        if (Request::IsAsync())
        {
            // Создаем массив для ответа
            $response = array(
                'status'    => 'success',
                'msg'       => $msg
            );

            // Добавляем в него доп параметры, если они есть
            if (!is_null($params) && is_array($params))
                $response += $params;
            return new JsonActionResult($response);
        }
        // Запрос не ассинхронный, действуем по старой схеме
        else
        {
            return new AdminSuccessView($msg, $link, $params);
        }
    }

    /**
     * Ошибка при выполнении действия.
     *
     * @return JsonActionResult
     */
    public function Error($msg, $link = null, $params = null)
    {
        // Запрос ассинхронный, возвращаем ответ в формате JSON
        if (Request::IsAsync())
        {
            // Создаем массив для ответа
            $response = array(
                'status'    => 'error',
                'msg'       => $msg,
                'error'     => $msg // Костыль для совместимости
            );

            // Добавляем в него доп параметры, если они есть
            if (!is_null($params) && is_array($params))
                $response += $params;
            return new JsonActionResult($response);
        }
        // Запрос не ассинхронный, действуем по старой схеме
        else
        {
            return new AdminErrorView($msg, $link, $params);
        }
    }

    /**
     * Action
     *
     * @return ActionResult
	*/
    public function Index($args = null)
    {
       return new RedirectActionResult(AppSettings::$ADMIN_ALIAS.'structure/');
    }

    /**
    * Заливает форму в шаблон
    *
    * @param string $tableName
    * @param int $rowID
    * @param array $data
    * @return FormGenerator
    */
    protected function AssignFormToTpl($tableName, $rowID = null, $data = null)
    {
        $formGenerator = new FormGenerator();
        $form = $formGenerator -> generateForm($tableName, $rowID, $data);

        for ($i = count($formGenerator -> includingJSFiles) - 1; $i >= 0; $i--)
        {
            ViewData::Append('SCRIPT_FILE', $formGenerator -> includingJSFiles[$i]);
        }

        for ($i = count($formGenerator -> includingJSLibs) - 1; $i >= 0; $i--)
        {
            ViewData::Assign('INCLUDE_'.$formGenerator -> includingJSLibs[$i], 1);
        }

        foreach ($form as $key => $value)
        {
            if (empty($value['controls'])) unset($form[$key]);
        }

        ViewData::Assign('FORM_BLOCKS', $form);
        return $formGenerator;
    }
}
?>