<?php
/**
 * Класс управляет выводом статических страниц.
 *
 * @version 1.1
 */
class FieldsregistryController extends AdminController 
{
	public function __construct()
	{
		parent::__construct();
                
                $this -> view = new FieldsregistryView();
	}

	
	public function Index($args = null)
	{
//		$this -> tpl -> tplPath = 'admin/single_column.tpl';
//		$this -> tpl -> contentTpl = 'admin/for_fields_registry.tpl';
		
		try
		{
			if (!empty($_POST['tableName']))
			{			
				$tableName = $_POST['tableName'];
				$tblStructure = new DBTableStructure($tableName);		
				if (empty($tblStructure -> PrimaryKey -> Name))		
					throw new Exception("Нет такой таблицы $_POST[$tableName] или у таблицы нет ключа");
				
                                ViewData::Assign('SHOW_FIELDS_REGISTRY_FORM', 1);
				$fieldslist = array();
				for ($i = 0; $i < count($tblStructure -> Fields); $i++)
				{
					$fieldslist[] = $tblStructure -> Fields[$i] -> Name;
				}
				ViewData::Assign('FIELDSLIST', $fieldslist);
				ViewData::Assign('TABLE_NAME', $tableName);
				
				$query = 'INSERT INTO `'.TablesNames::$FIELDS_REGISRTY_TABLE_NAME.'` (`tableName`, `fieldName`, `canChange`, `visible`, `controlType`, `description`, `isListingKey`, `isMultiLanguage`, `tip`, `controlSettings`) VALUES '.
				'<br/>';
				
				if (isset($_POST['doBuildQuery']))
				{
					// добавочные поля
					if (!empty($_POST["additionalFields"]))
						for ($i = 0; $i < count($_POST["additionalFields"]); $i++)
							$tblStructure -> Fields[] = new DBFieldStructure($_POST["additionalFields"][$i], null, null, null, null, null);
					
					$cnt = count($tblStructure -> Fields);
					for ($i = 0; $i < $cnt; $i++)
					{
						$query .= '('."'$tableName', '".$tblStructure -> Fields[$i]->Name."', ".
								(isset($_POST[$tblStructure -> Fields[$i]->Name.'_canChange']) ? 1 : 0). ", ".
								(isset($_POST[$tblStructure -> Fields[$i]->Name.'_visible']) ? 1 : 0). ", '".
								$_POST[$tblStructure -> Fields[$i]->Name.'_controlType'] . "', '".
								$_POST[$tblStructure -> Fields[$i]->Name.'_description'] . "', ".
								(isset($_POST[$tblStructure -> Fields[$i]->Name.'_isListingKey']) ? 1 : 0). ", ".
								(isset($_POST[$tblStructure -> Fields[$i]->Name.'_isMultiLanguage']) ? 1 : 0). ", '".
								(isset($_POST[$tblStructure -> Fields[$i]->Name.'_tip']) ? $_POST[$tblStructure -> Fields[$i]->Name.'_tip'] : "") . "', '".
								$_POST[$tblStructure -> Fields[$i]->Name.'_controlSettings'] . "')" . ($i + 1 < $cnt ? ', <br/>' : '');
					}
				}

				ViewData::Assign('QUERY', stripcslashes($query));
			}
			else
				ViewData::Assign('ERROR_MESSAGE', 'Введите имя таблицы');
		}
		catch (Exception $e)
		{
			ViewData::Assign('ERROR_MESSAGE', $e -> getMessage());
		}
		
//		return $this -> Parse();
                
                $this -> view -> Index();
                return $this -> view;
	}
}
?>