<?php

/**
 * Редактирование Файлов
 */
class EditorController extends AdminController
{

//	private $model;
	private $files = array(
		'robots'	 => 'robots.txt',
		'htaccess'	 => '.htaccess'
	);

	private $sectionAlias;
	private $fileID;

	public function __construct()
	{
		parent::__construct();
		$this->view				 = new EditorView();
		$this->sectionAlias	 =  $this->controllerAlias;
		ViewData::Assign('SECTION_ALIAS', $this->sectionAlias);
		ViewData::Assign("FILES", $this->files);
	}

	public function Index($args = null)
	{
		if (!empty($args))
		{
			return $this->Edit($args);
		}

		ViewData::Assign("FILE_ID", $this->fileID);
		$this->view->Index();
		return $this->view;
	}

	public function Edit($args)
	{
		if (!isset($this->files[$args[0]]))
		{
			return $this->Error("Файл не существует или запрещено его редактирование", $this->controllerAlias);
		}

		$this->fileID	 = $args[0];
		$file			 = new _File($this->files[$args[0]]);

		ViewData::Assign("FILE_ID", $this->fileID);
		if (isset(Request::$POST['fileData']))
		{
			try
			{
				if (!$file->Update(Request::$POST['fileData']))
				{
					return new JsonActionResult(array(
						'msg'		 => 'Ошибка',
						'text'		 => 'Невозвожно прочитать файл. Проверьте правильность содержимого.',
						'refresh'	 => true
					));
				}
				return new JsonActionResult(array(
					'msg'		 => 'Успех',
					'text'		 => 'Данные успешно сохранены!',
					'refresh'	 => true
				));
			}
			catch (Exception $ex)
			{
				return new JsonActionResult(array(
					'msg'		 => 'Ошибка',
					'text'		 => $ex->getMessage(),
					'refresh'	 => true
				));
			}
		}

		ViewData::Assign('EFILE', $file);
		$action = ucfirst($args[0]);
		$this->view->$action();

		return $this->view;
	}

}

class _File
{

	private $fileName;
	private $content;
	private $filePath;

	public function __construct($file)
	{
		$this->fileName	 = $file;
		$this->filePath	 = IncPaths::$ROOT_PATH . $file;
		$this->content	 = $this->_get_content();
	}

	private function _get_content()
	{
		return file_get_contents($this->filePath);
	}


	public function Update($content)
	{
		if ($content == $this->content)
		{
			throw new Exception("Файл не изменился");
		}
		return file_put_contents($this->filePath, $content);
	}

	public function GetContent()
	{
		return $this->content;
	}

	public function GetFileName()
	{
		return $this->fileName;
	}

}
