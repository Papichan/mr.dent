<?php
/**
 * Контроллер настроек
 *
 * @version 1.2
 */
class SettingsController extends AdminController
{
        /**
         * Менеджер настроек
         *
         * @var DBSettingsManager
         */
        public $settingsManager = null;

        /**
        * Путь к настройкам
        *
        */
        protected $sectionAlias;

        public function __construct()
        {
            parent::__construct();

            $this -> settingsManager = CachingDBSettingsManager::getInstance();

            $this -> view = new SettingsView();

            $this -> sectionAlias = AppSettings::$ADMIN_ALIAS.AppSettings::$SETTINGS_ALIAS;
            ViewData::Assign('SECTION_ALIAS', $this -> sectionAlias);
        }

        public function Index($args = null)
	{
	    // Смотрим следующий алиас после "settings", чтобы понять, куда идти
	    if (isset($args[0])) $action = $args[0];
            else $action = NULL;

            // Получаем дерево групп
	    $groupsTree = $this -> settingsManager -> GetGroupsTree();
	    ViewData::Assign('MENU_GROUPS', $groupsTree['groups']);

	    // Если ничего не задано, показываем настройки и подгруппы корневой группы
	    if (is_null($action))
	    {
	        return $this -> View(1);
	    }
	    // Если число, то это ID группы, настройки и подгруппы которой надо показать
	    else if (is_numeric($action))
	    {
	        return $this -> View($action);
	    }
//	    else if ($action == 'group')
//	    {
//	        return $this -> Group();
//	    }
	    else if ($action == 'setting')
	    {
	        return $this -> Setting($args);
	    }
	    else
	    {
	        throw new PageNotFoundException();
	    }
	}

	/**
	 * Показывает настройки указанной группы
	 *
	 * @param int $groupID
	 */
	private function View($groupID)
	{
	    // Обрабатываем запрос на сохранение изменений в настройках
	    if (isset($_POST['doSave']))
	    {
	        try
                {
                        foreach ($_POST as $key => $value)
                        {
                                if (is_numeric($key))
                                {
                                        $this -> settingsManager -> EditSetting($key, null, null, $value);
                                }
                        }

                        DBLog::Info('Settings changed, groupID=%d', $groupID);
                        return $this -> Success('Значения настроек обновлены', $this -> sectionAlias . ($groupID == 0 ? '' : $groupID .'/'));
                }
                catch (Exception $e)
                {
                        return $this -> Error('Ошибка при обновлении значений настроек: '.$e -> getMessage(), $this -> sectionAlias . ($groupID == 0 ? '' : $groupID .'/'));
                }
	    }

	    // Получаем настройки указанной группы
	    $settings = $this -> settingsManager -> GetSettings($groupID);

	    // Получаем информацию о группе
	    $group = $this -> settingsManager -> GetGroup($groupID);

	    // Передаем в шаблон группу и настройки
	    ViewData::Assign('GROUP', $group);
	    ViewData::Assign('GROUP_ID', $groupID);
		ViewData::Assign('EDIT_SETTINGS', $settings);

            $this -> view -> View();
            return $this -> view;
	}

	/**
	 * Группа методов для управления настройками
	 *
	 */
	private function Setting($args = null)
	{
            if (isset($args[1])) $action = $args[1];
            else $action = NULL;

	    if (is_null($action))
                throw new PageNotFoundException('');

            switch ($action)
            {
                // Добавление новой настройки
                case 'add':
                    return $this -> AddSetting($args);
                    break;

//                // Редактирование настройки (всех параметров)
//                case 'edit':
//                    break;
            }
	}

	private function AddSetting($args = null)
	{
        if (isset($args[2]) && is_numeric($args[2]))
            $groupID = $args[2];
        else
            $groupID = NULL;

        if (is_null($groupID))
                throw new PageNotFoundException('');

	    if (isset($_POST['doSave']))
	    {
            $sett = $_POST[TablesNames::$SETTINGS_TABLE_NAME][0];
            try
	        {
                $cm  = new ContentManager();
                $res = $cm->FlushData(TablesNames::$SETTINGS_TABLE_NAME, $_POST);
                if ($res)
                {
                    if (isset($_POST['async']))
                        die('{"id":"' . $res . '"}');
                    else
                        return $this->Success('Настройка добавлена', $this->sectionAlias . $groupID . '/');
                }
                else
                    throw new Exception('Произошла неизвестная ошибка.');
            }
	        catch (Exception $e)
	        {
	            if (isset($_POST['async']))
                        die('{"error":"'. $e -> getMessage().'"}');

	            //$this -> SavePost();
	            return $this -> Error($e -> getMessage(), $this -> sectionAlias .'setting/add/'. (!is_null($groupID) ? $groupID .'/' : ''));
	        }
	    }

	    $this -> AssignFormToTpl(TablesNames::$SETTINGS_TABLE_NAME);

        if (!is_null($groupID))
        {
            ViewData::Assign('SELECTED_GROUP_ID', $groupID);
            ViewData::Assign('GROUP_ID', $groupID);
        }

        $this -> view -> AddSetting();
            return $this -> view;
	}
}

?>