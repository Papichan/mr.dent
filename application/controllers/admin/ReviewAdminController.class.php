<?php

/**
 * Description of ReviewAdminController
 *
 * @author grom
 */
class ReviewAdminController extends AdminController
{

    /**
     * @var ReviewModel
     */
    //private $model;

    public function __construct()
    {
        parent::__construct();
        $this->view  = new ReviewsAdminView();
        $this->model = new ReviewModel();
    }

    public function Index($args = null)
    {
        if(!empty($args))
        {
            switch($args[0])
            {
                case 'edit': return $this->Edit($args[1]);
                case 'add': return $this->Add();
                case 'unapproved': return $this->Unapproved();
            }
        }
        if(isset(Request::$GET['page']) && is_numeric(Request::$GET['page']))
        {
            $pageNumber = Request::$GET['page'];
        }
        else
        {
            $pageNumber = 1;
        }

        $onPage = 30;

        $reviews = $this->model->GetReviews($onPage, $onPage * ($pageNumber - 1));

        $total    = $this->model->GetFoundRowsCount();
        $totPages = ceil($total / $onPage);
        ViewData::Assign('paginator', array('pageCount' => $totPages, 'pageCurrent' => $pageNumber, 'itemsOnPage' => $onPage));
        ViewData::Assign('REVIEWS', $reviews);
        $this->view->ShowList();
        return $this->view;
    }

    public function Unapproved($args = null)
    {
        ViewData::Assign("CURRENT_SUBSECTION_ALIAS", "unapproved");
        if(Request::IsAsync())
        {
            try
            {
                if(empty(Request::$POST['reviewID'])) throw new Exception('Ошибка: не задан ID отзыва');

                $reviewID = Request::$POST['reviewID'];

                if(empty(Request::$POST['action'])) throw new Exception('Ошибка: не задано действие');

                switch(Request::$POST['action'])
                {
                    case 'approve':
                        $this->model->SetApproved(array('reviewID' => $reviewID, 'isApproved' => true));
                        return new JsonActionResult(array(
                            'msg'      => 'ok',
                            'text'     => 'Отзыв одобрен',
                            'reviewID' => $reviewID
                        ));
                        break;

                    case 'delete':
                        $this->model->DeleteReview(array('reviewID' => $reviewID));
                        return new JsonActionResult(array(
                            'msg'      => 'ok',
                            'text'     => 'Отзыв удален',
                            'reviewID' => $reviewID
                        ));
                        break;

                    default:
                        throw new Exception('Ошибка: неизвестный параметр действия (' . Request::$POST['action'] . ')');
                        break;
                }
            }
            catch(Exception $e)
            {
                return new JsonActionResult(array(
                    'status' => 'error',
                    'msg'    => $e->getMessage(),
                    'error'  => $e->getMessage()
                ));
            }
        }

        if(isset(Request::$GET['page']) && is_numeric(Request::$GET['page']))
        {
            $pageNumber = Request::$GET['page'];
        }
        else
        {
            $pageNumber = 1;
        }

        $onPage = 30;

        $reviews = $this->model->GetUnapproved($onPage, $onPage * ($pageNumber - 1));

        $entireReviewsNumber = $this->model->GetFoundRowsCount();

        ViewData::Assign('REVIEWS', $reviews);
        $this->view->Unapproved($onPage, $entireReviewsNumber, $pageNumber, $this->controllerAlias . 'unapproved/?page=');
        return $this->view;
    }

    public function Edit($args = null)
    {
        if(empty($args[0])) return new RedirectActionResult($this->controllerAlias);
        //return $this -> Error('Не задан идентификатор отзыва', $this -> controllerAlias);

        $reviewID = (int) $args;
        if(isset(Request::$POST['doSave']))
        {
            try
            {
                $review = Request::$POST;
                $tm     = new ContentManager();
                $tm->FlushData(ReviewModel::REVIEWS_TABLE_NAME, $review);
////				trace($review);
//				$this -> model -> SaveReview($review);
//
//				$this -> model -> SetApproved($review);

                return $this->Success('Успешно', $this->controllerAlias . 'edit/' . $reviewID . '/');
            }
            catch(Exception $e)
            {
                return $this->Error($e->getMessage(), $this->controllerAlias);
            }
        }


        $this->view->AssignFormToTpl(ReviewModel::REVIEWS_TABLE_NAME, $reviewID);

        $this->view->Edit();
        return $this->view;
    }

    public function Delete($args = null)
    {
        if(empty($args[0])) return new RedirectActionResult($this->controllerAlias);
        $reviewID = (int) $args[0];
        try
        {
            $review = array('reviewID' => $reviewID);
            $this->model->DeleteReview($review);

            return $this->Success('Запись удалена успешно', $this->controllerAlias);
        }
        catch(Exception $e)
        {
            return $this->Error($e->getMessage(), $this->controllerAlias);
        }
    }

    public function Add($args = null)
    {
        ViewData::Assign("CURRENT_SUBSECTION_ALIAS", "new");
        if(isset(Request::$POST['doSave']))
        {
            try
            {
                $cm = new ContentManager();
                $cm->FlushData(ReviewModel::REVIEWS_TABLE_NAME, Request::$POST);
                return $this->Success('Запись успешно создана', $this->controllerAlias);
            }
            catch(Exception $e)
            {
                return $this->Error($e->getMessage(), $this->controllerAlias);
            }
        }
        $this->view->AssignFormToTpl(ReviewModel::REVIEWS_TABLE_NAME);

        $this->view->Edit();
        return $this->view;
    }

}
