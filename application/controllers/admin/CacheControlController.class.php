<?php
/**
 * Description of CacheControl
 *
 * @author ghost
 */
class CacheControlController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
        $this -> view = new CacheControllView();
    }

    public function Index($args = null)
    {
        $menu = '
            <h1>Очистка кэша</h1>
            <ul>
                <li><a href="/admin/cache/clearglobal/">Очистить кэш платформы</a></li>
                <li><a href="/admin/cache/clearrouting/">Очистить кэш роутинга</a></li>
                <li>
                    <a href="/admin/cache/cleartemplate/">Очистить кэш темплейтов</a>
                    <ul>
                        <li><a href="/admin/cache/clearadmin/">Темплейты админки</a></li>
                        <li><a href="/admin/cache/clearfront/">Темплейты фронта</a></li>
                    </ul>
                </li>
                <li><a href="/admin/cache/clearimages/">Очистить кэш картинок</a></li>
            </ul>';
        ViewData::Assign("CONTENT", $menu);
        return $this->view;
    }

    public function ClearGlobal()
    {
        ObjectsCache::KillCacheByPattern("settings*");
        ObjectsCache::KillCacheByPattern("Autoload*");
        ObjectsCache::KillCacheByPattern("Routing*");
        ObjectsCache::KillCacheByPattern("templates_c/*.tpl.php");
        return $this->Success("Глобальный кэш платформы успешно очищен!",'/admin/cache/');
    }
    public function ClearRouting()
    {
        $this->_routing();
        return $this->Success("Кэш роутинга успешно очищен!",'/admin/cache/');
    }
    public function ClearFront()
    {
        $this->_front();
        return $this->Success("Кэш теплейтов фронта успешно очищен!",'/admin/cache/');
    }
    public function ClearAdmin()
    {
        $this->_admin();
        return $this->Success("Кэш темплейтов админки успешно очищен!",'/admin/cache/');
    }

    public function ClearTemplate()
    {
        $this->_admin();
        $this->_front();
        return $this->Success("Кэш темплейтов успешно очищен!",'/admin/cache/');
    }

    public function ClearImages()
    {
        try
        {
            FileManager::DeleteAllInFolder(AppSettings::$IMAGES_CACHE_PATH,true,true);
        }
        catch(Exception $ex)
        {
            return $this->Error("Что-то пошло не так...",'/admin/cache/');
        }
        return $this->Success("Кэш картинок успешно очищен!",'/admin/cache/');
    }

    private function _routing()
    {
        try
        {
            ObjectsCache::KillCacheByPattern("Routing*");
        }
        catch(Exception $ex)
        {
            return $this->Error("Что-то пошло не так...",'/admin/cache/');
        }
    }

    private function _front()
    {
        ObjectsCache::KillCacheByPattern("templates_c/front*.tpl.php");
    }

    private function _admin()
    {
        ObjectsCache::KillCacheByPattern("templates_c/admin*.tpl.php");
    }

}
