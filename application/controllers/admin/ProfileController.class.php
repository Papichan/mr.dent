<?php
/**
 * Класс, управляющий авторизованным пользователем
 *
 *  @version 1.5
 */
class ProfileController extends AdminController
{
    /**
     * Путь к профилю
     *
     */
    protected $sectionAlias;

    /**
     * Менеджер пользователей
     *
     * @var CurrentProjectUsersManager
     */
    protected $usersManager = null;

    private $userInfo = null;

    public function __construct()
    {
        parent::__construct();

        //берем данные зарегистрированного пользователя
        $this -> userInfo = $this -> user -> ToArray();

        //инициализируем менеджер пользователей
        $this -> usersManager = CurrentProjectUsersManager::getInstance();

        $this -> view = new ProfileView();
        $this -> sectionAlias = AppSettings::$ADMIN_ALIAS.AppSettings::$PROFILE_ALIAS;
        ViewData::Assign('SECTION_ALIAS', $this -> sectionAlias);
    }

    public function Index($args = null)
    {
        return $this -> View();
    }

    public function PassChange($args = null)
    {
        if (isset($_POST['doSave']))
        {
            if (empty($_POST['newPass']) || empty($_POST['newPassConf']))
            {
                $_SESSION['passError'] = 'Необходимо указать новый пароль в обоих полях';
                return $this -> Error('Необходимо указать новый пароль в обоих полях', $this -> sectionAlias .'passchange/');
            }

            try
            {

                $this -> usersManager -> ChangePassword($_POST['newPass'], $_POST['newPassConf'], $_POST['oldPass'], $this -> userInfo['userID']);

                DBLog::Info('Password changed');
                return $this -> Success('Пароль сменен', $this -> sectionAlias);
            }
            catch (Exception $e)
            {
                $_SESSION['passError'] = $e -> getMessage();

                DBLog::Error('Error changing password: ' . $e -> getMessage());
                return $this -> Error('Ошибка смены пароля: ' . $e -> getMessage(), $this -> sectionAlias .'passchange/');
            }
        }

        if (isset($_SESSION['passError']))
        {
            ViewData::Assign('ERROR', $_SESSION['passError']);
            unset($_SESSION['passError']);
        }

        $this -> userInfo['roleInfo'] = $this -> usersManager -> Role($this -> userInfo['role']);

        ViewData::Assign('USER', $this -> userInfo);
        $this -> view -> PassChange();
        return $this -> view;
    }

    public function View($args = null)
    {
        $userInfo = $this -> usersManager -> GetUser(array('userID' => $this -> userInfo['userID']));
        $userInfo['registrationDate'] = date('d.m.Y H:i:s', strtotime($userInfo['registrationDate']));
        $userInfo['roleInfo'] = $this -> usersManager -> Role($this -> userInfo['role']);
        $userInfo['info'] = $this -> usersManager -> GetInfo($this -> userInfo['userID']);

        ViewData::Assign('USER', $userInfo);
        $this -> view -> View();
        return $this -> view;
    }

    public function Edit($args = null)
    {
        //проверим, создана ли уже для указанного пользователя записать в таблице информации
        $user = $this->user->ToArray();
        $usersInfoTableManager = new DBTableManager(TablesNames::$USERS_INFO_TABLE_NAME);
        $res = $usersInfoTableManager -> Count(array('userID' => $user['userID']));
        //..нет - создаем.
        if ($res == 0){
            $ar = array();
            $ar[1] = $user['userID'];

            $userEdit = new UsersController();
            return $userEdit->AddInfo($ar, $this -> sectionAlias);
        }

        if (isset($_POST['doSave']))
        {
            try
            {
                $cm = new ContentManager();
                $res = $cm -> FlushData(TablesNames::$USERS_INFO_TABLE_NAME, $_POST);
                if ($res)
                {
                    if (isset($_POST['async']))
                        die('{"id":"'.$res.'"}');
                    else
                        return $this -> Success('Профиль отредактирован', $this -> sectionAlias);
                }
                else
                    throw new Exception('Произошла неизвестная ошибка.');

                    DBLog::Info('Profile edit success');
            }
            catch (Exception $e)
            {
                DBLog::Error('Error editing profile');

                if (isset($_POST['async']))
                die('{"error":"'. $e -> getMessage().'"}');

                $this -> SavePost();
                return $this -> Error($e -> getMessage(), $this -> sectionAlias .'edit/');
            }
        }

        $user = $this -> user -> ToArray();
        $this -> AssignFormToTpl(TablesNames::$USERS_INFO_TABLE_NAME, $user['userID']);

        $this -> view -> Edit();
        return $this -> view;
    }

    public function Logout($args = null)
    {
        DBLog::Info('User logout');
        $this -> userInfo = null;
        unset($_SESSION['userInfo'], $_SESSION['userID']);

        //чистим куки
        setcookie('username', '', 1220000000, '/');
        setcookie('userpass', '', 1220000000, '/');

        header("Location: ". AppSettings::$ROOT_ALIAS);
    }
}
?>