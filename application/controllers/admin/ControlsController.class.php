<?php

class ControlsController extends AdminController 
{
    /**
     * Создаем объект структуры, задаем путь к шаблонами, 
     * алиас секции и вызываем родительский конструктор
     *
     */
    public function __construct()
    {
        if (!Request::IsAsync())
            $this -> Error('Ошибка запроса', AppSettings::$ADMIN_ALIAS);
        
        // Вызываем конструктор родительского класса
        parent::__construct();
    }
    
    public function TagsInput($args = null)
    {   
        if (!isset(Request::$POST['q'])) 
            $this -> Error('Не заданы необходимые параметры (буквы)');
        
        if (!isset(Request::$POST['objectType']))
            $this -> Error('Не заданы необходимые параметры (тип объекта)');
        
        $objectType = Request::$POST['objectType'];
        $letters = UTF8::strtolower(Request::$POST['q']);
        
//        $query = '
//            SELECT  t.*
//            FROM    `'. TablesNames::$TAGS_TABLE_NAME .'` t
//            JOIN    `'. TablesNames::$TAGS_LINKS_TABLE_NAME .'` tl
//                ON  t.`tagID` = tl.`tagID`
//            WHERE   tl.`objectType` = '. DB::Quote($objectType) .'
//                AND LOWER(t.`tag`) LIKE '. DB::Quote('%'. $letters .'%') .'
//            ORDER BY t.`tag`
//        ';
        
        $query = '
            SELECT  t.*
            FROM    `'. TablesNames::$TAGS_TABLE_NAME .'` t
            WHERE   LOWER(t.`tag`) LIKE '. DB::Quote('%'. $letters .'%') .'
            ORDER BY t.`tag`
        ';
        
        $qRes = DB::Query($query);
        
		$res = '';
        while ($row = DB::FetchAssoc($qRes))
        {
            $res .= $row['tag'].'|'.$row['tagID']."\n";
        }
        
        return new ContentActionResult($res);
    }
	
	public function VideosLinker($args = null)
	{
		$limit = 50;
		if (!empty(Request::$POST['videoID']) && is_numeric(Request::$POST['videoID']))
			$videoID = Request::$POST['videoID'];
		
		if (isset(Request::$POST['currentOffset']) && is_numeric(Request::$POST['currentOffset']))
			$currentOffset = Request::$POST['currentOffset'];
		else 
			$currentOffset = 0;
		
		if (isset(Request::$POST['offsetCorrection']) && is_numeric(Request::$POST['offsetCorrection']))
			$offsetCorrection = Request::$POST['offsetCorrection'];
		else 
			$offsetCorrection = 0;
		
		$currentOffset += $limit;

		$query = 'SELECT v.*, i.srcSmall FROM Videos v 
					LEFT JOIN '.TablesNames::$IMAGES_TABLE_NAME. ' i
					ON (v.picture = i.imageID) ';
				if (!empty($videoID))
				{
					$query .= 'WHERE v.videoID NOT IN (SELECT videoID1 FROM RelatedVideos WHERE videoID2 = '.$videoID.')
								 AND v.videoID NOT IN (SELECT videoID2 FROM RelatedVideos WHERE videoID1 = '.$videoID.')
								 AND v.videoID != '. $videoID; 
				}
				$query .= ' ORDER BY v.videoID DESC LIMIT '.$limit.' OFFSET '.($currentOffset - $offsetCorrection);

		$qres = DB::QueryToArray($query);
		$res = array();
		$res['videos'] = $qres;
		$res['newOffset'] = $currentOffset;
		return new JsonActionResult($res);
	}
	
	public function GetDependentObjectsList($args = null)
	{
		if (!isset(Request::$POST['dependsOnValue']) || !isset(Request::$POST['fieldsRegistryEntryID'])) 
			return new JsonActionResult(array('error' => 'Не заданы необходимые параметры'));
		if (!is_numeric(Request::$POST['fieldsRegistryEntryID']))
			return new JsonActionResult(array('error' => 'Неверные параметры'));

		// Узнаем что за поле
		$fieldsRegestryTableMngr = new DBTableManager(TablesNames::$FIELDS_REGISRTY_TABLE_NAME);
		$entries = $fieldsRegestryTableMngr -> Select(array($fieldsRegestryTableMngr -> TableStructure -> PrimaryKey -> Name => $_REQUEST['fieldsRegistryEntryID']));
		if (count($entries) == 0)
			return new JsonActionResult(array('error' => 'Field does not registered'));
		$fieldParams = $entries[0];

		// Посмотрим заданы ли JSON настройки
		if (isset($fieldParams['controlSettings']) && $fieldParams['controlSettings'] != '')
		{
			$Jresult = JSON::Decode($fieldParams['controlSettings'], true);
			if (isset($Jresult['referentTable']))
				$referentTableName = $Jresult['referentTable'];
			else 
				return new JsonActionResult(array('error' => 'Referent Table  is undefined. It should be defined in FieldsRegistry'));
			
			if (isset($Jresult['listingKey']))
				$listingKey = $Jresult['listingKey'];
			else 
				return new JsonActionResult(array('error' => 'listingKey is undefined. It should be defined in FieldsRegistry'));
			
			if (isset($Jresult['dependentField']))
				$dependentField = $Jresult['dependentField'];
			else 
				return new JsonActionResult(array('error' => 'Dependent field is not set'));
				
			if (isset($Jresult['sortOrder']))
				$sortOrder = $Jresult['sortOrder'];
		}
		else 
		{
			return new JsonActionResult(array('error' => 'Settings for are undefined. They should be defined in FieldsRegistry. "referentTable" setting needed.'));
		}

		$referentTablStructure = new DBTableStructure($referentTableName);
		$esc = '`';
		if (DB::$DBMS == 'PostgreSQL') $esc = '"';

		$returnEmptyResult = false;
		if (is_array(Request::$POST['dependsOnValue']))
		{
			$cnt = sizeof(Request::$POST['dependsOnValue']);
			if ($cnt == 0)
				$returnEmptyResult = true;
			$where = '';
			for ($i = 0; $i < $cnt; $i++)
			{
				if ($i > 0) $where .= ' OR ';
				$where .= "{$esc}{$dependentField}{$esc} = '".Request::$POST['dependsOnValue'][$i]."'";
			}
		}
		else 
		{
			if (strlen(trim(Request::$POST['dependsOnValue'])) == 0)
				$returnEmptyResult = true;
			$where = "{$esc}{$dependentField}{$esc} = '".Request::$POST['dependsOnValue']."'";
		}
		if (!$returnEmptyResult)
		{
			$query = "SELECT {$esc}{$referentTablStructure->PrimaryKey->Name}{$esc} as {$esc}value{$esc},
							 {$esc}{$listingKey}{$esc} as {$esc}text{$esc}
							 FROM {$esc}$referentTableName{$esc} 
							 WHERE $where ";
			if (!empty($sortOrder)) $query .= ' ORDER BY '. $sortOrder;
			
			$qRes = DB::Query($query);
		}
		
		$result = array();
		$result['controlName'] = $fieldParams['fieldName'];
		$result['options'] = array();

		while ($row = DB::FetchAssoc($qRes))
		{
			$result['options'][] = array('text' => $row['text'], 'value' => $row['value']);
		}
		return new JsonActionResult($result);
	}
}

?>