<?php
/**
 * Контроллер для отображения Всех членов персонала
 *
 * @version 1.0
 *
 */
class DoctorsController extends FrontController
{
    public $rootAlias = 'doctors';
    public $childrenAlias = 'card-doctor';

    public function __construct()
    {
        parent::__construct();

        $this->page = new Page($this->rootAlias);

        // Создаем модель
        $this -> model = new DoctorsModel();

        // Создаем представление
        $this -> view = new DoctorsView('doctors');
    }

    /**
     * Поиск совпадений среди запрошенногоID доктора и тех что есть в БД
     * @param array $personals, int $doctorID
     * @return bool
     */
    public function getShowPage($personals, $doctorID)
    {
        /* Найти совпадение по ID среди уже существующих докторов, Если есть совпадение то продолжить
        Если совпадений нету то вернуть false */
        if (!empty($personals))
        {
            foreach ($personals as $pers)
            {
                if ($doctorID == $pers['personalID'])
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Извлекает Указанное количество новостей из Указанной секции для Smarty
     * @param  int $countNews
     * @param  string $sectionName
     * @return array $news
     */
    public function getNews($sectionName, $countNews)
    {
        /* Получить список из указанногоколичества новостей */
        $newsObj = new NewsLine();
        $section = $newsObj->GetSections();
        if(isset($section[$sectionName])) {
            $getNews = new NewsLineModelFront($section[$sectionName]['alias']);
            $news = $getNews->GetNews($section[$sectionName]['sectionID'], 0, $countNews);
        }
        return $news;
    }


    /**
     * Получить контент страницы, добвить все пути к картинкам,
     * и установить его в Smarty. Также получить связанные блоки для этой страницы
     * @return null
     */
    public function appendContent()
    {
        $content = $this->page->Content();
        $content['topImage']    = $this->model->getImg($content['imageTop']);
        $content['bottomImage'] = $this->model->getImg($content['imageBottom']);
        $content['slider'] = $this->model->getSlider($content['doctorID']);

        $bestTeam = $this->model->getAdvantage($this->page->pageAttrs['pageID']);
        ViewData::Assign('BEST_TEAM', $bestTeam);
        ViewData::Assign('PAGE_CONTENT', $content);
    }


    /** Показать всех докторов
     * @param null $args
     * @return unknown
     * @throws PageNotFoundException
     */
    public function Index($args = null)
    {
        /* Если количество аргументов больше 2 то даю исключение */
        $count = count($args);
        if (($count > 2) || ($count == 1)) {
            throw new PageNotFoundException('Error');
        }

        /* Данные из БД о всех сотрудниках клиники */
        $personals = $this->model->getPersonal();
        /* Установить в Smarty контент от типа страницы */
        $this->appendContent();

        if ($count == 2)
        {
            if ($args[0] == $this->childrenAlias && is_numeric($args[1]))
            {
                $doctorID = $args[1];
                $showPage = $this->getShowPage($personals, $doctorID);
                if($showPage)
                {
                    return $this->OneDoctor($doctorID);
                }else{
                    throw new PageNotFoundException('Error');
                }
            }else{
                throw new PageNotFoundException('Error');
            }
        }

        $news = $this->getNews('novosti', 10);

        $blueBlocks = $this->model->GetBlueBlocks($this -> page->pageAttrs['pageID']);

        // Установка данных для Smarty
        ViewData::Assign('BLUE_BLOCKS', $blueBlocks);
        ViewData::Assign('PERSONALS',$personals);
        ViewData::Assign('NEWS', $news);
        
        // Возвращаем вид для отображения всех докторов
        return $this->view->Index();
    }


    /** Показать отделного доктора
     * @param null $args
     * @return unknown
     * @throws PageNotFoundException
     */
    public function OneDoctor($doctorID = null)
    {
        // Получить информацию о докторе
        if(is_numeric($doctorID)){
            $doctor = $this->model->getDoctor($doctorID);
        }else{
            throw new PageNotFoundException('Error');
        }

        // Установка данных о докторе для Smarty
        ViewData::Assign('DOCTOR',$doctor);

        $this->page->pageUrl = "http://" . Request::GetHost() . "/card-doctor/" . $doctorID . "/";

        // Возвращаем вид для отображения одного доктора
        return $this->view->OneDoctor();
    }

}
?>