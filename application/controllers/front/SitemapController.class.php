<?php
/**
 * Контроллер карты сайта (взято с 1с-netnord)
 *
 * @version 1.0
 */
class SitemapController extends FrontController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Index($args = NULL)
    {
        // Получаем дерево для карты сайта
        $this -> model = new SitemapModel();
        

        // Создаем представление
        if (isset($args[0]) && $args[0] === 'xml') {
            $tree = $this->model->GetPagesTree();
            $this->view = new FrontView('sitemap/XML.tpl');
            $this->view->contentTypeHeader = 'Content-Type: text/xml; charset=utf-8';
        }elseif(isset($args[0]) && $args[0] === 'yml'){
            //получаем список категорий
            $categories = $this->model->LinearizeTree($this->model->GetCategoriesList());
            $ar = array();
            foreach ($categories as &$item){
                $item['name'] = htmlspecialchars($item['name']);
                $ar[] = $item['pageID'];
            }
            
            //получаем список продукции
            $str_in = '('.implode(',', $ar).')';
            $tree = $this->model->GetProductList($str_in);
            foreach ($tree as &$item){
                $item['data']['name'] = htmlspecialchars($item['data']['name']);
                $item['data']['descriptionShort'] = htmlspecialchars($item['data']['descriptionShort']);
            }
            
            ViewData::Assign("CATS", $categories);
            ViewData::Assign('HOST_NAME', $_SERVER['SERVER_NAME']);
            $this->view = new FrontView('sitemap/YML.tpl');
            $this->view->contentTypeHeader = 'Content-Type: text/xml; charset=utf-8';
        }else{
            $tree = $this->model->GetPagesTree();
            $this->page = new Page(array('title' => 'Карта сайта', 'caption' => 'Карта сайта'), false);
//            ViewData::Assign('caption', 'Caption');
            $this->view = new SitemapView();
        }

        // Передаем переменные представлению
        ViewData::Assign('PAGES_TREE', $tree);
        return $this->view;
    }
}