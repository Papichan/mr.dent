<?php
/**
 * Контроллер для отображения страницы Контакты
 *
 * @version 1.0
 *
 */
class ContactsController extends FrontController
{
    public function __construct()
    {
        parent::__construct();

        // Создаем модель
        $this -> model = new ContactsModel();

        // Создаем представление
        $this -> view = new ContactsView('contacts');
    }

    /** Страница Контакты
     * @param null $args
     * @return unknown
     * @throws PageNotFoundException
     */
    public function Index($args = null)
    {
        
        // Получаем страницу из базы
        $this -> page = new Page(Request::GetAliases());
        //$content = $this->page->Content();

        $content = $this->page->Content();

        $blueBlocks = $this->model->GetBlueBlocks($this -> page->pageAttrs['pageID']);

        $content['topImage'] = $this->model->getImg($content['imageTop']);
        $content['bottomImage'] = $this->model->getImg($content['imageBottom']);
        $content['path'] = $this->model->getFile($content['fileID']);
        $content['files'] = $this->model->getFiles($content['fileID']);


        ViewData::Assign('BLUE_BLOCKS',$blueBlocks);
        ViewData::Assign('PAGE_CONTENT',$content);

        return $this->view->Index();
    }

}
?>