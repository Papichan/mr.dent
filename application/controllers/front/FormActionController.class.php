<?php
/**
 * Контроллер для обработки форм отправки сообщений
 *
 * @version 1.0
 *
 */
class FormActionController
{

    private $feedBack = null;

    public function __construct()
    {
        $this->feedBack = new Feedback();
    }

    /**Главный метод, разводящий между методами отправки сообщений
     * @param null $args
     * @return unknown
     * @throws PageNotFoundException
     */
    public function Index($args = null)
    {
        if ( isset(Request::$POST['messageType']) )
        {
            switch (Request::$POST['messageType']) {
                case 'appointment':
                    return $this->MessageAppointment();
                    break;
                case 'consultation':
                    return $this->MessageConsultation();
                    break;
                case 'question':
                    return $this->MessageQuestion();
                    break;
                case 'freeConsultation':
                    return $this->MessageFreeConsultation();
                    break;
                default:
                    return $this->MessageError();
                    break;
            }
        }
    }


    public function MessageError()
    {

    }


    public function MessageQuestion()
    {
        try
        {
             if (!Captcha::CheckCaptcha(Request::$POST['captcha'])) {
                 throw new Exception('Неверно введены цифры с картинки', 555);
             }

             $msg = array();

             $msg['text'] = '';

             if (isset(Request::$POST['messageType'])) {
                 $msg['type'] = strip_tags(Request::$POST['messageType']);
             }
             if (isset(Request::$POST['doctor'])) {
                 $msg['doctor'] = strip_tags(Request::$POST['doctor']);
             }
             if (isset(Request::$POST['message'])) {
                 $msg['text'] = strip_tags(Request::$POST['message']);
             }
             if (isset(Request::$POST['fio'])) {
                 $msg['userName'] = strip_tags(Request::$POST['fio']);
             }
             if (isset(Request::$POST['phone'])) {
                 $msg['userPhone'] = strip_tags(Request::$POST['phone']);
             }

             $msg['title'] = 'Сообщение с формы - Задайте вопрос врачу';

             $this->feedBack->AddMessage($msg); // Сохранение сообщения от пользвателя

            } catch(Exception $e){
            return new JsonActionResult(array('error' => $e->getMessage(), 'errno' => $e->getCode()));
        }
        return new JsonActionResult(array('status' => 'ok', 'msg' => 'Ваше сообщение отправлено'));
    }



    public function MessageAppointment()
    {
        $msg = array();

        $msg['text'] = '';

        if (isset(Request::$POST['messageType']))
            $msg['type'] = strip_tags(Request::$POST['messageType']);

        if (isset(Request::$POST['message']))
            $msg['text'] = strip_tags(Request::$POST['message']);

        if (isset(Request::$POST['fio']))
            $msg['userName'] = strip_tags(Request::$POST['fio']);

        if (isset(Request::$POST['phone']))
            $msg['userPhone'] = strip_tags(Request::$POST['phone']);

        if (isset(Request::$POST['email']))
            $msg['userEmail'] = strip_tags(Request::$POST['email']);

        $msg['title'] = 'Сообщение с формы - Записаться на прием';

        $this->feedBack -> AddMessage($msg);
    }




    public function MessageConsultation()
    {
        $msg = array();

        $msg['text'] = '';

        if (isset(Request::$POST['messageType']))
            $msg['type'] = strip_tags(Request::$POST['messageType']);

        if (isset(Request::$POST['message']))
            $msg['text'] = strip_tags(Request::$POST['message']);

        if (isset(Request::$POST['fio']))
            $msg['userName'] = strip_tags(Request::$POST['fio']);

        if (isset(Request::$POST['phone']))
            $msg['userPhone'] = strip_tags(Request::$POST['phone']);

        if (isset(Request::$POST['email']))
            $msg['userEmail'] = strip_tags(Request::$POST['email']);

        $msg['title'] = 'Сообщение с формы - Консультация';

        $this->feedBack -> AddMessage($msg);
    }



    public function MessageFreeConsultation()
    {
        $msg = array();

        $msg['text'] = 'Бесплатная консультация по телефону';

        if (isset(Request::$POST['messageType']))
            $msg['type'] = strip_tags(Request::$POST['messageType']);

        if (isset(Request::$POST['message']))
            $msg['text'] = strip_tags(Request::$POST['message']);

        if (isset(Request::$POST['phone']))
            $msg['userPhone'] = strip_tags(Request::$POST['phone']);

        $msg['title'] = 'Сообщение с формы - Бесплатная консультация';

        $this->feedBack -> AddMessage($msg);
    }

}
?>