<?php
/**
 * Контроллер для отображения страницы Услуг
 *
 * @version 1.0
 *
 */
class ServicesController extends FrontController
{
    public function __construct()
    {
        parent::__construct();

        // Создаем модель
        $this->model = new ServicesModel();

        // Создаем представление
        $this->view = new ServicesView('services');
    }

    /**Cтраница услуг
     * @param null $args
     * @return unknown
     * @throws PageNotFoundException
     */
    public function Index($args = null)
    {
        // Получаем страницу из базы
        $this->page = new Page(Request::GetAliases());

        $content = $this->page->Content();
        $content['topImage'] = $this->model->getImg($content['imageTop']);
        $content['bottomImage'] = $this->model->getImg($content['imageBottom']);


        ViewData::Assign('PAGE_CONTENT', $content);

        return $this->view->Index();
    }
    
    
    
    
    /*
    
     
     
      
     { "linkingTable" : "lechenieBlocks",
 "blOrder" : 0, 
"elemOrder" : 85,
 "className" : "", 
"size" : 10,
"orderBy" : "", 
"textFields" : {
 "title" : { 
	"label" : "Заголовок",
 	"showLabel" : true,
 	"containerType" : "line" }, 
"text" : { "label" : "Текст",
 	"showLabel" : true, 
	"containerType" : "textarea" },
 "priority" : {
	 "label" : "Приоритет",
 	"showLabel" : true, 
	"containerType" : "line" } 
} 
}





     
    { "blOrder" : 0, 
		    "elemOrder" : 50,

			"image" : { 
				"resize" : { 
					"strong" : false,
					"width" : 1000,
					"height" : 1000 
				}, 

				"thumb" : {
					"resize" : { 
						"strong" :  true, 
						"width" : 208, 
						"height" : 112 }, 
					"postfix" : "_small" 
				} 
			}, 

            "includeThumbCropDialog" : true,

			"maxFileSize" : 5048576,

			"filesType" : "image",
 
			"maxFilesNumber" : 50,

			"linkingTable" : "slidersImages",

			"allowedExtensions" : "*.jpg; *.gif; *.png", 

			"filesTable" : "greeny_images",
 
			"filesTableKey" : "imageID",
 
			"destinationDirectory" : "uploaded/sliders/[ID]",
 
			"usePriority"	:	true,

			"priorityField"	:	"priority",	

			"uploadScript" : "/admin/upload/" ,

			"textFields" : { 		
  	"title" : { 
                    "label" : "Заголовок", 
                    "showLabel" : true, 
                    "containerType": "line" 
                },
 	"text" : { 
                    "label" : "Текст", 
                    "showLabel" : true, 
                    "containerType": "text",
	            "class":"richEditor" 
                },
        "buttonTitle" : { 
                    "label" : "Причины заболевания", 
                    "showLabel" : true, 
                    "containerType": "line" 
                },
        "link" : {
                    "label" : "Ссылка", 
                    "showLabel" : true, 
                    "containerType": "line" 
                }
            }
     }
     * */
     
    
    
    /** Страница для раздела Ortoped
     * 
     * @param type $args
     * @return type
     */
     public function Ortoped($args = null)
    {
        // trace(Request::GetAliases());
        if( count(Request::GetAliases()) < 3) 
        {
            throw new PageNotFoundException('Error');
        }
        
        $serviceTable = DBTableManager::getInstance('greeny_pageStructure'); 
        $serviceTable -> SortOrder = 'priority ASC';
        $ortoped = $serviceTable->Select(array('parentID' => 43, 'isActive' => 1));
        $bool = false;
        foreach ($ortoped as $key => $value) 
        {
            if($value['alias'] == @$args[3]){
                $page = $args[0];
                $bool = true;
            }
        }
        if($bool){
            throw new PageNotFoundException('Error');
        }
           
        // Получаем страницу из базы
        $this -> page = new Page(Request::GetAliases());
/*
        trace($this -> page->pageAttrs);
        die();*/
        /* Данные из БД */
        $statistic = $this->model->getStatistic();
        $reviews = $this->model->getReviews();
        $personal = $this->model->getPersonal();
        $price = $this->model->getPrices(5);
        $reasons = $this->model->getReasons();
        $blueBlocks = $this->model->GetBlueBlocks($this -> page->pageAttrs['parentID']);
       
        /* Контент "типа страницы" */
        $content = $this->page->Content();

        $content['imageTop'] = $this->model->getImg($content['imageTop']);
        $content['imageBottom'] = $this->model->getImg($content['imageBottom']);
        $content['lechenieBlocks'] = $this->model->getLechenieBocks($content['lechenie_zubovID']);
        $content['slider'] = $this->model->getSlider($this -> page->pageAttrs['pageID']);
        $tabs = $this->model->getTabs($this -> page->pageAttrs['pageID']);

        
        /* Делим блок "Причины возникновения кариеса" 
         * на 2 равные половинки, чтобы вывод был симметричен */
        $count = count($reasons) / 2;
        $reasonsLeft  = array_slice($reasons, 0, $count);
        $reasonsRight = array_slice($reasons, $count);

        /* Достаю данные для блока новостей */
        $newsObj = new NewsLine();
        $section = $newsObj->GetSections();
        if(isset($section['novosti']))
        {
            $getNews = new NewsLineModelFront($section['novosti']['alias']);
            $news = $getNews->GetNews($section['novosti']['sectionID'], 0, 10);
        }
       
        /* Установка данных для Smarty (контент страницы) */
        ViewData::Assign('PAGE_CONTENT', $content);
        /* Установка данных для Smarty (остальное) */
        ViewData::Assign('TABS', $tabs[0]);
         ViewData::Assign('TABS_IMAGES', $tabs[1]);
        ViewData::Assign('BLUE_BLOCKS',$blueBlocks);
        ViewData::Assign('STATISTIC',$statistic);
        ViewData::Assign('REVIEWS',$reviews);
        ViewData::Assign('PERSONAL',$personal);
        ViewData::Assign('NEWS',$news);
        ViewData::Assign('PRICES', $price);
        ViewData::Assign('REASONS_LEFT', $reasonsLeft);// Левый Блок
        ViewData::Assign('REASONS_RIGHT', $reasonsRight);// Правый Блок

        return $this->view->Ortoped();
    }


}


?>