<?php
/**
 * Контроллер для отображения главной страницы
 *
 * @version 1.0
 *
 */
class LechenieZubovController extends FrontController
{
    public function __construct()
    {
        parent::__construct();

        // Создаем модель
        $this -> model = new LechenieZubovModel();

        // Создаем представление
        $this -> view = new LechenieZubovView('lechenieZubov');
    }

    /**Главная страница сайта
     * @param null $args
     * @return unknown
     * @throws PageNotFoundException
     */
    public function Index($args = null)
    {
        // Получаем страницу из базы
        $this -> page = new Page(Request::GetAliases());

        /* Данные из БД */
        $statistic = $this->model->getStatistic();
        $reviews = $this->model->getReviews();
        $personal = $this->model->getPersonal();
        $price = $this->model->getPrices(5);
        $lechenie = $this->model->getLechenie();
        $reasons = $this->model->getReasons();
        $blueBlocks = $this->model->GetBlueBlocks($this -> page->pageAttrs['pageID']);

        /* Делим блок на 2 равные половинки, чтобы вывод был симметричен */
        $count = count($reasons) / 2;
        $reasonsLeft  = array_slice($reasons, 0, $count);
        $reasonsRight = array_slice($reasons, $count);

        /* Достаю данные для блока новостей */
        $newsObj = new NewsLine();
        $section = $newsObj->GetSections();
        if(isset($section['novosti']))
        {
            $getNews = new NewsLineModelFront($section['novosti']['alias']);
            $news = $getNews->GetNews($section['novosti']['sectionID'], 0, 10);
        }

        ViewData::Assign('BLUE_BLOCKS',$blueBlocks);
        ViewData::Assign('STATISTIC',$statistic);
        ViewData::Assign('REVIEWS',$reviews);
        ViewData::Assign('PERSONAL',$personal);
        ViewData::Assign('NEWS',$news);
        ViewData::Assign('PRICES', $price);
        ViewData::Assign('LECHENIE_ZUBOV', $lechenie);
        ViewData::Assign('REASONS_LEFT', $reasonsLeft);// Левый Блок
        ViewData::Assign('REASONS_RIGHT', $reasonsRight);// Правый Блок

        return $this->view->Index();
    }




}
?>