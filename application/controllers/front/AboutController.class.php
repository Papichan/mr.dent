<?php
/**
 * Контроллер для отображения страницы о нашей компании
 *
 * @version 1.0
 *
 */
class AboutController extends FrontController
{
    public function __construct()
    {
        parent::__construct();

        // Создаем модель
        $this -> model = new AboutModel();

        // Создаем представление
        $this -> view = new AboutView('about');
    }

    /** Страница О Нас
     * @param null $args
     * @return unknown
     * @throws PageNotFoundException
     */
    public function Index($args = null)
    {
        // Получаем страницу из базы
        $this -> page = new Page(Request::GetAliases());
//trace($this -> page->pageAttrs);
//        die();
        /* Получить контент типа страницы, и его картинок */
        $content = $this->page->Content();
        $content['topImage'] = $this->model->getImg($content['imageTop']);
        $content['bottomImage'] = $this->model->getImg($content['imageBottom']);
        $content['teamImage'] = $this->model->getImg($content['imageTeam']);

        /* Данные из БД */
        $statistic = $this->model->getStatistic();
        $advantage = $this->model->getAdvantage($this -> page->pageAttrs['pageID']);
        $certificate = $this->model->getCertificate();
        $slider = $this->model->getSlider($content['about_companyID']);
        $blueBlocks = $this->model->GetBlueBlocks($this -> page->pageAttrs['pageID']);

        ViewData::Assign('BLUE_BLOCKS', $blueBlocks);
        ViewData::Assign('PAGE_CONTENT', $content);
        ViewData::Assign('STATISTIC', $statistic);
        ViewData::Assign('ADVANTAGE', $advantage);
        ViewData::Assign('CERTIFICATE', $certificate);
        ViewData::Assign('SLIDER_ABOUT', $slider);

        return $this->view->Index();
    }




}
?>