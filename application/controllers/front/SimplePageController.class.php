<?php
/**
 * Стандартный контроллер для SimplePage
 * @version 1.0
 *
 */
class SimplePageController extends FrontController
{
    public function __construct()
    {
        parent::__construct();
    }

    public function Index($args = null)
    {
        // Получаем страницу из базы
        $this -> page = new Page(Request::GetAliases());

        // Создаем представление
        $this -> view = new SimplePageView($this -> page);
        
        return $this -> view;
    }
}

?>