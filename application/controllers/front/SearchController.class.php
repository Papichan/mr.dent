<?php

class SearchController extends FrontController  
{
    /**
     * Поисковик
     *
     * @var WWEGSearcher
     */
    public $searcher = null;
    
    public function __construct()
    {
        parent::__construct();

        // Создаем представление
        $this -> view = new SearchView();
        
        // Получаем страницу из базы
        $this -> page = new Page('/search/');
        
        $this -> searcher = new WWEGSearcher();
    }
    
    public function Material($args = null)
    {
        //header('Content-Type: text/html; charset=utf-8');
        //$query = 'Барак Обама';
        //$result = $this -> searcher -> SearchMaterials($query, 0, 20);
        //trace($result);
        //exit();
        return $this -> view;
    }

    public function Index($args = null)
    {
        // Получаем контент
        $pageContent = $this -> page -> Content();
        ViewData::Assign('CONTENT', $pageContent['pageContent']);
   
        //Request::$GET['q'] = 'обучение';
        if (isset(Request::$GET['q']))
        {
            $searchQueryOrig = Request::$GET['q'];
            //require_once(IncPaths::$LIBS_PATH .'Url.class.php');
            //$searchQuery = URL::DecodeUnicodeURL($searchQueryOrig);
            //$searchQuery = htmlentities(strip_tags($searchQuery));
            $searchQuery = $searchQueryOrig;
            ViewData::Assign('SEARCH_QUERY', $searchQuery);
            
            $pageNumber = Request::GetAliases('page');
            if (empty($pageNumber))
                $pageNumber = 1;
                
            $onPage = 15;
            
            //$searchResults = $this -> searcher -> Search($searchQuery, $onPage*($pageNumber-1), $onPage);
            //$searchResults = $this -> searcher -> SearchMaterials($searchQuery, $onPage*($pageNumber-1), $onPage, true);
            $searchResults = $this -> searcher -> Search($searchQuery, $onPage*($pageNumber-1), $onPage);
            ViewData::Assign('SEARCH_RESULTS', $searchResults);
            
            $foundItems = $this -> searcher -> FoundItems();
            ViewData::Assign('ITEMS_FOUND', $foundItems);
            ViewData::Assign('FOUND_START', $onPage*($pageNumber-1) + 1);
            ViewData::Assign('FOUND_FINISH', $onPage*($pageNumber-1) + count($searchResults));
            
            if ($foundItems == 0)
                ViewData::Assign('NO_RESULTS', 1);
            
            $totalPages = ceil($foundItems / $onPage);
            if ($totalPages > 1)
            {
                ViewData::Assign('PAGINATOR_PAGES', $totalPages);
                ViewData::Assign('PAGINATOR_CURRENT', $pageNumber);
                
                //ViewData::Assign('PAGINATOR', $this -> GetPaginatorPages($pageNumber, $totalPages, $searchQueryOrig));
            }
        }

        return $this -> view;
    }
    
    private function HighlightKeywords(&$keywords, $res)
	{
	    $replacement = '<span style="background-color: Yellow;">$1</span>';
	    $res = preg_replace($keywords, $replacement, $res);

	    // Cutting
	    $pos = UTF8::strpos($res, '<span style="background-color: Yellow');
	    $startCutPos = ($pos !== false && $pos - 200 >= 0) ? $pos - 200 : 0;
	    $endCutPos = ($pos !== false && $pos + 200 <= UTF8::strlen($res)) ? $pos + 200 : UTF8::strlen($res);

	    $firstDotPos = UTF8::strpos($res, '.', $startCutPos);
	    $startCutPos = ($firstDotPos + 1 > $pos) ? $pos : $firstDotPos + 1;

	    $lastDotPos = UTF8::strpos($res, '.', $endCutPos - 50);
	    $endCutPos = ($lastDotPos !== false) ? $lastDotPos : $endCutPos;

	    return UTF8::substr($res, $startCutPos, $endCutPos - $startCutPos);
	}
	
	private function GetPaginatorPages($currentPage, $totalPages, $searchQuery)
	{
	    //$searchQueryEncoded = Url::EncodeUnicodeURL($searchQuery);
	    $searchQueryEncoded = $searchQuery;
	    $searchQuery = str_replace(' ', '+', $searchQuery);
	    
	    $pagesArray = array();
	    $showFirst = 3;
	    $showLast = 3;
	    $showAround = 2;
	    
	    $prevSpace = false;
	    for ($i = 1; $i <= $totalPages; $i++)
	    {
	        if ($i == $currentPage)
	        {
	            $prevSpace = false;
	            $pagesArray[] = array('type' => 'current', 'page' => $i);
	        }
	        elseif (abs($i - $currentPage) <= $showAround || $i <= $showFirst || $totalPages - $i < $showLast)
	        {
	            $prevSpace = false;
	            $pagesArray[] = array('type' => 'general', 'page' => $i, 'href' => '/search/page/'. $i .'/?q='. $searchQuery);
	        }
	        else 
	        {
	            if (!$prevSpace)
	            {
	                $prevSpace = true;
	                $pagesArray[] = array('type' => 'space');
	            }
	        }
	    }
	    
	    return $pagesArray;
	}
}

?>