<?php
/**
 * Контроллер для отображения главной страницы
 *
 * @version 1.0
 *
 */
class IndexController extends FrontController 
{
    public function __construct()
    {
        parent::__construct();

        // Создаем модель
        $this -> model = new IndexModel();

        // Создаем представление
        $this -> view = new IndexView('index');
    }

    /**Главная страница сайта
     * @param null $args
     * @return unknown
     * @throws PageNotFoundException
     */
    public function Index($args = null)
    {
        /* Получаем страницу из базы,
        название передаю в ручную так как страница привязана к пустому роутингу /... */
        $this -> page = new Page('index');


        /* Данные из БД */
       // $slider = $this->model->getSlider();       // Получить данные для Слайдера
        $statistic = $this->model->getStatistic(); // Раздел Клиника в цифрах
        $reviews = $this->model->getReviews(5);    // Показать Отзывы клиентов
        $personal = $this->model->getPersonal();   // Получить данные для Персонала
        $content = $this->page->Content();         // Получить данные типа страницы

        // Получить изображение для формы отправки
        $content['imageBottom'] = $this->model->getImg($content['imageBottom']);
        $content['videoMask'] = $this->model->getImg($content['videoMask']);
        $content['serviceImage'] = $this->model->getImg($content['imageID']);
        $content['slider'] = $this->model->getSlider($this->page->pageAttrs['pageID']);
        $imagesForService = $this->model->getImgesFromMenu();
        

        // Получить данные для новостей, 10 новостей
        $newsObj = new NewsLine();
        //$news = $newsObj->GetNews(['isPublished' => 1],10, 0);
        $section = $newsObj->GetSections();
        if(isset($section['novosti']))
        {
            $getNews = new NewsLineModelFront($section['novosti']['alias']);
            $news = $getNews->GetNews($section['novosti']['sectionID'], 0, 10);
        }

        /* Установка переменных в SMARTY */
        ViewData::Assign('SERVICE_IMAGES',$imagesForService);
        ViewData::Assign('PAGE_CONTENT',$content);
        ViewData::Assign('STATISTIC',$statistic);
        ViewData::Assign('REVIEWS',$reviews);
        ViewData::Assign('PERSONAL',$personal);
        ViewData::Assign('TEN_NEWS',$news);

        return $this->view->Index();
    }



    
}
?>