<?php
/**
 * Контроллер отзывов
 *
 * @author windeko
 */
class ReviewController extends FrontController
{
    private $target_dir = "uploaded/review/";

	public function __construct()
	{
		parent::__construct();
        $this->page	 = new Page(Request::GetAliases());
		$this -> view = new ReviewView();
		$this -> model = new ReviewModel();
	}

	private function Inform($data)
	{
		$settings	 = $this->settingsManager->GetSettingsAssoc('general');
		$toEmails	 = explode(',', trim($settings['general']['mailer']['notifyEmails']));
		if (!empty($toEmails))
		{
			$tplSfx = ('Eng' == $this->lang) ? 'en' : 'ru';

			$v		 = new FrontView('reviews/new_question_' . $tplSfx . '.tpl');
			$link	 = 'http://' . Request::GetHost() . '/admin/reviews/edit/' . $data['id'] . '/';

			ViewData::Assign('title', $data['title']);
			ViewData::Assign('name', $data['name']);
			ViewData::Assign('age', $data['age']);
			ViewData::Assign('question', $data['review']);
			ViewData::Assign('link', $link);
			ViewData::Assign('settings', $settings);

			$subject = (($this->lang == 'Eng') ? 'New review from site ' : 'Новый отзыв на сайте ') . $settings['general']['siteName'];
			$body	 = $v->GetExecuteResult();

			$mailer			 = new BitPlatformMailer();
			$mailer->MsgHTML($body);
			$mailer->Subject = $subject;
			$mailer->ClearAddresses();
			foreach ($toEmails as $eml)
			{
				$mailer->AddAddress($eml);
			}
			$mailer->Send();
		}
	}

	public function Index($args = null)
	{
		if (Request::IsAsync() && isset(Request::$POST['submit']))
		{
			$data = array();
			try
			{
				$this->model->Validate(Request::$POST, $this->dic);
				$data['title']	 = UTF8::trim(strip_tags(Request::$POST['title']));
				$data['name']	 = UTF8::trim(strip_tags(Request::$POST['name']));
				$data['review']	 = UTF8::trim(strip_tags(Request::$POST['review'], "b,i"));
				$data['age']	 = Request::$POST['age'];
//				$data['notify']	 = isset(Request::$POST['notify']) ? 1 : 0;
				$data['lang']	 = $this->lang;
				$data['userIp']	 = $_SERVER['REMOTE_ADDR'];
				$data['dateAdd'] = date('Y-m-d H:i:s');
				$data['id']		 = $this->model->AddNewReview($data);
				$this->Inform($data);
				return new JsonActionResult(array("success" => $this->dic['successAddReview']));
			}
			catch (ValidationException $ex)
			{
				return new JsonActionResult(array("error" => $this->dic['errorForm'], 'fields' => $ex->getFieldName()));
			}
			catch (Exception $ex)
			{
				return new JsonActionResult(array("error" => $ex->getMessage()));
			}
		}

		$content = $this->page->Content();

		ViewData::Assign("PAGE_CONTENT", $content['extendedPageText' . $this->lang]);

		$limit	 = 6;
		$page	 = isset(Request::$GET['page']) && is_numeric(Request::$GET['page']) ? Request::$GET['page'] : 1;
		$start	 = ($page - 1) * $limit;
		$reviews = $this->model->GetApproved($limit, $start, $this->lang);
		$total	 = $this->model->GetFoundRowsCount();

		ViewData::Assign("REVIEWS", $reviews);

		$this->setPaginator($limit, $total, $page);

//		if ($total > $limit)
//		{
//			ViewData::Assign('paginator', array('pageCurrent' => $page, 'pageCount' => ceil($total / $limit), 'recordTotal' => $total));
//		}
		//$countRows = $this -> model -> GetFoundRows();
        //$this -> setPaginator($limit, $countRows['cnt'], $page);


        return $this -> view;
	}

    public function UploadFile($file){
        $randdir = rand(100000000000000,999999999999999);
        $target_dir = $this->target_dir.$randdir."/";
        $target_file = $target_dir . basename($file['name']);
        $uploadOk = 1;
        $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);

        if (file_exists($target_file)) {
            trace("Извините, файл с таким названием уже существует.");
            $uploadOk = 0;
        }

        if ($uploadOk == 0) {
            trace("Извините, файл с таким названием уже существует.");
        } else {
            if($this->CreateDir($target_dir)){
                if (move_uploaded_file($file["tmp_name"], $target_file)) {
                    //trace("The file ". basename($file["name"]). " has been uploaded.");
                    $id = $this->model->AddFileRow($target_file);
                    return $id;
                } else {
                    trace("Sorry, there was an error uploading your file.");
                }
            }else{
                trace("Error with creating the directory ".$target_dir);
            }

        }
    }

    public function CreateDir($dir){
        if(!file_exists($dir)){
            mkdir($dir,0755);
            return true;
        }else{
            return false;
        }
    }

}