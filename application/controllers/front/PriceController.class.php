<?php
/**
 * Контроллер для отображения страницы цен на услуги
 *
 * @version 1.0
 *
 */
class PriceController extends FrontController
{
    public function __construct()
    {
        parent::__construct();

        // Создаем модель
        $this -> model = new PriceModel();

        // Создаем представление
        $this -> view = new PriceView('price');
    }

    /**Страница Цен на услуги
     * @param null $args
     * @return unknown
     * @throws PageNotFoundException
     */
    public function Index($args = null)
    {
        // Получаем страницу из базы
        $this -> page = new Page(Request::GetAliases());

        $prices = $this->model->getPrices(); // Получить список всех цен на услуги

        // Ичзымаю
        $content = $this->page->Content();
        $content['topImage'] = $this->model->getImg($content['imageTop']);
        $content['bottomImage'] = $this->model->getImg($content['imageBottom']);

        ViewData::Assign('PAGE_CONTENT', $content);
        ViewData::Assign('PRICES',  $prices);

        return $this->view->Index();
    }




}
?>