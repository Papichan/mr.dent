<?php
/**
 * Базовый контроллер фронт-энда
 *
 * @version 1.3
 *
 */
class BaseFrontController extends SuperPowerController
{
    /**
	 * Информация о текущей странице (title, description, keywords...)
	 *
	 * @var Page
	 */
    protected $page;

    /**
     * Массив с типами страниц
     *
     * @var array
     */
    protected $pagesTypes = null;

    public function __construct()
    {
        parent::__construct();

        $this -> CheckMaintenance();

        // Определяем массив типов страниц
        $this -> LoadPagesTypesConstants();

        // Загрузим главное меню
        $this -> SetMainMenu();
    }

    protected function LoadPagesTypesConstants()
    {
        $pagesTypesMngr = new DBTableManager(TablesNames::$PAGES_TYPE_TABLE_NAME, null, false);
        $allPagesTypes = $pagesTypesMngr -> Select();
        $this -> pagesTypes = array();
        foreach ($allPagesTypes as $type)
        {
                $this -> pagesTypes[$type['constantName']] = $type['pageTypeID'];
        }
    }

    protected function SetMainMenu()
    {
        $mainMenuMngr	 = new MainMenuManager();
		//todo вынести в настройки вид меню
		// представление меню в виде дерева
		//$menu			 = $mainMenuMngr -> GetMainMenuTree();
		// представление меню в виде линейного массива
		$menu         = $mainMenuMngr->GetMainMenuTree();

        for ($i = count($menu) - 1; $i >= 0; $i--)
        {
            if ($menu[$i]['fullAlias'] == '/main/')
                $menu[$i]['fullAlias'] = '/';
        }
        ViewData::Assign('MAIN_MENU', $menu);

        //получаем alias родительского раздела
        $pos = strpos($_SERVER['REQUEST_URI'], '/', 1);
        $alias = substr($_SERVER['REQUEST_URI'], 1, $pos-1);
        ViewData::Assign('MENU_ACTIVE_ALIAS', $alias);
    }

    /**
     * Action
     *
     * @param unknown_type $args
     * @return unknown
     */
    public function Index($args = null)
    {
        $this -> view = new FrontView();

        return $this -> view;
    }

    public function Complete()
    {
        parent::Complete();

        if (!Request::IsAsync())
        {
        $settings = $this -> settingsManager -> GetSettingsAssoc();

            // Устанавливаем мета-информацию по умолчанию, если что-то не задано
            if (!empty($this -> page))
            {
                    if (empty($this -> page -> title))
                $this -> page -> title = $this -> page -> caption; //$settings['front']['defaultTitle'];
            else
                $this -> page -> title = htmlspecialchars_decode($this -> page -> title);

                    if (empty($this -> page -> description))
                $this -> page -> description = $settings['front']['defaultDescription'];

                    if (empty($this -> page -> keywords))
                $this -> page -> keywords = $settings['front']['defaultKeywords'];

            ViewData::Assign('PAGE', $this -> page -> ToArray());
            }
            if (isset($settings['general']['PHONE']))
            {
                $settings['general']['PHONE'] = $this->PreparePhone($settings['general']['PHONE']);
            }

            /*================ ИЗМЕНЕНИЕ ПОПКОВ (Обратная связь и Меню)  ==================*/
            /* Запуск проверки нажатия кнопки любой из форм отправки */
            $formAction = new FormActionController();
            $formAction->Index();

            // Установка Менюшек для всех страниц
            $this->getSliceServices(); // Установить Меню для Сервиса
            /*=================================*/
            
            ViewData::Append('JS_VARS', array('IMAGE_ALIAS' => AppSettings::$IMAGES_ALIAS));
            ViewData::Assign('SETTINGS', $settings);
            ViewData::Assign('CONTROLLER_ALIAS', $this -> controllerAlias);
        }
    }
    
    
    
    
    /** ИЗМЕНЕН ДОБАВЛЕН ! ПОПКОВ СЕРГЕЙ !
     * Получить Меню для севиса и передать в Smarty
     */
    public function getSliceServices()
    {
        $menu = new CustomMenuModel;
        $allMenu = $menu->GetListMenu();
      
        $servicesMenu = $menu->GetInnerMenu($allMenu[0]['menuListID']);

        // Массив для среза по частям меню для сервиса в футер 
        $slice = [2, 2, 1, 2, 3];
        $count = count($slice);
        $itterator = 0;
        $from = 0;

        /* Срезаю массив на 5 частей, для отображения в футере */
        while ($itterator <= ($count-1) )
        {
            $result[$itterator] = array_slice($servicesMenu, $from, $slice[$itterator]);
            $from += $slice[$itterator];
            $itterator++;
        }
       
        ViewData::Assign('SERVICES_MENU', $servicesMenu);
        ViewData::Assign('SLICE_SERVICES_FOOTER_MENU', $result);
    }
    
    
    
    
    
    
    
    
    

    protected function CheckMaintenance()
    {
        $settingsMgr = CachingDBSettingsManager::getInstance();

        $setting = $settingsMgr->GetSetting('admin.maintenanceEnabled');
        $enabled = $setting['value'];
        if ($enabled && Request::GetAliases(0) != 'getimage')
        {
            // Если пользователь авторизирован и он админ - пустить его во фронт
            @session_start();
            $user = new User();
            if ('admin' == $user->GetRole())
                return;

            $setting = $settingsMgr->GetSetting('admin.maintenancePageText');
            $text = $setting['value'];
            $text = '<!DOCTYPE html> <html><head></head><body>' . $text . '</body></html>';


            $protocol = 'HTTP/1.0';
            if ( 'HTTP/1.1' == $_SERVER['SERVER_PROTOCOL'] )
            $protocol = 'HTTP/1.1';
            header( "$protocol 503 Service Unavailable", true, 503 );
            header( 'Retry-After: 3600' );
            echo $text;

            exit;
        }

    }

    private function PreparePhone($phone)
    {
        $phone_arr          = array();
        $phone_arr['SMALL'] = preg_replace(array('/\(/', '/\)/', '/-/', '/\s/'), '', $phone);
        $phone_arr['PHONE'] = $phone;
        if (preg_match('/(\+?\d{1,3})?\s?\((\d{1,4})\)\s?([\d\-]*)/', $phone, $mch))
        {
            $phone_arr['ccode'] = $mch[1];
            $phone_arr['tcode'] = $mch[2];
            $phone_arr['body']  = $mch[3];
        }
        return $phone_arr;
    }

}

?>