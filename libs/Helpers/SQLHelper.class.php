<?php
/**
 * Description of SQLHelper
 *
 * @author grom
 */
class SQLHelper
{
    public static function Limit($limit, $start)
    {
        return empty($limit) ? '' : 'LIMIT ' . (empty($start) ? '' : ((int)$start . ', ') ) . (int)$limit;
    }

    public static function In($values, $forceInts = false)
    {
        if (!is_array($values))
            return '(' . DB::Quote($values) . ')';

        foreach ($values as $key => $value)
        {
            if ($forceInts)
                $values[$key] = (int)$value;
            else
                $values[$key] = DB::Quote($value);
        }
        return '(' . implode(', ', $values) . ')';
    }
}
