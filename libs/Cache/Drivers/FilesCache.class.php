<?php

require_once(IncPaths::$LIBS_PATH .'Cache/Drivers/ICacheDriver.interface.php');

/**
 * Кеширование на файлах
 *
 * @version 1.0
 */
class FilesCache implements ICacheDriver 
{
    /**
     * Получает на вход путь к директории для записи файлов с кешем
     * Проверяет, чтобы были права для записи в данную директорию
     *
     * @param string $directory
     * @return FilesCache
     */
    public function __construct($directory = null)
    {
        if (!is_null($directory))
        {
            $this -> directory = $directory;
        }
        
        if ($this -> directory{0} == '/')
            $this -> directory = substr($this -> directory, 1);
        
        $this -> directory = IncPaths::$ROOT_PATH . $this -> directory;
        
		// Убедимся, что есть доступ для записи в указанную директорию
		if (!is_dir($this -> directory) || !is_writable($this -> directory))
			throw new Exception('cache.unwritable');
			//throw new Exception('cache.unwritable', $directory);
    }
    
    /**
     * Папка, в которую сохраняются файлы с кешем
     *
     * @var string
     */
    protected $directory = '/cache/';
    
    /**
     * Расширение файлов с кешем (без ".")
     *
     * @var string
     */
    protected $fileEXT = 'cache';
    
    protected $separator = '~';
    protected $tagsSeparator = '+';
    protected $serializationMark = '<serialized>';
    
    /**
     * Сохраняет данные в кеше
     *
     * @param string $dataID
     * @param mixed $data
     * @param int $lifetime
     * @return bool
     */
    public function Save($dataID, $data, $tags = null, $lifetime = null)
    {
        // Удаляем старый кеш
        $this -> Delete($dataID);
        
        // Валидируем время жизни
        if (empty($lifetime) || !is_numeric($lifetime))
            $lifetime = 0;
        else 
            $lifetime = time() + $lifetime;
            
        // Валидируем массив тегов
        if (empty($tags))
            $tags = array();
        
        // Генерируем название файла
        $fileName = $dataID . 
            $this -> separator . $lifetime .
            $this -> separator . implode($this -> tagsSeparator, $tags) . 
            '.'. $this -> fileEXT;
            
        // Если нужно, сериализуем кешируемый объект
        if (!is_string($data))
        {
            $data = serialize($data);
            
            // Добавляем метку сериализации
            $data = $this -> serializationMark . $data;
        }
            
        // Записываем файл
        return (bool) file_put_contents($this -> directory . $fileName, $data);
    }
    
    /**
     * Берет данные из кеша, если они там есть
     *
     * @param string $dataID
     * @return mixed
     */
    public function Load($dataID)
    {
        // Получаем название файла
        if (($file = $this -> GetFile($dataID)) == null)
            return null;
            
        // Вырезаем расширение у файла
        $fileName = str_replace('.'. $this -> fileEXT, '', $file);
        
        // Если истек срок хранение кеша, удаляем его
        if ($this -> Expired($fileName))
        {
            $this -> Delete($dataID);
            return null;
        }
        
        // Читаем данные из файла
        $data = file_get_contents($file);
        
        // Если данные были сериализованы, десериализуем их
        if (substr($data, 0, strlen($this -> serializationMark)) == $this -> serializationMark)
        {
            $data = substr($data, strlen($this -> serializationMark));
            $data = unserialize($data);
        }
        
        return $data;
    }
    
    /**
     * Возвращает все данные из кеша, помеченные указанными тегами
     * Передавать можно как массив тегов, так и строку с одним тегом
     *
     * @param array|string $tags
     */
    public function Find($tags)
    {
        
    }
    
    /**
     * Удаляет файл с кешем
     *
     * @param string $dataID
     * @return bool
     */
    public function Delete($dataID)
    {
        // Проверяем, сущестует ли нужный файл
        $file = $this -> GetFile($dataID);
        
        // Если файла нет, то все нормально, возвращаем true
        if (empty($file))
            return true;

        unlink($file);
            
        return true;
    }
    
    /**
	 * Проверяет, свежий кеш или нет по названию файла
	 *
	 * @param string $fileName
	 * @return bool
	 */
	protected function Expired($fileName)
	{
		// Получаем дату истечения срока
		$expires = substr($fileName, strrpos($fileName, $this -> separator) + 1);

		// Expirations of 0 are "never expire"
		return ($expires !== 0 && $expires <= time());
	}
	
	/**
	 * Возвращает название файла с кешем по его идентификатору
	 *
	 * @param string $dataID
	 * @return string
	 */
	protected function GetFile($dataID)
	{
	    // Паттерн для нахождения требуемого файла
	    $pattern = $dataID . $this -> separator .'*'. $this -> separator .'*.'. $this -> fileEXT;
	    
	    // Ищем файлы, удовлетворяющие паттерну
	    $files = glob($this -> directory . $pattern);
	    
	    return empty($files) ? null : $files[0];
	}
    
    /**
     * Удаляет данные из кеша, помеченные указанными тегами
     * Передавать можно как массив тегов, так и строку с одним тегом
     *
     * @param array|string $tags
     * @return bool
     */
    public function DeleteByTags($tags)
    {
        
    }
}

?>