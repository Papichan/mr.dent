<?php

require_once(IncPaths::$LIBS_PATH .'Cache/Drivers/ICacheDriver.interface.php');

/**
 * Кеширование с помощью Memcache
 *
 * @version 1.0
 */
class MemcacheCache implements ICacheDriver 
{
    public function __construct()
    {
        // Проверяем, чтобы расширение было установлено
        if (!extension_loaded('memcache'))
			throw new Exception('cache.extension_not_loaded', 'memcache');
    }
    
    /**
     * Папка, в которую сохраняются файлы с кешем
     *
     * @var string
     */
    protected $directory = '';
    
    /**
     * Сохраняет данные в кеше
     *
     * @param string $dataID
     * @param mixed $data
     * @param int $lifetime
     * @return bool
     */
    public function Save($dataID, $data, $tags = null, $lifetime = null)
    {
        
    }
    
    /**
     * Берет данные из кеша, если они там есть
     *
     * @param unknown_type $dataID
     * @return mixed
     */
    public function Load($dataID)
    {
        
    }
    
    /**
     * Удаляет данные из кеша
     *
     * @param string $dataID
     * @return bool
     */
    public function Delete($dataID)
    {
        
    }
}

?>