<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 06.04.15
 * Time: 14:16
 */

class XML {

    public static function load($filename)
    {
        return @simplexml_load_file($filename);
    }

	public static function loadString($string)
	{
		return simplexml_load_string($string);
	}

	public static function attribute($object, $attribute)
	{
        if (isset($object[$attribute]))
            return (string) $object[$attribute];
    }

    public static function value($object)
    {
        return (string) $object;
    }

    //put your code here

}