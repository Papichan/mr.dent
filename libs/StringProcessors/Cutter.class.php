<?php

/**
 * Класс для обрезания строк
 *
 */
class Cutter
{
	/**
	 * Обрезает текст
	 *
	 * @param string $text			
	 * @param int $minLength
	 * @param int $maxLength
	 * @param array	$delims
	 * @param bool $leaveDelim
	 * @param string $tagsToLeave
	 * @return string
	 */
	public static function CutText(
		/**
		 * Исходный текст
		 */
		$text, 
		/**
		 * Минимальная длина обрезанного текста
		 */
		$minLength, 
		/**
		 * Максимальная длина обрезанного текста
		 */
		$maxLength, 
		/**
		 * Массив разделителей, по которым будет разрезаться текст
		 */
		$delims, 
		/**
		 * Оставлять разделитель в тексте или нет
		 */
		$leaveDelim = false, 
		/**
		 * Теги, которые надо оставить (для функции strip_tags). NULL - убрать все теги
		 */
		$tagsToLeave = null
	)
	{
		/**
		 * Сначала вырезаем лишние теги
		 */
		$text = strip_tags($text, $tagsToLeave);
		
		/**
		 * Если в массиве разделителей нету пробела, добавляем его туда
		 */
		if (!in_array(" ", $delims))
			$delims[] = " ";
		
		if (strlen($text) > $maxLength)
		{
			$cuttedText = substr($text, 0, $maxLength);
			$isFound = false;
			
			foreach ($delims as $delim)
			{
				if ($pos = strrpos($cuttedText, $delim))
				{
					if ($pos <= $minLength)
							$isFound = false;
						else 
							$isFound = true;
				}
				
				if ($isFound)
				{
					if ($leaveDelim)
						$pos += strlen($delim);
					break;
				}
			}
			
			if (!$isFound)
			{
				$pos = max($maxLength, strlen($text)) - 1;
			}
			
			$text = substr($text, 0, $pos);
		}
		
		return $text;
	}
}

?>