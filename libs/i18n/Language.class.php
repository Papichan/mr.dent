<?php

use ItForFree\rusphp\File\Path;

/**
 * Для поддрежки мультиязычности,  в частности для определения текущего языка
 * на основании текущего url.
 * 
 * Например:
 * example.com/ru/about   -- русский
 * example.com/en/about   -- английский
 *
 */
class Language
{
    /**
     * @var boolean выдавать ли пустое значение для сокращение языка, если текущий язык совпадает с тем, что считается стандартным
     */
    public static $emptyShorcatForDefaltLanguage = true;
    
    protected static $langs = ['en', 'zn'];

    /**
     * Уберет из маршрута секцию языка (если таковая есть), чтобы не мешать стандартному функционалу.
     * 
     * После вызова url типа /ru/example/page  будет восприниматься ядром как /example/page 
     * 
     * @param  string[] $aliases  массив сегментов url, формруемый классов ядра Request
     * @return string[]
     */
    public static function handleLanguageByAliases($aliases)
    {
//        ppre(self::$langs);
        if (!empty($aliases[0]) 
                && in_array($aliases[0], self::$langs)) {
            i18n::$language = $aliases[0];
            unset($aliases[0]);
        }
        
                
        if (empty($aliases)) { // ели других элементов нет
            $aliases = ['']; // для главной страницы
        }
         
        
        $result = array_values($aliases);
        
        return $result;
    }
    
    /**
     * Вернёт ссылку (URL) с добавлением в начало аббревиатуры текущего языка (напре /en/)
     * поддерживает абсолютные ссылки, так как 
     * с относительными не получится просто так подставлять краткое название языка в начало url)
     * 
     * @param array $params           возможные (необязательные) параметры
     *  @param array $params['url'] - url ссылки, без учета языка (все, что после доменного имени)
     * 
     * @return string
     */
    public static function getHrefUrl($params)
    {
        $current = i18n::$language;
        if (self::$emptyShorcatForDefaltLanguage
                && i18n::$language == i18n::$defaultLanguage) {
            $current = '';
        }
        return Path::addStartSlash(
            $current . Path::addStartSlash($params['url'], '/'), '/');
    }
    
    /**
     * Вернет переданный текст, толко в том случае, 
     * если текущий язык совпадает с указанным в переданных параметрах
     * 
     * @param array $params           возможные (необязательные) параметры
     * @param array $params['t']    -- текст, который надо вывести, если текущий язык совпадается с $params['lang']
     * @param array $params['lang'] -- язык, для которого надо выводить контент из $params['t']
     * 
     * @return string
     */
    public static function getIfLanguage($params)
    {
        $current = i18n::$language;
        $result = '';
        if ($current == $params['lang']) {
            $result = $params['t'];
        }
        return $result;
    }
    
    
    /**
     * Изменит имя переданного поля (ключ) массива, 
     * добавив к нему аббревиатуру текущего языка, через нижнее подчеркивание,
     * если этот язык не совмадает с языком по умолчанию (ьтогда имя поле не изменяется)
     * -- после чего будет возвращено значение измпереданного массива по получившемуся ключу.
     * 
     * @param array $params           возможные  параметры:
     *  $params['arr'] - массив
     *  $params['field'] -- базовое имя напр. caption, если текущий язык
     *  - английский, то извлечение будет по полю caption_en 
     *  (этот метод _en добавит и извлечет по нему данные)
     * 
     * @return string
     */
    public static function getKeyByCurrentLanguage($params)
    {
        $current = '_' . i18n::$language;
        if (self::$emptyShorcatForDefaltLanguage
                && i18n::$language == i18n::$defaultLanguage) {
            $current = '';
        }
        return $params['arr'][$params['field']. $current];
    }
    
    public static function registerSmartyFunctions(&$smarty)
    {
        i18n::registerSmartyMultyLangFuncion($smarty);
        $smarty->register_function("lhref", "Language::getHrefUrl");
        $smarty->register_function("lfield", "Language::getKeyByCurrentLanguage");
        $smarty->register_function("lprint", "Language::getIfLanguage");
    }
    
}
