<?php

require_once(IncPaths::$LIBS_PATH .'SettingsManager/DBSettingsProvider.class.php');

/**
 * Менеджер настроек из базы данных
 *
 * @version 1.0
 */
class DBSettingsManager extends DBSettingsProvider 
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }
    
    /**
     * Добавление новой настройки
     *
     * @param string|int $groupPath
     * @param string $name
     * @param string $desc
     * @param string $value
     * @return int
     */
    public function AddSetting($groupPath, $name, $desc, $value)
    {
        // Проверка входных данных на корректность
        $this -> CheckSettingName($name);
        $this -> CheckSettingDesc($desc);
        $this -> CheckSettingValue($value);
        
        // Получаем группу
        $group = $this -> GetGroup($groupPath);
        
        // Добавляем настройку в базу
        $this -> InitSettingsTableManager();
        $settingID = $this -> settingsTableManager -> Insert(array(
            'groupID'   => $group['groupID'],
            'name'      => $name,
            'desc'      => $desc,
            'value'     => $value
        ));
        
        return $settingID;
    }
    
    /**
     * Редактирование настройки
     *
     * @param string|int $settingPath
     * @param string $newName
     * @param string $newDesc
     * @param string $newValue
     * @param string|int $newGroupPath
     */
    public function EditSetting($settingPath, $newName = null, $newDesc = null, $newValue = null, $newGroupPath = null)
    {
        // Получаем настройку
        $setting = $this -> GetSetting($settingPath);
        
        $newSettingParams = array();
        
        // Проверяем входные данные и в случае корректности записываем в массив изменений
        if (!is_null($newName))
        {
            $this -> CheckSettingName($newName);
            $newSettingParams['name'] = $newName;
        }
        
        if (!is_null($newDesc))
        {
            $this -> CheckSettingDesc($newDesc);
            $newSettingParams['desc'] = $newDesc;
        }
        
        if (!is_null($newValue))
        {
            $this -> CheckSettingValue($newValue);
            $newSettingParams['value'] = $newValue;
        }
        
        if (!is_null($newGroupPath))
        {
            $newGroup = $this -> GetGroup($newGroupPath);
            $newSettingParams['groupID'] = $newGroup['groupID'];
        }
        
        // Если никаких изменений нет, выходим
        if (empty($newSettingParams))
            return;
            
        // Обновляем настройку в базе
        $this -> InitSettingsTableManager();
        $this -> settingsTableManager -> Update($newSettingParams, $setting['settingID']);
    }
    
    /**
     * Удаление настройки
     *
     * @param string|int $settingPath
     */
    public function DeleteSetting($settingPath)
    {
        // Получаем настройку, заодно и проверяем ее наличие в базе
        $setting = $this -> GetSetting($settingPath);
        
        // Удаляем настройку из базы
        $this -> InitSettingsTableManager();
        $this -> settingsTableManager -> Delete(array('settingID' => $setting['settingID']));
    }
    
    /**
     * Проверка имена настройки на корректность
     * По умолчанию в случае ошибки выбрасывается исключение, 
     * но если задать значение второго параметра 'false', то будет возвращаться bool
     *
     * @param string $name
     * @param bool $throwException
     * @return mixed
     */
    protected function CheckSettingName($name, $throwException = true)
    {
        if (empty($name))
        {
            if ($throwException)
                throw new Exception('Название настройки должно быть не пустое');
            else 
                return false;
        }
        
        if (!preg_match('/[a-zA-Z0-9_]/', $name))
        {
            if ($throwException)
                throw new Exception('Название настройки может содержать только латинские буквы и символ «_»');
            else 
                return false;
        }
        
        return true;
    }
    
    /**
     * Проверка описания настройки на корректность
     * По умолчанию в случае ошибки выбрасывается исключение, 
     * но если задать значение второго параметра 'false', то будет возвращаться bool
     *
     * @param string $desc
     * @param bool $throwException
     * @return mixed
     */
    protected function CheckSettingDesc($desc, $throwException = true)
    {
        if (empty($desc))
        {
            if ($throwException)
                throw new Exception('Описание настройки должно быть не пустое');
            else 
                return false;
        }
        
        return true;
    }
    
    /**
     * Проверка значения настройки на корректность
     * По умолчанию в случае ошибки выбрасывается исключение, 
     * но если задать значение второго параметра 'false', то будет возвращаться bool
     *
     * @param string $value
     * @param bool $throwException
     * @return mixed
     */
    protected function CheckSettingValue($value, $throwException = true)
    {
        if (empty($value) && ($value != 0))
        {
            if ($throwException)
                throw new Exception('Значение настройки должно быть не пустое');
            else 
                return false;
        }
        
        return true;
    }
}

?>