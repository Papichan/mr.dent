<?php

$settings = array
(
    'groups'    =>  array
    (
        array
        (
            'groupName' =>  'general',
            'groupDesc' =>  'Общие настройки',
            'settings'  =>  array
            (
                array
                (
                    'name'  =>  'siteName',
                    'desc'  =>  'Название сайта',
                    'value' =>  'The Best Site!'
                ),
                array
                (
                    'name'  =>  'phone',
                    'desc'  =>  'Телефон для связи',
                    'value' =>  '+7 (911) 123-45-67'
                )
            ),
            'groups'    =>  array
            (
                array
                (
                    'groupName' =>  'subGeneral',
                    'groupDesc' =>  'Подгруппа группы общих настроек',
                    'settings'  =>  array
                    (
                        array
                        (
                            'name'  =>  's1',
                            'desc'  =>  'd1',
                            'value' =>  'v1'
                        ),
                        array
                        (
                            'name'  =>  's2',
                            'desc'  =>  'd2',
                            'value' =>  'v2'
                        )
                    )
                ),
            )
        ),
        
        array
        (
            'groupName' =>  'mailer',
            'groupDesc' =>  'Настройки мейлера',
            'settings'  =>  array
            (
                array
                (
                    'name'  =>  'senderEmail',
                    'desc'  =>  'Почтовый адрес, с которого будет происходить рассылка писем',
                    'value' =>  'grant@ayaco.ru'
                ),
                array
                (
                    'name'  =>  'senderName',
                    'desc'  =>  'Имя отправителя',
                    'value' =>  'Грант'
                )
            )
        )
    )
);

?>