<?php
/**
 * Кеширующий менеджер настроек
 * 
 * @version 1.0
 * @author George
 *
 */
class CachingDBSettingsManager extends DBSettingsManager 
{
    public static $cacheNamePrefix = 'settings_';
    
    public function __construct()
    {
        parent::__construct();
    }
    
    
    /**
     * @return CachingDBSettingsManager
     */
    public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }

    public function GetSettingsAssoc($groupPath = null)
    {
        $cacheName = null;
        if (is_null($groupPath))
            $cacheName = 'null';
        elseif (is_string($groupPath))
            $cacheName = $groupPath;

        if (empty($cacheName))
            return parent::GetSettingsAssoc($groupPath);

        $cacheName = self::$cacheNamePrefix . $cacheName;
        $settings = ObjectsCache::GetCache($cacheName);
        if (is_null($settings))
        {
            $settings = parent::GetSettingsAssoc($groupPath);
	        ObjectsCache::Cache($settings, $cacheName);
        }
        return $settings;
    }
    
    public function AddSetting($groupPath, $name, $desc, $value)
    {
        ObjectsCache::KillCacheByPattern(self::$cacheNamePrefix.'*');
        return parent::AddSetting($groupPath, $name, $desc, $value);
    }
    
    public function EditSetting($settingPath, $newName = null, $newDesc = null, $newValue = null, $newGroupPath = null)
    {
        ObjectsCache::KillCacheByPattern(self::$cacheNamePrefix.'*');
        return parent::EditSetting($settingPath, $newName, $newDesc, $newValue, $newGroupPath);
    }
    
    public function DeleteSetting($settingPath)
    {
        ObjectsCache::KillCacheByPattern(self::$cacheNamePrefix.'*');
        return parent::DeleteSetting($settingPath);
    }
}
?>