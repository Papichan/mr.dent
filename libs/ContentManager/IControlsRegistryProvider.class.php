<?php
/**
 * Интерфейс провайдера настроек контролов и параметров полей
 * 
 * @version 1.0
 *
 */
interface IControlsRegistryProvider
{
    /**
     * Получение объекта параметров всех контролов указанной формы
     *
     * @param string $formName
     */
    public function GetControlsParams($formName);
    
    /**
     * Получение объекта параметров заданного контрола указанной формы
     *
     * @param string $formName
     * @param string $controlName
     */
    public function GetControlParams($formName, $controlName);
}
?>