    <table id="fieldsTable" style="width: 100%;">
        <thead>
            <tr>
                <th style="width: 200px;">Название поля</th>
                <th style="width: 200px;">Служебное название</th>
                <th style="width: 200px;">Тип</th>
                <th>Настройки</th>
            </tr>
        </thead>
        <tbody>
            <tr id="newControl">
                <td class="fc_title"><input type="text" name="title" value="" style="width: 200px" /></td>
                <td class="fc_name"><input type="text" name="name" value="" style="width: 200px" /></td>
                <td class="fc_control">
                    <select name="control" style="width: 200px" class="controlsSelect">
                        {foreach from=$localVars.CONTROLS item=CONTROL}
                        <option value="{$CONTROL.name}">{$CONTROL.description}</option>
                        {/foreach}
                    </select>
                </td>
                <td class="fc_settings"></td>
            </tr>
        </tbody>
    </table>
