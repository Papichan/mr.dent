<?php

/**
 * Селект из страниц
 *
 * @version 2.2
 *
 */
class PagesSelect extends Control
{
    const TREE_VIEW = 'tree';
    const LIST_VIEW = 'list';

    /**
     * Типы страниц, которые надо выводить
     *
     * @var array
     */
    private $pagesTypes = null;

    /**
     * Формат вывода (дерево или список)
     *
     * @var string
     */
    private $treeOrList = self::TREE_VIEW;

    /**
     * Дополнительные атрибуты для фильтрации страниц
     *
     * @var array
     */
    private $attributes = null;

    /**
     * Менеджер страниц
     *
     * @var SiteStructure
     */
    private $struct = null;

	/**
	 * Загрузка настроек
	 *
	 */
	private function LoadSettings()
	{
		if (isset($this -> controlSettings['pagesTypes']) && !empty($this -> controlSettings['pagesTypes']))
		    $this -> pagesTypes = explode(',', $this -> controlSettings['pagesTypes']);

	    if (isset($this -> controlSettings['treeOrList']) && !empty($this -> controlSettings['treeOrList']))
	        if ($this -> controlSettings['treeOrList'] == self::TREE_VIEW || $this -> controlSettings['treeOrList'] == self::LIST_VIEW)
                $this -> treeOrList = $this -> controlSettings['treeOrList'];

	    if (isset($this -> controlSettings['attributes']) && !empty($this -> controlSettings['attributes']) && is_array($this -> controlSettings['attributes']))
		    $this -> attributes = $this -> controlSettings['attributes'];
	}

	public function InitControl($value = null, $rowID = null)
	{
	    // Загружаем настройки
		$this -> LoadSettings();

		$this -> controlTplPath = parent::$CONTROLS_PATH .__CLASS__.'/control.tpl';

		// Переменные для шаблона
		$this -> controlTplVars = array();
		$this -> controlTplVars['NAME'] = $this -> name;
		$this -> controlTplVars['TIP'] = $this -> tip;
		$this -> controlTplVars['VALUE'] = $value;

		if ($this -> disabled)
		    $this -> controlTplVars['READONLY'] = 1;

		if ($this -> required)
            $this -> controlTplVars['REQUIRED'] = 1;

        // Используемые JS-библиотеки
        $this -> controlJSLibs = array();

        // Инициализирующая функция
        $this -> controlJSFiles = array();

        // Переменные, необходимые JS
        $this -> controlJSVars = array();

		$this -> type = __CLASS__;

        // Формируем массив атрибутов для
        $attrs = is_null($this -> attributes) ? array() : $this -> attributes;

        $this -> struct = SiteStructure::getInstance();

        // Массив подходящих страниц
        $pages = array();

        // Заданы типы страниц
        if (!empty($this -> pagesTypes))
        {
            // Посчитаем, сколько типов задано
            $pagesTypesCnt = count($this -> pagesTypes);

            // Менеджер таблицы типов страниц
            $pagesTypesTableManager = DBTableManager::getInstance(TablesNames::$PAGES_TYPE_TABLE_NAME);

            // Для каждого типа получим страницы
            for ($i = 0; $i < $pagesTypesCnt; $i++)
            {
                $pageTypeID = trim($this -> pagesTypes[$i]);

                // Задана константа, соответствующая типу страницы
                if (!is_numeric($pageTypeID))
                {
                    $pageType = $pagesTypesTableManager -> SelectFirst(array('constantName' => $pageTypeID));

                    if (empty($pageType))
                        continue;

                    $pageTypeID = $pageType['pageTypeID'];
                }
                $pages = array_merge($pages, $this -> struct -> GetPages($attrs + array('pageTypeID' => $pageTypeID)));
            }
        }
        // Типы страниц не заданы
        else
        {
            $pages = $this -> struct -> GetPages($attrs);
        }

        // Ставим флаг у найденных страниц, позволяющий их выбирать в селекте
        $pagesCnt = count($pages);
        for ($i = 0; $i < $pagesCnt; $i++)
        {
            $pages[$i]['canBeSelected'] = 1;
        }

        if ($this -> treeOrList == self::TREE_VIEW)
        {
            // Получаем дерево страниц
            $tree = $this -> GenerateTree($pages);

            $list = $this -> LinearizeTree($tree);
        }
        else
        {
            $list = $pages;
        }

        if (!$this -> required)
            array_unshift($list, array('caption' => ' - ', 'canBeSelected' => 1, 'pageID' => 0, 'level' => 0));

        $this -> controlTplVars['PAGES_LIST'] = $list;
		if (isset($this->tableName)) $this -> controlTplVars['tableName'] = $this->tableName;
		if (isset($this->formIndex)) $this -> controlTplVars['formIndex'] = $this->formIndex;

	}

	public function PostBackHandler(&$data, $rowID = null)
	{

	}

	private function GenerateTree($pages)
	{
	    $pagesCnt = count($pages);

	    $tree = array();

	    for ($i = 0; $i < $pagesCnt; $i++)
	    {
	        // Получаем родителей страницы
	        //$parents = $this -> struct -> GetParentsForPage($pages[$i]['pageID']);
	        //$parents = array_reverse($parents);

	        $page = $pages[$i];
	        $parentID = $page['parentID'];
	        $parents = array();

	        while ($parentID != 0)
	        {
	            // Добавляем страницу в начало массива
	            array_unshift($parents, $page);

	        	$page = $this -> struct -> GetPage(array('pageID' => $parentID));
	        	$parentID = $page['parentID'];
	        }

	        array_unshift($parents, $page);

	        $this -> InsertPagesIntoTree(&$tree, $parents);
	    }

	    return $tree;
	}

	private function InsertPagesIntoTree(&$tree, $pages)
	{
	    $page = array_shift($pages);

	    if (!isset($tree[$page['pageID']]))
	    {
	        if (empty($tree))
	        {
                $tree[$page['pageID']] = $page;
	        }
	        else
	        {
	            $placePos = 0;
	            $saveTree = $tree;
	            $tree = array();

	            $added = false;
	            foreach ($saveTree as $pageNodeID => $pageNode)
	            {
	                if ($page['priority'] > $pageNode['priority'] && !$added)
	                {
                        $tree[$page['pageID']] = $page;
                        $added = true;
	                }
                    $tree[$pageNodeID] = $pageNode;
	            }
	            if (!$added)
                    $tree[$page['pageID']] = $page;

	            //$tree = array_slice($tree, 0, $placePos) + array($page['pageID'] => $page) + array_slice($tree, $placePos);
	        }
	    }
	    else
	    {
	        if (isset($page['canBeSelected']))
                $tree[$page['pageID']]['canBeSelected'] = $page['canBeSelected'];
	    }

	    // Если остались еще страницы, вызываем для них рекурсивно эту же функцию
	    if (!empty($pages))
	    {
	        if (!isset($tree[$page['pageID']]['children']))
                $tree[$page['pageID']]['children'] = array();

	        $this -> InsertPagesIntoTree($tree[$page['pageID']]['children'], $pages);
	    }
	}

	private function LinearizeTree(&$tree, $level = 0)
	{
	    $lineTree = array();

	    foreach ($tree as $pageID => $page)
	    {
	        $page['level'] = $level;

	        $childrenTree = null;
	        if (isset($page['children']))
	        {
    	        $childrenTree = $page['children'];
    	        unset($page['children']);
	        }

	        $lineTree[] = $page;

	        $childrenLineTree = array();
	        if (!empty($childrenTree))
	        {
                $childrenLineTree = $this -> LinearizeTree($childrenTree, $level + 1);
	        }

	        if (!empty($childrenLineTree))
	        {
                $lineTree = array_merge($lineTree, $childrenLineTree);
	        }
	    }

	    return $lineTree;
	}
}

