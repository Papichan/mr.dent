<select name="{$localVars.tableName}[{$localVars.formIndex}][{$localVars.NAME}]" id="{$localVars.NAME}_{$localVars.formIndex}"{if $localVars.CLASS} class="{$localVars.CLASS}"{/if}{if !empty($localVars.READONLY)} readonly="readonly"{/if}{if $localVars.TIP} title="{$localVars.TIP}"{/if}>
{foreach from=$localVars.OPTIONS item=OPTION}
	<option value="{$OPTION.value}"{if $OPTION.selected} selected="selected"{/if} {if $OPTION.disabled} disabled="disabled"{/if}>{$OPTION.text}</option>
{/foreach}
</select>