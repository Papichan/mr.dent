<table style="width: 100%; float: left;" id="{$localVars.NAME}_table">
    <thead>
        <tr>
            <th style="width: 20px">№</th>
            <th>Текст</th>
            <th style="width: 50px;">Удаление</th>
        </tr>
    </thead>
    <tbody>
        {if $localVars.ROWS}
            {foreach from=$localVars.ROWS item=ROW key=I}
            <tr>
                <td>{$I+1}</td>
                <td>
                    <textarea name="{$localVars.NAME}__edit[{$ROW.rowID}]" style="width:{$localVars.WIDTH}; height:{$localVars.HEIGHT}" class="MRT_fck {$localVars.TOOLBARSET}">{$ROW.value}</textarea>
                </td>
                <td><input type="checkbox" name="{$localVars.NAME}__delete[]" value="{$ROW.rowID}" /></td>
            </tr>
            {/foreach}
        {/if}
    </tbody>
</table>
<div style="clear: both;">
    <a href="#" id="{$localVars.NAME}_add">[ добавить поле ]</a>
</div>
