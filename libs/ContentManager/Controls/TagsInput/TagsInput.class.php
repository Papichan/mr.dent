<?php

/**
 * TagsInput
 * 
 * @version 1.0
 *
 */
class TagsInput extends Control implements IFlushingControl
{
	private $size = 42;
	
	private $objectType = null;
	
	public function InitControl($value = null, $rowID = null)
	{
	    $this -> LoadSettings();	
		
		//$value = ($value === null) ? '' : htmlspecialchars($value);
		
		$this -> controlTplPath = parent::$CONTROLS_PATH .__CLASS__.'/control.tpl';
        
        $this -> controlTplVars['NAME'] = $this -> name;
        $this -> controlTplVars['VALUE'] = $value;
        $this -> controlTplVars['TIP'] = $this -> tip;
        $this -> controlTplVars['CLASS'] = $this -> CSSClass;
        $this -> controlTplVars['SIZE'] = $this -> size;

        // Инициализирующая функция
        $this -> controlJSFiles = array();
        $this -> controlJSFiles[] = parent::$CONTROLS_ALIAS.__CLASS__.'/init.js';
        
        // Используемые JS-библиотеки
        $this -> controlJSLibs = array();
        
        $this -> controlJSVars['name'] = $this -> name;
        $this -> controlJSVars['objectType'] = $this -> objectType;
		
		if (!is_null($rowID))
		{
		    $query = '
                SELECT  t.*
                FROM    `'. TablesNames::$TAGS_TABLE_NAME .'` t
                JOIN    `'. TablesNames::$TAGS_LINKS_TABLE_NAME .'` tl
                    ON  t.`tagID` = tl.`tagID`
                WHERE   tl.`objectType` = '. DB::Quote($this -> objectType) .'
                    AND tl.`objectID` = '. DB::Quote($rowID) .'
                ORDER BY t.`tag`
            ';
		    
    		$tags = DB::QueryToArray($query);
    		$this -> controlTplVars['TAGS'] = $tags;
		}
		
		$this -> type = __CLASS__;
	}
	
	private function LoadSettings()
	{
	    if (isset($this -> controlSettings['size']) && is_numeric($this -> controlSettings['size']))
			$this -> size = $this -> controlSettings['size'];

		if (isset($this -> controlSettings['objectType']))
			$this -> objectType = $this -> controlSettings['objectType'];
	}
	
	public function Flush(&$data, $rowID)
	{
	    $this -> LoadSettings();
	    
	    // Загрузим существующие теги данной записи
	    $query = '
            SELECT  t.*
            FROM    `'. TablesNames::$TAGS_TABLE_NAME .'` t
            JOIN    `'. TablesNames::$TAGS_LINKS_TABLE_NAME .'` tl
                ON  t.`tagID` = tl.`tagID`
            WHERE   tl.`objectType` = '. DB::Quote($this -> objectType) .'
                AND tl.`objectID` = '. DB::Quote($rowID) .'
            ORDER BY t.`tag`
        ';
	    $rowTags = DB::QueryToArray($query, 'tagID');
	    
	    // Оставшиеся теги
	    $tagsToRemain = isset($data['tags']) ? $data['tags'] : array();
	    
	    // Вычислим, какие теги надо удалить
	    $tagsToRemainCnt = count($tagsToRemain);
	    for ($i = 0; $i < $tagsToRemainCnt; $i++)
	    {
	        if (isset($rowTags[$tagsToRemain[$i]]))
	        {
	            unset($rowTags[$tagsToRemain[$i]]);
	        }
	    }
	    
	    // Если в массиве rowTags остались записи, их надо удалить
	    if (!empty($rowTags))
	    {
	        $tagsLinksTableManager = DBTableManager::getInstance(TablesNames::$TAGS_LINKS_TABLE_NAME);
            foreach ($rowTags as $tagID => $tag)
            {
                $tagsLinksTableManager -> Delete(array(
                    'tagID'         => $tagID,
                    'objectType'    => $this -> objectType,
                    'objectID'      => $rowID
                ));
            }
	    }
	    
	    // Добавляем новые теги
	    if (isset($data['newTags']) && !empty($data['newTags']))
	    {
	        $newTags = $data['newTags'];
	        
	        // Получим еще раз все теги объекта
	        $query = '
                SELECT  t.*
                FROM    `'. TablesNames::$TAGS_TABLE_NAME .'` t
                JOIN    `'. TablesNames::$TAGS_LINKS_TABLE_NAME .'` tl
                    ON  t.`tagID` = tl.`tagID`
                WHERE   tl.`objectType` = '. DB::Quote($this -> objectType) .'
                    AND tl.`objectID` = '. DB::Quote($rowID) .'
                ORDER BY t.`tag`
            ';
    	    $rowTags = DB::QueryToArray($query, 'tagID', 'tag');
    	    
    	    $tagsTableManager = DBTableManager::getInstance(TablesNames::$TAGS_TABLE_NAME);
    	    $tagsLinksTableManager = DBTableManager::getInstance(TablesNames::$TAGS_LINKS_TABLE_NAME);
    	    foreach ($newTags as $tag)
    	    {
    	        // Проверим, не совпадает ли новый тег с каким-нибудь тегом из существующих
    	        $query = '
                    SELECT  t.*
                    FROM    `'. TablesNames::$TAGS_TABLE_NAME .'` t
                    WHERE   LOWER(`tag`) LIKE '. DB::Quote(UTF8::strtolower($tag));
    	        
    	        $matches = DB::QueryToArray($query);
    	        if (!empty($matches))
    	        {
    	            $tagID = $matches[0]['tagID'];
    	        }
    	        else 
    	        {
    	            // Совпадений нет, добавляем новый тег в базу
    	            $tagID = $tagsTableManager -> Insert(array('tag' => $tag));
    	        }
    	        
    	        // Проверим, есть ли уже этот тег у объекта
    	        if (!isset($rowTags[$tagID]))
    	        {
    	            $tagsLinksTableManager -> Insert(array(
                        'tagID'         => $tagID,
                        'objectType'    => $this -> objectType,
                        'objectID'      => $rowID
    	            ));
    	        }
    	    }
	    }
	}
    
    public function PostBackHandler(&$data, $rowID = null){}
}
?>