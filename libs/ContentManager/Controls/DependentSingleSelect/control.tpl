<div style="width:550px;float:left;">
    <select name="{$localVars.tableName}[{$localVars.formIndex}][{$localVars.NAME}]" id="{$localVars.NAME}_{$localVars.formIndex}" class="select2 {if $localVars.CLASS}{$localVars.CLASS}{/if}" {if !empty($localVars.READONLY)} readonly="readonly"{/if}{if $localVars.TIP} title="{$localVars.TIP}"{/if} style="width:400px;">
	{if !empty($localVars.OPTIONS)}
		{foreach from=$localVars.OPTIONS item=OPTION}
		<option value="{$OPTION.value}"{if $OPTION.selected} selected="selected"{/if}>{$OPTION.text}</option>
		{/foreach}
	{/if}
    </select>
</div>
<div style="clear:left"></div>