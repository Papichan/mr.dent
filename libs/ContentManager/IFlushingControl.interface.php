<?php
/**
 * Интерфейс для самозаписывающихся контролов
 * @version 1.0
 *
 */
interface IFlushingControl 
{
   	/**
	 * Записывает данные в поле, которому соответствует данный 
	 * элемент управления. Используется для сложных контролов,
	 * или абстрактных полей.
	 *
	 * @param array $data
	 * @param int $rowID
	 */
	public function Flush(&$data, $rowID);
}
?>