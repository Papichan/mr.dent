<?php
/**
 * Обёртка для стандартных функций json  в PHP
 * 
 * @version 1.0
 */
class JSON
{
    /**
     * Преобразовывает объект в JSON-строку
     * 
     * Флаги работают только с версией >= 5.3.0
     * 
     * Флаг $forceEncodeAsObject для того, чтобы все переданные 
     * переменные кодировались как объект (напрмер, масиив)
     * Non-associative array output as array: [[1,2,3]]
     * Non-associative array output as object: {"0":{"0":1,"1":2,"2":3}}
     * 
     * Флаг $specialCharsToHex для преобразования символов <>'"& 
     * в их HEX эквиваленты
     *
     * @param object $object
     * @param bool $forceEncodeAsObject
     * @param bool $specialCharsToHex
     * @return string
     */
    public static function Encode($object, $forceEncodeAsObject = false, $specialCharsToHex = false)
    {
        $options = 0;
        if ($specialCharsToHex)
            $options |= JSON_HEX_TAG|JSON_HEX_APOS|JSON_HEX_QUOT|JSON_HEX_AMP;
        if ($forceEncodeAsObject)
            $options |= JSON_FORCE_OBJECT;
        return phpversion() >= '5.3.0' ? json_encode($object, $options) : json_encode($object);
    }

    /**
     * Преобразовывает строку в формате JSON в объект
     *
     * @param string $string
     * @param bool $objectsToAssoc
     * @param int $maxDepth
     * @return mixed
     */
    public static function Decode($string, $objectsToAssoc = false, $maxDepth = 512)
    {
        return phpversion() >= '5.3.0' ? json_decode($string, $objectsToAssoc, $maxDepth) : json_decode($string, $objectsToAssoc);
    }
    
    /**
     * Возвращает код последней ошибки
     * 
     * Returns an integer, the value can be one of the following constants:
     * 
     *  JSON error codesConstant	Meaning
     *  JSON_ERROR_NONE         No error has occurred
     *  JSON_ERROR_DEPTH        The maximum stack depth has been exceeded
     *  JSON_ERROR_CTRL_CHAR    Control character error, possibly incorrectly encoded
     *  JSON_ERROR_SYNTAX       Syntax error
     *
     * @return int
     */
    public static function GetLastError()
    {
        return json_last_error();
    }
}
?>