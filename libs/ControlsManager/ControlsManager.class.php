<?php

/**
 * Управление контролами
 * 
 * @version 1.0
 *
 */
class ControlsManager
{
    public function __construct()
    {
        $this -> controlsTableManager = new DBTableManager(TablesNames::$CONTROLS_TABLE_NAME);
        $this -> controlsSettingsTableManager = new DBTableManager(TablesNames::$CONTROLS_SETTINGS_TABLE_NAME);
    }
    
    /**
     * Enter description here...
     *
     * @var DBTableManager
     */
    public $controlsTableManager = null;
    
    /**
     * Enter description here...
     *
     * @var DBTableManager
     */
    public $controlsSettingsTableManager = null;
    
    /**
     * Возвращает массив контролов
     *
     * @param array $attrs
     * @return array
     */
    public function GetControls($attrs = null)
    {
        return $this -> controlsTableManager -> Select($attrs);
    }
    
    public function GetControl($controlID)
    {
        return array_shift($this -> controlsTableManager -> Select(array('controlID' => $controlID)));
    }
    
    public function AddControl($name, $description, $version = null, $isPublic = 0)
    {
        if ($this -> controlsTableManager -> Count(array('name' => $name)) != 0)
            throw new ArgumentException('Контрол с именем '. $name .' уже существет');
            
        $attrs = array();
        $attrs['name'] = $name;
        
        if (!is_null($description))
            $attrs['description'] = $description;
        
        if (!is_null($version))
            $attrs['version'] = $version;
            
        if (!is_null($isPublic))
            $attrs['isPublic'] = $isPublic == 1 ? 1 : 0;
            
        return $this -> controlsTableManager -> Insert($attrs);
    }
    
    public function EditControl($controlID, $name = null, $description = null, $version = null, $isPublic = null)
    {
        $attrs = array();
        
        if (!empty($name))
        {
            $controls = $this -> GetControls(array('name' => $name));
            
            if (count($controls) == 1 && $controls[0]['controlID'] != $controlID)
                throw new ArgumentException('Контрол с именем '. $name .' уже существет');
                
            $attrs['name'] = $name;
        }
        
        if (!is_null($description))
            $attrs['description'] = $description;
        
        if (!is_null($version))
            $attrs['version'] = $version;
            
        if (!is_null($isPublic))
            $attrs['isPublic'] = $isPublic == 1 ? 1 : 0;
            
        if (empty($attrs))
            return;
            
        $this -> controlsTableManager -> Update($attrs, $controlID);
    }
    
    public function GetSettings($controlID, $attrs = null)
    {
        if (is_null($attrs) || !is_array($attrs))
            $attrs = array();
            
        $attrs['controlID'] = $controlID;
        return $this -> controlsSettingsTableManager -> Select($attrs);
    }
    
    public function GetSetting($settingID)
    {
        return array_shift($this -> controlsSettingsTableManager -> Select(array('settingID' => $settingID)));
    }
    
    public function AddSetting($controlID, $name, $defaultValue, $description, $isPublic = 0)
    {
        $attrs = array();
        
        if (empty($controlID))
            throw new Exception('Не задан ID контрола');
            
        if ($this -> controlsTableManager -> Count(array('controlID' => $controlID)) == 0)
            throw new Exception('Не существует контрола с указанным ID');
        
        $attrs['controlID'] = $controlID;
        
        if (empty($name))
            throw new Exception('Не задано имя настройки');
            
        if ($this -> controlsSettingsTableManager -> Count(array('name' => $name, 'controlID' => $controlID)) != 0)
            throw new Exception('У данного контрола уже есть настройка с таким именем - '. $name);
            
        $attrs['name'] = $name;
        
        if (!empty($defaultValue))
            $attrs['defaultValue'] = $defaultValue;
            
        if (empty($description))
            throw new Exception('Не задано описание настройки');
            
        $attrs['description'] = $description;
        $attrs['isPublic'] = $isPublic == 1 ? 1 : 0;
        
        return $this -> controlsSettingsTableManager -> Insert($attrs);
    }
    
    public function EditSetting($settingID, $controlID = null, $name = null, $defaultValue = null, $description = null, $isPublic = null)
    {
        $attrs = array();
        
        if ($this -> controlsSettingsTableManager -> Count(array('settingID' => $settingID)) == 0)
            throw new Exception('Настройки с указанным ID ['. $settingID .'] не сущестует');
            
        if (!empty($controlID))
        {
            if ($this -> controlsTableManager -> Count(array('controlID' => $controlID)) == 0)
                throw new Exception('Контрола с указанным ID ['. $controlID .'] не сущестует');
                
            $attrs['controlID'] = $controlID;
        }
        
        if (!empty($name))
        {
            $settings = $this -> GetSettings($controlID, array('name' => $name));
            
            if (count($settings) == 1 && $settings[0]['settingID'] != $settingID)
                throw new Exception('Настройка с именем '. $name .' уже существет');
                
            $attrs['name'] = $name;
        }
        
        if (!is_null($defaultValue))
            $attrs['defaultValue'] = $defaultValue;
            
        if (empty($description))
            throw new Exception('Не задано описание настройки');
            
        $attrs['description'] = $description;
        
        if (!is_null($isPublic))
            $attrs['isPublic'] = $isPublic == 1 ? 1 : 0;
            
        if (empty($attrs))
            return;
            
        $this -> controlsSettingsTableManager -> Update($attrs, $settingID);
    }
}

?>