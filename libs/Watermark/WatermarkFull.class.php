<?php

class WatermarkFull
{
    public static $watermarkImagePath = 'logo.png';

    public static function AddWatermark($imageFilePath)
    {
        $watermark = imagecreatefrompng(self::$watermarkImagePath);

        $watermark_width  = imagesx($watermark);
        $watermark_height = imagesy($watermark);



        /**
         * Переводим абсолютный путь в относительный
         */
        if($imageFilePath{0} == "/") $imageFilePath = IncPaths::$ROOT_PATH . substr($imageFilePath, 1);

        /**
         * Провеяем файл на существование
         */
        if(!file_exists($imageFilePath)) throw new Exception("Файл " . $imageFilePath . " не найден");

        /**
         * Проверяем, чтобы указанный файл был изображением
         */
        if(!($imageInfo = @getimagesize($imageFilePath))) throw new Exception("Файл " . $imageFilePath . " не является картинкой");

        /**
         * Получаем параметры изобраения:
         * 		- ширину
         * 		- высоту
         * 		- тип изображения (gif, jpeg, png...)
         */
        $imageWidth  = $imageInfo[0];
        $imageHeight = $imageInfo[1];
        $imageType   = $imageInfo[2];

        switch($imageType)
        {
            case IMAGETYPE_GIF:
                $image = imagecreatefromgif($imageFilePath);
                break;

            case IMAGETYPE_JPEG:
                $image = imagecreatefromjpeg($imageFilePath);
                break;

            case IMAGETYPE_PNG:
                $image = imagecreatefrompng($imageFilePath);
                break;

            default:
                throw new Exception("Данный тип файла не поддерживается");
                break;
        }

//========================================================
//                //resize watermark to 13%
//
//                $iNewX = intval( $watermark_width * sqrt($imageWidth * $imageHeight * 0.13 /($watermark_width*$watermark_height)));
//                $fNewK = $watermark_width / $iNewX;
//                $iNewY = intval( $watermark_height / $fNewK );
//
//		$newWatermark = imagecreatetruecolor($watermark_width, $watermark_height);
//
//                if ($iNewX < $watermark_width)
//                {
//                    imagecolortransparent($newWatermark, imagecolorallocate($newWatermark, 0, 0, 0));
//                    imagealphablending($newWatermark, false);
//                    imagesavealpha($newWatermark, true);
//                    imagecopyresampled( $newWatermark, $watermark, 0, 0, 0, 0, $iNewX, $iNewY, $watermark_width, $watermark_height );
//                }
//                else
//                {
//                    $newWatermark = $watermark;
//                    $iNewX = $watermark_width;
//                    $iNewY = $watermark_height;
//                }
//========================================================
        $maxHeight = 100;
        $maxWidth  = 100;
        $fK        = $watermark_width / $watermark_height;
        $fDefK     = $maxWidth / $maxHeight;
        if($fDefK > $fK)
        {
            $iNewY = $maxHeight;
            $fNewK = $watermark_height / $maxHeight;
            $iNewX = intval($watermark_width / $fNewK);
        }
        else
        {
            $iNewX = $maxWidth;
            $fNewK = $watermark_width / $maxWidth;
            $iNewY = intval($watermark_height / $fNewK);
        }

        $newWatermark = imagecreatetruecolor($watermark_width, $watermark_height);

        if($iNewX < $watermark_width)
        {
            imagecolortransparent($newWatermark, imagecolorallocate($newWatermark, 0, 0, 0));
            imagealphablending($newWatermark, false);
            imagesavealpha($newWatermark, true);
            imagecopyresampled($newWatermark, $watermark, 0, 0, 0, 0, $iNewX, $iNewY, $watermark_width, $watermark_height);
        }
        else
        {
            $newWatermark = $watermark;
            $iNewX        = $watermark_width;
            $iNewY        = $watermark_height;
        }

//========================================================



        $dest_x = $imageWidth - $iNewX - 5;
        $dest_y = $imageHeight - $iNewY - 5;


        imagecopy($image, $newWatermark, $dest_x, $dest_y, 0, 0, $iNewX, $iNewY);

        switch($imageType)
        {
            case IMAGETYPE_GIF:
                imagegif($image, $imageFilePath);
                break;

            case IMAGETYPE_JPEG:
                imagejpeg($image, $imageFilePath, 100);
                break;

            case IMAGETYPE_PNG:
                imagepng($image, $imageFilePath);
                break;

            default:
                throw new Exception("Данный тип файла не поддерживается");
                break;
        }

        imagedestroy($image);
        imagedestroy($watermark);
    }

}
