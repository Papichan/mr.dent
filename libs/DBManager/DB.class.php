<?php
/**
 * New version based on PDO
 * @version  2.0
 *
 */
class DB
{
    /**
     * Название базы данных
     *
     * @var string
     */
    public static $DBName = null;

    /**
     * Хост
     *
     * @var string
     */
    public static $DBHost = 'localhost';

    /**
     * Номер порта
     *
     * @var int
     */
    public static $DBPort = '3306';

    /**
     * Имя пользователя
     *
     * @var string
     */
    public static $DBUserName = null;

    /**
     * Пароль
     *
     * @var string
     */
    public static $DBPassword = null;

    /**
     * СУБД
     *
     * @var string
     */
    public static $DBMS = 'MySql';

    /**
     * Счётчик запросов, вызванных через DB::Query
     *
     * @var int
     */
    public static $QueriesCounter = 0;

    /**
     * Указатель на PDO
     * По умолчанию в приложении используется один объект PDO
     * Для подключения к другим базам данных, используйте
     * другие экземпляры
     *
     * @var PDO
     */
    public static $PDO = null;

    /**
     * Подключается к базе данных,
     * Инициализирует PDO
     *
     * @return PDO
     */
    public static function Connect()
    {
        if (!is_null(self::$PDO)) return;
        // Установим соединение
        try
        {
            $attrs = array();
            if (self::$DBMS == 'MySql')
                $attrs[PDO::MYSQL_ATTR_INIT_COMMAND] = 'SET NAMES utf8';

            self::$PDO = new PDO(strtolower(self::$DBMS) .
                                   ':dbname=' . self::$DBName .
                                   ';host=' . self::$DBHost .
                                   ';port='. self::$DBPort,
                                self::$DBUserName,
                                self::$DBPassword,
                                $attrs);
            self::$PDO -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        catch (PDOException $e)
        {
            throw new SQLException('Could not connect to Data Base: '. $e -> getMessage());
        }
        return self::$PDO;
    }

    /**
     * Возаращает PDOStatement
     *
     * @param string $query
     * @return PDOStatement
     */
    public static function Query($query)
    {
        self::$QueriesCounter++;
        self::Connect();
        $statement = self::$PDO -> query($query);
        return $statement;
    }

    /**
     * Выполняет запрос с плейсхолдерам (prepare, execute) и возвращает объект PDOStatement
     *
     * @param string $query Запрос
     * @param array $params Параметры вида array('fieldName' => array($fieldValue, 'str')), вторым параметром в массиве указывается тип переменной (по-умолчанию 'str')
     * @return PDOStatement|false Обект PDOStatemnet или false в случае ошибки
     */
	public static function QueryEx($query, $params)
	{
		self::$QueriesCounter++;
		self::Connect();
		$statement = self::$PDO->prepare($query);
		foreach ($params as $paramName => $param)
		{
			if (is_string($paramName))
				$paramName = ':' . $paramName;  // named params
			else
				$paramName += 1;  // indexed params

			if (is_array($param) && !empty($param[1]))
				$statement->bindValue($paramName, $param[0], self::ParamTypeStrToPDO($param[1]));
			else
				$statement->bindValue($paramName, is_array($param) ? $param[0] : $param);
		}

		if (!$statement->execute())
			return false;
		return $statement;
	}

	/**
     * Возвращает первое значение первой строки выборки с плейсхолдерами
     *
     * @param string $query
     * @param array $params @see DB::QueryEx()
     * @return mixed Значение
     */
    public static function QueryOneValueEx($query, $params)
	{
		$statement = self::QueryEx($query, $params);
		if (!$statement)
			return false;

		$row = $statement -> fetch(PDO::FETCH_NUM);
        return isset($row[0]) ? $row[0] : null;
	}

	/**
     * Возвращает первое значение первой строки выборки
     *
     * @param sql $query
     * @return mixed
     */
    public static function QueryOneValue($query)
    {
        $statement = self::Query($query);
        $row = $statement -> fetch(PDO::FETCH_NUM);
        return isset($row[0]) ? $row[0] : null;
    }

    /**
     * Возвращает следующую запись как ассоциативный массив
     *
     * @param PDOStatement $pdoStatement
     * @return array
     */
    public static function FetchAssoc($pdoStatement)
    {
        return $pdoStatement -> fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Возвращает следующую запись как индексированный массив
     *
     * @param PDOStatement $queryResult
     * @return array
     */
    public static function FetchArray($pdoStatement)
    {
        return $pdoStatement -> fetch(PDO::FETCH_NUM);
    }

    /**
     * Возвращает число выбранных записей
     *
     * @param PDOStatement $pdoStatement
     * @return int
     */
    public static  function ReturnedRows($pdoStatement)
    {
        return $pdoStatement -> rowCount();
    }

    /**
     * Возвращает число выбранных записей
     * Делает то же самое, что ReturnedRows
     *
     * @param PDOStatement $pdoStatement
     * @return int
     */
    public static function AffectedRows($pdoStatement)
    {
        return $pdoStatement -> rowCount();
    }

    /**
     * Выполняет запрос и возвращает результат в виде массива строк
     *
     * @param string $query
     * @param string $keyFieldName
     * @param string $valueFieldName
     * @return array
     */
    public static function QueryToArray($query, $keyFieldName = null, $valueFieldName = null)
    {
        $statement = self::Query($query);
        $res = array();
        while ($row = self::FetchAssoc($statement))
        {
            if ($keyFieldName != null) $res[$row[$keyFieldName]] = ($valueFieldName == null ? $row : $row[$valueFieldName]);
            else $res[] = ($valueFieldName == null ? $row : $row[$valueFieldName]);
        }
        $statement = null;
        return $res;
    }

	private static function ParamTypeStrToPDO($paramType)
	{
		switch ($paramType)
		{
			case 'bool':
				return PDO::PARAM_BOOL;
			case 'int':
			case 'integer':
				return PDO::PARAM_INT;
			case 'str':
			case 'string':
				return PDO::PARAM_NULL;
			default:
				return PDO::PARAM_STR;
		}
	}

    /**
     * Выполняет запрос (с плейсхолдерами) и возвращает результат в виде массива строк
     *
     * @param string $query Запрос
     * @param array $params Параметры @see DB::QueryEx()
     * @param string $keyFieldName
     * @param string $valueFieldName
     * @return array Массив строк результата
     */
	public static function QueryToArrayEx($query, $params, $keyFieldName = null, $valueFieldName = null)
	{
		$statement = self::QueryEx($query, $params);
		if (!$statement)
			return false;

		$res = array();
		while ($row = self::FetchAssoc($statement))
        {
            if ($keyFieldName != null) $res[$row[$keyFieldName]] = ($valueFieldName == null ? $row : $row[$valueFieldName]);
            else $res[] = ($valueFieldName == null ? $row : $row[$valueFieldName]);
        }
		return $res;
	}

	/**
     * Выполняет запрос и возвращает результат в виде массива столбцов
     *
     * @param string $query Запрос
     * @param array $params Параметры @see DB::QueryEx()
     * @param string $keyFieldName
     * @param string $valueFieldName
     * @return array Массив строк результата
     */
    public static function QueryOneRecordToArrayEx($query, $params, $keyFieldName = null, $valueFieldName = null)
	{
		$statement = self::QueryEx($query, $params);
		if (!$statement)
			return false;

		$res = array();
		$row = self::FetchAssoc($statement);
		if ($row)
		{
			if ($keyFieldName != null) $res[$row[$keyFieldName]] = ($valueFieldName == null ? $row : $row[$valueFieldName]);
            else $res = ($valueFieldName == null ? $row : $row[$valueFieldName]);
		}

		return $res;
	}

	/**
     * Подготавливает выражение для последующего выполнения с помощью Execute
     *
     * @param string $query Запрос
     * @return PDOStatement|false Объект запроса
     */
    public static function Prepare($query)
	{
		return self::$PDO->prepare($query);
	}

	/**
     * Выполняет подготовленны с помощью Prepare запрос
	 *
	 * @param PDOStatement $statement
	 * @param array $params Параметры @see DB::QueryEx()
	 * @return PDOStatement|false Объект запроса
	 */
	public static function Execute(PDOStatement $statement, $params)
	{
		foreach ($params as $paramName => $param)
		{
			if (is_string($paramName))
				$paramName = ':' . $paramName;  // named params
			else
				$paramName += 1;  // indexed params

			if (is_array($param) && !empty($param[1]))
				$statement->bindValue($paramName, $param[0], self::ParamTypeStrToPDO($param[1]));
			else
				$statement->bindValue($paramName, is_array($param) ? $param[0] : $param);
		}
		if (!$statement->execute())
			return false;

		return $statement;
	}

	/**
     * Выполняет запрос и возвращает результат в виде массива столбцов
     *
     * @param string $query
     * @param string $keyFieldName
     * @param string $valueFieldName
     * @return array
     */
    public static function QueryOneRecordToArray($query, $keyFieldName = null, $valueFieldName = null)
    {
        $statement = self::Query($query);
        $res = array();
        while ($row = self::FetchAssoc($statement))
        {
            if ($keyFieldName != null) $res[$row[$keyFieldName]] = ($valueFieldName == null ? $row : $row[$valueFieldName]);
            else $res[] = ($valueFieldName == null ? $row : $row[$valueFieldName]);
        }
        if (count($res) > 0)
            $res       = $res[0];
        $statement = null;
        return $res;
    }

    public static function FreeResult($pdoStatement)
    {
        $pdoStatement = null;
    }

    public static function TableExists($tableName)
    {
        $result = self::Query('SHOW TABLES');
        while ($row = self::FetchArray($result))
        {
            if ($tableName == $row[0])
            {
               self::FreeResult($result);
               return true;
            }
        }
        return false;
    }

    public static function Quote($string)
    {
        return self::$PDO -> quote($string);
    }

    /**
     * Начинает новую транзакцию
     *
     * @return bool true в случае успеха, false в случае ошибки
     */
    public static function StartTransaction()
    {
        return self::$PDO ->beginTransaction();
    }

    /**
     * Подтверждает начатую транзакцию
     *
     * @return bool true в случае успеха, false в случае ошибки
     */
    public static function CommitTransaction()
    {
        return self::$PDO ->commit();
    }

    /**
     * Откатывает начатую транзакцию
     *
     * @return bool true в случае успеха, false в случае ошибки
     */
    public static function RollBackTransaction()
    {
        return self::$PDO ->rollBack();
    }

    /**
     * Возвращает ID последней вставленной записи (MySQL)
     *
     * @return int ID записи
     */
    public static function LastInsertID()
    {
        return self::$PDO ->lastInsertId();
    }
    
    /**
     * УДАЛИТ всё содержимое таблицы
     * 
     * операция НЕтранзакционна @see http://fkn.ktu10.com/?q=node/8300
     * 
     * @param string $tableName
     * @return  PDOStatement
     */
    public static function truncateTable($tableName)
    {
        return self::Query("TRUNCATE TABLE `$tableName`;");
    }
    
    /**
     * псевдоним для self::truncateTable()
     * УДАЛИТ всё содержимое таблицы
     * 
     * операция НЕтранзакционна @see http://fkn.ktu10.com/?q=node/8300
     * 
     * @param string $tableName
     * @return  PDOStatement
     */
    public static function deleteTableQuickly($tableName)
    {
        return self::truncateTable($tableName);
    }
}
?>