<?php
require_once('DBFieldStructure.class.php');

/**
 * Класс, представляющий таблицу в базе данных. 
 * Облегчённая версия без связей.
 * Использует новый класс DB с PDO
 * 
 * @version 3.3
 *
 */
class DBTableStructure
{
	/**
	 * Имя таблицы
	 *
	 * @var string
	 */
	public $TableName;

	/**
	 * Список полей
	 *
	 * @var array
	 */
	public $Fields;

	/**
	 * Указатель на первичный ключ таблицы.
	 *
	 * @var DBFieldStructure
	 */
	public $PrimaryKey;

    /**
     * Созданные объекты класса
     *
     * @var array
     */
    public static $instances;

	/**
	 * Конструктор
	 *
	 * @param string $tableName Имя таблицы, с которой работаем.
	 * @return DBTableStructure
	 */
	function __construct($tableName)
	{
		// Проверим корректность параметров. 
		// Если некорректны, то выбрасываем исключение.
		if (!is_string($tableName)) 
			throw new Exception('Некорректные значения параметров. $tableName должны быть строками!');
		// Инициализируем класс
		$this -> TableName = $tableName;
		// Получим информацию о полях таблицы
		//if (!DB::TableExists($this -> TableName))
		//  throw new Exception('Table '.$this -> TableName .' does not exist.', 1222);
 
		$statement =  DB::Query('SHOW COLUMNS FROM ' . $this -> TableName);
		// Забиваем массив полей
		while ($field = DB::FetchAssoc($statement))
		{
		    $fieldType = $field['Type'];
            $match = preg_match('/\((\d+)\)/', $fieldType, $matches);
            $length = 0;
            if ($match !== false && isset($matches[1]))
                $length = $matches[1];

			$isPrimaryKey = $field['Key'] == 'PRI';
			$isUniqueKey = $field['Key'] == 'UNI';
			
			$newField = new DBFieldStructure(
					$field['Field'],
					$fieldType,
					$length,
					false,
					$isPrimaryKey,
					($field['Null'] == 'NO')
				);

			$newField -> IsUnique = $isUniqueKey;
				
			$this -> Fields[] = $newField;
			if ($isPrimaryKey) $this->PrimaryKey = &$this -> Fields[count($this -> Fields) - 1];
		}

	}

    /**
     * Возвращает структуру таблицы
     *
     * @param string $tableName
     * @return DBTableStructure
     */
    public static function getInstance($tableName)
    {
        if (!isset(self::$instances[$tableName]))
        {
            self::$instances[$tableName] = new DBTableStructure($tableName);
        }
        
        return self::$instances[$tableName];
    }
		
	/**
	 * Возвращает массив имен полей
	 *
	 * @return array
	 */
	public function GetFieldsNames()
	{
		$fieldsNames = array();
		
		foreach ($this -> Fields as $field)
		{
			$fieldsNames[] = $field -> Name;
		}
		
		return $fieldsNames;
	}
	
	/**
	 * Превращает данные, о таблице в HTML строку.
	 *
	 * @return HTML строку.
	 */
	public function ToString() 
	{
		$tab = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		$nl = "<br />";
		$res = "<p>Table ".$this->TableName;
		$res .= $nl."Fields:";
		
		$len = count($this -> Fields);
		for ($i = 0; $i < $len; $i++) 
		{
			$res .= $nl.$tab. $this -> Fields[$i] -> ToString();
		}
		return $res."</p>";
		
 	}
}
?>