<?php

/**
 * Типы страниц
 *
 * @version 1.0
 */
class PagesTypes 
{
    protected static $constants = null;
    
    /**
     * Возвращает массив с константами типов страниц
     *
     * @return array
     */
    public static function GetConstants()
    {
        if (empty(self::$constants))
        {
            $pagesTypesTableManager = DBTableManager::getInstance(TablesNames::$PAGES_TYPE_TABLE_NAME);
    		$allPagesTypes = $pagesTypesTableManager -> Select();
    		self::$constants = array();
    		foreach ($allPagesTypes as $type)
    		{
    			self::$constants[$type['constantName']] = $type['pageTypeID'];
    		}
        }
        
        return self::$constants;
    }
}

?>