<?php

/**
 * Класс-обертка для Adjacency List дерева. Строит древовидный массив по данным из БД.
 *
 * @author grom
 */
class ALTree
{
	protected $tree;
	protected $treeIndex;
	protected $idFldName;
	protected $pidFldName;
	protected $pathFldName;
	protected $rootIdVal;
	protected $genFullPathPropName;
	protected $fullPathDelim;
	protected $genChildrenPropName;
	protected $genParentRefPropName;
	protected $genLevelPropName;

	protected $linear;

	/**
	 * Конструктор, строит дерево из плоской таблицы
	 *
	 * @param array $plainTable Плоский массив с данными
	 * @param string $idFldName Название поля-ключа
	 * @param string $pidFldName Название поля ключа родителя
	 * @param string $pathFldName Название поля с названием ноды
	 * @param int $rootIdVal Значение pid для корнеовй ноды
	 * @param string $genFullPathPropName Название генерируемого поля для полного пути
	 * @param string $fullPathDelim Разделитель пути, например '/'
	 * @param string $genChildrenPropName Название генерируемого поля для детей ноды
	 * @param string $genParentRefPropName Название генерируемого поля для родителей ноды (ссылка, может быть NULL)
	 * @param string $genLevelPropName Название генерируемого поля уровня вложенности
	 */
	public function __construct(
		$plainTable,
		$idFldName = 'ID',
		$pidFldName = 'parentID',
		$pathFldName = 'alias',
		$rootIdVal = 0,
		$genFullPathPropName = 'fullAlias',
		$fullPathDelim = '/',
		$genChildrenPropName = 'children',
		$genParentRefPropName = 'parent',
		$genLevelPropName = 'level'
	)
	{
		$this->idFldName = $idFldName;
		$this->pidFldName = $pidFldName;
		$this->pathFldName = $pathFldName;
		$this->rootIdVal = $rootIdVal;
		$this->genFullPathPropName = $genFullPathPropName;
		$this->fullPathDelim = $fullPathDelim;
		$this->genChildrenPropName=$genChildrenPropName;
		$this->genParentRefPropName = $genParentRefPropName;
		$this->genLevelPropName = $genLevelPropName;

		$this->_table2Tree($plainTable, $this->rootIdVal, $this->idFldName, $this->pidFldName, $this->genChildrenPropName);
		$this->_genProps();
//		trace($this->treeIndex);
//		trace($this->tree);

	}

	/**
	 * Находит и возвращает ноду по идентификатору
	 *
	 * @param int $nodeId Идентификатор ноды
	 * @return array Нода
	 */
	public function &FindNode($nodeId)
	{
		return $this->treeIndex[$nodeId];
	}

	/**
	 * Находит и возвращает ноду по полному пути
	 *
	 * @param string|array $path Массив или строка с полным путём ноды
	 * @return array|null Нода или null
	 */
	public function &FindNodeByFullPath($path)
	{
		$nullGuard = null;
		if (is_array($path))
			$path = $this->fullPathDelim . implode($this->fullPathDelim, $path) . $this->fullPathDelim;
		foreach ($this->treeIndex as &$item)
			if ($item[$this->genFullPathPropName] == $path)
				return $item;
		return $nullGuard;
	}

	/**
	 * Возвращает массив всех родителей ноды до корня
	 *
	 * @param int $nodeId Идентификатор ноды
	 * @return array|null Массив родителей или null
	 */
	public function GetParents($nodeId)
	{
		$parents = array();
		$current = $this->FindNode($nodeId);
		if (empty($current))
			return null;
		while (true)
		{
			$parents[] = $current;
			if (!empty($current['parent']))
				$current = $current['parent'];
			else
				break;
		}
		return $parents;
	}

	/**
	 * Выполняет пользовательскую функцию для каждой ноды дерева
	 *
	 * @param callable $callback Функция, вызываемая для каждой ноды
	 */
	public function Iterate($callback)
	{
		$this->_iterate($callback, $this->tree);
	}

	/**
	 * Возвращает дерево в виде массива
	 *
	 * @return array
	 */
	public function ToArray()
	{
		return $this->tree;
	}

	/**
	 * Загоняем дерево в линейный массив. Простой select из БД тут не поможент - ноды надо сортировать в правильном порядке.
	 *
	 * @return array Линейный массив
	 */
	public function Linear()
	{
		if (!empty($this->linear))
			return $this->linear;

		$this->Iterate(array($this, '_linear'));
		return $this->linear;
	}

	/**
	 * Возвращает линейный список всех детей ноды
	 * @param int $nodeId
	 * @return array Массив детей ноды
	 */
	public function GetChildrenRecursive($nodeId)
	{
		$res = array();
		$node = $this->FindNode($nodeId);

		if (!empty($node[$this->genChildrenPropName]))
		{
			foreach ($node[$this->genChildrenPropName] as $child)
			{
				$res[] = $child;
				$res = array_merge($res, $this->GetChildrenRecursive($child[$this->idFldName]));
//				$res[] = array_merge($child, $this->GetChildrenRecursive($child[$this->idFldName]));
			}
		}

		return $res;
	}

	private function _iterate($callback, &$subTree)
	{
		foreach ($subTree as &$val)
		{
//			$callback(&$val);
			call_user_func($callback, $val);
			if (!empty($val[$this->genChildrenPropName]))
				$this->_iterate($callback, $val[$this->genChildrenPropName]);
		}
	}

	private function _genProps(&$treeArray = null, $level = 0)
	{
		if (is_null($treeArray))
			$treeArray = &$this->tree;
		foreach ($treeArray as &$item)
		{
			// gen full path
            if (!empty($this->genFullPathPropName) && !empty($this->pathFldName))
                $item[$this->genFullPathPropName] = (isset($this->treeIndex[$item[$this->pidFldName]]) ? $this->treeIndex[$item[$this->pidFldName]][$this->genFullPathPropName] : $this->fullPathDelim) . $item[$this->pathFldName] . $this->fullPathDelim;
			//$item[$this->genFullPathPropName] = ((isset($item[$this->genParentRefPropName])) ? $item[$this->genParentRefPropName][$this->genFullPathPropName] : $this->fullPathDelim) . $item[$this->pathFldName] . $this->fullPathDelim;
			// gen parent references
			if (!empty($item[$this->genChildrenPropName]))
			{
				if (!empty($this->genParentRefPropName))
				{
					foreach($item[$this->genChildrenPropName] as &$child)
					{
						$child[$this->genParentRefPropName] = &$item;
					}
				}
				// recrsive call
				$this->_genProps($item[$this->genChildrenPropName], $level + 1);
			}
			// gen level
			if (!empty($this->genLevelPropName))
				$item[$this->genLevelPropName] = $level;
		}
	}

	private function _table2Tree($table, $rootId, $idFld, $pidFld, $childrenFld = 'children')
	{
		$this->tree = array();
		$keys = array();
		$nodes = array();
		foreach ($table as &$row)
		{
			$nodes[$row[$idFld]] = $row;
			$keys[] = $row[$idFld];
			unset($row);
		}
		foreach ($keys as $key)
		{
			if ($rootId == $nodes[$key][$pidFld])
				$this->tree[] = &$nodes[$key];
			else
			{
				if (isset($nodes[$nodes[$key][$pidFld]]))
				{
					if (!isset($nodes[$nodes[$key][$pidFld]][$childrenFld]))
						$nodes[$nodes[$key][$pidFld]][$childrenFld] = array();

					$nodes[$nodes[$key][$pidFld]][$childrenFld][] = &$nodes[$key];
				}
			}
		}
		$this->treeIndex = $nodes;

		//return $tree;
	}

	private function _linear($node)
	{
		$_node = $node;
		unset($_node['children']);
		$this->linear[] = $_node;
	}
}
