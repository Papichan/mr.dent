<?php
/**
 * Класс для сортировки и ворматирования массивов,
 * содержащих 2 ключа: ID и parentID, добавляет поле level,
 * соответствующий уровню вложенности и отодвигает текст
 * в зависимости от значения level
 *
 * @author Ghost
 * @version 1.01
 */
class Tree {

    private $separator = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

    public function __construct($separator = null) {
        if ($separator)
            $this->separator = $separator;
    }

    /**
     *
     * @param type $map
     * @param type $source
     * @param type $parent_level
     * @param type $parent_id
     * @param type $idName
     * @param type $result
     */
    public function SortSubtree(&$map, &$source, $parent_level, $parent_id, $idName, &$result) {
//        trace($map);
                //FB::log($map);
                //FB::log($parent_level);
                //FB::log($parent_id);
                //FB::log($idName);

        if (isset($map[$parent_id])) { // у узла есть дети
            foreach ($map[$parent_id] as $map_data) {

                // добавляем в результат все поля + level
                $result[$map_data['key']] = $source[$map_data['key']];
                $result[$map_data['key']]['lvl'] = $parent_level + 1;

                // на уровень ниже
                $this->SortSubtree($map, $source, $parent_level + 1, $map_data[$idName], $idName, $result);
            }
        }
    }

    /**
     * Cортирует и переиндексирует массив,
     * содержащий 2 ключа: ID и parentID, добавляет поле level,
     * соответствующий уровню вложенности и отодвигает текст
     * в зависимости от значения level
     *
     * @param array $source  входящий массив
     * @param string $idName  имя ключевого поля
     * @param string $name имя текстового поля
     * @return array
     *
     * @todo Добавить имя поля parentID как входящий параметр, скорей всего необязательный.
     *
     */
    public function SortTree(&$source, $idName, $name, $parentName, $startLevel = 0) {
        $map = array();
        foreach ($source as $key => $data) {
            if (!isset($data[$parentName]))
                return $source;
            if (!isset($map[$data[$parentName]])) {
                $map[$data[$parentName]] = array();
            }

            $map[$data[$parentName]][] = array($idName => $data[$idName], 'key' => $key);
        }
        $result = array();
        // результирующий массив гоняется по ссылке между рекурсивными вызовами и там формируется
        $this->SortSubtree($map, $source, -1, $startLevel, $idName, $result);
        return $this->TreeToList($result, $name);
    }
    /**
     * Добавляет в начало текстового поля $name [3 пробела] * [уровень вложенности]
     * @param string $data
     * @param string $name  при отсутствии этого параметра функция возвращает исходный массив
     * @return array
     */
    public function TreeToList($data, $name = false) {
        if (!$name)
            return $data;
        foreach ($data as $key => $value) {
            if (isset($value['lvl']))
                $data[$key][$name] = str_repeat($this->separator, $value['lvl']) . $value[$name];
//            trace($data[$key][$name]);
        }
        return array_values($data);
    }

    /**
     *
     * @param type $table
     * @param type $rootId
     * @param type $idFld
     * @param type $pidFld
     * @param type $childrenFld
     * @return type
     */
    public static function Table2Tree($table, $rootId, $idFld, $pidFld, $childrenFld = 'children')
    {
        $tree = array();
        $keys = array();
        $nodes = array();
        foreach ($table as &$row)
        {
            $nodes[$row[$idFld]] = $row;
            $keys[] = $row[$idFld];
            unset($row);
        }

        foreach ($keys as $key)
        {
            if ($rootId == $nodes[$key][$pidFld])
            {
                $tree[] = &$nodes[$key];
            }
            else
            {
//                FB::log($nodes[$key], 'nodes');
				if (isset($nodes[$nodes[$key][$pidFld]]))
                {
                    if (!isset($nodes[$nodes[$key][$pidFld]][$childrenFld]))
                        $nodes[$nodes[$key][$pidFld]][$childrenFld] = array();

                    $nodes[$nodes[$key][$pidFld]][$childrenFld][] = & $nodes[$key];
                }
            }
        }

        return $tree;
    }

    public function AddFullAliasToTree($tree,$childrenField, $aliasField,$fullAliasField, $rootAlias)
    {
        foreach ($tree as $key => $val)
        {
            $tree[$key][$fullAliasField] = $rootAlias.$tree[$key][$aliasField]."/";
            if (isset($val[$childrenField]))
                $tree[$key][$childrenField] = $this->AddFullAliasToTree ($val[$childrenField], $childrenField, $aliasField, $fullAliasField, $tree[$key][$fullAliasField]);
        }
        return $tree;
    }

    /**
     * Рекурсивный метод линеаризации дерева
     *
     * @param array $tree
     * @param int $level
     * @return array
     */
    public function LinearizeTree($tree, $level = 0, $fields = NULL, $withChildren= false)
    {
        $options = array();
        $cnt = count($tree);
        for ($i = 0; $i < $cnt; $i++)
        {
            if (is_array($tree[$i]))
            {
                $array = array();
                foreach($tree[$i] as $key => $val)
                {
                    if ($key != "children")
                    {
                        if (!empty($fields))
                        {
                            if (is_array($fields))
                            {
                                if (in_array($key, $fields))
                                    $array[$key] = $val;
                            }
                            elseif ($key == $fields)
                                $array = $val;
                        }
                        else
                            $array[$key] = $val;
                    }
                }
                $options[] = $array;
            }
            if ($withChildren)
                if (isset($tree[$i]['children']))
                    $options = array_merge($options, $this -> LinearizeTree($tree[$i]['children'], $level + 1,$fields));
        }

        return $options;
    }


}

?>
