<?php
/**
 * Логгер
 * @version 1.1
 * @author George
 */
class Log 
{     
    public static $logFile = 'php.log'; 

    public static function Debug($message) 
    {
        self::Flush('Debug: '.date('d.m.y H:i:s') .' '. $message ."\n");
    } 

    public static function Info($message)
    {
        self::Flush('Info: '.date('d.m.y H:i:s').' '.$message. "\n");
    } 

    public static function Warn($message)
    { 
        self::Flush('Warning: '.date('d.m.y H:i:s').' '.$message. "\n");
    } 

    public static function Error($message)
    { 
        self::Flush('Error: '.date('d.m.y H:i:s').' '.$message ."\n");
    }

    public static function Flush($msg)
    { 
        $fp = fopen(self::$logFile, 'a+');
        if ($fp === false) return;
        flock($fp, LOCK_EX); 
        fwrite($fp, $msg); 
        flock($fp, LOCK_UN); 
        fclose($fp);
    }
}
?>