<?php
/**
 * Класс для генерации капчи
 * @version  1.2
 */
class Captcha
{
    /**
     * Запрос на капчу
     * Возвращает поток.
     *
     */
    public static function GetCaptcha()
    { 
        $FONTS_PATH = IncPaths::$LIBS_PATH.'Captcha/font/';

        header('Content-type: image/png');
        //setcookie('picid',4,0);
        $captcha = rand(1000,9999)."";
        @session_start();

        $_SESSION['CAPTCHA'] = $captcha;

        $fon = imagecreatetruecolor(100,30);
        $fonRed = rand(0, 10) > 5 ? rand(0, 70) : rand(180, 255);
        $fonGreen = rand(0, 10) > 5 ? rand(0, 50) : rand(200, 255);
        $fonBlue = rand(0, 85);
        $fonColor = $fonRed * 256 + $fonGreen * 256 * 256 + $fonBlue;
        imagefill($fon, 0, 0, $fonColor);

        $range = 0;
        
        for($i = 0; $i < 4; $i++)
        {
        	$textRed = 255 - $fonRed;
        	$textGreen = 255 - $fonGreen;
        	$textBlue = 255 - $fonBlue;
        	
        	$textRed   += ($textRed    >= 127 ? -1 : 1) * rand(0, 100);
        	$textGreen += ($textGreen  >= 127 ? -1 : 1) * rand(0, 150);
        	$textBlue  += ($textBlue   >= 127 ? -1 : 1) * rand(0, 50);

        	$textColor = ($textRed) * 256 + $textGreen * 256 * 256 + $textBlue;
        	imagettftext($fon, rand(20, 28), rand(0, 35), 10 + $range, 30, $textColor, $FONTS_PATH.rand(1,10).'.ttf', $captcha[$i]);
        	$range += $i < 4 ? rand(15, 27) : 10;
        }
        
        $lineNum = rand(20, 50);
        for($i = 0; $i < $lineNum; $i++)
        {
        	imageline($fon, rand(0,100), rand(0,30), rand(0,100), rand(0,30), rand());
        }
        //imagestring ($fon, 5, 55, 5, $chislo+1, imagecolorallocate($fon, 233, 0, 91));
        imagepng($fon);
        imagedestroy($fon);
    }

    /**
     * Проверка капчи
     *
     * @param string $chislo
     * @return boolean
     */
    public static function CheckCaptcha($captcha)
    {
        @session_start();	
        $ok = (isset($_SESSION['CAPTCHA']) && ($captcha == $_SESSION['CAPTCHA']));
        unset($_SESSION['CAPTCHA']);
        return $ok;
    }
} 
?>