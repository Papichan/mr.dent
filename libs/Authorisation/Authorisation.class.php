<?php
/**
 * Содержит методы авторизации пользователя
 *
 * @version 1.1
 *
 */
class Authorisation
{
    const EXPECTED_POST_VAR_LOGIN_ABSENT = 1;
    const EXPECTED_POST_VAR_PASSWORD_ABSENT = 2;
    const EXPECTED_COOKIE_VAR_LOGIN_ABSENT = 3;
    const EXPECTED_COOKIE_VAR_PASSWORD_ABSENT = 4;
    const USER_NOT_FOUND = 5;
    const WRONG_PASSWORD = 6;
    const USER_NOT_ACTIVE = 7;
    const USER_NOT_ROLE = 8;

    /**
     * Проверка авторизации по логину и паролю, переданным через POST
     *
     * @param string $loginKey
     * @param string $passwordKey
     * @param bool $saveInCookies
     * @param bool $checkLogin
     * @param bool $checkEmail
     * @return mixed
     */
    public static function PostAuthorisation($admin = true, $loginKey = 'username', $passwordKey = 'userpass', $saveInCookies = true, $checkLogin = true, $checkEmail = true)
    {
        if (empty(Request::$POST[$loginKey]))
            return self::EXPECTED_POST_VAR_LOGIN_ABSENT;

        if (empty(Request::$POST[$passwordKey]))
            return self::EXPECTED_POST_VAR_PASSWORD_ABSENT;

        $username = Request::$POST[$loginKey];
        $userpass = Request::$POST[$passwordKey];

        $res = self::GetUser($username, $userpass, $checkLogin, $checkEmail, $admin);

        if (is_array($res) && $saveInCookies)
        {
            $inTwoMonths = 60 * 60 * 24 * 60 + time();
            setcookie($loginKey, $username, $inTwoMonths, '/');
            $userpassHashed = $res['password'];
            setcookie($passwordKey, $userpassHashed, $inTwoMonths, '/');
            DBLog::Info('Authorisation, username=%s, userID=%d', array($username, $res['userID']));
        }
        else if (!is_array($res))
            DBLog::Warning('Authorisation failed, username=%s, code=%d', array($username, $res));

        return $res;
    }

    /**
     * Проверка авторизации по логину и паролю, переданным через COOKIE
     *
     * @param string $loginKey
     * @param string $passwordKey
     * @param bool $checkLogin
     * @param bool $checkEmail
     *
     * @return mixed
     */
    public static function CookieAuthorisation($loginKey = 'username', $passwordKey = 'userpass',  $checkLogin = true, $checkEmail = true)
    {
        if (empty(Request::$COOKIE[$loginKey]))
            return self::EXPECTED_COOKIE_VAR_LOGIN_ABSENT;

        if (empty(Request::$COOKIE[$passwordKey]))
            return self::EXPECTED_COOKIE_VAR_PASSWORD_ABSENT;

        $username = Request::$COOKIE[$loginKey];
        $userpass = Request::$COOKIE[$passwordKey];

        return self::GetUser($username, $userpass, $checkLogin, $checkEmail);
    }

    /**
     * Поиск пользователя по логину и паролю, используемый функциями авторизации
     *
     * @param string $username
     * @param string $userpass
     * @param string $checkLogin
     * @param string $checkEmail
     * @return mixed
     */
    private static function GetUser($username, $userpass,  $checkLogin = true, $checkEmail = true, $admin = true)
    {
        $usersManager = UsersManager::getInstance();

        $attrs = array();
        if ($checkEmail)
        {
            $attrs['email'] = $username;
            $user = $usersManager -> GetUser($attrs);
        }

        if (empty($user) && $checkLogin)
        {
            unset($attrs['email']);
            $attrs['login'] = $username;
            $user = $usersManager -> GetUser($attrs);
        }

        if (empty($user))
            return self::USER_NOT_FOUND;

        $t_hasher = new PasswordHash();
        $check = $t_hasher->CheckPassword($userpass, Password::$SALT, $user['password']);

        if (!$check)
            return self::WRONG_PASSWORD;

        if (!$user['isActive'])
            return self::USER_NOT_ACTIVE;
        
        if ($admin && ($user['role'] == 'user'))
            return self::USER_NOT_ROLE;
        
        return $user;
    }
}
?>