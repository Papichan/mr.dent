<?php

class WWEGSearcher extends Searcher 
{
    public $foundProgramsCnt = 0;
    public $foundNewsCnt = 0;
    public $foundCountryPagesCnt = 0;
    public $foundFAQCnt = 0;
    public $foundSimplePagesCnt = 0;
    public $foundSidePagesCnt = 0;
    
    private $programs = array();
    private $news = array();
    private $countryPages = array();
    private $faq = array();
    private $sidePages = array();
    
    private $programsCnt = 0;
    private $newsCnt = 0;
    private $countryPagesCnt = 0;
    private $faqCnt = 0;
    private $sidePagesCnt = 0;
    
    private $p = 0;
    private $n = 0;
    private $cp = 0;
    private $f = 0;
    private $s = 0;
    
    public function &Search($query, $start = 0, $limit = 0)
    {
        $this -> programs = $this -> SearchPrograms($query, 0, $limit + $start);
        $this -> news = $this -> SearchNews($query, 0, $limit + $start);
        $this -> countryPages = $this -> SearchCountryPages($query, 0, $limit + $start);
        $this -> sidePages = $this -> SearchSidePages($query, 0, $limit + $start);
        
        $this -> programsCnt = count($this -> programs);
        $this -> newsCnt = count($this -> news);
        $this -> countryPagesCnt = count($this -> countryPages);
        $this -> sidePagesCnt = count($this -> sidePages);
        
        //trace($this -> programs);
        //trace($this -> news);
        //trace($this -> countryPages);
        
        // результат
        $r = 0;
        $results = array();
                
        $i = 0;
        while ($r < $limit)
        {
            $i++;
            $nextItem = $this -> GetNext();

            if (empty($nextItem))
                break;
                
            if ($i <= $start)
                continue;
                
            $results[] = $nextItem;
            $r++;
        }
        
		return $results;
    }
    
    public function FoundItems()
    {
        return  $this -> foundProgramsCnt + 
                $this -> foundCountryPagesCnt + 
                $this -> foundFAQCnt + 
                $this -> foundNewsCnt + 
                $this -> foundSimplePagesCnt + 
                $this -> foundSidePagesCnt;
    }
    
    private function GetNext()
    {
        $fromArray = '';
        $rel = 0;
        
        if ($this -> p < $this -> programsCnt)
        {
            if ($this -> programs[$this -> p]['relevancy'] > $rel)
            {
                $rel = $this -> programs[$this -> p]['relevancy'];
                $fromArray = 'programs';
            }
        }
        
        if ($this -> n < $this -> newsCnt)
        {
            if ($this -> news[$this -> n]['relevancy'] > $rel)
            {
                $rel = $this -> news[$this -> n]['relevancy'];
                $fromArray = 'news';
            }
        }
        
        if ($this -> cp < $this -> countryPagesCnt)
        {
            if ($this -> countryPages[$this -> cp]['relevancy'] > $rel)
            {
                $rel = $this -> countryPages[$this -> cp]['relevancy'];
                $fromArray = 'countryPages';
            }
        }
        
        if ($this -> s < $this -> sidePagesCnt)
        {
            if ($this -> sidePages[$this -> s]['relevancy'] > $rel)
            {
                $rel = $this -> sidePages[$this -> s]['relevancy'];
                $fromArray = 'sidePages';
            }
        }
        
        if (empty($fromArray))
            return null;
            
        $returnItem = null;
        switch ($fromArray)
        {
            case 'programs':
                $returnItem = $this -> programs[$this -> p];
                $returnItem['resultType'] = 'program';
                $this -> p++;
                
                $this -> HighlightProgram($returnItem);
                break;
                
            case 'news':
                $returnItem = $this -> news[$this -> n];
                $returnItem['resultType'] = 'news';
                $this -> n++;
                
                $struct = SiteStructure::getInstance();
                $returnItem['fullAlias'] = $struct -> GetFullAlias($returnItem['parentID']) . $returnItem['alias'] .'/';
                
                $this -> HighlightNewsItem($returnItem);
                break;
                
            case 'countryPages':
                $returnItem = $this -> countryPages[$this -> cp];
                $returnItem['resultType'] = 'countryPage';
                $this -> cp++;
                
                $struct = SiteStructure::getInstance();
                $returnItem['fullAlias'] = $struct -> GetFullAlias($returnItem['parentID']) . $returnItem['alias'] .'/';
                
                $this -> HighlightCountryPage($returnItem);
                break;
                
            case 'sidePages':
                $returnItem = $this -> sidePages[$this -> s];
                $returnItem['resultType'] = 'sidePage';
                $this -> s++;
                
                $struct = SiteStructure::getInstance();
                $returnItem['fullAlias'] = $struct -> GetFullAlias($returnItem['parentID']) . $returnItem['alias'] .'/';
                
                $this -> HighlightSidePage($returnItem);
                break;
        }
        
        return $returnItem;
    }
    
    public function SearchPrograms($query, $start = 0, $limit = 0, $addInfo = false)
    {
        $searchQuery = $this -> GetSearchQuery($query);
         
        // 'material' as resultType
        $dbQuery = '
            SELECT SQL_CALC_FOUND_ROWS 
                p.*,
            MATCH(  
                p.`title`,
    		    p.`description`,
    		    p.`info` ) 
            AGAINST (\''.$searchQuery.'\') AS relevancy 
            FROM    `'. ProgramsManager::PROGRAMS_TABLE_NAME .'` p
            HAVING  relevancy > 0.5 
            ORDER BY relevancy DESC
            LIMIT '. $start .', '. $limit .'
        ';
        
        $result = DB::QueryToArray($dbQuery);
        $this -> foundProgramsCnt = DB::QueryOneValue('SELECT FOUND_ROWS() as cntVal');
        
        $programsManager = ProgramsManager::getInstance();
        $programsManager -> AddFullAliases($result);
        
        return $result;
    }
    
    private function HighlightProgram(&$program)
	{
	    $replacement = '<span style="background-color: Yellow;">$1</span>';
	    $quoted1 = preg_quote('<span style="background-color: Yellow;">');
        $quoted2 = preg_quote('</span>');
	    
	    if (!empty($program['description']))
	    {
	        $stripped = strip_tags($program['description']);
	        $replaced = preg_replace($this -> keyWordsForHighlight, $replacement, $stripped, 1, $matchesCnt);
	        
	        $pattern = '#'. $quoted2 .'\s*'. $quoted1 .'#';
	        $replaced = preg_replace($pattern, ' ', $replaced);
	        
	        $pattern = '#(.*?)'. $quoted1 . $quoted1 .'(.*?)'. $quoted2 .'(.*?)'. $quoted2 .'(.*?)#';
	        $replaced = preg_replace($pattern, '${1}<span style="background-color: Yellow;">${2}${3}</span>${4}', $replaced);

	        if ($matchesCnt > 0)
	        {
	            $pattern = '#\s*(.{0,200})('. $quoted1 .'.*?'. $quoted2 .')(.{0,200})\s*#ui';
	            //$pattern = '#\s(.{0,100})(<span style="background-color: Yellow;">.*?)(.{0,100})\s#ui';
	            /*$pattern = '/<span.*?>.*?<\/span>/';*/
	            $hasMatches = preg_match($pattern, $replaced, $matches);
	            if ($hasMatches)
    	        {
    	            $program['searchResultText'] = $matches[1] . $matches[2] . $matches[3];
    	            return;
    	        }
	        }
	    }
	    
	    if (!empty($program['info']))
	    {
	        $stripped = strip_tags($program['info']);
	        $replaced = preg_replace($this -> keyWordsForHighlight, $replacement, $stripped, 1, $matchesCnt);

	        $pattern = '#'. $quoted2 .'\s*'. $quoted1 .'#';
	        $replaced = preg_replace($pattern, ' ', $replaced);
	        
	        $pattern = '#(.*?)'. $quoted1 . $quoted1 .'(.*?)'. $quoted2 .'(.*?)'. $quoted2 .'(.*?)#';
	        $replaced = preg_replace($pattern, '${1}<span style="background-color: Yellow;">${2}${3}</span>${4}', $replaced);

	        if ($matchesCnt > 0)
	        {
	            $pattern = '#\s*(.{0,200})('. $quoted1 .'.*?'. $quoted2 .')(.{0,200})\s*#ui';
	            //$pattern = '#\s(.{0,100})(<span style="background-color: Yellow;">.*?)(.{0,100})\s#ui';
	            /*$pattern = '/<span.*?>.*?<\/span>/';*/
	            $hasMatches = preg_match($pattern, $replaced, $matches);
	            if ($hasMatches)
    	        {
    	            $program['searchResultText'] = $matches[1] . $matches[2] . $matches[3];
    	            return;
    	        }
	        }
	    }
	}
	
	public function SearchNews($query, $start = 0, $limit = 0, $addInfo = false)
	{
        $searchQuery = $this -> GetSearchQuery($query);
        $pagesTypes = PagesTypes::GetConstants();
         
        // 'material' as resultType
        $dbQuery = '
            SELECT SQL_CALC_FOUND_ROWS 
                n.*,
                ps.`pageID`, ps.`parentID`, ps.`alias`,
            MATCH(  
                n.`title`,
    		    n.`summary`,
    		    n.`text` ) 
            AGAINST (\''.$searchQuery.'\') AS relevancy 
            FROM    `'. NewsManager::NEWS_TABLE_NAME .'` n
            JOIN    `'. TablesNames::$PAGE_STRUCTURE_TABLE_NAME .'` ps
                ON  n.`newsID` = ps.`contentID`
            WHERE   ps.`pageTypeID` = '. $pagesTypes[NewsManager::NEWS_PAGE_ID] .'
                AND ps.`isActive` = 1
                AND ps.`isDeleted` = 0
            HAVING  relevancy > 0.5 
            ORDER BY relevancy DESC
            LIMIT '. $start .', '. $limit .'
        ';
        

        $result = DB::QueryToArray($dbQuery);
        $this -> foundNewsCnt = DB::QueryOneValue('SELECT FOUND_ROWS() as cntVal');
        
        return $result;
    }
    
    private function HighlightNewsItem(&$news)
	{
	    $replacement = '<span style="background-color: Yellow;">$1</span>';
	    $quoted1 = preg_quote('<span style="background-color: Yellow;">');
        $quoted2 = preg_quote('</span>');

	    if (!empty($news['summary']))
	    {
            $replaced = preg_replace($this -> keyWordsForHighlight, $replacement, $news['summary'], -1, $matchesCnt);
            if ($matchesCnt > 0)
            {
                $news['searchResultText'] = $replaced;
                return;
            }
	    }
	    
	    if (!empty($news['text']))
	    {
	        $stripped = strip_tags($news['text']);
	        $replaced = preg_replace($this -> keyWordsForHighlight, $replacement, $stripped, 1, $matchesCnt);
	        
	        $pattern = '#'. $quoted2 .'\s*'. $quoted1 .'#';
	        $replaced = preg_replace($pattern, ' ', $replaced);
	        
	        $pattern = '#(.*?)'. $quoted1 . $quoted1 .'(.*?)'. $quoted2 .'(.*?)'. $quoted2 .'(.*?)#';
	        $replaced = preg_replace($pattern, '${1}<span style="background-color: Yellow;">${2}${3}</span>${4}', $replaced);

	        if ($matchesCnt > 0)
	        {
	            $pattern = '#\s*(.{0,200})('. $quoted1 .'.*?'. $quoted2 .')(.{0,200})\s*#ui';
	            //$pattern = '#\s(.{0,100})(<span style="background-color: Yellow;">.*?)(.{0,100})\s#ui';
	            /*$pattern = '/<span.*?>.*?<\/span>/';*/
	            $hasMatches = preg_match($pattern, $replaced, $matches);
	            if ($hasMatches)
    	        {
    	            $news['searchResultText'] = $matches[1] . $matches[2] . $matches[3];
    	            return;
    	        }
	        }
	    }
	}
	
	public function SearchCountryPages($query, $start = 0, $limit = 0, $addInfo = false)
	{
        $searchQuery = $this -> GetSearchQuery($query);
        $pagesTypes = PagesTypes::GetConstants();
         
        // 'material' as resultType
        $dbQuery = '
            SELECT SQL_CALC_FOUND_ROWS 
                cp.*,
                ps.`pageID`, ps.`parentID`, ps.`alias`,
            MATCH(  
                cp.`title`,
    		    cp.`previewText`,
    		    cp.`content`,
    		    cp.`rightPanel` ) 
            AGAINST (\''.$searchQuery.'\') AS relevancy 
            FROM    `'. CountriesManager::COUNTRY_PAGE_TABLE_NAME .'` cp
            JOIN    `'. TablesNames::$PAGE_STRUCTURE_TABLE_NAME .'` ps
                ON  cp.`countryPageID` = ps.`contentID`
            WHERE   ps.`pageTypeID` = '. $pagesTypes[CountriesManager::COUNTRY_PAGE_TYPE_ID] .'
                AND ps.`isActive` = 1
                AND ps.`isDeleted` = 0
            HAVING  relevancy > 0.5 
            ORDER BY relevancy DESC
            LIMIT '. $start .', '. $limit .'
        ';
        
        $result = DB::QueryToArray($dbQuery);
        $this -> foundCountryPagesCnt = DB::QueryOneValue('SELECT FOUND_ROWS() as cntVal');
        
        return $result;
    }
    
    private function HighlightCountryPage(&$countryPage)
	{
	    $replacement = '<span style="background-color: Yellow;">$1</span>';
	    $quoted1 = preg_quote('<span style="background-color: Yellow;">');
        $quoted2 = preg_quote('</span>');

	    if (!empty($countryPage['previewText']))
	    {
            $replaced = preg_replace($this -> keyWordsForHighlight, $replacement, $countryPage['previewText'], -1, $matchesCnt);
            if ($matchesCnt > 0)
            {
                $countryPage['searchResultText'] = $replaced;
                return;
            }
	    }
	    
	    if (!empty($countryPage['content']))
	    {
	        $stripped = strip_tags($countryPage['content']);
	        $replaced = preg_replace($this -> keyWordsForHighlight, $replacement, $stripped, 1, $matchesCnt);
	        
	        $pattern = '#'. $quoted2 .'\s*'. $quoted1 .'#';
	        $replaced = preg_replace($pattern, ' ', $replaced);
	        
	        $pattern = '#(.*?)'. $quoted1 . $quoted1 .'(.*?)'. $quoted2 .'(.*?)'. $quoted2 .'(.*?)#';
	        $replaced = preg_replace($pattern, '${1}<span style="background-color: Yellow;">${2}${3}</span>${4}', $replaced);

	        if ($matchesCnt > 0)
	        {
	            $pattern = '#\s*(.{0,200})('. $quoted1 .'.*?'. $quoted2 .')(.{0,200})\s*#ui';
	            //$pattern = '#\s(.{0,100})(<span style="background-color: Yellow;">.*?)(.{0,100})\s#ui';
	            /*$pattern = '/<span.*?>.*?<\/span>/';*/
	            $hasMatches = preg_match($pattern, $replaced, $matches);
	            if ($hasMatches)
    	        {
    	            $countryPage['searchResultText'] = $matches[1] . $matches[2] . $matches[3];
    	            return;
    	        }
	        }
	    }
	    
	    if (!empty($countryPage['rightPanel']))
	    {
	        $stripped = strip_tags($countryPage['rightPanel']);
	        $replaced = preg_replace($this -> keyWordsForHighlight, $replacement, $stripped, 1, $matchesCnt);
	        
	        $pattern = '#'. $quoted2 .'\s*'. $quoted1 .'#';
	        $replaced = preg_replace($pattern, ' ', $replaced);
	        
	        $pattern = '#(.*?)'. $quoted1 . $quoted1 .'(.*?)'. $quoted2 .'(.*?)'. $quoted2 .'(.*?)#';
	        $replaced = preg_replace($pattern, '${1}<span style="background-color: Yellow;">${2}${3}</span>${4}', $replaced);

	        if ($matchesCnt > 0)
	        {
	            $pattern = '#\s*(.{0,200})('. $quoted1 .'.*?'. $quoted2 .')(.{0,200})\s*#ui';
	            //$pattern = '#\s(.{0,100})(<span style="background-color: Yellow;">.*?)(.{0,100})\s#ui';
	            /*$pattern = '/<span.*?>.*?<\/span>/';*/
	            $hasMatches = preg_match($pattern, $replaced, $matches);
	            if ($hasMatches)
    	        {
    	            $countryPage['searchResultText'] = $matches[1] . $matches[2] . $matches[3];
    	            return;
    	        }
	        }
	    }
	}
	
	public function SearchSidePages($query, $start = 0, $limit = 0, $addInfo = false)
	{
        $searchQuery = $this -> GetSearchQuery($query);
        $pagesTypes = PagesTypes::GetConstants();
         
        // 'material' as resultType
        $dbQuery = '
            SELECT SQL_CALC_FOUND_ROWS 
                s.*, ps.`caption` as title,
                ps.`pageID`, ps.`parentID`, ps.`alias`,
            MATCH(  
                s.`content`,
    		    s.`sidePanel` ) 
            AGAINST (\''.$searchQuery.'\') AS relevancy 
            FROM    `'. SimplePageController::SIDE_PANLE_PAGE_TABLE_NAME .'` s
            JOIN    `'. TablesNames::$PAGE_STRUCTURE_TABLE_NAME .'` ps
                ON  s.`sidePanelPageID` = ps.`contentID`
            WHERE   ps.`pageTypeID` = '. $pagesTypes['SIDE_PANEL_PAGE_ID'] .'
                AND ps.`isActive` = 1
                AND ps.`isDeleted` = 0
            HAVING  relevancy > 0.5 
            ORDER BY relevancy DESC
            LIMIT '. $start .', '. $limit .'
        ';
        
        $result = DB::QueryToArray($dbQuery);
        $this -> foundSidePagesCnt = DB::QueryOneValue('SELECT FOUND_ROWS() as cntVal');
        
        return $result;
    }
    
    private function HighlightSidePage(&$sidePage)
	{
	    $replacement = '<span style="background-color: Yellow;">$1</span>';
	    $quoted1 = preg_quote('<span style="background-color: Yellow;">');
        $quoted2 = preg_quote('</span>');
	    
	    if (!empty($sidePage['content']))
	    {
	        $stripped = strip_tags($sidePage['content']);
	        $replaced = preg_replace($this -> keyWordsForHighlight, $replacement, $stripped, 1, $matchesCnt);
	        
	        $pattern = '#'. $quoted2 .'\s*'. $quoted1 .'#';
	        $replaced = preg_replace($pattern, ' ', $replaced);
	        
	        $pattern = '#(.*?)'. $quoted1 . $quoted1 .'(.*?)'. $quoted2 .'(.*?)'. $quoted2 .'(.*?)#';
	        $replaced = preg_replace($pattern, '${1}<span style="background-color: Yellow;">${2}${3}</span>${4}', $replaced);

	        if ($matchesCnt > 0)
	        {
	            $pattern = '#\s*(.{0,200})('. $quoted1 .'.*?'. $quoted2 .')(.{0,200})\s*#ui';
	            //$pattern = '#\s(.{0,100})(<span style="background-color: Yellow;">.*?)(.{0,100})\s#ui';
	            /*$pattern = '/<span.*?>.*?<\/span>/';*/
	            $hasMatches = preg_match($pattern, $replaced, $matches);
	            if ($hasMatches)
    	        {
    	            $sidePage['searchResultText'] = $matches[1] . $matches[2] . $matches[3];
    	            return;
    	        }
	        }
	    }
	    
	    if (!empty($sidePage['sidePanel']))
	    {
	        $stripped = strip_tags($sidePage['sidePanel']);
	        $replaced = preg_replace($this -> keyWordsForHighlight, $replacement, $stripped, 1, $matchesCnt);
	        
	        $pattern = '#'. $quoted2 .'\s*'. $quoted1 .'#';
	        $replaced = preg_replace($pattern, ' ', $replaced);
	        
	        $pattern = '#(.*?)'. $quoted1 . $quoted1 .'(.*?)'. $quoted2 .'(.*?)'. $quoted2 .'(.*?)#';
	        $replaced = preg_replace($pattern, '${1}<span style="background-color: Yellow;">${2}${3}</span>${4}', $replaced);

	        if ($matchesCnt > 0)
	        {
	            $pattern = '#\s*(.{0,200})('. $quoted1 .'.*?'. $quoted2 .')(.{0,200})\s*#ui';
	            //$pattern = '#\s(.{0,100})(<span style="background-color: Yellow;">.*?)(.{0,100})\s#ui';
	            /*$pattern = '/<span.*?>.*?<\/span>/';*/
	            $hasMatches = preg_match($pattern, $replaced, $matches);
	            if ($hasMatches)
    	        {
    	            $sidePage['searchResultText'] = $matches[1] . $matches[2] . $matches[3];
    	            return;
    	        }
	        }
	    }
	}
}

?>