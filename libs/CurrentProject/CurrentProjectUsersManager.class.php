<?php

require_once(IncPaths::$LIBS_PATH .'UsersManager/UsersWithInfoManager.class.php');

/**
 * Класс работы с пользователями трансплантологами.
 *
 * @version 1.0
 */
class CurrentProjectUsersManager extends UsersWithInfoManager 
{
	function __construct()
	{
		parent::__construct();
	}
	
	public static function getInstance()
    {
        $childClassName = @func_get_arg(0);
        return parent::getInstance(!empty($childClassName) ? $childClassName : get_class());
    }
}

?>