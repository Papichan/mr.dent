<?php



/**
 * (в_БП) -- весь класс
 * 
 * Класс для работы с меню
 */
class Menu
{
    
    public static $activeName = 'is_active'; 
    
    /**
     * Входная функция - для обхода массива меню и определения активных элементов
     * 
     * @param  array  $menuArray -- массив разных меню, уже внутри которых находятся иерархии их элементов
     * @return array
     */
    public static function setActiveElementsInFirstLevel(&$menuArray)
    {           
        foreach ($menuArray as $key => $menu) {
            self::setActiveElements($menuArray[$key]);
        }
    
        return $menuArray;
    }
    
    /**
     * Рекурсивная функция -- обойдёт элементы, найдёт актиные, в случае необходимости выставит родительские элементы активными,
     *  если они таковыми ещё не выставлены, а среди их потомков есть активные
     * 
     * @see В данный момент имеет место попытка просто определить активный элемент по первому сегменту url-а это оказывается полезным для выставления актиных элементов 
     *     в меню главного уровня
     * 
     * @param type $menu
     * @return int
     */
    public static function setActiveElements(&$menu)
    {          
        $known = false;

        foreach ($menu as $itemKey => $menuItem) {
            
            // определим активен ли раздел
            if (self::isThisAliasActive($menuItem['alias'])) {
                $menu[$itemKey][self::$activeName] = 1;
                $known = true; // элемент на это уровне уже найден
            }

            // рекурсивный вызов для потомков
            if (!empty($menu[$itemKey]['children']) && is_array($menu[$itemKey]['children'])) {
                self::setActiveElements($menu[$itemKey]['children']);

                 if (empty($menu[$itemKey][self::$activeName]) // если родительский элемент не активен
                         && self::hasActiveChild($menu[$itemKey]['children'])) { // но есть активные потомки
                     $menu[$itemKey][self::$activeName] = 1; // отмечаем активность родителя, в следствие активности потомка
                 }
            }
            
            if ($known) { // если активный элемент на этом уровне найден, то не будем смотреть другие эементы и их потоков
                break;
            }
        }

        return $menu;
    }
    
    /**
     * ВАЖНО!! В этой функции принимается решение о том - является ли alias (фактичеки - путь)
     * данного элемента меню соответствующим понятию "активный".
     * 
     * @param string $alias
     * @return bool
     */
    public static function isThisAliasActive($alias)
    {
        $aliases = Request::GetAliases();
        $currentFirstLevel =  $aliases[0];
        
        $result = (self::getFirstLevelFromPath($alias) == $currentFirstLevel); // просто сравним первые сегменты массива фрагментов URL и массива сегментов пути элемента меню
        
        return $result;
    }
    
    /**
     * Извлечет первый сегмент из пути - адреса элемента меню (в том видео в котором они предоставляются в данный момент)
     */
    public static function getFirstLevelFromPath($path)
    {
        $frags = explode('/', $path);
        if (!empty($frags[1])) {
            $frag = $frags[1];
        } else {
            $frag = $frags[0];
        }
        
        $element = str_replace('#', '', $frag);
        return $element;
    }
    
    /**
     * Определит - если ли среди элементов активный
     */
    public static function hasActiveChild($menuItems)
    {
        $result = false;
        foreach ($menuItems as $key => $Item) {
            if (!empty($Item[self::$activeName])) {
                $result = true;
                break;
            }
            
        }
        
        return $result;
    }
}
?>