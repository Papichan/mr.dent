<?php
/**
 * Пути битплатформы
 *
 * @version 1.0
 */
class IncPaths
{
    /**
     * Абсолютный путь корня
     *
     * @var string
     */
    public static $ROOT_PATH = '/';

    /**
     * Физический путь приложения
     *
     * @var string
     */
    public static $APPLICATION_PATH = 'application/';
    /**
     * Физический путь настроек
     *
     * @var string
     */
    public static $CONFIG_PATH      = 'config/';

    /**
     * Физический путь папки с библиотеками
     *
     * @var string
     */
    public static $LIBS_PATH = 'libs/';

    /**
     * Физический путь папки с ядром платформы
     *
     * @var string
     */
    public static $CORE_PATH = 'core/';

    /**
     * Физический путь папки с модулями
     *
     * @var string
     */
    public static $MODULES_PATH = 'modules/';

    /**
     * Путь к папке с внешними библиотеками
     *
     * @var string
     */
    public static $EXTERNAL_LIBS_PATH = 'externalLibs/';

    public static function Init()
    {
        $fullDirPath = str_replace('\\', '/', getcwd());
        $localDirPath = substr($_SERVER['SCRIPT_NAME'], 0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
        $rootDir =  str_replace($localDirPath, '/', $fullDirPath) . '/';

        // Инициализация переменных
        self::$ROOT_PATH          = str_replace('//', '/', $rootDir . self::$ROOT_PATH);
        self::$APPLICATION_PATH   = self::$ROOT_PATH . self::$APPLICATION_PATH;
        self::$CONFIG_PATH        = self::$APPLICATION_PATH . self::$CONFIG_PATH;
        self::$LIBS_PATH          = self::$ROOT_PATH . self::$LIBS_PATH;
        self::$CORE_PATH          = self::$ROOT_PATH . self::$CORE_PATH;
        self::$MODULES_PATH       = self::$ROOT_PATH . self::$MODULES_PATH;
        self::$EXTERNAL_LIBS_PATH = self::$ROOT_PATH . self::$EXTERNAL_LIBS_PATH;
    }
}
?>