<footer class="footer">
    <div class="footer__up">
        <div class="content-wrapper ">

            <!-- Вывод основных разделов сайта -->
            {if !empty($MAIN_MENU) }
                <ul class="footer__menu">   
                    {assign var="count" value=$MAIN_MENU|@count}

                    {section name = mySection start=0 loop=$count step=1}
                        <li><a href="{$MAIN_MENU[$smarty.section.mySection.index].fullAlias}">
                                {$MAIN_MENU[$smarty.section.mySection.index].caption}
                        </a></li>
                    {/section}
                </ul>
            {/if}

            <!-- Вывод всехразделов для категории Сервиса -->
            <div class="footer__services">
                {if !empty($SLICE_SERVICES_FOOTER_MENU)}
                    {foreach from=$SLICE_SERVICES_FOOTER_MENU item=se}
                        <div class="footer__block">
                    {foreach from=$se item=services}
                            <span class="footer__block-title">{$services.name}</span>
                            <ul class="footer__block-list">
                                    {foreach from=$services.children item=serv}
                                        <li><a href="/services/{$services.alias}/{$serv.alias}">{$serv.name}</a></li>
                                    {/foreach}
                            </ul>
                    {/foreach}
                        </div>
                    {/foreach}
                {/if}
            </div>
        </div>
    </div>


    <!-- СОЦ СЕТИ И ПРОЧЕЕ  -->
    <div class="footer__down">
        <div class="footer__social content-wrapper">
            <div class="footer__social-wrapper footer__left">
                <div class=" footer__item footer__address">{$SETTINGS.general.addressTown} <br>{$SETTINGS.general.addressStreet}</div>
                <div class="footer__item footer__number"><a href="tel:{$SETTINGS.general.telephone}">{$SETTINGS.general.telephone}</a></div>
                <div class="footer__item  footer__mail"><a href="mailto:{$SETTINGS.general.email}">{$SETTINGS.general.email}</a></div>
            </div>
            <div class="footer__social-wrapper footer__right">
                <a href="{$SETTINGS.front.vkontakte}"><img width="33" height="33" src="{$IMAGES_ALIAS}icon_vk.png" alt="Vkontakte"></a>
                <a href="{$SETTINGS.front.facebook}"><img width="33" height="33" src="{$IMAGES_ALIAS}icon_fb.png" alt="Facebook"></a>
                <a href="{$SETTINGS.front.twitter}"><img width="33" height="33" src="{$IMAGES_ALIAS}icon_twitter.png" alt="Twitter"></a>
            </div>
        </div>


    </div>
    </div>
</footer>



<!--  СТАНДАРТНЫЙ ФУТЕР МИТЛАБСА И ПОДКЛЮЧЕНИЕ СТИЛЕЙ -->
<!--
<div id="footer">
    {$SETTINGS.general.siteName} &copy; {$smarty.now|date_format:"%Y"} <a href="http://mitlabs.ru/" title="Компания МИТЛабс">МИТЛабс</a>
</div>
-->

<!-- Подключение скриптов и jQuery -->
{include file="scripts.tpl"}