<script type="text/javascript" src="{$SCRIPTS_ALIAS}jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="{$SCRIPTS_ALIAS}script.js"></script>

<script type="text/javascript" src="{$SCRIPTS_ALIAS}jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="{$SCRIPTS_ALIAS}jquery-migrate-3.0.1.min.js"></script>
<script type="text/javascript" src="{$SCRIPTS_ALIAS}slick.js"></script>
<script type="text/javascript" src="{$SCRIPTS_ALIAS}jquery.simplebox.min.js"></script>
<script type="text/javascript" src="{$SCRIPTS_ALIAS}main.js"></script>

{if isset($SCRIPT_FILE)}
    {foreach from=$SCRIPT_FILE item=FILE}
        <script type="text/javascript" src="{$FILE}"></script>
    {/foreach}
{/if}

