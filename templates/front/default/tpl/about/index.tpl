<main>

    
    
    
    <!-- ФОРМА ОТПРАВКИ ВЕРХ -->
    <section class="up up__company">
        <div class=" content-wrapper">
            <div class="up__img">
                <img src="{$PAGE_CONTENT.topImage}" alt="">
            </div>
            <div class="up__wrapper">
                <div class="up__back">
                    <ul class="breadcrumbs up__breadcrubms">
                        <li><a href="index.html">Главная <span style="color: #42c6d1;">></span></a></li>
                        <li><a href="">О компании</a></li>
                    </ul>
                    <div class="up__text">
                        {$PAGE_CONTENT.titleTop}
                    </div>
                    <div class="up__button">
                        <a href="#" class="button button--blue">Записаться на прием <span>>></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>




                    
                    
<!-- СТАТИСТИКА КЛИНИКИ -->
{if isset($STATISTIC)}
    <section class="statistic">
        <h2 class="statistic__title content-wrapper">Клиника в цифрах</h2>
        <div class="statistic__wrapper content-wrapper">
            {foreach from=$STATISTIC item='stat'}
                <div class="statistic__block">
                    <div class="statistic__img">
                        <img src="{$stat.img}" alt="">
                    </div>
                    <div class="statistic__block-wrap">
                        <p class="statistic__number">{$stat.nubmer}</p>
                        <p class="statistic__text">{$stat.title}</p>
                    </div>
                </div>
            {/foreach}
        </div>
    </section>
{/if}






<!-- ФОТО КОМАНДЫ -->
<section  class="column2 card-doctor column2__company">
    <h2 class="column2__title">{$PAGE_CONTENT.titleTeam}</h2>
    <div class="content-wrapper  column2__wrapper">
        <div class="column2__left">
            <img  src="{$PAGE_CONTENT.teamImage}" alt="">
        </div>
        <div class="column2__right">
            <p class="column2__name">{$PAGE_CONTENT.titleTeam}</p>
            {$PAGE_CONTENT.textTeam}
            <br><br>
        </div>
    </div>
</section>


            
            
            
            
<!-- ГОЛУБОЙ БЛОК БОЛИТ ЗУБ? -->
    {if !empty($BLUE_BLOCKS) }
<section class="blue-block blue-block2">
    <div class="content-wrapper blue-block__wrapper">
        <div class="psevdo-wrap">
            <img src="{$BLUE_BLOCKS[0].imageIcon}" alt="">
            <div class="blue__left-block">Болит зуб?</div>
        </div>
        <div class="blue__right-block">
            <img src="{$BLUE_BLOCKS[0].imageAva}" width="190" height="190" alt="">
            <div class="blue__text">
                {$BLUE_BLOCKS[0].textTop}
                <div class="blue__link-wrapper">
                    <div class="blue__link-number"><p>Бесплатная консультация</p>
                        <a href="tel:{$SETTINGS.general.telephone}">{$SETTINGS.general.telephone}</a>
                    </div>
                    <div class="blue__button">
                        <a href="{$BLUE_BLOCKS[0].buttonLink}" class="button--blue">{$BLUE_BLOCKS[0].buttonTitle} <span>>></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{/if}





<!-- ТЕКСТ "С любовью к своей работе" -->
<section class="promotion  promotion__company">
    <h2 class="visually-hidden">{$PAGE_CONTENT.title}</h2>
    <div class="content-wrapper promotion__wrapper">
        <h2 class="promotion__title">{$PAGE_CONTENT.title}</h2>
        <br>
        <p class="promotion__text" >
            {$PAGE_CONTENT.text}
        </p>
    </div>
</section>


        
        
        
        
        
<!-- СЛАЙДЕР -->
    {if !empty($SLIDER_ABOUT)}
<section id="photo-slider" class="photo-slider">
    <div class="sl-ps content-wrapper photo-slider__wrapper photo__slider ">
        {foreach from=$SLIDER_ABOUT item=slider}
        <div>
            <div class="photo__slide">
                <div class="prev"></div>
                <div class="photo__slide-img">
                    <img width="100%" height="439" src="{$slider.sliderImage}" alt="">
                </div>
                <div class="next"></div>
            </div>
        </div>
        {/foreach}
    </div>
    <div class="sl-count">Фото: <span class="sl-count__num">1</span> / <span class="sl-count__total"></span></div>
</section>
    {/if}



<!-- Преимущества нашей клиники -->
<section  class="reasons reasons__company">
    <div class="content-wrapper ">
        <div class=" reasons__wrapper">
            <div class="">
                <h2 class="reasons__title">Преимущества нашей клиники</h2>
                <p class="reasons__text"></p>
                <div class="reasons__block-wrapper">
                    {if !empty($ADVANTAGE)}
                        {foreach from=$ADVANTAGE item=article}
                    <div class="reasons__block">

                        <p class="reasons__block-text1">{$article.title}</p>
                        <p class="reasons__block-text2">{$article.text}</p>
                        <img src="{$article.image}" alt="">

                    </div>
                        {/foreach}
                    {/if}
                </div>
            </div>
        </div>
</section>




<!-- ВЫВОД ВСЕХ СЕРТИФИКАТОВ В ЦИКЛЕ -->
    <section id="sertificat" class="sertificat ">
        <div>
            <h2 class="sertificat__title">Награждены сертификатами</h2>
            <p class="content-wrapper">
                {$PAGE_CONTENT.certificateText}
            </p>
        </div>
        <div class="sl-sf sertificat__wrapper sertificat__slider ">
            {if !empty($CERTIFICATE)}
                {foreach from=$CERTIFICATE item=sertif}
                    <div>
                        <div class="sertificat__item">
                            <div class="sertificat__av">
                                <img width="157" height="217" src="{$sertif.image}" alt="">
                            </div>
                            <div class="sertificat__text-wrapper">
                                <p class="sertificat__name">{$sertif.name} <br> <span class="sertificat__spec">{$sertif.summary}</span></p>
                                <div class="sertificat__slide-block">
                                    <div class="sertificat__button">
                                        <a href="#sertificat1" class="simplebox  button button--blue">Увеличить <span>>></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                {/foreach}
            {/if}
        </div>
        <div class="sl-count"><span class="sl-count__num">1</span> / <span class="sl-count__total"></span></div>
    </section>
            


            

<!-- ФОРМА ОТПРАВКИ НИЗ -->
<section id="appointment" class="form  form__chair">
    <div class=" content-wrapper">
        <img src="{$PAGE_CONTENT.bottomImage}" alt="">
        <div class="form-wrapper">
            <!-- Загрузка формы отправки "Записаться на прием" -->
            {include file='form/form-appointment.tpl'}
        </div>
    </div>
</section>

</main>
