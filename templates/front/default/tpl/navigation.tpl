		<ul class="main-nav" id="main-nav">
			{foreach from=$MAIN_MENU item=MENU_ITEM}
				<li class="{if ($MENU_ACTIVE_ALIAS == $MENU_ITEM.alias)}active{/if}{if empty($MENU_ITEM.children)} no-drop{/if}">
					{if empty($MENU_ITEM.children)}
						<a href="{$MENU_ITEM.fullAlias}" {if $MENU_ITEM.external} target="_blank"{/if}>{$MENU_ITEM.caption}</a>
					{else}
						<a href="{$MENU_ITEM.fullAlias}">{$MENU_ITEM.caption}</a>
						<ul>
							{foreach from=$MENU_ITEM.children item=menu}
								<li class="{if $menu.pageID == $PAGE.pageID}active{/if}">
									<a href="{$menu.fullAlias}" {if $menu.external} target="_blank"{/if}>{$menu.caption}</a>
								</li>
							{/foreach}
							<li class="last"></li>
						</ul>
					{/if}
				</li>
			{/foreach}
		</ul>