{debug}
<main id="doctors">

    <!-- Верх -->
    <section class="up doctors">
        <div class="doctors-wrapper">
            <div class="up__back">
                <div class="up__img">
                    <img src="{$PAGE_CONTENT.topImage}" alt="">
                </div>
                <div class="up__wrapper content-wrapper">
                    <ul class="breadcrumbs up__breadcrubms">
                        <li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
                        <li class="current-link"><a>Врачи</a></li>
                    </ul>
                    <div class="up__text">
                        {$PAGE_CONTENT.titleTop}
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Мы гордимся своими врачами -->
    <section class="column2">
        <h2 class="column2__title">{$PAGE_CONTENT.title}</h2>
        <div class="content-wrapper  column2__wrapper">

            <div class="column2__left">
                {if !empty($PAGE_CONTENT.slider)}
                    <div class="column2__slider">
                        {foreach from=$PAGE_CONTENT.slider item=slid}
                        <div>
                            <div class="column2__left-slide">
                                <img width="100%" src="{$slid.image}" alt="">
                            </div>
                        </div>
                        {/foreach}
                    </div>
                {/if}
            </div>

            <div class="column2__right">
                <p align="left">
                    {$PAGE_CONTENT.text}
                </p>
            </div>

        </div>
    </section>



    <!-- ГОЛУБОЙ БЛОК "О команде" -->
    {if !empty($BLUE_BLOCKS[0])}
    <section class="blue-block blue-block__doctors" >
        <div class="content-wrapper blue-block__wrapper">
            <div class="psevdo-wrap">
                <img src="{$BLUE_BLOCKS[0].imageIcon}" alt="">
                <div class="blue__left-block">{$BLUE_BLOCKS[0].imageIconTitle}</div>
            </div>
            <div class="blue__right-block">
                <div class="blue__right-block-img">
                    <img src="{$BLUE_BLOCKS[0].dependendDoctor[0].imageAva}" width="190" height="190" alt="">
                    <p>{$BLUE_BLOCKS[0].dependendDoctor[0].name}</p>

                    <span>{$BLUE_BLOCKS[0].dependendDoctor[0].position}</span>

                </div>
                <div class="blue__text">
                    <p align="left">
                        {$BLUE_BLOCKS[0].textTop}
                    <div class="blue__link-wrapper">

                    </div>
                </div>

            </div>
        </div>
    </section>
    {/if}


    
    <!-- В нашей команде только лучшие! -->
    <section class="reasons reasons__doctors">
        <div class="content-wrapper ">
            <div class=" reasons__wrapper">
                <div class="">
                    <h2 class="reasons__title">{$PAGE_CONTENT.titlTwo}</h2>
                    <p class="reasons__text">{$PAGE_CONTENT.textTwo}</p>
                    <div class="reasons__block-wrapper">

                        <!-- Цикл вывода Блоков -->
                        {if !empty($BEST_TEAM) }
                            {foreach from=$BEST_TEAM item=block}
                                <div class="reasons__block">
                                    <p class="reasons__block-text1">{$block.title}</p>
                                    <p class="reasons__block-text2">{$block.text}</p>
                                    <img src="{$block.image}" alt="">
                                </div>
                            {/foreach}
                        {/if}

                    </div>
                </div>
            </div>
    </section>


                        

    <!-- Наши специалисты -->
    {if !empty($PERSONALS) }
    <section id="personal" class="personal">
        <h2 class="personal__title">Наши специалисты</h2>
        <div class=" personal__wrapper  content-wrapper">

            {foreach from=$PERSONALS item=person}
            <div class="personal__item">
                <div class="personal__av">
                    <img  src="{$person.imageAva}" alt="">
                </div>
                <div class="personal__text-wrapper">
                    <p class="personal__name">{$person.name} <br> <span class="personal__spec">{$person.profession}</span></p>

                    <div class="personal__button">
                        <a href="{$CONTROLLER_ALIAS}card-doctor/{$person.personalID}" class="button button--blue">Подробнее <span>>></span></a>
                    </div>
                </div>
            </div>
            {/foreach}

        </div>
    </section>
    {/if}




    <!-- ФОРМА ОТПРАВКИ НИЗ -->
    <section id="appointment" class="form">
        <div class=" content-wrapper">
            <img src="{$PAGE_CONTENT.bottomImage}" alt="">
            <div class="form-wrapper">
                <!-- Загрузка формы отправки "Записаться на прием" -->
                {include file='form/form-appointment.tpl'}
            </div>
        </div>
    </section>

</main>