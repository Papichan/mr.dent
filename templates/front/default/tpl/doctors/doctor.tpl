{debug}

<main>

<!-- ВЕРХ -->
    <section class="up card-doctor">
        <div class="doctors-wrapper">
            <div class="up__back">
                <div class="up__img">
                    <img src="{$IMAGES_ALIAS}team.png" alt="">
                </div>
                <div class="up__wrapper content-wrapper">
                    <ul class="breadcrumbs up__breadcrubms">
                        <li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
                        <li><a href="{$BREADCRUMBS[0].alias}">Врачи<span style="color: #42c6d1;">></span></a></li>
                        <li><a>{$DOCTOR[0].name}</a></li>

                    </ul>
                    <div class="up__text">
                        <div class="up__text1">Наша лучшая  </div><br>
                        <div class="up__text2">Команда <br>специалистов</div>

                    </div>
                    <div class="up__button">
                        <a href="#" class="button button--blue">Записаться на прием <span>>></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <!-- СТАТИСТИКА ДОКТОРА -->
    <section class="statistic card-doctor">

        <div class="statistic__wrapper content-wrapper">
            <div class="statistic__block">
                <div class="statistic__img">
                    <img src="{$IMAGES_ALIAS}icon-tooth1.png" alt="">
                </div>
                <div class="statistic__block-wrap">
                    <p class="statistic__number">{$DOCTOR[0].experience} лет</p>
                    <p class="statistic__text">Стаж работы</p>
                </div>
            </div>
            <div class="statistic__block">
                <div class="statistic__img">
                    <img src="{$IMAGES_ALIAS}icon-tooth2.png" alt="">
                </div>
                <div class="statistic__block-wrap">
                    <p class="statistic__number">{$DOCTOR[0].certificate}</p>
                    <p class="statistic__text">Сертификаты</p>
                </div>
            </div>
            <div class="statistic__block">
                <div class="statistic__img">
                    <img src="{$IMAGES_ALIAS}icon-tooth3.png" alt="">
                </div>
                <div class="statistic__block-wrap">
                    <p class="statistic__number">{$DOCTOR[0].reviews}</p>
                    <p class="statistic__text">Отзывы</p>
                </div>
            </div>
    </section>


    <!-- КАРТОЧКА ДОКТОРА -->
    {if !empty($DOCTOR) }
    <section  class="column2 card-doctor">
        <h2 class="column2__title">{$DOCTOR[0].name}</h2>
        <div class="content-wrapper  column2__wrapper">
            <div class="column2__left">
                <img  src="{$DOCTOR[0].imageFull}" alt="">
            </div>

            <div class="column2__right">
                <p class="column2__name">{$DOCTOR[0].name}</p>
                <p>Должность: {$DOCTOR[0].position}<br>
                    Специализация: {$DOCTOR[0].profession}<br>
                    Категория: {$DOCTOR[0].category}<br>
                    {$DOCTOR[0].description}
                </p>
            </div>
        </div>
    </section>
    {/if}


    <!-- ФОРМА НИЗ -->
    <section id="appointment" class="form  form__chair">
        <div class=" content-wrapper">
            <img src="{$IMAGES_ALIAS}form-image4.png" alt="">
            <div class="form-wrapper">
                <!-- Загрузка формы отправки "Записаться на прием" -->
                {include file='form/form-appointment.tpl'}
            </div>
        </div>
    </section>

</main>