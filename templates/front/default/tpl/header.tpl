<!--================= НАЧАЛО ОБЩЕГО HEADER ================-->
<header class="header ">
   
    <div class="header__first  header--closed header--no-js  content-wrapper">
        <div class="header__modal-overlay">

            
            <!-- ЭТО ОКНО ДЛЯ СЖАТОЙ ВЕРСИИ САЙТА (ОТОБРАЖАЕТСЯ В МАЛЕНЬКОМ ОКНЕ) -->
            <div class="header__modal">
                <div class="header__modal__back-wrapper">
                    <div class="header__modal__back">Главное меню</div>
                    <button  class="header__modal-toggle2" aria-label="Назад"></button>
                </div>
                <div class="header__modal-menu">
                    <ul>
                        
                        <!-- ЦИКЛ ВЫВОДА ГЛАВНОГО МЕНЮ ДЛЯ ВЕРСИИ МАЛЬНЬКОГО ОКНА БРАУЗЕРА -->
                        <!-- ОДНО ИСКЛЮЧЕНИЕ У РАЗДЕЛА "УСЛУГИ" ЕСТЬ УНИКАЛЬНЫЕ СТИЛИ -->
                        {assign var="count" value=$MAIN_MENU|@count}
                        {section name = mySection start=1 loop=$count step=1}
                                {if ( $MAIN_MENU[$smarty.section.mySection.index].alias == 'services') }
                                    <li class="header__modal-menu-item menu1">
                                        <a data-toggle="tab2" href="#menu2">
                                            {$MAIN_MENU[$smarty.section.mySection.index].caption}
                                        </a>
                                    </li> 
                                {else}
                                    <li>
                                        <a href="{$MAIN_MENU[$smarty.section.mySection.index].fullAlias}">
                                            {$MAIN_MENU[$smarty.section.mySection.index].caption}
                                        </a>
                                    </li>
                                {/if}
                        {/section}
    
                    </ul>
                </div>
            </div>
            
             
                        
                        
<!--============================= МЕНЮ ДЛЯ МАЛЕНЬКОГО ОКНА БРАУЗЕРА ========================-->

            <!-- ВЫПОДАЮЩИЕ ПОДМЕНЮ ДЛЯ РАЗДЕЛА = УСЛУГИ (ВЫПАДАЮЩИЙ СПИСОК ВСЕХ РАЗДЕЛОВ СТРАНИЦЫ УСЛУГ) -->
            <div id="menu2" class="menu2 menu-main">
                <div class="header__modal__back-wrapper">
                    <button  class="header__modal-toggle4" aria-label="Назад"></button>
                    <div class="header__modal__back">Услуги</div>   
                </div>
                <ul>
                    {assign var="count" value=2}
                    <!-- !!! ПРОБЛЕМКА ВЫВОД ЕСЛИ НЕТУ TAB КАК ? !!! -->
                    {foreach from=$SERVICES_MENU item='submenu'}
                        {if ($count == 2) }
                            <li><a href="/services/{$submenu.alias}">{$submenu.name}</a></li>
                            {assign var="count" value=$count+1}
                        {else}
                            <li><a data-toggle="tab2" href="#menu{$count++}">{$submenu.name}</a></li>
                        {/if}
                    {/foreach}
                </ul>
            </div>

                
            <!-- ЦИКЛ ДЛЯ ВЫВОДА ВСЕХ ПОДРАЗДЕЛОВ СТРАНИЦЫ УСЛУГ -->        
            {assign var="count" value=2} 
            {assign var="styleMenu" value=4} 
            
            {foreach from=$SERVICES_MENU item='submenu'}
                 <div id="menu{$count++}" class=" {if ($styleMenu == 4)}menu4{$styleMenu++}{/if}  menu-main">
                    <div class="header__modal__back-wrapper">
                        <button  class="header__modal-toggle3" aria-label="Назад"></button>
                        <div class="header__modal__back">{$submenu.name}</div>
                    </div>
                    <ul>
                    <!-- У КАЖДОГО ПОДРАЗДЕЛА ЕСТЬ СВОЙ ВЫПАДАЮЩИЙ СПИСОК -->    
                    {foreach from=$submenu.children item='menu'}
                        <li ><a href="/services/{$submenu.alias}/{$menu.alias}">{$menu.name}</a></li >
                    {/foreach}
                    </ul>
                </div>
            {/foreach}
             
        </div>
<!--============== КОНЕЦ МЕНЮ ДЛЯ МАЛЕНЬКОГО ОКНА БРАУЗЕРА ==============-->
                
                    




                    
<!--============================= ВЫВОД МЕНЮ ОБЫЧНОГО РАЗМЕРА ========================-->       

        <!-- ЛОГО, АДРЕССА ТЕЛЕФОНЫ ГОРОД -->
        <div class="header__first-logo">
            <a href="/">
                <img class="logo1 " src="{$IMAGES_ALIAS}logo.svg" width="110" height="95" alt="Логотип">
                <img class="logo2 none" src="{$IMAGES_ALIAS}logo.svg" width="77" height="67" alt="Логотип">
            </a>
            <div class="header__second header__first-address">
                {$SETTINGS.general.addressTown}<br> {$SETTINGS.general.addressStreet}
            </div>
            <div class="header__second header__first-tel">
                <div class="header__first-number">
                    <a class="telephone" href="tel:{$SETTINGS.general.telephone}">{$SETTINGS.general.telephone}</a>
                </div>
                <div class="header__first-order">
                    <a href="#">Заказать звонок</a>
                </div>
            </div>
            <button class="header__toggle" id="toggle-button" type="button" aria-label="Меню"> </button>
        </div>


                

        <div class="header__tablet">
            <div class="header__first-middle">
                
                <!-- ВЫВОД ГОРОД/УЛИЦА ЧАСЫ/ДНИ/РАБОТЫ -->
                <div class="header__first-work">
                    <div class="header__first-address">
                        {$SETTINGS.general.addressTown} {$SETTINGS.general.addressStreet}
                    </div>
                    <div class="header__first-hours">
                        {$SETTINGS.general.officeTitle} {$SETTINGS.general.officeHourse}
                    </div>
                </div>
                    
                    

                    
                <!-- ВЫВОД HEADER  -->
                {if !empty($MAIN_MENU) }
                        <nav class="main-nav">
                            <ul class="main-nav__list">

                                <!-- ВЫВОД ВСЕГО МЕНЮ (Имеет 2 исключения для УСЛУГИ и для ДОКТОРА у них уникальные стили) -->
                                {foreach from=$MAIN_MENU item=menu}

                                        <!-- УНИКАЛЬНЫЙ ЦИКЛ ВЫВОДА ТОЛЬКО ДЛЯ МЕНЮ = УСЛУГИ -->
                                        {if ($menu.fullAlias == '/services/') }
                                            <li class="item__services hover-shadow"><a href="{$menu.fullAlias}">{$menu.caption}</a>

                                                <div class="submenu services__submenu">
                                                    <div class="submenu__wrapper">

                                                        <!-- ЦИКЛ ПОКАЗАТЬ ВСЕ ПОДКАТЕГОРИИ СТРАНИЦЫ = УСЛУГ -->
                                                        <ul class="submenu__list">
                                                            {foreach from=$SERVICES_MENU item=service}
                                                                
                                                                <!-- Исключение для блока = gigiena (Уникальные css стили) -->
                                                                {if $service.alias == 'gigiena' }
                                                                    <li class="submenu__list-item submenu__list-item--current">
                                                                        <a data-toggle="tab" href="#{$service.alias}">
                                                                            {$service.name}
                                                                        </a>
							            </li>
                                                                {else}    
                                                                    <li class="submenu__list-item">
                                                                        <a data-toggle="tab" href="#{$service.alias}">
                                                                            {$service.name}
                                                                        </a>
                                                                    </li>
                                                                {/if}
                                                            {/foreach}
                                                        </ul>

                                                        
                                                        
                                                        <!-- Пример работы --><!--
                                                        <ul id="therapy" class="submenu-2__list">
                                                            <li class="submenu-2__list-item"><a href="lechenie_zubov.html">Лечение кариеса</a></li>
                                                            <li class="submenu-2__list-item"><a href="#">Реставрация зубов</a></li>
                                                            <li class="submenu-2__list-item"><a href="#">Лечение пульпита</a></li>
                                                            <li class="submenu-2__list-item"><a href="#">Лечение периодонтита</a></li>
                                                        </ul>
                                                                              
                                                        <ul id="gigiena" class="submenu-2__list   active">
                                                            <li class="submenu-2__list-item" >
                                                                <a data-toggle="block" href="#uzi">
                                                                    Ультразвуковая чистка
                                                                </a>
                                                            </li>
                                                            <li class="submenu-2__list-item airflow">
                                                                <a data-toggle="block" class="airflow" href="#airflow">
                                                                    Ультразвуковая чистка + AirFlow
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <!-- -->
                                                        
                                                        {assign var="count" value=1} 
                                                        <!-- ЦИКЛ ПОКАЗАТЬ ВСЕ РАЗДКЛЫ ПОДКАТЕГОРИЙ СТРАНИЦЫ = УСЛУГ --> 
                                                        <div class="submenu-2">
                                                            {foreach from=$SERVICES_MENU item=service}
                                                                <!-- Исключение для блока = gigiena (уникальные css стили) -->
                                                                {if $service.alias == 'gigiena' }
                                                                    <ul id="{$service.alias}" class="submenu-2__list active">
                                                                        {foreach from=$service.children item=submenu}
                                                                            <li class="submenu-2__list-item">
                                                                                <a href="/services/{$service.alias}/{$submenu.alias}">
                                                                                    {$submenu.name}
                                                                                </a>
                                                                            </li>
                                                                        {/foreach}
                                                                    </ul>
                                                                {else}                                                                 
                                                                    <ul id="{$service.alias}" class="submenu-2__list">
                                                                        {foreach from=$service.children item=submenu}
                                                                            <li class="submenu-2__list-item">
                                                                                <a href="/services/{$service.alias}/{$submenu.alias}">
                                                                                    {$submenu.name}
                                                                                </a>
                                                                            </li>
                                                                        {/foreach}
                                                                    </ul>
                                                                {/if}
                                                            {/foreach}
                                                        </div>        
                                                    </div>               
                           
                                                    <!-- Вывод текста и картинок для разделов категорий страницы "Услуги" -->    
                                                    <div class="submenu-3">

                                                        <div id="uzi" class="submenu__block">
                                                            <img width="340" height="192" src="{$IMAGES_ALIAS}image1.png" alt="">
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                                                                Sit accusantium qui fugiat consequatur laboriosam voluptas autem sint, 
                                                                asperiores possimus nostrum delectus soluta molestiae accusamus eveniet velit, 
                                                                dignissimos omnis in maxime!
                                                            </p>
                                                        </div>

                                                        <div id="airflow" class="submenu__block active">
                                                            <img width="340" height="192" src="{$IMAGES_ALIAS}image1.png" alt="">
                                                            <p>
                                                                Гигиеническая чистка зубов Air Flow представляет собой процедуру, при которой 
                                                                аппарат распыляет на зубы смесь из воды, воздуха и кристаллов соды под давлением.
                                                                <br>
                                                                <br>
                                                                Степень такого давления регулирует врач-стоматолог. При помощи 
                                                                данного прибора различные типы отложений удаляются не только с поверхности зуба, 
                                                                но и из межзубных промежутков.
                                                            </p>
                                                        </div>
                                                       
                                                    </div> 
                                                    <!-- -->    
                                                                         
                                       
                                                </div>
                                            </li>
                                                 
                                        {elseif (($menu.fullAlias != '/doctors/')) }
                                            <!-- УНИКАЛЬНЫЕ СТИЛИ ДЛЯ СТРАНИЦЫ ДОКТОРОВ -->
                                            <li class="doctors main-nav__list-item"><a href="{$menu.fullAlias}">{$menu.caption}</a></li>
                                        {else}
                                            <!-- ЕСЛИ НЕ УСЛУГИ И НЕ ДОКТОРА ТО ВЫВОД ВСЕГО ОСТАЛЬНОГО -->
                                            <li class="main-nav__list-item"><a href="{$menu.fullAlias}">{$menu.caption}</a>  </li>
                                        {/if}
                                {/foreach}

                    </ul>
                </nav>
                {/if}
             
            </div>


                
            <!-- ТЕЛЕФОН, ЗАКАЗАТЬ ЗВОНОК -->    
            <div class="header__first-tel">
                <div class="header__first-number">
                    <a class="telephone" href="tel:{$SETTINGS.general.telephone}">{$SETTINGS.general.telephone}</a>
                </div>
                <div class="header__first-order">
                    <a href="#">Заказать звонок</a>
                </div>
            </div>
                
        </div>
    </div>
</header>
<!--================================================= КОНЕЦ HEADER ================================================-->