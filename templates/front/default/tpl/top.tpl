<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru">
    <head>
        {if (isset($PAGE.isNoindex) and $PAGE.isNoindex)}<meta name="robots" content="noindex,nofollow">{/if}
            <title>{if isset($PAGE.title)}{$PAGE.title}{/if}</title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- Старые меты
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <meta name="viewport" content="width=device-width" />
        -->
            {if isset($PAGE.description)}<meta name="description" content="{$PAGE.description}" />{/if}
            {if isset($PAGE.keywords)}<meta name="keywords" content="{$PAGE.keywords}" />{/if}

            <link rel="stylesheet" type="text/css" href="{$STYLES_ALIAS}style.css" media="all"/>
            <link rel="stylesheet" type="text/css" href="{$STYLES_ALIAS}myStyles.css" />

            <link rel="stylesheet" type="text/css" href="{$STYLES_ALIAS}normalize.css" media="all"/>
            <link rel="stylesheet" type="text/css" href="{$STYLES_ALIAS}slick.css" />
            <link rel="stylesheet" type="text/css" href="{$STYLES_ALIAS}slick-theme.css" media="all"/>
            <link rel="stylesheet" type="text/css" href="{$STYLES_ALIAS}main.min.css" />
                
                
            {if isset($STYLE_FILE)}
                {foreach from=$STYLE_FILE item=FILE}
                    <link rel="stylesheet" href="{$FILE}" type="text/css" />
                {/foreach}
            {/if}

            {if isset($JS_VARS)}
                <script type="text/javascript">
                    {foreach from=$JS_VARS item=JS_VAR}
                        {foreach from=$JS_VAR item=JS_VAR_VALUE key=JS_VAR_NAME}
                                {if is_array($JS_VAR_VALUE) || is_object($JS_VAR_VALUE)}
            var {$JS_VAR_NAME} = {$JS_VAR_VALUE|@json_encode};
                                {else}
            var {$JS_VAR_NAME} = "{$JS_VAR_VALUE}";
                            {/if}
                        {/foreach}
                    {/foreach}
                </script>
            {/if}
    </head>
