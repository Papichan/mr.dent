<!-- Форма отправки "Записаться на прием" -->
<form action="" method="post">
    <div class="form__fieldset">
        <strong class="form__title">Записаться на прием</strong>

        <input type="hidden" name="messageType" value="appointment">

        <ul class="form__list" role="none">
            <li class="form__name">
                <label for="fio">ФИО</label>
                <input required type="text" name="fio" id="name">
            </li>
            <li class="form__number">
                <label for="phone">Телефон для связи</label>
                <input required type="tel" name="phone" id="number">
            </li>
            <li class="form__mail">
                <label for="email">E-mail</label>
                <input required  type="email" name="email" id="email">
            </li>
            <li class="form__message">
                <label for="message">Сообщение</label>

                <textarea name="message" id="message" cols="30" rows="3"></textarea>
            </li>
        </ul>

        <div class="form__wrap">
            <div class="form__checkbox">

                <input type="checkbox" name="confidential" id="confidential" checked>
                <label for="confidential">Согласен с условиями <a href="#">конфиденциальности</a></label>
            </div>
            <div class="form__submit-wrapper">
                <input type="submit" value="Записаться >>">
            </div>
        </div>
    </div>
</form>


