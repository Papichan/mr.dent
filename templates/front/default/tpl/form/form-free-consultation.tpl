<!-- Форма отправки "Записаться на бесплатную консультацию" -->
<form action="" method="post">

    <input type="hidden" name="messageType" value="freeConsultation">

    <div class="form__fieldset tab-block__form-fieldset">
        <strong class="form__title">Записаться на бесплатную консультацию:</strong>
        <div class="tab-block__form-wrapper">
            <ul class="form__list tab-block__form-list" role="none">
                <li class="form__name">
                    <label for="fio">Ваше имя</label>
                    <input required type="text" name="fio" id="name">
                </li>
                <li class="form__number">
                    <label for="phone">Телефон для связи</label>
                    <input required type="tel" name="phone" id="number">
                </li>
                <li class="form__submit-wrapper">
                    <input type="submit" value="Записаться >>">
                </li>
            </ul>

        </div>
    </div>
</form>