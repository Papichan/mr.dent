{* Успешнаяотправка сообщения *}
<div id="contactsSuccess" style="display:none">
    Ваше сообщение отправлено.
</div>

{* Неудача *}
<div id="error" class="error" style="display:none">

</div>

<!-- Форма отправки "Задайте вопрос врачу" -->
<form action="" method="post">
    <ul class="form__list" role="none">

        <input type="hidden" name="messageType" value="question">

        <input type="hidden" name="doctor" value="{$BLUE_BLOCKS[0].dependendDoctor[0].name}">

        <li class="form__name">
            <label for="fio">ФИО</label>
            <input required type="text" name="fio" id="name">
        </li>
        <li class="form__number">
            <label for="phone">Телефон для связи</label>
            <input required  type="tel" name="phone" id="number">
        </li>

        <li class="form__message">
            <label for="message">Сообщение</label>

            <textarea required name="message" id="message" cols="30" rows="3"></textarea>
        </li>
    </ul>

    <div id="captcha" class="form__wrap form__wrap-contacts">

        <!-- ВЕРСТКА ОРИГИНАЛ
        <div class="form__checkbox">
            <label for="captcha">Вставьте цифры с картинки</label>
            <input type="text" name="captcha">

        </div>
        -->

        <!-- ТА ЧТО ИСПОЛЬЗУЕТСЯ -->
        <div class="row">
            <div id="wrongCaptcha" class="error">Неверно введены цифры с картинки</div>
            <label for="captcha">Введите цифры на картинке</label>
            <div class="control">
                <img src="/getcaptcha/" alt="captcha" id="captchaImg" title="Кликните по картинке, чтобы обновить её" style="display:block;float:left"/>
                <input type="text" name="captcha" id="captcha" class="required" style="height:24px; width:50px; padding-top:3px; font-size:1.5em;display:block;float:left" />
            </div>
        </div>
        <!-- -->

        <div class="form__submit-wrapper">
            <input type="submit" value="Отправить >>">
        </div>
    </div>

    </div>

</form>

<!-- ВЕРСТКА ОТ МОДУЛЯ ОБРАТНАЯ СВЯЗЬ
<div class="row">
    <div id="wrongCaptcha" class="error">Неверно введены цифры с картинки</div>
    <label for="captcha">Введите цифры на картинке</label>
    <div class="control">
        <img src="/getcaptcha/" alt="captcha" id="captchaImg" title="Кликните по картинке, чтобы обновить её" style="display:block;float:left"/>
        <input type="text" name="captcha" id="captcha" class="required" style="height:24px; width:50px; padding-top:3px; font-size:1.5em;display:block;float:left" />
    </div>
</div>
-->