{debug}

<main>
    <h1 class="visually-hidden"> Центр стоматологии «Mr.Dent»</h1>

    
    <!-- СЛАЙДЕР -->
    <section class="main-block">
        <div class="main-block__wrapper content-wrapper">
            <div class="sl-mb  main-block__slider">
                {if isset($PAGE_CONTENT.slider)}
                    {foreach from=$PAGE_CONTENT.slider item='slid'}
                    <div>
                        <div class="main-block__slide-img">
                            <img src="{$slid.image}" alt="slide1">
                        </div>
                        <div class="main-block__slide main-block__slide1">
                            <p>{$slid.text}</p>
                            <div class="main-block__button"><a href="{$slid.link}" class="button button--blue">{$slid.buttonTitle}<span>>></span></a></div>
                        </div>
                    </div>
                    {/foreach}
                {/if}
            </div>
            <div class="sl-count__main"><span class="sl-count__num__main">1</span> / <span class="sl-count__total__main"></span></div>
        </div>
    </section>

            
            

    <!-- СТАТИСТИКА (Клиника в цифрах) -->
    {if isset($STATISTIC)}
    <section class="statistic">
        <h2 class="statistic__title content-wrapper">Клиника в цифрах</h2>
        <div class="statistic__wrapper content-wrapper">
            {foreach from=$STATISTIC item='stat'}
            <div class="statistic__block">
                <div class="statistic__img">
                    <img src="{$stat.img}" alt="">
                </div>
                <div class="statistic__block-wrap">
                    <p class="statistic__number">{$stat.nubmer}</p>
                    <p class="statistic__text">{$stat.title}</p>
                </div>
            </div>
            {/foreach}
        </div>
    </section>
    {/if}
    
    

    <!-- О КЛИНИКЕ -->
    <section class="aboutblock">
        <h2 class="aboutblock__title aboutblock__title--tablet"> О клинике Mr.Dent</h2>
        <div class="aboutblock__wrapper content-wrapper">
            <div class="aboutblock__left">
                <a class="simplebox" href="#modal01">
                    <div class="video__mask"><img src="{$PAGE_CONTENT.videoMask}" alt=""></div>
                </a>
            </div>
            <div class="aboutblock__right">
                <h2 class="aboutblock__title  aboutblock__title--desktop">{$PAGE_CONTENT.titleAboutClinic}</h2>
                <p class="aboutblock__text">
                    {$PAGE_CONTENT.textAboutClinic}
                </p>
                <a class="button button--pink aboutblock__button" href="/about_company/">Подробнее <span>>></span></a>
            </div>
        </div>
        <div id="modal01" class="modal">
            <iframe id="if"  style="border-radius: 5px" src="{$PAGE_CONTENT.linkVideo}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </section>



        



    <!-- УСЛУГИ КОМПАНИИ -->
    <section class="services">
        <img class="services__img" src="{$PAGE_CONTENT.serviceImage}" alt="">
        <h2  class="services__title"> Услуги компании</h2>
        <div class=" content-wrapper services__height">
            <div class="services__wrapper ">
                <!-- У каждого блока уникальный css стиль поэтому используется итератор -->
                {assign var=itterator value=1}
                {foreach from=$SERVICE_IMAGES item=service}
                <div class="services__item  services__{$itterator++}">
                    <a  href="{$service.alias}">
                        {$service.name} <img src="{$service.smallImage}" alt="">
                    </a>
                </div>
                {/foreach}
            </div>
            <div class="services__button">
                <p class="services__button-text">Беспокоит боль или хотите узнать
                    о состоянии ваших зубов!</p>
                <div class="services__button-wrapper">
                    <a href="#" class="button button--blue">Бесплатная<br> консультация  </a>
                </div>
            </div>
        </div>
    </section>

            


    <!-- ОТЗЫВЫ КЛИЕНТОВ  -->
    {if isset($REVIEWS)}
    <section id="review" class="review">
        <h2 class="review__title">Отзывы клиентов</h2>
        <div class="review__wrapper review__slider content-wrapper">
            {foreach from=$REVIEWS item='review'}
            <div>
                <div class="review__item">
                    <div class="prev"></div>
                    <div class="review__av">
                        <img width="172" height="172" src="{$review.image}" alt="">
                    </div>
                    <div class="review__text">
                        <div class="review__data">{$review.dateAdd|date_format:"%e.%m.%Y"}</div>
                        <div class="review__name">{$review.name}</div>
                        <div class="review__text">{$review.review} </div>
                    </div>
                    <div class="next"></div>
                </div>
            </div>
            {/foreach}
        </div>
        <div class="review__button">
            <a href="#" class="button button--pink">Все отзывы <span>>></span></a>
        </div>
    </section>
    {/if}



    
    <!-- Наши специалисты -->
    {if isset($PERSONAL)}
    <section id="personal" class="personal">
        <h2 class="personal__title">Наши специалисты</h2>
        <div class="sl-fb personal__wrapper personal__slider ">

            {foreach from=$PERSONAL item='person'}
            <div>
                <div class="personal__item">
                    <div class="personal__av">
                        <img width="77" height="77" src="{$person.imageAva}" alt="">
                    </div>
                    <div class="personal__text-wrapper">
                        <p class="personal__name">{$person.name} <br> <span class="personal__spec">{$person.profession}</span></p>
                        <div class="personal__slide-block">
                            <div class="personal__button">
                                <a href="###" class="button button--blue">Записаться <span>>></span></a>
                            </div>
                            <div class="personal__link">
                                <a href="/doctors/card-doctor/{$person.personalID}">Информация о враче <span>>></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/foreach}

        </div>
        <div class="sl-count"><span class="sl-count__num">1</span> / <span class="sl-count__total"></span></div>
    </section>
    {/if}




    <!-- НОВОСТИ -->
    {if isset($TEN_NEWS)}
    <section class="news">
        <h2 class="news__title">Новости</h2>
        <div  class="news__wrapper news__slider content-wrapper">

            {foreach from=$TEN_NEWS item='new'}
            <div>
                <div class="news__item">
                    <a href="/newsline/{$new.alias}/{$new.newsID}">
                        <div class="news__item-img">
                            <img src="{$new.srcSmall}" alt="">
                        </div>
                        <div class="news__item-title">
                            <a href="/newsline/{$new.alias}/{$new.newsID}">{$new.summary}</a></div>
                        <div class="news__item-data">{$new.publicationDate|date_format:"%e.%m.%Y"}</div>
                    </a>
                </div>
            </div>
            {/foreach}

        </div>
        <div class="news__button">
            <a href="/newsline/" class="button button--pink">Все новости <span>>></span></a>
        </div>
    </section>
    {/if}



    <!-- Текст для продвижения -->
    <section class="promotion">
        <h2 class="visually-hidden">{$PAGE_CONTENT.promotionTitle}</h2>
        <div class="content-wrapper promotion__wrapper">
            <h2 class="promotion__title">{$PAGE_CONTENT.promotionTitle}</h2>
            <p class="promotion__text">
                {$PAGE_CONTENT.promotionText}
            </p>
        </div>
    </section>



    <!-- Yandex MAP -->
    <!-- https://yandex.ua/map-widget/v1/?um=constructor%3Acba5c5b98d71d61f9a1f93de3f46fdfd168eefd035f476d9b3540980e95e312d&amp;source=constructor -->
    <section class="map">
        <div class="map__wrapper ">

            <iframe src="{$SETTINGS.front.YandexMap}" width="100%" height="611" frameborder="0"></iframe>

            <div class="map__address-wrapper">
                <div class="map__address">
                    <div class="map__logo">
                        <img  src="{$IMAGES_ALIAS}logo.svg" alt="Логотип">
                    </div>
                    <div class="map__text">
                        <p class="map__street">{$SETTINGS.general.addressTown} <br>{$SETTINGS.general.addressStreet}</p>
                        <p class="map__tel"><a href="tel:{$SETTINGS.general.telephone}">{$SETTINGS.general.telephone}</a></p>
                        <p class="map__mail"><a href="mailto:{$SETTINGS.general.email}">{$SETTINGS.general.email}</a></p>
                        <p class="map__work">{$SETTINGS.general.officeTitle} <br>{$SETTINGS.general.officeHourse}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <!-- ФОРМА ОТПРАВКИ НИЗ -->
    <section id="appointment" class="form">
        <div class=" content-wrapper">
            <img src="{$PAGE_CONTENT.imageBottom}" alt="">
            <div class="form-wrapper">
                <!-- Форма отправки "Записаться на прием" -->
                {include file='form/form-appointment.tpl'}
            </div>
        </div>
    </section>

</main>
