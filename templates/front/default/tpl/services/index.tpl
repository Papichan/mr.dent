{debug}

<main>

    <!-- ФОРМА ОТПРАВКИ ВЕРХ  -->
    <section  class="consult consult__services">
        <div class="content-wrapper consult__wrapper">
            <div class="consult__img">
                <img  src="{$PAGE_CONTENT.topImage}" alt="">
            </div>
            <div class="consult__left">
                <ul class="breadcrumbs consult__breadcrubms">
                    <li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
                    <li><a class="current-link">Услуги</a></li>
                </ul>
                <div class="consult__text">
                   {$PAGE_CONTENT.titleTop}
                </div>
            </div>
            <div class="consult__right">
                <div id="appointment" class="form-wrapper consult__form">
                    <!-- Загрузка формы отправки "Бесплатная консультация" -->
                    {include file='form/form-consultation.tpl'}
                </div>
            </div>
        </div>
    </section>





    <!-- ВЫВОД ВСЕХ РАЗДЕЛОВ С ИХ ПОДРАЗДЕЛАМИ ДЛЯ СТРАНИЦЫ "УСЛУГИ" -->
    <section class="uslugi content-wrapper">
        <h2 class="title uslugi__title">{$PAGE_CONTENT.servicesTitle}</h2>
        <p class="uslugi__descr">{$PAGE_CONTENT.servicesText}</p>
        <span class="uslugi__razdel-title block_mobile">Профессиональная гигиена полости рта</span>
        <div class="uslugi__wrapper ">

            
                        <!-- Вывод происходит по 4 коротких блока, каждый 5-й блок имеет уникальные стили, растягиваясь на весь экран -->    
                        <!-- порядок блоков берется из порядка в Админка->меню --> 
                        {assign var=itterator value=1}<!-- Итератор для счета количества выведенных разделоов с услугами -->
                        {assign var=blok value=0}<!-- Итератор для счте вывода <div> -->

                        {foreach from=$SERVICES_MENU item=menu}<!-- Циклвывода разделов с услугами -->
                            
                            <!-- Условие сработает если блок является пятым (у него уникальные стили) -->
                            {if ($itterator == 5) }
                                <div class="uslugi__razdel uslugi__razdel--wide uslugi__razdel{$itterator++} ">
                                    <img src="{$menu.bigImage}" alt="">
                                    <span class="uslugi__razdel-title block_desktop">{$menu.name}</span>
                                    <ul class="uslugi__razdel-list">
                                        {foreach from=$menu.children item=line}
                                            <!-- Вывод блоков по 3 штуки в колонку -->
                                            {if ($blok == 0) } <div> {/if}
                                                <li><a href="{$menu.alias}/{$line.alias}">{$line.name}</a></li>
                                                {assign var=blok value=$blok+1}
                                            {if ($blok == 3) } </div> {assign var=blok value=0} {/if}
                                        {/foreach}
                                    </ul>
                                </div>
                                {assign var=itterator value=1}
                            <!-- Вывод по 4 обычных блока -->    
                            {else}<!-- Если раздел не пятый то для него другие стили -->
                                 <div class="uslugi__razdel uslugi__razdel{$itterator++}">
                                     <img src="{$menu.bigImage}" alt="">
                                     <span class="uslugi__razdel-title block_desktop">{$menu.name}</span>
                                     <ul class="uslugi__razdel-list">
                                         {foreach from=$menu.children item=line}
                                             <li><a href="{$menu.alias}/{$line.alias}">{$line.name}</a></li>
                                         {/foreach}
                                     </ul>
                                 </div>
                            {/if}
                            
                        {/foreach}

        </div>
    </section>
        
        

        
<!--  Текст для продвижения -->
<section class="promotion">
    <h2 class="visually-hidden">{$PAGE_CONTENT.promotionTitle}</h2>
    <div class="content-wrapper promotion__wrapper">
        <h2 class="promotion__title">{$PAGE_CONTENT.promotionTitle}</h2>
        <p class="promotion__text">{$PAGE_CONTENT.promotionText}</p>
    </div>
</section>



    
<!-- ФОРМА ОТПРАВКИ НИЗ -->
<section id="appointment" class="form">
    <div class=" content-wrapper">
        <img src="{$PAGE_CONTENT.bottomImage}" alt="">
        <div class="form-wrapper">
            <!-- Загрузка формы отправки "Записаться на прием" -->
            {include file='form/form-appointment.tpl'}
        </div>
    </div>
</section>
        
        

</main>