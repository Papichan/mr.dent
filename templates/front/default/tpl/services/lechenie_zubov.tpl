{debug}

<main>

    <!-- Форма отправки Верх -->
    <h1 class="visually-hidden"> Центр стоматологии «Mr.Dent»</h1>
    <section class="up">

        <div class="content-wrapper">
            <div class="up__back">
                <div class="up__img">
                    <img src="{$PAGE_CONTENT.imageTop}" alt="">
                </div>
                <div class="up__wrapper">
                    <ul class="breadcrumbs up__breadcrubms">
                        <li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
                        <li><a href="{$BREADCRUMBS[0].alias}">Услуги<span style="color: #42c6d1;">></span></a></li>
                        <li><a class="current-link">Лечение зубов</a></li>
                    </ul>
                    <div class="up__text">
                        {$PAGE_CONTENT.titleTop}
                    </div>
                </div>
            </div>
        </div>
    </section>

                    
<!-- !!!!! ВИДЕО МАСКА КУДА ЕЕ СУНУТЬ ДАРАБОТАТЬ !!!!! -->
    <!-- ЛЕЧЕНИЕ ЗАБОЛЕВАНИЯ -->
    <section class="column2">
        <h2 class="column2__title">{$PAGE_CONTENT.titleSlider}</h2>
        <div class="content-wrapper  column2__wrapper">
            <div class="column2__left">
                <div class="column2__slider">
                    
                   <!-- НУЖНО НЕ УБИРАТЬ КАРТИНКА МАСКА ДЛЯ СЛАЙДЕРА с КНОПКОЙ ЗАЧЕМ ОНА НУЖНА ?
                   <div> 
                        <div class="column2__left-slide">
                           <img width="100%" src="{$IMAGES_ALIAS}img10.png" alt="">
                            <a href="#"><img src="{$IMAGES_ALIAS}icon__play.png" alt=""></a>
                        </div> 
                    </div>
                   -->
                    {foreach from=$PAGE_CONTENT.slider item=slid}
                    <div>
                        <div class="column2__left-slide">
                            <img width="100%" src="{$slid.image}" alt="">
                        </div>
                    </div>
                    {/foreach}
                </div>
            </div>
            <div class="column2__right">
                {$PAGE_CONTENT.textSlider}
            </div>
        </div>
    </section>

            
            
            

    <!-- ГОЛУБОЙ БЛОК БОЛИТ ЗУБ ? -->
    {if !empty($BLUE_BLOCKS[0])}
    <section class="blue-block blue-block2">
        <div class="content-wrapper blue-block__wrapper">
            <div class="psevdo-wrap">
                <img src="{$BLUE_BLOCKS[0].imageIcon}" alt="">
                <div class="blue__left-block">Болит зуб?</div>
            </div>
            <div class="blue__right-block">
                <img src="{$BLUE_BLOCKS[0].imageAva}" width="190" height="190" alt="">
                <div class="blue__text">
                    {$BLUE_BLOCKS[0].textTop}
                    <div class="blue__link-wrapper">
                        <div class="blue__link-number"><p>Бесплатная консультация</p>
                            <a href="tel:{$SETTINGS.general.telephone}">{$SETTINGS.general.telephone}</a>
                        </div>
                        <div class="blue__button">

                            <a href="{$BLUE_BLOCKS[0].buttonLink}" class="button--blue">{$BLUE_BLOCKS[0].buttonTitle} <span>>></span></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    {/if}

    
    

    <!-- Причины возникновения кариеса -->
    <section class="reasons" id="reasons">
        <div class="content-wrapper ">
            <div class=" reasons__wrapper">
                <div class="">
                    <h2 class="reasons__title">{$PAGE_CONTENT.titleReason}</h2>
                    <p class="reasons__text">
                        {$PAGE_CONTENT.textReason}
                    </p>
                    <div class="reasons__block-wrapper">
                        <div class="reasons__block">
                            {foreach from=$REASONS_LEFT item=reason}
                                <ul>
                                    <li>{$reason.description}</li>
                                </ul>
                            {/foreach}
                        </div>
                        <div class="reasons__block ">
                            {foreach from=$REASONS_RIGHT item=reason}
                                <ul>
                                    <li>{$reason.description}</li>
                                </ul>
                            {/foreach}
                        </div>
                    </div>
                </div>
            </div>
    </section>


                        
    <!-- СТЕПЕНИ КАРИЕСА -->
    <section class="tab-block">
        <h2 class="tab-block__title">{$PAGE_CONTENT.titleStep}</h2>
            <p class="tab-block__descr">{$PAGE_CONTENT.textStep}</p>
        <div class="content-wrapper">

            
            
            
            <!-- БЛОКИ С ФОРМОЙ И СЛАЙДЕРАМИ -->
            <div class="tab-block__wrapper">
                
                {assign var="count" value=0}
                
                <ul class="tab-block__list">
                {foreach from=$TABS item=tab}      
                    <li {if ($count == 0) } class="tab-block--active" {/if} >
                        <a data-toggle="t1" href="#karies{$count++}">{$tab.title}</a>
                    </li>
                {/foreach}
                </ul>
                
                <div  class="tab-block__content">
                    {assign var="count" value=0}
                    {foreach from=$TABS item=tab}
                     <div id="karies{$count++}" class="tab-block__tab {if ($count == 1)}tab-block--current{/if}">
                        <p class="tab__block-text" align="justify">
                            {$tab.text}
                        </p>
                        <div class="tab-block__tab-img">
                            {foreach from=$TABS_IMAGES item=gallery}
                                {if ($gallery.tabsID == $tab.tabsID) }
                                <img width="254"  height="190" src="{$gallery.image}" alt="">
                                {/if}
                            {/foreach}
                        </div>
                    </div>
                     {/foreach}
                </div>
                
                <!--
                <ul class="tab-block__list">
                    <li class="tab-block--active"><a data-toggle="t1" href="#karies1">Темное пятно</a></li>
                    <li><a data-toggle="t1" href="#karies2">Поверхностный кариес</a></li>
                    <li><a data-toggle="t1" href="#karies3">Средний кариес</a></li>
                    <li><a data-toggle="t1" href="#karies4">Глубокий кариес</a></li>
                </ul>
                
                <div  class="tab-block__content">
                
                    <div id="karies1" class="tab-block__tab tab-block--current">
                
                        <p class="tab__block-text" align="justify">Кариес в стадии белого или темного пятна. Это начальный этап патологии, 
                            во время которого отсутствует анатомическое разрушение эмали. Однако на поверхности зуба образуется пятнышко. 
                            Лечение кариеса в стадии пятна предполагает использование методик, основанных на минимальном вмешательстве в структуру зуба.
                        </p>
                
                        <div class="tab-block__tab-img">
                            <img width="254"  height="190" src="img/tab1.png" alt="">
                            <img width="254"  height="190" src="img/tab2.png" alt="">

                            <img width="254"  height="190" src="img/tab3.png" alt="">

                        </div>
                
                    </div>
                
                
                
                
                
                    <div id="karies2" class="tab-block__tab">
                        <p class="tab__block-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                            Quisquam deleniti illo recusandae, nostrum harum molestiae eligendi ab, atque quasi similique dolorem architecto ea. 
                            Eos repellendus reprehenderit dicta? Voluptatum, quisquam, suscipit.
                        </p>
                        <div class="tab-block__tab-img"></div>
                    </div>
                
                
                    <div id="karies3" class="tab-block__tab">
                        <p class="tab__block-text">Кариес в стадии белого или темного пятна. Это начальный этап патологии, 
                            во время которого отсутствует анатомическое разрушение эмали. Однако на поверхности зуба образуется пятнышко. 
                            Лечение кариеса в стадии пятна предполагает использование методик, основанных на минимальном вмешательстве в структуру зуба.
                        </p>
                        <div class="tab-block__tab-img"></div>
                    </div>
                    <div id="karies4" class="tab-block__tab">
                        <p class="tab__block-text">Кариес в стадии белого или темного пятна. Это начальный этап патологии, 
                            во время которого отсутствует анатомическое разрушение эмали. Однако на поверхности зуба образуется пятнышко. 
                            Лечение кариеса в стадии пятна предполагает использование методик, основанных на минимальном вмешательстве в структуру зуба.
                        </p>
                        <div class="tab-block__tab-img"></div>
                    </div>
                
                </div>
                -->
                
                
                
                
                
                
                
                
                
                
                <!-- НИЗ ФОРМА ОТПРАВКИ -->
                <div class="tab-block__alarm">
                    <div class="tab-block__alarm-title">{$PAGE_CONTENT.titleForm}</div>
                    <p class="tab-block__alarm-text">
                       {$PAGE_CONTENT.textForm}
                    </p>
                </div>
                <div id="tab-form" class="tab-block__form">
                    <div class="tab-block__form-container">
                        <!-- Загрузка формы отправки "Записаться на бесплатную консультацию" -->
                        {include file='form/form-free-consultation.tpl'}
                    </div>
                </div>
            </div>
        </div>
    </section>
                   
                    

                    
    <!-- Процесс лечения кариеса -->
    {if !empty($PAGE_CONTENT.lechenieBlocks)}
    <section class="spoiler">
        <h2 class="spoiler__title">Процесс лечения кариеса</h2>
        <div class="content-wrapper">
            <div class="spoiler__wrapper">

                {assign var=itterator value=1}
                {foreach from=$PAGE_CONTENT.lechenieBlocks item=article}
                <div class="spoiler__content">
                    <div class="spoiler__click">
                        <div class="spoiler__click-number">{$itterator++}</div>
                        <div class="spoiler__click-name">{$article.title}</div>
                    </div>
                    <div class="spoiler__text" align="left">{$article.text}</div>
                </div>
                {/foreach}

            </div>
        </div>
        </h2>
    </section>
    {/if}

    
    


    <!-- ГОЛУБОЙ БЛОК ЛЕЧЕНИЕ -->
    {if !empty($BLUE_BLOCKS[1])}
    <section class="blue-block">
        <div class="content-wrapper blue-block__wrapper">
            <div class="psevdo-wrap">
                <img src="{$BLUE_BLOCKS[1].imageIcon}" alt="">
                <div class="blue__left-block">Боитесь?<br>Лечите во сне</div>
            </div>
            <div class="blue__right-block">
                <img src="{$BLUE_BLOCKS[1].imageAva}" width="190" height="190" alt="">
                <div class="blue__text">
                    {$BLUE_BLOCKS[1].textTop}
                    <div class="blue__link-wrapper">
                        <div class="blue__button">
                            <a href="{$BLUE_BLOCKS[1].buttonLink}" class="button--blue">{$BLUE_BLOCKS[1].buttonTitle} <span>>></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {/if}

    
    


    <!-- НАШИ СПЕЦЫ -->
    {if !empty($PERSONAL)}
    <section id="personal" class="personal">
        <h2 class="personal__title">Наши специалисты</h2>
        <div class="sl-fb personal__wrapper personal__slider content-wrapper">
            {foreach from=$PERSONAL item='person'}
            <div>
                <div class="personal__item">
                    <div class="personal__av">
                        <img width="77" height="77" src="{$person.imageAva}" alt="">
                    </div>
                    <div class="personal__text-wrapper">
                        <p class="personal__name">{$person.name} <br> <span class="personal__spec">{$person.profession}</span></p>
                        <div class="personal__slide-block">
                            <div class="personal__button">
                                <a href="###" class="button button--blue">Записаться <span>>></span></a>
                            </div>
                            <div class="personal__link">
                                <a href="/doctors/card-doctor/{$person.personalID}">Информация о враче <span>>></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {/foreach}
        </div>
        <div class="sl-count"><span class="sl-count__num">1</span> / <span class="sl-count__total"></span></div>
    </section>
    {/if}




    <!-- ЦЕНЫ -->
    <section class="price-block">
        <div class="content-wrapper">
            <h2 class="price-block__title">Стоимость услуг</h2>
            <div class="price-block__wrapper">
                <table class="price-block__table">
                    <tr class="price-block__row-title">
                        <th class="price-block__column1 price-block__title1 ">Услуга</th>
                        <th class="price-block__title2">Описание</th>
                        <th class="price-block__title3">Стоимость</th>
                    </tr> <!--ряд с ячейками заголовков-->
                    {foreach from=$PRICES item=price}
                    <tr class="price-block__row">
                        <td class="price-block__column1">{$price.title}</td>
                        <td class="price-block__column2">{$price.description}</td>
                        <td class="price-block__column3">от {$price.cost} ₽</td>
                    </tr> <!--ряд с ячейками тела таблицы-->
                    {/foreach}
                </table>
                <div class="price-block__button">
                    <a href="/price/" class="button"> Все цены <span>>></span></a>
                </div>
            </div>
        </div>
    </section>



    <!--ОТЗЫВ -->
    <section id="review" class="review review--white">
        <h2 class="review__title">Отзывы клиентов</h2>
        <div class="review__wrapper review__slider content-wrapper">
            {foreach from=$REVIEWS item='review'}
            <div>
                <div class="review__item">
                    <div class="prev"></div>
                    <div class="review__av">
                        <img width="172" height="172" src="{$review.image}" alt="">
                    </div>
                    <div class="review__text">
                        <div class="review__data">{$review.dateAdd|date_format:"%e.%m.%Y"}</div>
                        <div class="review__name">{$review.name}</div>
                        <div class="review__text">{$review.review}</div>
                    </div>
                    <div class="next"></div>
                </div>
            </div>
            {/foreach}
        </div>
        <div class="review__button">
            <a href="/reviews/" class="button button--pink">Все отзывы <span>>></span></a>
        </div>
    </section>


        
        
    <!-- НОВОСТИ -->
    <section class="news news__lechenie">
        <h2 class="news__title">Статьи по теме</h2>
        <div  class="news__wrapper news__slider content-wrapper">

            {foreach from=$NEWS item='new'}
            <div>
                <div class="news__item">
                    <a href="/newsline/{$new.alias}/{$new.newsID}">
                        <div class="news__item-img">
                            <img src="{$new.srcSmall}" alt="">
                        </div>
                        <div class="news__item-title">
                            <a href="/newsline/{$new.alias}/{$new.newsID}">{$new.summary}</a></div>
                        <div class="news__item-data">{$new.publicationDate|date_format:"%e.%m.%Y"}</div>
                    </a>
                </div>
            </div>
            {/foreach}

        </div>
        <div class="news__button">
            <a href="/newsline/" class="button button--pink">Все статьи <span>>></span></a>
        </div>
    </section>


    <!-- Форма Отправки Низ -->
    <section id="appointment" class="form">
        <div class=" content-wrapper">
            <img src="{$PAGE_CONTENT.imageBottom}" alt="">
            <div class="form-wrapper">
                <!-- Форма отправки "Записаться на прием" -->
                {include file='form/form-appointment.tpl'}
            </div>
        </div>
    </section>
</main>