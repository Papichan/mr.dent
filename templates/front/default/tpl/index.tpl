
{include file="top.tpl"}
<body>
{include file="header.tpl"}
 <h1>Главная страница</h1>
    {include file="navigation.tpl"}
    <p>Битплатформа успешно установлена и весело работает.</p>

    {if isset($CONTENT)}{$CONTENT}{/if}
    {if isset($CONTENT_TPL)}
        {include file="$CONTENT_TPL"}
    {/if}

    {include file="footer.tpl"}
</body>
</html>
