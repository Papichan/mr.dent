{debug}

<main >

    <!-- ФОРМА ОТПРАВКИ ВЕРХ -->
    <section  class="consult  consult__contacts">
        <div class="content-wrapper consult__wrapper">
            <div class="consult__img">

                <img src="{$PAGE_CONTENT.topImage}" alt="">
            </div>
            <div class="consult__left">
                <ul class="breadcrumbs consult__breadcrubms">
                    <li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
                    <li><a class="current-link">Цены</a></li>
                </ul>
                <div class="consult__text">
                    <div class="consult__text1"> </div>
                    <div class="consult__text2">{$PAGE_CONTENT.titleTop}</div>
                </div>
            </div>
            <div class="consult__right">
                <div id="appointment" class="form-wrapper consult__form">
                    <!-- Загрузка формы отправки "Бесплатная консультация" -->
                    {include file='form/form-consultation.tpl'}
            </div>
        </div>
        </div>
    </section>



<!-- СПИСОК ЦЕН НА УСЛУГИ -->
    <section class="price-block price-block--white">
        <div class="content-wrapper">
            <h2 class="price-block__title">Стоимость услуг</h2>
            <div class="price-block__wrapper">
                <table class="price-block__table">
                    <tr class="price-block__row-title">
                        <th class="price-block__column1 price-block__title1 ">Услуга</th>
                        <th class="price-block__title2">Описание</th>
                        <th class="price-block__title3">Стоимость</th>
                    </tr> <!--ряд с ячейками заголовков-->
                    {foreach from=$PRICES item=price}
                        <tr class="price-block__row">
                            <td class="price-block__column1">{$price.title}</td>
                            <td class="price-block__column2">{$price.description}</td>
                            <td class="price-block__column3">от {$price.cost} ₽</td>
                        </tr>
                    {/foreach}
                </table>

            </div>

        </div>
    </section>



<!-- ФОРМА ОТПРАВКИ "Записаться на прием" С НИЗУ -->
    <section id="appointment" class="form  form__chair">
        <div class=" content-wrapper">
            <img src="{$PAGE_CONTENT.bottomImage}" alt="">
            <div class="form-wrapper">
                <!-- Загрузка формы отправки "Записаться на прием" -->
                {include file='form/form-appointment.tpl'}
            </div>
        </div>
    </section>
</main>

