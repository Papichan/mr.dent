{debug}

<main>
    <!-- ФОРМА ОТПРАВКИ ВЕРХ -->
    <section  class="consult  consult__contacts">

        <div class="content-wrapper consult__wrapper">
            <div class="consult__img">
                <img  src="{$PAGE_CONTENT.topImage}" alt="">
            </div>
            <div class="consult__left">
                <ul class="breadcrumbs consult__breadcrubms">
                    <li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
                    <li><a class="current-link">Контакты</a></li>
                </ul>
                <div class="consult__text">
                    {$PAGE_CONTENT.titleTop}
                </div>
            </div>
            <div class="consult__right">
                <div id="appointment" class="form-wrapper consult__form">
                    <!-- Загрузка формы отправки "Бесплатная консультация" -->
                    {include file='form/form-consultation.tpl'}
            </div>
        </div>
        </div>
    </section>



    <!-- ТЕКСТ -->
    <section class="promotion promotion__contacts">
        <h2 class="visually-hidden">{$PAGE_CONTENT.title}</h2>
        <div class="content-wrapper promotion__wrapper">
            <h2 class="promotion__title">{$PAGE_CONTENT.title}</h2>
            <p class="promotion__text">{$PAGE_CONTENT.text}</p>
        </div>
    </section>



    <!-- О НАС, КОНТАКТЫ, РЕКВИЗИТЫ, ТЕЛЕФОН. -->
    <section  class="column2 card-doctor column2__contacts">
        <h2 class="visually-hidden">Карта проезда</h2>
        <div class="content-wrapper  column2__wrapper">
            <div class="column2__left">
                <iframe src="{$SETTINGS.front.YandexMapContacts}" width="100%" height="439" frameborder="0"></iframe>
                <!--<iframe src="https://yandex.ua/map-widget/v1/?um=constructor%3Acba5c5b98d71d61f9a1f93de3f46fdfd168eefd035f476d9b3540980e95e312d&amp;source=constructor" width="100%" height="439" frameborder="0"></iframe> -->
            </div>

            <div class="column2__right">
                <p>Звоните сейчас</p>
                <br>
                <div class="column2__tel1">
                    <a href="tel:{$SETTINGS.general.telephone}">{$SETTINGS.general.telephone}</a>
                </div> <br>
                <div class="column2__tel2">
                    <a href="{$SETTINGS.general.telephone}">Перезвоните мне</a>
                </div> <br>
                <p > Наш адрес:</p><br>

                <p style="font-size: 17px"> {$SETTINGS.general.addressTown} {$SETTINGS.general.addressStreet} </p>
                <br>
                <br>
                <p> Реквизиты организации:</p><br>
                <div class="link-download">
                    <a download href="{$PAGE_CONTENT.path}">{$PAGE_CONTENT.fileName}</a>
                </div>
            </div>
        </div>
    </section>



    <!-- ФОРМА ЗАДАЙТЕ ВОПРОС ВРАЧУ -->
    <section id="doctors" class="blue-block">
        <div class="content-wrapper blue-block__wrapper">
            <div class="psevdo-wrap">
                <div class="blue__left-block block_desktop">{$BLUE_BLOCKS[0].imageIconTitle}</div>
                <div class="blue__left-block block_tablet">{$BLUE_BLOCKS[0].imageIconTitle}</div>
            </div>
            <div class="blue__right-block blue__text-contacts">
                <div class="blue__right-block-img">
                    <img src="{$BLUE_BLOCKS[0].dependendDoctor[0].imageAva}" width="178" height="178" alt="">
                    <p>{$BLUE_BLOCKS[0].dependendDoctor[0].name}</p>

                    <span>{$BLUE_BLOCKS[0].dependendDoctor[0].position}</span>

                </div>
                <div class="blue__text blue__text-contacts">
                    <div id="appointment" class="">
                        <!-- Загрузка формы отправки "Задайте вопрос врачу" -->
                        {include file='form/form-question.tpl'}
                </div>
            </div>
        </div>
        </div>
    </section>


    <!-- Дополнительные документы -->
    <section class="download">
        <h2 class="download__title"> Дополнительные документы</h2>
        <div class="content-wrapper download__wrapper">
            {if !empty($PAGE_CONTENT.files) }
                {foreach from=$PAGE_CONTENT.files item=file}
                    <div class="link-download">
                        <a download href="{$file.path}">{$file.name}</a>
                    </div>
                {/foreach}
            </div>
            {/if}
        </div>
    </section>


    <!-- ФОРМА ОТПРАВКИ НИЗ -->
    <section id="appointment" class="form">
        <div class=" content-wrapper">
            <img src="{$PAGE_CONTENT.bottomImage}" alt="">
            <div class="form-wrapper">
                <!-- Загрузка формы отправки "Записаться на прием" -->
                {include file='form/form-appointment.tpl'}
            </div>
        </div>
    </section>

</main>
