/**
    Document   : store.js
    Created on : 02.06.2013, 10:16:58
    Author     : Ivkov Eugene
    Version    : 1.01a
    Description:
        Набор скриптов для контрольной панели магазина.
*/

$(document).ready(function () {
	if (typeof $("#addOption #optionAlias_0") != "undefined" && $("#addOption #optionAlias_0").val() != 'color') {
		$("#addOption .DamnUpload").remove();
	}
	if (typeof $("#addProduct #tab-3 .formBlockHeader") != "undefined"){
		$("#addProduct #tab-3 > .extendedFields > .formBlockHeader").each(function(){
			if (!$(this).hasClass("color")){
				$(this).next(".formBlock").find(".DamnUpload").remove();
			}
		});
	}
    $("#addValue").live('click',function(){
		$.ajax({
			type: "GET",
			url: "/admin/store/options/addValue/",
			dataType: "JSON"
		}).done(function(data){
			$("#addOption .optionValues").append(data.value);
			$(window).scrollTop($("label[for=optionValue_"+data.index+"]").offset().top);
			if ($("#optionAlias_0").val() != 'color'){
				$(".DamnUpload").remove();
			}
			$("#optionValue_"+data.index).focus();

        });
    });
    $("#addAttribute").validate({
	submitHandler: function(form) {
		$(form).ajaxSubmit({success:saveAttributeResponse, beforeSubmit:showRequest, dataType:"json"});
		return false;
	}
    });
    $("#addOption").validate({
	rules: {},
	submitHandler: function(form) {
		$(form).ajaxSubmit({success:saveOptionsResponse, beforeSubmit:showRequest, dataType:"json"});
		return false;
	}
    });
    $("#addCollection").validate({
	rules: {},
	submitHandler: function(form) {
		$(form).ajaxSubmit({success:saveCollectionResponse, beforeSubmit:showRequest, dataType:"json"});
		return false;
	}
    });
    $("#addBrand").validate({
	rules: {},
	submitHandler: function(form) {
		$(form).ajaxSubmit({success:saveBrandResponse, beforeSubmit:showRequest, dataType:"json"});
		return false;
	}
    });
    $("#addCategory").validate({
	rules: {
	    "Store_Categories[0][categoryParentID]": {
		notEqual:"#categoryID_0"
	    }
	},
	messages: {
	    "Store_Categories[0][categoryParentID]": {
		notEqual: "Категория не может быть родителем самой себя"
	    }
	},
	submitHandler: function(form) {
		$(form).ajaxSubmit({success:saveCategoryResponse, beforeSubmit:showRequest, dataType:"json"});
		return false;
	}
    });
    $("#addProduct").validate({
	rules: {},
	submitHandler: function(form) {
		$(form).ajaxSubmit({success:saveProductResponse, beforeSubmit:showRequest, dataType:"json"});
		return false;
	}
    });
    $("#addProduct #categoryID_0").change(function(){
        var link = "/admin/store/products/change-type/"+$(this).val()+"/";
        $.post(link, {async: true}, changeTypeResponse, "json");
    });
    $(".cCheckBox").customCheckbox();
    $(".ccCheckBox").customCheckbox();
//    $(".deleteEntry, .deleteEntry a").unbind("click").click(deleteItemClick);

//    $("#addProduct .formBlock").each(function(){
//	$(this).hide();
//    });
//    $("#addProduct .formBlock:eq(0)").show();
    $("#addProduct .formBlockHeader").toggle(
	function(){
	    $(this).next(".formBlock").slideDown(500);
	},
	function(){
	    $(this).next(".formBlock").slideUp(500);
	}
    );

	// сортировка таблицы заказов при клике по полю заголовка
	$("a.sort-table").on('click', function(e) {
//		console.log(window.location.search);
		var field = $(this).attr('data-field');
		var args = getUrlArgs();
		if (typeof args['sort'] != 'undefined')
		{
			var sort = args['sort'];
			if (sort == field)
			{
				delete args['sort'];
				args['rsort'] = field;
			}
			else
			{
				args['sort'] = field;
			}
		}
		else if (typeof args['rsort'] != 'undefined')
		{
			var rsort = args['rsort'];
			delete args['rsort'];

			if (rsort != field)
			{
				args['sort'] = field;
			}
		}
		else
			args['sort'] = field;

		window.location.search = constructUrlSearch(args);
		return false;

	});

	$("#orderStatusSelect").on('change', function(e) {
		var newStatus = $(this).val();
		$.ajax({
			dataType: 'JSON',
			type: 'POST',
			data: {
				status: newStatus
			},
			success: function(data, status, jqXHR) {
				if ('ok' != data.status)
				{
					$.jGrowl('<span style="color:red">Ошибка изменения статуса</span>');
				}
			},
			error: function(jqXHR, status) {
				$.jGrowl('<span style="color:red">' + status + '</span>');
			}
		});
		//$.jGrowl('error');
	});

	$("#isPaid").on('click', function(e) {
		var $this = $(this);
		var paid = $this.hasClass('paid');

		// invert status
		paid = !paid;

		$.ajax({
			dataType: 'JSON',
			type: 'POST',
			data: {
				paid: (paid ? 1 : 0)
			},
			success: function(data, status, jqXHR) {
				if ('ok' != data.status)
				{
					$.jGrowl('<span style="color:red">Ошибка изменения статуса оплаты</span>');
				}
				else
				{
					if (!paid)
					{
						$this.removeClass('paid').addClass('not-paid').text('Нет');
					}
					else
					{
						$this.removeClass('not-paid').addClass('paid').text('Да');;
					}
				}
			},
			error: function(jqXHR, status) {
				$.jGrowl('<span style="color:red">' + status + '</span>');
			}
		});
	});

	// Обработка удаления значения
	$(document).on('click', 'a.delete-value', function(e) {
		$this = $(this);
		valueID = $this.attr('data-id');
//		console.log();
		$this.prev().slideUp(300, function(e) {$(this).remove();});
                if (valueID != '')
                    $this.replaceWith($('<input/>').attr({type: 'hidden', name: 'deleteValues[]', value: valueID}));
                else
                    $this.remove();
		return false;
	});


    // обработка страницы сортировки
    $('#sortProducts').each(function() {
        var $hold = $(this);
        var $form = $hold.find('form');
        var $table = $hold.find('table:first');
        var $loadBtn = $hold.find('.load-more');
        var $applySelect = $hold.find('.apply-to-selected');
		var $selnew = $hold.find('.sel > select');
//			$selnew.customForm('destroy');
//		var $sel = $selnew.clone();
//			console.log($selnew);
		var $selclon = $selnew.clone();
        var $emptyRow = $table.find('tr:eq(1)').clone()

		$selclon.attr("name", "_");
        $emptyRow.find('td:lt(5):gt(0)').text('');


        $hold.find('.select-all').on('click', function(){
           $hold.find('.select-product').prop('checked', true);
           return false;
        });

        $hold.find('.deselect-all').on('click', function(){
            $hold.find('.select-product').prop('checked', false);
            return false;
        });

        $table.on('mouseenter', '.product-image', function(e) {
            $this = $(this);
            var $bigImage = $('<img/>').attr('src', $this.attr('data-big')).css({
				position: 'absolute',
				left: e.pageX,
				top: e.pageY,
				"max-width": 300
            }).appendTo($('body'));

            $this.on('mouseleave', function() {
                $bigImage.remove();
            });
        });



        $table.on('click', 'tr', function(e) {
            if (!$(e.target).is('td'))
                return;
            var $tr = $(this);
            var val = $tr.find('.select-product').prop('checked');
            $tr.find('.select-product').prop('checked', !val);
        });

        if (!$table.attr('data-page'))
            $table.attr('data-page', 2);
        $loadBtn.on('click', function() {
            $hold.find('.loader').show();
            $.ajax({
                url: '',
                dataType: 'JSON',
                type: 'post',
                data: 'page=' + $table.attr('data-page'),

                complete: function() {
                    $hold.find('.loader').hide();
                },
                success: function (data)  {
					var $tbody = $table.find('tbody');
                    if (typeof data.products != 'undefined')
                    {
                        $.each(data.products, function(idx) {
                            var $newRow = $emptyRow.clone();
                            if (idx % 2) {
								$newRow.toggleClass('even odd');
							}
							$newRow.find('td:eq(1)').append('<input type="checkbox" class="select-product" />');
                            $newRow.find('td:eq(2)').text(this.productID);
                            $newRow.find('td:eq(3)').append($('<img/>').attr({
                                src: this.imageSrcSmall,
                                'data-big': this.imageSrc,
                                width: '100',
								class: 'product-image'
                            }));
							$newRow.find('td:eq(4)').text(this.productName);
							$newRow.find('td:eq(5)').text(this.productArticul);
							$newRow.find('td:eq(6)').text(this.brandName);
                            $newRow.find('td:eq(7)').text(this.productPrice);
                            $newRow.find('td:eq(8)').text(this.productDescription).append("<br /><br/><b>Исходный раздел:<br/>" + this.productPath + "</b>");
							$newRow.find('td:eq(10)').empty().append($selclon.clone());
							$newRow.find('td:eq(10) select').attr("name", "products[" + this.productID + "]");
                            $tbody.append($newRow);
                        });
                        $hold.find('.shown').text(parseInt($hold.find('.shown').text()) + data.products.length);
						if (parseInt($hold.find('.shown').text()) == parseInt($hold.find('.totalUnsorted').text()))
						{
							$hold.find(".load-more").hide();
						}
                    }

                    highlightableInit();

                    $table.attr('data-page', parseInt($table.attr('data-page')) + 1);
					$("select.select2").select2();
//					jQuery('form').customForm({
//						select: {
//							elements: 'select',
//							structure: '<div class="selectArea"><a tabindex="-1" href="#" class="selectButton"><span class="center"></span><span class="right"><span>&nbsp;</span><span>&nbsp;</span></span></a><div class="disabled"></div></div>',
//							optStructure: '<div class="selectOptions"><div class="custom-scroll"><ul></ul></div></div>',
//							maxHeight: 350
//						}
//					});
//					$('.custom-scroll').customScrollV();
                }
            });
        });

        $applySelect.on('change', function() {
            var id = $(this).val();
			console.log(id);
//            $hold.find('.select-product:checked').parent().parent().find('select:first').val(id);
			$hold.find('.select-product:checked').parent().parent().find('select:first').each(function () {
				$(this).select2().select2("val", id)
			});
        });


        $form.on('submit', function () {
			var link = $form.attr('action');
			var selected_product = '';
			$.each($(".select-product"), function () {
				if ($(this).parent().parent().find("select.select2 option:selected").val() != '') {
					selected_product += "<strong>" + $(this).parent().parent().find("select.select2 option:selected").text() + "</strong>" + " - " + $(this).parent().parent().find(".productName").text() + "<br/>";
				}
			});
			var b = {
				"Принять": function () {
					var _dialog = $(this);
//					var delprod = $(".delproduct .unsorted-count");
//					var blockprod = $(".blockproduct .unsorted-count");
//					var allprod = $(".allproducts .unsorted-count");
//					var unsortprod = $(".unsortproducts .unsorted-count");
					$hold.find('.loader').show();
					$.ajax({
						type: 'post',
						dataType: "JSON",
						url: link,
						data: $form.serialize(),
						complete: function () {
							$hold.find('.loader').hide();
						},
						success: function (data)
						{
							_dialog.dialog("close");
							if (typeof data.msg != 'undefined' && 'ok' == data.msg)
							{
								window.location.reload();
							}
							else if (typeof data.error != 'undefined')
							{
								$.jGrowl(data.error);
							}

						},
						error: function (data) {
							console.log(data);
						}
					});
				},
				"Отклонить": function () {
					$(this).dialog("close");
				}
			}
			dialogOpen("Внимание!", "<strong>Вы действительно хотите изменить раздел для выбранных продуктов?</strong><br/>" + selected_product, b);

			alert("here");
			return false;
        });
    });

    InitValues();
    InitDialogs();
    initTabs();

});
function InitSlide()
{
    $(".extendedOptions > .formBlock").hide();

    $(".extendedOptions .formBlockHeader").toggle(
	function(){
	    $(this).next("#addProduct .formBlock").slideDown(500);
	},
	function(){
	    $(this).next("#addProduct .formBlock").slideUp(500);
	}
    );

}
function InitValues()
{
    $("#addProduct .ccCheckBox.outtaHere").change(function(){
	var index = $("#addProduct .ccCheckBox.outtaHere").index($(this));
	if (!$(this).prop("checked"))
	    $('.block_'+index).hide();
	else
	    $('.block_'+index).show();
    });
    $("#addProduct .ccCheckBox.outtaHere").each(function(){
        var index = $("#addProduct .ccCheckBox.outtaHere").index($(this));
//        console.log(index);
        if (!$(this).prop("checked"))
	{
	    $('.block_' + index).hide();
	    $('.block_' + index + ' .formBlock').show();
	}
	else
	{
	    $('.block_'+index).show();
	    $('.block_' + index + ' .formBlock').show();
	}
    });
}

function InitDialogs()
{
	if (typeof $.fn.dialog == "undefined")
		return;
	window.deleteDialog = $( "#dialog-delete-record" ).dialog({
		resizable: false,
		height:160,
		modal: true,
		autoOpen: false,
		buttons: {
			"Удалить": function() {
				$( this ).dialog( "close" );
				$.post(window.deleteDialog.data.link, {confirmToken: window.deleteDialog.data.confirmToken}, itemDeleteResponse, "json");
			},
			"Отмена": function() {
				$( this ).dialog( "close" );
			}
		}
	});
}

function saveOptionsResponse(data, status)
{
	$(".storeForm input:submit").attr("disabled",false);
	if (data.msg == "ok")
	{
	    $.jGrowl("Поле сохранено");
	    setTimeout("window.location = '/admin/store/options/edit/"+data.info.optionID+"/'", 4000);
	};
}
function saveAttributeResponse(data, status)
{
	$(".storeForm input:submit").attr("disabled",false);
	if (data.msg == "ok")
	{
	    $.jGrowl("Атрибут сохранен");
	    setTimeout("window.location = '/admin/store/attributes/'", 4000);
	};
}
function saveCollectionResponse(data, status)
{
	$(".storeForm input:submit").attr("disabled",false);
	if (data.msg == "ok")
	{
	    $.jGrowl("Колекция сохранена");
	    setTimeout("window.location = '/admin/store/collections/edit/"+data.info.collectionID+"/'", 3000);
	};
}
function saveBrandResponse(data, status)
{
	$(".storeForm input:submit").attr("disabled",false);
	if (data.msg == "ok")
	{
	    $.jGrowl("Бренд сохранен");
	    setTimeout("window.location = '/admin/store/brands/edit/"+data.info.brandID+"/'", 3000);
	};
}
function saveCategoryResponse(data, status)
{
	$(".storeForm input:submit").attr("disabled",false);
	if (data.msg == "ok")
	{
	    $.jGrowl("Категория сохранена");
	    console.log(data.info);
	    setTimeout("window.location = '/admin/store/categories/edit/"+data.info.categoryID+"/'", 3000);
	}
	else
	    $.jGrowl(data.error);
}
function saveProductResponse(data, status)
{
	$(".storeForm input:submit").attr("disabled",false);
	if (data.msg == "ok")
	{
	    $.jGrowl("Товар сохранен");
	    console.log(data.info);
	    setTimeout("window.location = '/admin/store/products/edit/"+data.info.productID+"/'", 3000);
	}
	else
	    $.jGrowl(data.error);
}
function showRequest(form)
{
    $(".storeForm input:submit").attr("disabled",true);
}

/**
 * jQuery Custom Checkbox min v1.0.0
 * Copyright (c) 2012 JetCoders
 * email: yuriy.shpak@jetcoders.com
 * www: JetCoders.com
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 **/

jQuery.fn.customCheckbox=function(_options){var _options=jQuery.extend({checkboxStructure:'<div></div>',checkboxDisabled:'disabled',checkboxDefault:'checkboxArea',checkboxChecked:'checkboxAreaChecked'},_options);return this.each(function(){var checkbox=jQuery(this);if(!checkbox.hasClass('outtaHere')&&checkbox.is(':checkbox')){var replaced=jQuery(_options.checkboxStructure);replaced.addClass(checkbox.attr('class'));this._replaced=replaced;if(checkbox.is(':disabled')){replaced.addClass(_options.checkboxDisabled);if(checkbox.is(':checked'))replaced.addClass('disabledChecked');}else if(checkbox.is(':checked'))replaced.addClass(_options.checkboxChecked);else replaced.addClass(_options.checkboxDefault);replaced.click(function(){if(!replaced.hasClass('disabled')){if(checkbox.is(':checked'))checkbox.removeAttr('checked');else checkbox.attr('checked','checked');changeCheckbox(checkbox);}});checkbox.click(function(){changeCheckbox(checkbox);});replaced.insertBefore(checkbox);checkbox.addClass('outtaHere');}});function changeCheckbox(_this){if(_this.is(':checked'))_this.get(0)._replaced.removeClass().addClass(_options.checkboxChecked);else _this.get(0)._replaced.removeClass().addClass(_options.checkboxDefault);_this.trigger('change');}}

function deleteItemClick(event)
{
	event.preventDefault();
	alert("here");
	var that = $(this);
	var link = (that.is("a")) ? that.attr("href") : that.children('a').attr('href');
	window.deleteDialog.data.link = link;
	$.post(link, {}, itemDeleteResponse, "json");
	return false;
}
function blockItemClick(event)
{
	event.preventDefault();
	alert("here2");
	var that = $(this);
	var link = (that.is("a")) ? that.attr("href") : that.children('a').attr('href');
	window.blockDialog.data.link = link;
	$.post(link, {}, itemDeleteResponse, "json");
	return false;
}

function itemDeleteResponse(data, status)
{
	if (!standartAJAXResponseHandler(data, status)) return;
	if (data.msg == "ok")
	{
		$(".deleteEntry a[href*=\"/"+data.rowID+"/\"]").parent().parent().remove();
		var list = $("#entriesList");
		$.jGrowl("Статус записи изменен");
		if ($("#entriesList tbody").length == 0) list.replaceWith( "Таблица пуста" );
	}
	else if(data.dependentEntries)
	{
//		$('#dialog').jqm({modal: false});
//		var dependentItemsContainer = $("#dialog div").eq(1).empty();
//		var link = 0;
//		for (var i = 0; i < data.dependentEntries.length; i++)
//		{
//			dependentItemsContainer.append($("<h2>"+data.dependentEntries[i].tableName+"</h2>"));
//			for (var l = 0; l < data.dependentEntries[i].itemsLinks.length; l++)
//			{
//				link = data.dependentEntries[i].itemsLinks[l];
//				dependentItemsContainer.append($("<div>- <a href='"+link+"'>"+link+"</a></div>"));
//			}
//		}
//		$('#dialog').jqmShow();
                if (data.option){
                    $("#field-name").text(data.option.optionName);
                }
                if (data.attribute){
                    $("#field-name").text(data.attribute.attributeName);
                }

		$("#products-number").text(data.dependentEntries);

		window.deleteDialog.data.confirmToken = data.confirmToken;
		window.deleteDialog.dialog("open");
	}
}
function changeTypeResponse(data, status)
{
    if (!standartAJAXResponseHandler(data, status)) return;

    if (data.msg == "ok")
    {
        $(".extendedOptions").html(data.FIELDS);
        $(".ccCheckBox").customCheckbox();
        InitValues();
        InitSlide();
    }

}

function getUrlArgs()
{
	var args = {};
	if (window.location.search.length == 0)
		return args;
	var splited = window.location.search.substr(1).split('&');

	for (var i = 0; i < splited.length; i++)
	{
		var arg = splited[i].split('=');
		args[arg[0]] = arg[1];
	}

	return args;
}

function constructUrlSearch(args)
{
	var search = '?';
	for (var name in args)
	{
		search += name + '=' + args[name] + '&'
		//console.log(args[name]);
	}
	search = search.substr(0, search.length - 1);
	return search;
}

