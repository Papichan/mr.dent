$(document).ready(function () {
	$('a.branch').on('mouseover', function(e) {
		$(this).parent().parent().find('a.add').animate({opacity: '1'}, 400);
	}).on('mouseleave', function(e) {
		$(this).parent().parent().find('a.add').stop().animate({opacity: '0'}, 400);
	});

	$('a.delete').on('click', function(e) {
		return confirm('Вы действительно хотите удалить ветку и все ее подветки?');
	});
});