{if !empty($PAGES_TREE)}
<ul class="listContainer parent_{$PAGES_TREE[0].parentID} level_{$LEVEL}">
{foreach from=$PAGES_TREE item=PAGE_ITEM}
    <li class="row pageID_{$PAGE_ITEM.pageID}">
        <div class="leftControlsContainer">
            <span class="item checkbox"><input type="checkbox" name="checked[]" value="{$PAGE_ITEM.pageID}" /></span>
            <span class="item arrows"><img src="{$ADMIN_IMAGES}icons/arrows_updown.png" alt="Переместить" /></span>
        </div>
        <div class="captionContainer">
            <span class="item caption" style="margin-left: {math equation="lev * step" lev=$LEVEL step=15}px;"><span class="toggleArrow{if $PAGE_ITEM.isSection} {if !empty($PAGE_ITEM.children)}opened{else}unloaded{/if}{/if}">{if $PAGE_ITEM.isSection}<img src="{$ADMIN_IMAGES}icons/{if !empty($PAGE_ITEM.children)}arrow_down.png{else}arrow_right.png{/if}" align="absmiddle" />{/if}</span>{$PAGE_ITEM.caption}</span>
        </div>
        <div class="rightControlsContainer">
            <span class="item delete"><img src="{$ADMIN_IMAGES}icons/cross_bg.png" title="Удалить" alt="Удалить" /></span>
            <span class="item clone"><img src="{$ADMIN_IMAGES}icons/page_copy_bg.png" title="Клонировать" alt="Клонировать" /></span>
            <span class="item edit"><img src="{$ADMIN_IMAGES}icons/pencil_bg.png" title="Редактировать" alt="Редактировать" /></span>
        </div>
    </li>
    {if !empty($PAGE_ITEM.children)}

    {include file='structure/pages_level.tpl' PAGES_TREE=$PAGE_ITEM.children LEVEL=$LEVEL+1}

    {/if}
{/foreach}
</ul>
{/if}
