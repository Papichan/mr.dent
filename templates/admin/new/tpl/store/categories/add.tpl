<h1>Управление категориями товаров</h1>
<div class="bottom20">
	<a class="button" href="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/add/">Добавить еще одну</a>
</div>
<form action="" method="post" enctype="multipart/form-data" id="addCategory" class="storeForm">
    <div class="tabs-area">
        <ul class="tabset">
            <li><a href="#tab-1" class="tab active">Основная информация</a></li>
            <li><a href="#tab-2" class="tab">Атрибуты</a></li>
            <li><a href="#tab-3" class="tab">Опции</a></li>
        </ul>
        <div class="tab-content" id="tab-1">
            <div class="formBlockHeader">Основные данные</div>
            {include file='formBlocks.tpl'}
        </div>
        <div class="tab-content" id="tab-2">
            <div class="formBlockHeader">Атрибуты</div>
            <div class="formBlock">
                {if !empty($ATTRIBUTES)}
                    <table class="highlightable" id="faqTable">
                        <thead>
                            <tr>
                                <th colspan="2">№</th>
                                <th>Название атрибута</th>
                                <th style="width: 10px;">вкл/выкл</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach from=$ATTRIBUTES key=key item=field}
                                <tr>
                                	<td class="hov">&nbsp;</td>
        <!--			    <td style="display: none;"><a href="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/edit/{$field.attributeID}/" /></td>-->
                                    <td style="width: 20px;">{$key+1}</td>
                                    <td>
                                        {$field.attributeName}
                                    </td>
                                    <td class="switchEntry {if !empty($field.categoryAttributeID) && ($field.categoryAttributeID == -1)}inheritEntry{else}{/if}" align="center">
                                        <div class="hold-checkbox">
                                            {if !empty($field.categoryAttributeID) && ($field.categoryAttributeID == -1)}
                                                <div style="text-align: left; height: 16px;"><img title="Наследуется от родительских категорий" src="{$ADMIN_IMAGES}active_inherit.png" /></div>
                                            {else}
                                                <input type="checkbox" class="cCheckBox" name="attributes[{$field.attributeID}]" value="{$field.attributeID}" id="checkbox{$key}" {if isset($field.categoryAttributeID)}checked="checked"{/if}>
                                            {/if}
                                        </div>
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                {else}
                    <div class="formBlock">
                    	<div class="error">Атрибутов не найдено</div>
                    </div>
                {/if}
            </div>
        </div>
        <div class="tab-content" id="tab-3">
            <div class="formBlockHeader">Опции</div>
            <div class="formBlock">
                {if !empty($OPTIONS)}
                    <table class="highlightable" id="faqTable">
                        <thead>
                            <tr>
                                <th colspan="2">№</th>
                                <th>Название опции</th>
                                <th style="width: 10px;">вкл/выкл</th>
                            </tr>
                        </thead>
                        <tbody>
                            {foreach from=$OPTIONS key=key item=field}
                                <tr>
                                	<td class="hov">&nbsp;</td>
        <!--			    <td style="display: none;"><a href="{$CONTROLLER_ALIAS}{$METHOD_ALIAS}/edit/{$field.optionID}/" /></td>-->
                                    <td style="width: 20px;">{$key+1}</td>
                                    <td>
                                        {$field.optionName}
                                    </td>
                                    <td class="switchEntry {if !empty($field.categoryOptionID) && ($field.categoryOptionID == -1)}inheritEntry{else}{/if}" align="center">
                                        <div class="hold-checkbox">
                                            {if !empty($field.categoryOptionID) && ($field.categoryOptionID == -1)}
                                                <div style="text-align: left; height: 16px;"><img title="Наследуется от родительских категорий" src="{$ADMIN_IMAGES}active_inherit.png" /></div>
                                            {else}
                                                <input type="checkbox" class="cCheckBox" name="options[{$field.optionID}]" value="{$field.optionID}" id="checkbox{$key}" {if isset($field.categoryOptionID)}checked="checked"{/if}>
                                            {/if}
                                        </div>
                                    </td>
                                </tr>
                            {/foreach}
                        </tbody>
                    </table>
                {else}
                    <div class="formBlock">
                    	<div class="error">Опций не найдено</div>
                    </div>
                {/if}
            </div>
        </div>
    </div>
    <div class="formBlock">
        <div class="formElement " style="margin:0 0 0 4px;">
           <input type="submit" value="Сохранить" name="doSave" class="handyButton" />
        </div>
    </div>
</form>
<div id="dialog-delete-record" style="display:none;" title="Подтвердить удаление">
	<p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>Вы действительно хотите удалить поле "<span id="field-name" style="font-weight: bold;"></span>"? Поле используется в <span id="products-number"></span> продуктах.</p>
</div>
