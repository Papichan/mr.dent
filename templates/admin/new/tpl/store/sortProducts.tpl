<h1>Сортровка товаров</h1>

<div id='sortProducts'>
    <form method='POST' action=''>
		<table class="highlightable" id="faqTable_">
			<thead>
				<tr>
					<th colspan="2">
						<div class="sel-link">
							<a href="#" class="select-all">Выбрать все</a>
							<a href="#" class="deselect-all">Снять выбор</a>
						</div>
					</th>
					<th><span>ID</span></th>
					<th><span>Изображение</span></th>
					<th><span>Название</span></th>
					<th><span>Артикул</span></th>
					<th><span>Бренд</span></th>
					<th><span>Цена</span></th>
					<th><span>Описание</span></th>
					<th><span>Имп</span></th>
					<th><span>Категория</span></th>
					<th colspan="2">&nbsp;</th>
		</tr>
			</thead>
			<tbody>
				{if !empty($PRODUCTS)}
					{foreach from=$PRODUCTS item=THEME key=I}
						<tr id="tr-{$THEME.productID}">
							<td class="hov">1</td>
							<td><input type='checkbox' class="select-product" /></td>
							<td>{$THEME.productID}</td>
							<td>
								{if $THEME.imageSrc}
									<img src='{$THEME.imageSrcSmall}' data-big='{$THEME.imageSrc}' width="100" class='product-image' />
								{else}
									&nbsp;
								{/if}
							</td>
							<td  class="productName">
								{$THEME.productName}
							</td>
							<td>
								{$THEME.productArticul}
							</td>
							<td>
								{$THEME.brandName}
							</td>
							<td>
								{$THEME.productPrice}
							</td>
							<td>
								{$THEME.productDescription}<br /><br/>
								<b>Исходный раздел:<br/>
									{$THEME.productPath}</b>
							</td>
							<td>
								{$THEME.importName}
							</td>
							<td>
								<select name="products[{$THEME.productID}]" class='select2' style="width: 280px;">
									<option value=''>Не выбрано</option>
									{foreach from=$CATEGORIES item=CATEGORY}
										<option value="{$CATEGORY.categoryID}" {if $CATEGORY.level== 0} class="level1"{/if}>{'&nbsp'|str_repeat:$CATEGORY.level*3}{$CATEGORY.categoryName}{if isset($CATEGORY.productsCount)} ({$CATEGORY.productsCount}){/if}</option>
									{/foreach}
								</select>
							</td>
							{if $THEME.status == 3}
								<td colspan="2">
									<a class="actproduct" href="#restoreproduct" data-link="/admin/store/products/restore/{$THEME.productID}/" title="Восстановить"><img src="{$IMAGES_ALIAS}icons/arrow_undo.png" data-alt="восстановить" style="border: 0;" /></a>
								</td>
							{else}
								{if $THEME.status == 2}
									<td>
										<a class="actproduct" href="#unblockproduct" data-link="/admin/store/products/unblock/{$THEME.productID}/" title="Включить"><img src="{$IMAGES_ALIAS}icons/active1.png" data-alt="включить" style="border: 0;" /></a>
									</td>
								{else}
									<td>
										<a class="actproduct" href="#blockproduct" data-link="/admin/store/products/block/{$THEME.productID}/" title="Отключить"><img src="{$IMAGES_ALIAS}icons/delete.png" data-alt="отключить" style="border: 0;" /></a>
									</td>
								{/if}
								<td>
									<a class="actproduct" href="#delproduct" data-link="/admin/store/products/del/{$THEME.productID}/" title="Удалить"><img src="{$IMAGES_ALIAS}icons/delete_icon.png" data-alt="удалить" style="border: 0;" /></a>
								</td>
							{/if}

						</tr>
					{/foreach}
				</tbody>
				<tfoot>
					<tr class="last">
						<td colspan="2">
							<div class="sel-link">
								<a href="#" class="select-all">Выбрать все</a>
								<a href="#" class="deselect-all">Снять выбор</a>
							</div>
						</td>
						<td colspan="9">
								{*
								<div class="formBlockHeader" style="font-weight:bold;"><strong class="title">Применить к выбранным категорию:</strong></div>
								<div class="formBlock">
								<ul>
								<li style="margin:0; padding: 0;"><input type="radio" class="apply-to-selected" name="opt" value="" id="opt0"><label for="opt0">Не выбрано</label></li>
								{foreach from=$CATEGORIES item=CATEGORY key=key}
								<li style="margin:0; padding: 0;"><input type="radio" class="apply-to-selected" name="opt" value="{$CATEGORY.categoryID}" id="opt{$key}"> <label for="opt{$key}">{'-'|str_repeat:$CATEGORY.level*7} {$CATEGORY.categoryName}</label></li>
								{/foreach}
								</ul>
								</div>
								*}
								<strong class="title">Применить к выбранным категорию:</strong>
								<select class='apply-to-selected select2' style="width:400px">
									<option value=''>Не выбрано</option>
									{foreach from=$CATEGORIES item=CATEGORY}
										<option value="{$CATEGORY.categoryID}" {if $CATEGORY.level== 0} class="level1"{/if}>
											{'&nbsp'|str_repeat:$CATEGORY.level*3}{$CATEGORY.categoryName}{if isset($CATEGORY.productsCount)} ({$CATEGORY.productsCount}){/if}
										</option>
								{/foreach}
								</select>
								<div style="float:right; display: inline"><button class="blue-button" type="submit">Сохранить</button></div>
						</td>
					</tr>
				</tfoot>
			</table>

			<div class="b3">
				<h3>Показано продуктов <strong><span class='shown'>50</span> из <span class='totalUnsorted'>{$PRODUCTS_COUNTER.unsorted}</span></strong> </h3>
				<button type="button" class="load-more">Загрузить еще 50</button><img class='loader' style='display: none' src='{$ADMIN_IMAGES}ajax-loader.gif' />
			</div>
			<div style='margin: 10px 0 10px 0'>
				{include file='paginator_new.tpl'}
			</div>
				{else}
					</tbody>
					</table>
					<div class="error">Записей не найдено</div>
				{/if}
<div class="sel">
	<select name="" class="select2" style="width: 280px;">
		<option value=''>Не выбрано</option>
		{foreach from=$CATEGORIES item=CATEGORY}
			<option value="{$CATEGORY.categoryID}" {if $CATEGORY.level== 0} class="level1"{/if}>{'&nbsp'|str_repeat:$CATEGORY.level*3}{$CATEGORY.categoryName}{if isset($CATEGORY.productsCount)} ({$CATEGORY.productsCount}){/if}</option>
		{/foreach}
	</select>
</div>
</div>
<div id="dialog-func" style="display:none;" title="">
	<p></p>
</div>
