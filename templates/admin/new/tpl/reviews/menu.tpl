	<ul>
		<li {if empty($CURRENT_SUBSECTION_ALIAS)} class="active"{/if}>
			<a href="{$CONTROLLER_ALIAS}" title="Список отзывов">Список отзывов</a>
		</li>
		<li {if !empty($CURRENT_SUBSECTION_ALIAS) AND $CURRENT_SUBSECTION_ALIAS == "unapproved"} class="active"{/if}>
			<a href="{$CONTROLLER_ALIAS}unapproved/" title="Просмотр немодерированных отзывов">Новые</a>
		</li>
		<li {if !empty($CURRENT_SUBSECTION_ALIAS) AND $CURRENT_SUBSECTION_ALIAS == "new"} class="active"{/if}>
			<a href="{$CONTROLLER_ALIAS}/add/" title="Добавить">Добавить</a>
		</li>
	</ul>