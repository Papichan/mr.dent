<h1>Отзывы</h1>

{*<form metho="get">
	<button type="submit" value="Добавить отзыв">Добавить отзыв</button>
</form>*}
{if !empty($REVIEWS)}
	{include file='paginator_new.tpl'}
	<table class="highlightable" id="freviewsTable">
	<colgroup>
		<col>
		<col>
		<col>
		<col>
		<col style="width: 10px;">
		<col>
	</colgroup>
    <thead>
        <tr>
            <th>№</th>
            <th>Имя</th>
            <th>IP</th>
			<th>Дата</th>
			<th>Язык</th>
            <th>Утвержден</th>
            <th style="width: 10px;"><img src="{$ADMIN_IMAGES}icons/delete_icon.png" /></th>
        </tr>
    </thead>
    <tbody>
    {foreach from=$REVIEWS item=REVIEW}
        <tr>
            <td style="display: none;"><a href="{$CONTROLLER_ALIAS}edit/{$REVIEW.reviewID}/" /></td>
            <td style="width: 20px;">{$REVIEW.reviewID}</td>
            <td>
                {$REVIEW.name}
            </td>
            <td>
                {$REVIEW.userIp}
            </td>
            <td>
                {$REVIEW.dateAdd|date_format:"%d.%m.%Y %H:%M"}
            </td>
            <td>
                {if empty($REVIEW.lang)}RU{else}EN{/if}
			</td>
						<td>
							{if $REVIEW.isApproved == 1}<span style="color: green">Да</span>{else}<span style="color: red">Нет</span>{/if}
            </td>
            <td class="deleteEntry">
                <a href="{$CONTROLLER_ALIAS}delete/{$REVIEW.reviewID}/" title="Удалить"><img src="{$ADMIN_IMAGES}delete_icon.png" alt="Удалить" style="border: 0;" /></a>
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>
	{include file='paginator_new.tpl'}
{else}
	Отзывов пока нет
{/if}