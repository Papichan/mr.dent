{literal}
<script type="text/javascript">
    $(document).ready(function(){
        $('.approveBtn').click(ApproveReview);
        $('.deleteBtn').click(DeleteReview);
    });

    function ApproveReview(event)
    {
        var reviewID = $(this).siblings('input[name=reviewID]').val();
        $.post(
            '{/literal}{$CONTROLLER_ALIAS}{literal}unapproved/',
            {
                reviewID : reviewID,
                action : "approve"
            },
            ModerateReviewResponse,
            'json'
        );
    }

    function DeleteReview(event)
    {
        var reviewID = $(this).siblings('input[name=reviewID]').val();
        $.post(
            '{/literal}{$CONTROLLER_ALIAS}{literal}unapproved/',
            {
                reviewID : reviewID,
                action : "delete"
            },
            ModerateReviewResponse,
            'json'
        );
    }

    function ModerateReviewResponse(data, status)
    {
        if (!standartAJAXResponseHandler(data, status)) return;
        if (data.msg == "ok")
        {
            $.jGrowl(data.text);
            RemoveReviewRow(data.reviewID);
        };
    }

    function RemoveReviewRow(reviewID)
    {
//		alert('RemoveReviewRow ' + reviewID);
//		alert($('input[name=reviewID][value='+ reviewID +']').parent().parent())
        $('input[name=reviewID][value='+ reviewID +']').parent().parent().fadeOut('slow',
            function()
            {
                $(this).remove();
                $('#moderationTable tbody tr').removeClass('even').removeClass('odd');

                $('#moderationTable tbody tr:even').addClass('even');
                $('#moderationTable tbody tr:odd').addClass('odd');

//                $('#moderationTable tbody tr').each(function(i)
//                {
//                    $(this).children('td:first').text(i+1);
//                });
            }
        );
    }
</script>
{/literal}

<h1>Новые отзывы</h1>

{include file='paginator.tpl'}
<table class="highlightable" id="moderationTable">
    <thead>
        <tr>
            <th>№</th>
            <th>Имя</th>
            <th>Заголовок</th>
			<th>Отзыв</th>
            <th>Дата</th>
            <th style="width: 80px;">Действие</th>
        </tr>
    </thead>
    <tbody>
    {foreach from=$REVIEWS item=REVIEW key=I}
        <tr>
            <td style="width: 20px;">{$REVIEW.reviewID}</td>
            <td>
                {$REVIEW.name}
            </td>
            <td>
                {$REVIEW.title}
            </td>
            <td>
                {$REVIEW.review}
            </td>
            <td style="width: 120px;">
                {$REVIEW.dateAdd|date_format:"%d.%m.%Y %H:%M"}
            </td>
            <td>
                <input type="hidden" name="reviewID" value="{$REVIEW.reviewID}" />
                <span class="approveBtn"><img src="{$ADMIN_IMAGES_ALIAS}icons/accept.png" title="Одобрить" /></span>
                <span class="deleteBtn"><img src="{$ADMIN_IMAGES_ALIAS}icons/cancel.png" title="Удалить" /></span>
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>                                                