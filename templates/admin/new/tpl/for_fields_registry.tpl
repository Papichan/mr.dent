
<script type="text/javascript">
{literal}
$(document).ready(init);
function init()
{
	var select = $(".controlTypeSelect").change(controlTypeSelectChangeHandler);
	select.append($("<option value=''>-</option>"));
	for (var i in standartSettings)
		select.append($("<option/>").val(i).text(i));
	select.append($("<option value='other'>Другое</option>"));

	$("#addField").click(function()
	{
		var name = $("#newField").val();
		if (name == null || name.length == 0)
		{
			$.jGrowl("Надо ввести какое-нибудь имя поля на англицком");
			return;
		}
		var tr = $("<tr>");
		tr.append($("<td>").text(name));

		var select = $("<select>")
			.addClass("controlTypeSelect")
			.attr("name", name+ "_controlType")
			.change(controlTypeSelectChangeHandler);
		select.append($("<option value=''>-</option>"));
		for (var i in standartSettings)
			select.append($("<option/>").val(i).text(i));
		select.append($("<option value='other'>Другое</option>"));
		tr.append($("<td>").append(select));

		tr.append($("<td>").append($("<input>").attr("type", "text").attr("name", name+"_description")));

		tr.append($("<td>")
						.append($("<input>").attr("type", "checkbox").attr("name", name+"_canChange").attr("id", name+"_canChange").attr("checked", true))
						.append($("<label>").attr("for", name+"_canChange").text("canChange"))
						.append($("<br/>"))
						.append($("<input>").attr("type", "checkbox").attr("name", name+"_visible").attr("id", name+"_visible").attr("checked", true))
						.append($("<label>").attr("for", name+"_visible").text("visible"))
						.append($("<br/>"))
						.append($("<input>").attr("type", "checkbox").attr("name", name+"_isListingKey").attr("id", name+"_isListingKey"))
						.append($("<label>").attr("for", name+"_isListingKey").text("isListingKey"))
						.append($("<br/>"))
						.append($("<input>").attr("type", "checkbox").attr("name", name+"_isMultiLanguage").attr("id", name+"_isMultiLanguage"))
						.append($("<label>").attr("for", name+"_isMultiLanguage").text("isMultiLanguage"))
					);

		tr.append($("<td>").append($("<input>").attr("type", "text").attr("name", name+"_tip")));
		tr.append($("<td>").append($("<textarea>").attr("cols", 50).attr("rows", 10).attr("name", name+"_controlSettings").addClass("controlSettings")));

		$("#registryTable").append(tr);
		$("#registryForm").append("<input type='hidden' name='additionalFields[]' value='"+name+"'>");
	});

	if (!$.browser.opera)
		$("#betterUseOpera").slideToggle();
}

var standartSettings = new Object();
standartSettings["LineInput"] =  {
               "blOrder"     : 0,
               "elemOrder"   : 0,
               "size"        : 60,
			   "className"   : " ",
			   "defaultValue": ""
           };

standartSettings["HiddenInput"] = {
               "blOrder"   : 0,
               "elemOrder" : 0
			};

standartSettings["RichText"] = {
               "blOrder"   : 0,
               "elemOrder" : 0,
			   "width"	: "100%",
			   "height" : 500,
			   "toolbarSet" : ""
			};

standartSettings["TextArea"] =  {
               "blOrder"   : 0,
               "elemOrder" : 0,
               "cols"        : 60,
               "rows"        : 5,
			   "className"	: " "
           };

standartSettings["SingleSelect"] = {
			   "enum"          : false,
               "referentTable" : " ",
               "listingKey"    : " ",
               "blOrder"       : 0,
               "elemOrder"     : 0,
               "emptyValue"    : 0,
			   "className"	   : " ",
               "emptyText"     : " - "
           };

standartSettings["Radio"] = {
			   "enum"             : false,
			   "referentTable"    : " ",
			   "listingKey"       : " ",
               "blOrder"          : 0,
               "elemOrder"        : 0,
			   "className"		  : " "
           };

standartSettings["CheckBox"] = {
				"checkedDefault" : false,
				"blOrder"        : 0,
				"elemOrder"      : 0,
				"className"		 : " "
};

standartSettings["MultipleSelect"] = {
				"referentTable"    : " ",
				"listingKey"       : " ",
				"linkingTable"     : " ",
				"blOrder"      	   : 0,
				"elemOrder" 	   : 0,
				"className"		   : " ",
				"size"			   : 10
			};

standartSettings["TagSelect"] = {
				"referentTable"    : " ",
				"listingKey"       : " ",
				"linkingTable"     : " ",
				"blOrder"      	   : 0,
				"elemOrder" 	   : 0,
				"className"		   : " ",
				"size"			   : 10
			};

standartSettings["DateInput"] =
			{
				"dateFormat" 				 : "d.m.Y",
				"yearRange"	 				 : "-10:+5",
				"insertCurrentDateIfEmpty"	 : false,
				"ignoreUserInput"	 		 : false,
				"blOrder"    				 : 0,
				"blName"    				 : " ",
				"elemOrder" 				 : 0
           };

standartSettings["DateMaskedInput"] =
			{
				"dateFormat" 				 : "d.m.Y",
				"mask"	 				 	 : "99.99.9999",
				"insertCurrentDateIfEmpty"	 : false,
				"ignoreUserInput"	 		 : false,
				"blOrder"    				 : 0,
				"blName"    				 : " ",
				"elemOrder" 				 : 0
           };

standartSettings["SWFUpload"] = {
    "blOrder" : 0,
    "elemOrder" : 0,
    "image" :
    {
        "resize" :
        {
            "strong" : false,
            "width" : 1000,
            "height" : 1000
        },
        "thumb" :
        {
            "resize" :
            {
                "strong" : false,
                "width" : 150,
                "height" : 150
            },
            "postfix" : "_small"
        }
    },
    "includeThumbCropDialog" : false,
    "maxFileSize" : 5048576,
    "filesType" : "file|image",
    "maxFilesNumber" : 1,
    "linkingTable" : "",
    "allowedExtensions" : "*.jpg; *.gif; *.png",
    "filesTable" : "greeny_files или greeny_images",
    "filesTableKey" : "fileID или imageID",
    "destinationDirectory" : "uploaded/",
    "uploadScript" : "/admin/upload/",
    "includeTitle" : true,
    "titleField" : "photoTitle",
    "usePriority" : true,
    "priorityField" : "priority"
};


standartSettings['DamnUpload'] = {
    'blOrder' : 0,
    'elemOrder' : 0,
    'image' :
    {
        'resize' :
        {
            'strong' : false,
            'width' : 1000,
            'height' : 1000
        },
        'thumb' :
        {
            'resize' :
            {
                'strong' : false,
                'width' : 150,
                'height' : 150
            },
            'postfix' : '_small'
        }
    },
    'includeThumbCropDialog' : false,
    'maxFileSize' : 5048576,
    'filesType' : 'file|image',
    'maxFilesNumber' : 1,
    'linkingTable' : '',
    'allowedExtensions' : '*.jpg; *.jpeg; *.gif; *.png',
    'filesTable' : 'greeny_files или greeny_images',
    'filesTableKey' : 'fileID или imageID',
    'destinationDirectory' : 'uploaded/',
    'uploadScript' : '/admin/upload/',
    'includeTitle' : true,
    'titleField' : 'photoTitle',
    'usePriority' : true,
    'priorityField' : 'priority'
};

standartSettings['DependendTable'] = {
	"linkingTable" : "",
	"blOrder" : 0,
	"elemOrder" : 0,
	"className" : " ",
	"size" : 10,
	"orderBy" : "",
	"textFields": {
	         	"field_name": {
				"label" : "field_label",
				"showLabel" : true,
				"containerType" : "line"
			}
	}

};
standartSettings['MediaGallery'] = {
	"linkingTable": "",
	"blOrder": 0,
	"elemOrder": 0,
	"className": " ",
	"size": 10,
	"orderBy": "",
	"textFields": {
		"field_name": {
			"label": "field_label",
			"showLabel": true,
			"containerType": "line"
		}
	}
};
standartSettings['DependentSingleSelect'] = {
		"referentTable": "table_name",
		"listingKey": "field_name",
		"orderBy": "",
		"dependsOn": "dependentField",
		"dependentField": "dependentField",
		"serverScript": "/dynamicselectbackend/getdependentselectoptions/",
		"blOrder": 0,
		"elemOrder": 0,
		"emptyValue": 0,
		"className": " ",
		"emptyText": " - "
	};

function onbjectToJsonString(obj, offset)
{
	if (obj == null) return "";
	if (typeof offset == "undefined") offset = 0;
	var res = "{\n";
	var tabs = "";
	for (var i = 0; i < offset; i++) tabs += "\t";
	var first = true;
	for (var i in obj)
	{
		if (first) first = false;
		else res += ",\n";
		if (typeof obj[i] == "object")
			res += tabs + '"' + i + "\"\t:\t" + onbjectToJsonString(obj[i], offset+1);
		else
		{
			res += tabs + '"' + i + "\"\t:\t";
			if (typeof obj[i] == "string")
				res += "\""+ obj[i] + "\"";
			else
				res += obj[i];
		}
	}
	return res + "\n}";
}
function controlTypeSelectChangeHandler(event)
{
	var that = $(this);
	if (that.val() == "other")
		that.parent().append($("<input type='text' name='"+that.attr("name")+"'/>")).end().remove();
	that.parent().siblings(":last").children().text(onbjectToJsonString(standartSettings[that.val()]));
}
{/literal}
</script>

	<div id="betterUseOpera" style="border:1px solid #ddd; padding: 20px; display:none; width:50%;">
	<p>Используйте лучше оперу с этим помощником, поскольку он не сохраняет состояние сгенерированной формы при репосте, а опера при возврате по истории назад страницу не перегружает и заполненные поля не сбрасывает.
	</p>
	<p style="margin-top:1em">Если Вы ненавидите оперу, можете использовать при этом резиновые перчатки, а потом протереться антисептиком или юзать свой тормозной ФФ с миллионом плагинов, но если вы что-то заполните не так, то придётся делать всё с начала. Я предупреждал :)
	</p>
	<p style="margin-top:1em">Уа-ха-ха</p>
	</div>

	{if isset($ERROR_MESSAGE)}<p style="color:#cc0000; font-size:1.5em; margin:15px 0;">{$ERROR_MESSAGE}</p>{/if}

	<form method="post" action="">
		<label for="tableName">Введите имя таблицы</label>
		<input type="text" value="{if isset($TABLE_NAME)}{$TABLE_NAME}{/if}" name="tableName" id="tableName" />
		<input type="submit" name="doBuildForm" value="Сгенерировать форму"/>
	</form>

	{if isset($SHOW_FIELDS_REGISTRY_FORM)}
	<form method="post" action="" id="registryForm">
	<input type="hidden" value="{if isset($TABLE_NAME)}{$TABLE_NAME}{/if}" name="tableName" />
	<input type="hidden" value="1" name="doBuildQuery" />
	<h1>tableName: {if isset($TABLE_NAME)}{$TABLE_NAME}{/if}</h1>

	<table id="registryTable">
	<tr>
		<th>fieldName</th>
		<th>controlType</th>
		<th>description</th>
		<th style="text-align:left">canChange / <br/>visible / <br/>isListingKey / <br/>isMultiLanguage	</th>
		<th>tip</th>
		<th>controlSettings</th>
	</tr>
	{foreach from=$FIELDSLIST item=field_item}
		<tr>
		<td>{$field_item}</td>
		<td>
			<select name="{$field_item}_controlType" class="controlTypeSelect">

			</select>
		</td>
		<td><input type="text" name="{$field_item}_description" /></td>
		<td>

			<input type="checkbox" name="{$field_item}_canChange" id="{$field_item}_canChange" checked="checked"/> <label for="{$field_item}_canChange">canChange</label><br/>
			<input type="checkbox" name="{$field_item}_visible" id="{$field_item}_visible" checked="checked"/> <label for="{$field_item}_visible">visible</label><br/>
			<input type="checkbox" name="{$field_item}_isListingKey" id="{$field_item}_isListingKey"/> <label for="{$field_item}_isListingKey">isListingKey</label><br/>
			<input type="checkbox" name="{$field_item}_isMultiLanguage" id="{$field_item}_isMultiLanguage"/> <label for="{$field_item}_isMultiLanguage">isMultiLanguage</label><br/>

		</td>
		<td><input type="text name="{$field_item}_tip" /></td>
		<td><textarea cols="50" rows="10" name="{$field_item}_controlSettings" class="controlSettings"></textarea></td>
		</tr>
	{/foreach}
	</table>
	<div style="margin:20px;">
	<input type="text" id="newField" name="newField"/> <input type="button" value=" Добавить ещё поле " id="addField"/>
	</div>
	<input type="submit" value=" YARRRR " style="width:200px; height: 40px;margin:20px;" />
	</form>
	{$QUERY}
	{/if}
{*
	{literal}
	<pre>

<h3>Файл с описанием элементов управления</h3>
----------------------------------------------

Настройки вывода общие для всех элементов управления

blName: - название блока, в котором будет отображаться это поле
blOrder: - задает порядок следования блоков (от меньшего к большему)
elemOrder: - порядок следования элементов внутри блока
className: - CSS класс, который применится к контейнеру контрола или к полю ввода (зависит от реализации)

----------------------------------------------

MultipleImageUpload


Пример настроек:

{
	"blClass"		:	"mvCover",
	"referentTable"	:	"images",
	"linkingTable"	: 	"personPhotos",
	"maxInputNum"	:	5,
	"size"			:	30,
	"image"			:
	{
		"resize" :
		{
			"strong"	: false,
			"width"		: 600,
			"height"	: 1000
		},
		"thumb"	 :
		{
			"resize" :
			{
				"strong"	: false,
				"width"		: 150,
				"height"	: 150
			},
			"postfix" : "_small"
		}
	}
}

----------------------------------------------

FloatInput
	- текстовое поле для ввода вещественного числа с заданной точностью

Настройки:

precision - точность округления (количество знаков после запятой). Если 0, то округляться не будет

Пример настроек:

{
	"blOrder"	: 0,
	"blClass"	: "showInline width300",
	"size"		: 30,
	"precision"	: 1
}

----------------------------------------------

DurationInput
	- 4 текстовых поля (ЧЧ:ММ:СС:КК)

Настройки:

Пример настроек:

{
    "blOrder"    : 0,
    "blClass"    : "clr",
    "elemOrder"  : 60,
    "durField"   : "duration",
    "frameField" : "durationFrame"
}

----------------------------------------------

IntegerInput
    - целое число
----------------------------------------------

	</pre>
	{/literal}
*}