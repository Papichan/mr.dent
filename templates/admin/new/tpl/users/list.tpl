<h1>Список пользователей</h1>

{literal}
<script type="text/javascript">
    $(document).ready(function(){
        $('#showFiltersBtn').click(function(){
            $('#filterForm').toggle();
        });
    });
</script>
{/literal}

<div style="text-align: right; margin: -30px 0 5px 0;">
<form action="" method="post">
    <input type="button" id="showFiltersBtn" value="Фильтры" />
    <input type="button" id="goAddUser" class="goToBtn" value="Добавить пользователя" />
    <input type="hidden" id="goAddUser_link" value="{$SECTION_ALIAS}add/" />
</form>
</div>
{include file='users/filters.tpl'}

{if $USERS}
{include file='paginator.tpl'}
<table id="entriesList" class="highlightable users">
    <thead>
        <tr>
            <th style="width: 200px;">Логин</th>
            <th>E-mail</th>
            <th>ФИО</th>
            <th>Тип пользователя</th>
            <th>Дата регистрации</th>
            <th>Активность</th>
            <th>Удалить</th>
        </tr>
    </thead>
    <tbody>
    {foreach from=$USERS item=user_item}
        <tr>
            <td style="display: none;"><a href="{$SECTION_ALIAS}edit/{$user_item.userID}/"></a></td>
            <td>
                {$user_item.login}
            </td>
            <td>
                {$user_item.email}
            </td>
            <td>
                {$user_item.FIO}
            </td>
            <td>
                {$user_item.roleName}
            </td>
            <td>
                {$user_item.registrationDate|date_format:"%d.%m.%Y %H:%M:%S"}
            </td>
            <td>
                {if $user_item.isActive == 1}
                    <span style="color: green;">Активен</span>
                {else}
                    <span style="color: red;">Не активен</span>
                {/if}
            </td>
            <td>
                <a href="/admin/users/delete/{$user_item.userID}/" title="Удалить пользователя" onclick="return AreYouSure('Удалить пользователя')"><img src="{$ADMIN_IMAGES}delete.png"></a>
            </td>
        </tr>
    {/foreach}
    </tbody>
</table>
{else}
Список пользователей пуст.
{/if}