<h1>Редактирование пользователя [<a href="/admin/users/info/{$EDIT_USER.userID}/" title="Редактировать дополнительные данные"><img src="{$ADMIN_IMAGES}icons/pencil.png"></a>][<a href="/admin/users/delete/{$EDIT_USER.userID}/" title="Удалить пользователя" onclick="return AreYouSure('Удалить пользователя')"><img src="{$ADMIN_IMAGES}icons/cross.png"></a>]</h1>

<form action="" method="post" id="contentForm" enctype="multipart/form-data">
    {include file='formBlocks.tpl'}

	<div style="margin-left:200px">
		<input type="submit" value="Сохранить" name="doSave" class="handyButton" />
	</div>
</form>