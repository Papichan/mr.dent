<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-language" content="ru" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title>{$SITE_NAME} &mdash; Bitplatform</title>

</head>
<body>

<h2>Регистрация на сайте «<a href="{$HOST_NAME}">{$SETTINGS.general.siteName}</a>»</h2>

<p>
    <div style="font-size: 120%;">Параметры доступа:</div>
    <strong>Имя пользователя:</strong> {$LOGIN}<br />
    <strong>Пароль:</strong> {$PASSWORD}
</p>

<p style="margin-top: 30px">
    Данное сообщение автоматически сгенерировано системой рассылки сайта «{$SETTINGS.general.siteName}». На него отвечать не нужно.
</p>

</body>
</html>