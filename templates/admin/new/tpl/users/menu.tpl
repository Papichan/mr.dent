<ul>
	<li {if !isset($EDIT_USER)} class="active"{/if}>
		<a href="{$SECTION_ALIAS}" title="Список всех пользователей">Список пользователей</a>
	</li>
	{if isset($EDIT_USER)}
		<li {if !isset($INFO_EDIT)}class="active"{/if}>
			<a href="{$SECTION_ALIAS}edit/{$EDIT_USER.userID}/" title="Редактирование основных данных пользователя">Редактирование пользователя</a>
		</li>
		<li {if isset($INFO_EDIT)}class="active"{/if}>
			<a href="{$SECTION_ALIAS}passchange/{$EDIT_USER.userID}/" title="Сменить пароль">Смена пароля</a>
		</li>
	{/if}
</ul>
