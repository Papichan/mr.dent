<h1>Режим обслуживания</h1>
<form method='POST' action ='' class='maintenanceForm'>
    
    <div class='formElement'>
        <input type='hidden' name='maintenanceEnabled' value='0' />
        <input type='checkbox' name='maintenanceEnabled' id='maintenanceEnabled' {if !empty($maintenanceEnabled)}checked='checked'{/if} value='1' />
        <label for='maintenanceEnabled'><strong>Включить режим обслуживания сайта</strong></label>
    </div>
    <div class='formElement'>
        <label for='maintenancePageText'>Содержимое страницы обслуживания:</label><br/>
        <textarea name='maintenancePageText' class="fck" id="maintenancePageText">{$maintenancePageText}</textarea>
    </div>
    <input type="submit" value='Сохранить' />
</form>