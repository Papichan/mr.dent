<ul>
    {if isset($GROUPS)}
    {foreach from=$GROUPS item=GROUP}
		<li {if ($GROUP.groupID == $GROUP_ID)}class="active"{/if}>
			<a href="{$SECTION_ALIAS}{$GROUP.groupID}/">{$GROUP.desc}</a>
        {if isset($GROUP.groups) && $GROUP.groups|@count > 0}
            {include file='settings/menu_group.tpl' GROUPS=$GROUP.groups}
        {/if}
    </li>
    {/foreach}
    {/if}
</ul>