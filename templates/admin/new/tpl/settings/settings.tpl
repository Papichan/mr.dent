<h1>{$GROUP.desc}</h1>

<div>
    <div>
        <input type="button" id="goAddSetting" class="goToBtn" value="Добавить настройку" />
        <input type="hidden" id="goAddSetting_link" value="{$SECTION_ALIAS}setting/add/{$GROUP.groupID}/" />
    </div>
    {if $EDIT_SETTINGS|@count == 0}
    В данной группе настроек нет
    {else}
	<form action="" method="post">
		<ul>
			{foreach from=$EDIT_SETTINGS item=SETTING}
			<li>[ <strong>{$SETTING.name}</strong> ] {$SETTING.desc}
				<br />
                <input type="text" name="{$SETTING.settingID}" size="60" value="{$SETTING.value}" {if $SETTING.isVisible == 0}readonly='readonly'{/if} />
            </li>
			{/foreach}
		</ul>
		<div><input type="submit" value="Сохранить изменения" name="doSave" /></div>
	</form>
	{/if}
</div>