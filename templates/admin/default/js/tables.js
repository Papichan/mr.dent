$(document).ready(function()
{
	//$('.elementsList td:not(.skip)').hover(rowHoverOver, rowHoverOut);
  	//$('.elementsList td:not(.skip)').click(rowClick);
  	
  	$('.highlightable td:not(.skip)').hover(highlightableRowHoverOver, highlightableRowHoverOut);
	$('.highlightable td:not(.skip)').click(highlightableRowClick);
  	
  	$('.highlightable tbody tr:even').addClass('even');
  	$('.highlightable tbody tr:odd').addClass('odd');
  	
  	$('.elementsList tbody tr:even').addClass('even');
  	$('.elementsList tbody tr:odd').addClass('odd');
  	
  	$('.elementsList tbody tr td input:checkbox').change(deleteCheck);
  	deleteCheck();
});

function highlightableRowHoverOver(event)
{
    $(this).addClass('hoverRow').siblings().addClass('hoverRow');
}
function highlightableRowHoverOut(event)
{
    $(this).removeClass('hoverRow').siblings().removeClass('hoverRow');
}
function highlightableRowClick(event)
{
    var link = $(this).parent().children().eq(0).children('a').attr('href');
    if (typeof link != 'undefined')
    {
        var target = $(this).parent().children().eq(0).children('a').attr('target');
        if (typeof target != 'undefined' && target == '_blank')
        {
            window.open(link); 
        }
        else
        {
            window.location.href = link;
        }
    }
}

// Для кнопки удаления
function deleteCheck(event)
{
	var n = $('.elementsList tbody tr td input:checked').length;
	if (n == 0)
		$('#deleteItems').attr('disabled', 'disabled');
	else
		$('#deleteItems').removeAttr('disabled');
}