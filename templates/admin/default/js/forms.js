$(document).ready(initAjaxFormsUI);

function initAjaxFormsUI()
{
	$(document).ajaxError(function(event, request, settings){$.jGrowl('AJAX Error')});
	$('.ajaxForm').validate(
	{
        submitHandler: function(form) 
        {
		   	$(form).ajaxSubmit(
		   	{
		   	    success:      ajaxFormSuccess, 
		   	    beforeSubmit: ajaxFormSetRequest,
		   	    dataType:     "json"
	   	    });
			return false;
		}
   });
   
   $("[required]").each(function()
   { 
        $(this).rules("add", {"required" : true});
   });
}

function ajaxFormSetRequest(form, jObject, options)
{
    $('#submitResult').remove();
	form.push({name:"async",value:"1"});
	form.push({name:"doSave",value:"1"});
}

function ajaxFormSuccess(data, status)
{
	if (status == 'success')
	{
		if (data.error)
		{
		    ajaxFormWriteError(data.error);
			return false;
		}
	}
	else
	{
		ajaxFormWriteError("Ошибка запроса");
		return;
	}
	
	if (data.id > 0)
	{
	    ajaxFormWriteSuccess();
	    $('.ajaxForm').resetForm();
	}
	else 
	{
	    ajaxFormWriteError("Неизвестная ошибка. Обратитесь к разработчикам.");
	}
}

function ajaxFormWriteSuccess()
{
    var block = '<div class="sFormRow" id="submitResult"><div class="sFormRow_Left">&nbsp;</div><div class="sFormRow_Right"><span style="color: green;">Запись сохранена</span></div></div>';
    
    $('.ajaxForm').before(block);
}

function ajaxFormWriteError(error)
{
    var block = '<div class="sFormRow" id="submitResult"><div class="sFormRow_Left">&nbsp;</div><div class="sFormRow_Right"><span style="color: red;">Запись не сохранена: </span>' + error + '</div></div>';
    
    $('.ajaxForm').before(block);
}