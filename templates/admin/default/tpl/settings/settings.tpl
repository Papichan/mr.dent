<h1>{$GROUP.desc}</h1>
			
<div>
    <div>
        <input type="button" id="goAddSetting" class="goToBtn" value="Добавить настройку" />
        <input type="hidden" id="goAddSetting_link" value="{$SECTION_ALIAS}setting/add/{$GROUP.groupID}/" />
    </div>
    {if $EDIT_SETTINGS|@count == 0}
    В данной группе настроек нет
    {else}
	<form action="" method="post">
		<ul>
			{foreach from=$EDIT_SETTINGS item=SETTING}
                        {if $SETTING.isVisible}
                            {if ($SETTING.settingID == 8)}
                            <!-- авторизация для фронт-енда -->
                            <li>
                                {$SETTING.desc}:<br />
                                <input {if $SETTING.value}checked{/if} type="radio" name="{$SETTING.settingID}" value="1"> Да<br />
                                <input {if !$SETTING.value}checked{/if} type="radio" name="{$SETTING.settingID}" value="0"> Нет<br />
                            </li>
                            {else}
                            <li>[ <strong>{$SETTING.name}</strong> ] {$SETTING.desc}
                                    <br />
                                    <input type="text" name="{$SETTING.settingID}" size="60" value="{$SETTING.value}" />
                            </li>
                            {/if}
                        {/if}
			{/foreach}
		</ul>					
		<div><input type="submit" value="Сохранить изменения" name="doSave" /></div>
	</form>
	{/if}
</div>