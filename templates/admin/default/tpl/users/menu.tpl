            <div class="menuBlock">
				<ul>
					<li>
						<a href="{$SECTION_ALIAS}" title="Список всех пользователей">Список пользователей</a>
					</li>
					{if isset($INFO_EDIT)}
					<li>
    					<a href="{$SECTION_ALIAS}edit/{$EDIT_USER.userID}/" title="Редактирование основных данных пользователя">Основные данные</a>
					</li>
					{/if}
					{if isset($MAIN_EDIT)}
					<li>
    					<a href="{$SECTION_ALIAS}info/{$EDIT_USER.userID}/" title="Редактирование дополнительных данных пользователя">Дополнительные данные</a>
					</li>
					{/if}
					<li>
						<a href="{$SECTION_ALIAS}passchange/{$EDIT_USER.userID}/" title="Сменить пароль">Смена пароля</a>
					</li>
				</ul>
			</div>
			<div id="sidebar_bottom"></div>