<h1>Элементы управления для форм генератора</h1>


<script type="text/javascript">
    var sectionAlias = '{$SECTION_ALIAS}';
{literal}
    $(document).ready(function(){
        $('.controlItem .name').css('cursor', 'pointer').click(ShowControlForm);
        $('.controlFormContainer .close').css('cursor', 'pointer').click(CloseControlForm);
        
        $('.controlFormContainer').jqm({
            modal: true
        });
        
        $('.showSettingsBtn').css('cursor', 'pointer').click(ShowNewSettingForm);
        $('.settingFormContainer .close').css('cursor', 'pointer').click(CloseSettingForm);
        
        $('.settingFormContainer').jqm({
            modal: true
        });
        
        $('.controlFormContainer form').each(function(){
            $(this).validate({
                submitHandler: function(form) 
                {
        		   	$(form).ajaxSubmit({
        		   	    success: SaveFormResponse, 
        		   	    beforeSubmit: ShowRequest, 
        		   	    dataType: "json",
        		   	    type: "post"
        		   	});
        			return false;
        		}
        	});
        });
        
        $('.showSettingsBtn').click(function(){
            $(this).parent().next('.controlSettingsContainer').toggle();
            if ($(this).text() == '[показать]')
                $(this).text('[скрыть]');
            else
                $(this).text('[показать]');
        });
    });
    
    function ShowRequest(form, jObject, options)
    {
    	form.push({name:"async",value:"1"});
		form.push({name:"doSave",value:"1"});
    }
    
    function SaveFormResponse(data, status)
    {
        CloseControlForm();
        
    	if (!standartAJAXResponseHandler(data, status)) return;
    	if (typeof(data) == 'undefined')
    	    $.jGrowl("Возникла ошибка при редактировании");

	    UpdateControlParams(data);
	    $.jGrowl('Контрол обновлен');
    }

    function UpdateControlParams(control)
    {
        var controlItem = openned.parent();
        
        if (typeof(controlItem) == 'undefined')
        {
            $.jGrowl('Возникла ошибка при обнолении данных на странице. Перезагрузите страницу.');
            return;
        }
        
        controlItem.children('.name').eq(0).text(control.name);
        controlItem.children('.description').eq(0).text(control.description);
        controlItem.children('.version').eq(0).text("Версия: <strong>" + control.version + "</strong>");
    }
    
    var openned = null;
    
    function ShowControlForm(event)
    {
        openned = $(this).siblings('.controlFormContainer');
        openned.jqmAddClose($(this).siblings('.controlFormContainer .close'));
        openned.jqmShow();
    }
    
    function CloseControlForm(event)
    {
        openned.jqmHide();
        //openned = null;
    }
    
    
    function ShowNewSettingForm(event)
    {
        openned = $('.settingFormContainer');
        openned.jqmShow();
    }
    
    function ShowSettingForm(event)
    {
        openned = $(this).siblings('.controlFormContainer');
        openned.jqmAddClose($(this).siblings('.controlFormContainer .close'));
        openned.jqmShow();
    }
    
    function CloseSettingForm(event)
    {
        openned.jqmHide();
        //openned = null;
    }
{/literal}
</script>


{if $CONTROLS|@count > 0}

{foreach from=$CONTROLS item=CONTROL}
    <div class="controlItem">
        <input type="hidden" name="controlID" value="{$CONTROL.controlID}" />
        <div class="name">{$CONTROL.name}</div>
        <div class="description">{$CONTROL.description}</div>
        <div class="version">Версия: <strong>{$CONTROL.version}</strong></div>
        <div class="settings">Настроек: <strong>{$CONTROL.settings|@count}</strong> {if $CONTROL.settings|@count > 0}<span class="showSettingsBtn">[показать]</span>{/if} <input type="button" class="showSettingsBtn" value="Добавить" /></div>
        
        <table class="controlSettingsContainer">
            {foreach from=$CONTROL.settings item=SETTING}
            <tr>
                <td class="title">{$SETTING.description}</td>
                <td class="description">{$SETTING.defaultValue}</td>
            </tr>
            {/foreach}
        </table>
        
        <div class="controlFormContainer jqmWindow">
            <div style="width: 100%; text-align: right;"><span class="close">Закрыть</span></div>
            <form action="{$SECTION_ALIAS}savecontrol/{$CONTROL.controlID}/" method="post">
                <div class="row">
                    <div class="left"><label for="name">Название</label></div>
                    <div class="right"><input type="text" name="name" value="{$CONTROL.name}" /></div>
                </div>
                <div class="row">
                    <div class="left"><label for="description">Описание</label></div>
                    <div class="right"><input type="text" name="description" value="{$CONTROL.description}" /></div>
                </div>
                <div class="row">
                    <div class="left"><label for="version">Версия</label></div>
                    <div class="right"><input type="text" name="version" value="{$CONTROL.version}" /></div>
                </div>
                
                <div style="text-align: center;">
                    <input type="submit" name="doSave" value="Сохранить" style="width: 100px;"/>
                </div>
            </form>
        </div>
    </div>
{/foreach}
    
<div class="settingFormContainer jqmWindow">
    <div style="width: 100%; text-align: right;"><span class="close">Закрыть</span></div>
    <form action="{$SECTION_ALIAS}addsetting/" method="post" id="">
        <div class="row">
            <div class="left"><label for="name">Название</label></div>
            <div class="right"><input type="text" name="name" value="" /></div>
        </div>
        <div class="row">
            <div class="left"><label for="description">Описание</label></div>
            <div class="right"><input type="text" name="description" value="" /></div>
        </div>
        <div class="row">
            <div class="left"><label for="defaultValue">Значение</label></div>
            <div class="right"><input type="text" name="defaultValue" value="" /></div>
        </div>
        
        <div style="text-align: center;">
            <input type="submit" name="doSave" value="Сохранить" style="width: 100px;"/>
        </div>
    </form>
</div>
    
{else}
    Нет ни одного контрола.
{/if}