-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: localhost    Database: bitplatform
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `MainMenu`
--

DROP TABLE IF EXISTS `MainMenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MainMenu` (
  `entryID` int(11) NOT NULL AUTO_INCREMENT,
  `pageID` int(11) NOT NULL,
  PRIMARY KEY (`entryID`),
  KEY `pageID` (`pageID`),
  CONSTRAINT `MainMenu_ibfk_1` FOREIGN KEY (`pageID`) REFERENCES `greeny_pageStructure` (`pageID`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MainMenu`
--

LOCK TABLES `MainMenu` WRITE;
/*!40000 ALTER TABLE `MainMenu` DISABLE KEYS */;
INSERT INTO `MainMenu` VALUES (5,5),(22,23),(25,26),(27,28),(28,29),(29,30),(30,43),(31,50),(32,54),(33,55);
/*!40000 ALTER TABLE `MainMenu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Menu`
--

DROP TABLE IF EXISTS `Menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Menu` (
  `itemID` int(11) NOT NULL AUTO_INCREMENT,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `menuListID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `smallImage` smallint(4) DEFAULT NULL,
  `bigImage` smallint(4) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `isButton` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`itemID`),
  KEY `FK_Menu_MenuList` (`menuListID`),
  CONSTRAINT `FK_Menu_MenuList` FOREIGN KEY (`menuListID`) REFERENCES `MenuList` (`menuListID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Menu`
--

LOCK TABLES `Menu` WRITE;
/*!40000 ALTER TABLE `Menu` DISABLE KEYS */;
INSERT INTO `Menu` VALUES (18,0,2,'-','-',NULL,NULL,0,1,0),(19,18,2,'Консультация','consultation',88,214,1,1,0),(20,18,2,'Профессиональная гигиена полости рта','gigiena',89,130,2,1,0),(21,18,2,'Имплантация','implant',90,148,3,1,0),(22,18,2,'Терапевтическая стоматология','ortoped',91,149,4,1,0),(23,18,2,'Ортопедическая стоматология','consultation',92,150,5,1,0),(24,18,2,'Хирургическая стоматология','hirurg',93,151,6,1,0),(25,18,2,'Ортодонтия','ortodont',94,215,10,1,0),(26,18,2,'Лечение дёсен','desny',95,153,8,1,0),(27,18,2,'Диагностика','diagnos',96,154,9,1,0),(28,18,2,'Отбеливание','white',97,217,7,1,0),(29,19,2,'Бесплатная консультация','free-consultation',NULL,NULL,1,1,0),(30,20,2,'Ультразвуковая чистка','ultra-сleaning',NULL,NULL,1,1,0),(31,20,2,'Ультразвуковая чистка + AirFlow','air-flow',NULL,NULL,2,1,0),(32,21,2,'Классическая имплантация','classic-implant',NULL,NULL,1,1,0),(33,21,2,'Одномоментная имплантация','Single-implant',NULL,NULL,2,1,0),(34,21,2,'All-on-4','all-on-4',NULL,NULL,3,1,0),(35,21,2,'Костная пластика','bone-grafting',NULL,NULL,4,1,0),(36,21,2,'Синус-лифтинг','sinus-lifting',NULL,NULL,5,1,0),(37,22,2,'Лечение кариеса','lechenie_caries',NULL,NULL,1,1,0),(38,22,2,'Реставрация зуба','tooth_restoration',NULL,NULL,2,1,0),(39,22,2,'Лечение пульпита','pulpit',NULL,NULL,3,1,0),(40,22,2,'Лечение периодонтита','periodontit',NULL,NULL,4,1,0),(41,23,2,'Металлокерамические коронки','metal-ceramic ',NULL,NULL,1,1,0),(42,23,2,'Циркониевые коронки','zirconia-crowns',NULL,NULL,2,1,0),(43,23,2,'Керамические коронки E.Max','ceramic-emax',NULL,NULL,3,1,0),(44,23,2,'Полный съёмный протез','full-prosthesis',NULL,NULL,4,1,0),(45,23,2,'Бюгельный протез','clasp-prosthesis',NULL,NULL,5,1,0),(46,23,2,'Протез без нёба','prosthetic-palate',NULL,NULL,6,1,0),(47,23,2,'All-on-4','all-on-4',NULL,NULL,7,1,0),(48,23,2,'Керамические виниры','ceramic-veneer',NULL,NULL,8,1,0),(49,23,2,'Установка виниров','veneers-installation',NULL,NULL,9,1,0),(50,24,2,'Удаление однокорневого зуба','removal-tooth',NULL,NULL,1,1,0),(51,24,2,'Удаление многокорневого зуба','removal-multi-tooth',NULL,NULL,2,1,0),(52,24,2,'Сложное удаление зуба','complex-tooth',NULL,NULL,3,1,0),(53,24,2,'Резекция верхушки корня','root-resection',NULL,NULL,4,1,0),(54,28,2,'Отбеливание Zoom 4','white-4-zoom',NULL,NULL,1,1,0),(55,27,2,'Рентген зубов','x-ray-teeth',NULL,NULL,1,1,0),(56,26,2,'Стоматит','stomatit',NULL,NULL,1,1,0),(57,26,2,'Пародонтоз','periodontos',NULL,NULL,2,1,0),(58,26,2,'Гингивит','gingivit',NULL,NULL,3,1,0),(59,26,2,'Пародонтит','periodontit',NULL,NULL,4,1,0),(60,25,2,'Металлические брекеты','metal-bracet',NULL,NULL,1,1,0),(61,25,2,'Керамические брекеты','ceramic-bracet',NULL,NULL,2,1,0),(62,25,2,'Сапфировые брекеты','sapphire-bracet',NULL,NULL,3,1,0),(63,25,2,'Самолигирующие брекеты','self-bracet',NULL,NULL,4,1,0),(64,25,2,'Лингвальные брекеты','lingual-braces',NULL,NULL,5,1,0),(65,25,2,'Брекеты Incognito','braces-Incognito ',NULL,NULL,6,1,0),(66,25,2,'Элайнеры','aligners',NULL,NULL,7,1,0);
/*!40000 ALTER TABLE `Menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MenuList`
--

DROP TABLE IF EXISTS `MenuList`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MenuList` (
  `menuListID` int(11) NOT NULL AUTO_INCREMENT,
  `menuListName` varchar(255) NOT NULL,
  `menuListAlias` varchar(255) NOT NULL,
  `menuListComment` varchar(255) NOT NULL,
  PRIMARY KEY (`menuListID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MenuList`
--

LOCK TABLES `MenuList` WRITE;
/*!40000 ALTER TABLE `MenuList` DISABLE KEYS */;
INSERT INTO `MenuList` VALUES (2,'Меню для сервиса','SERVICES_MENU','Меню для  раздела Услуги');
/*!40000 ALTER TABLE `MenuList` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Roles`
--

DROP TABLE IF EXISTS `Roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Roles` (
  `role` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `level` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`role`),
  KEY `Role` (`role`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Roles`
--

LOCK TABLES `Roles` WRITE;
/*!40000 ALTER TABLE `Roles` DISABLE KEYS */;
INSERT INTO `Roles` VALUES ('admin','Администратор',100),('moderator','Модератор',80),('user','Пользователь',20);
/*!40000 ALTER TABLE `Roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `FIO` varchar(255) NOT NULL,
  `role` enum('user','moderator','admin') NOT NULL DEFAULT 'user',
  `isActive` tinyint(1) NOT NULL,
  `registrationDate` datetime DEFAULT NULL,
  PRIMARY KEY (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (1,'admin','ie@committee.ru','$2a$08$4D9weBAt/hWz3Wmb1buEwOsBjbyOKrhy0nPYiSXGw4md1Gido2d26','admin','admin',1,'2009-07-01 20:00:00');
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UsersIdentity`
--

DROP TABLE IF EXISTS `UsersIdentity`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UsersIdentity` (
  `entryID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL DEFAULT '0',
  `identity` varchar(16) NOT NULL DEFAULT '',
  PRIMARY KEY (`entryID`),
  KEY `userID` (`userID`),
  CONSTRAINT `UsersIdentity_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UsersIdentity`
--

LOCK TABLES `UsersIdentity` WRITE;
/*!40000 ALTER TABLE `UsersIdentity` DISABLE KEYS */;
/*!40000 ALTER TABLE `UsersIdentity` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UsersInfo`
--

DROP TABLE IF EXISTS `UsersInfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UsersInfo` (
  `userID` int(11) NOT NULL,
  `FIO` varchar(255) NOT NULL,
  `birthdate` date DEFAULT NULL,
  `gender` enum('Мужской','Женский') NOT NULL DEFAULT 'Мужской',
  PRIMARY KEY (`userID`),
  CONSTRAINT `UsersInfo_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `Users` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UsersInfo`
--

LOCK TABLES `UsersInfo` WRITE;
/*!40000 ALTER TABLE `UsersInfo` DISABLE KEYS */;
INSERT INTO `UsersInfo` VALUES (1,'Admin','1979-06-26','Мужской');
/*!40000 ALTER TABLE `UsersInfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `about_company`
--

DROP TABLE IF EXISTS `about_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `about_company` (
  `about_companyID` int(11) NOT NULL AUTO_INCREMENT,
  `titleTop` varchar(255) DEFAULT NULL,
  `imageTop` smallint(4) DEFAULT NULL,
  `imageTeam` smallint(4) DEFAULT NULL,
  `titleTeam` varchar(255) DEFAULT NULL,
  `textTeam` text,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `certificateText` varchar(255) DEFAULT NULL,
  `titleBottom` varchar(255) DEFAULT NULL,
  `imageBottom` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`about_companyID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `about_company`
--

LOCK TABLES `about_company` WRITE;
/*!40000 ALTER TABLE `about_company` DISABLE KEYS */;
INSERT INTO `about_company` VALUES (1,'<div class=\"up__text1\">Несколько слов</div>\r\n<p>&nbsp;</p>\r\n<div class=\"up__text2\">О клинике</div>',67,68,'Мы работаем, чтобы вы улыбались ','<p>Главная цель стоматологии &laquo;Mr.Dent&raquo; &mdash; делать вашу  улыбку совершенной. Наша клиника неоднократно завоёвывала статус лучшей  стоматологии в региональных и национальных конкурсах, что подтверждает  качество работы специалистов центра.                       <br />\r\n<br />\r\nПерсональный подход к вашей ситуации, внимательный  и высококвалифицированный персонал помогут решить вашу проблему и  принесут исключительно положительные эмоции.</p>\r\n<p>&nbsp;</p>','С любовью к своей работе ','<p>Непревзойдённое качество лечения, искренность и внимание по отношению к  пациентам, безупречная репутация и положительные эмоции от посещения  клиники &laquo;Виртуоз&raquo; &mdash; заслуга наших стоматологов.         <br />\r\n<br />\r\n<br />\r\nКаждый врач нашего центра &mdash; сертифицированный специалист с  многолетним опытом работы и большим количеством постоянных клиентов. Мы  искренне любим свою работу и выполняем её на высоком профессиональном  уровне</p>','Наши врачи идут в ногу со временем и перенимают опыт специалистов с  мировым именем, чтобы оказывать пациентам только современную  стоматологическую помощь.',NULL,69);
/*!40000 ALTER TABLE `about_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `advantage`
--

DROP TABLE IF EXISTS `advantage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advantage` (
  `advantageID` int(11) NOT NULL AUTO_INCREMENT,
  `pageID` smallint(4) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `imageID` int(11) DEFAULT NULL,
  `isApproved` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`advantageID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advantage`
--

LOCK TABLES `advantage` WRITE;
/*!40000 ALTER TABLE `advantage` DISABLE KEYS */;
INSERT INTO `advantage` VALUES (5,30,'Мастерство исполнения !','<p>! Высокий уровень лечения, быстрая помощь даже в сложных ситуациях, внимательное                            отношение к каждому пациенту, гарантия                            долгосрочного результата превращает работу                            наших специалистов в настоящее искусство                            по созданию улыбок.</p>',74,1),(6,30,'Искренний сервис','<p>Вы достойны самого лучшего отношения к себе и самого высокого уровня лечения. Доброжелательность наших специалистов,                            искренность, умение находить подход к                            каждому дадут вам возможность                            почувствовать себя самым важным                            посетителем.</p>',42,1),(7,30,'Премиальное качество','<p>Мы изучаем и внедряем все последние                            достижения мировой медицины. Предлагаем                            своим пациентам высочайший уровень                            обслуживания и качества лечения.                            Клиника &laquo;Виртуоз&raquo; отвечает статусу                            лучшей стоматологии Воронежа                            с высоким уровнем обслуживания.</p>',43,1),(8,30,'Передовые технологии','<p>Современная стоматология &laquo;Виртуоз&raquo;                            обладает стоматологическим и                            диагностическим оборудования последнего                            поколения, применяет в работе проверенные                            материалы от известных производителей,                            активно применяет методы комфортного                            лечения без боли.</p>',44,1),(9,26,'Многолетний опыт','<p>В нашей стоматологии работают только сертифицированные специалисты с опытом работы не менее 8 лет</p>',124,1),(10,26,'Безупречная репутация','<p>Мастерство врачей клиники &laquo;Mr.Dent&raquo; подтверждено тысячами положительных отзывов от благодарных пациентов</p>',125,1);
/*!40000 ALTER TABLE `advantage` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `blue_blocks`
--

DROP TABLE IF EXISTS `blue_blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blue_blocks` (
  `blue_blocksID` int(11) NOT NULL AUTO_INCREMENT,
  `parentPage` varchar(255) DEFAULT NULL,
  `textTop` text,
  `imageIconID` smallint(4) DEFAULT NULL,
  `imageIconTitle` varchar(255) DEFAULT NULL,
  `dependendDoctor` smallint(4) DEFAULT NULL,
  `imageAvaTitle` varchar(255) DEFAULT NULL,
  `imageAvaID` smallint(4) DEFAULT NULL,
  `buttonTitle` varchar(255) DEFAULT NULL,
  `buttonLink` varchar(255) DEFAULT NULL,
  `isActive` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`blue_blocksID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blue_blocks`
--

LOCK TABLES `blue_blocks` WRITE;
/*!40000 ALTER TABLE `blue_blocks` DISABLE KEYS */;
INSERT INTO `blue_blocks` VALUES (1,'30','<p>Заболевания зубов &mdash; самая частая причина обращений к стоматологам. Если вас беспокоят боли или вы сами обнаружили у себя кариес, не медлите с походом к врачу.                     <br />\r\nДля точного диагноза обращайтесь к специалистам!</p>\r\n',102,'Болит зуб?',NULL,NULL,103,'Записаться','#Записаться',1),(3,'43','<p>Заболевания зубов &mdash; самая частая причина обращений к стоматологам. Если вас беспокоят боли или вы сами обнаружили у себя кариес, не медлите с походом к врачу. Для точного диагноза обращайтесь к специалистам!</p>',106,'Болит зуб?',NULL,NULL,107,'Записаться','Записаться - 1',1),(4,'43','<p>Программа &laquo;Антистресс&raquo; &mdash; это стоматологическое лечение во сне,  включающее применение внутривенной седации и местного обезболивания,  которая подбирается индивидуально в зависимости от необходимого объема,  вида лечения, и индивидуальных особенностей &ndash; страха.</p>',108,'Боитесь? Лечите во сне',NULL,NULL,109,'Подробнее','#Подробнее',1),(6,'26','<p>Лучшая команда стоматологов города Воронежа всегда к вашим услугам!                     <br />\r\n<br />\r\nМы стремимся оказывать качественные услуги и  предоставлять максимально комфортные условия лечения. Мы создаём для  своих пациентов доброжелательную и расслабляющую атмосферу. Лучшая  благодарность для нас &mdash; видеть здоровые и счастливые улыбки на ваших  лицах.</p>',112,'О команде',8,NULL,NULL,NULL,NULL,1),(8,'23',NULL,NULL,'Задайте<br> вопрос врачу',8,NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `blue_blocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certificate`
--

DROP TABLE IF EXISTS `certificate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `certificate` (
  `certificateID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `summary` text NOT NULL,
  `imageID` int(11) DEFAULT NULL,
  `isApproved` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`certificateID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certificate`
--

LOCK TABLES `certificate` WRITE;
/*!40000 ALTER TABLE `certificate` DISABLE KEYS */;
INSERT INTO `certificate` VALUES (5,'Сертификат 1','Особенности постэндодонтической  реставрации 1',24,1),(6,'Сертификат 2','Особенности постэндодонтической  реставрации 2',25,1),(7,'Сертификат 3','Особенности постэндодонтической  реставрации 3',26,1),(8,'Сертификат 4','Особенности постэндодонтической  реставрации 4',27,1),(9,'Сертификат 5','Особенности постэндодонтической  реставрации 5',28,1);
/*!40000 ALTER TABLE `certificate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contact_files`
--

DROP TABLE IF EXISTS `contact_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contact_files` (
  `contact_filesID` int(11) NOT NULL AUTO_INCREMENT,
  `contactsID` int(11) NOT NULL,
  `fileID` smallint(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contact_filesID`),
  KEY `FK_contact_files_contacts` (`contactsID`),
  CONSTRAINT `FK_contact_files_contacts` FOREIGN KEY (`contactsID`) REFERENCES `contacts` (`contactsID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contact_files`
--

LOCK TABLES `contact_files` WRITE;
/*!40000 ALTER TABLE `contact_files` DISABLE KEYS */;
INSERT INTO `contact_files` VALUES (1,1,2,'Структура компании НОВАЯ !'),(2,1,3,'О компании !'),(3,1,4,'Документы компании за 2019 !'),(4,1,5,'Новая документация 2019 !');
/*!40000 ALTER TABLE `contact_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacts`
--

DROP TABLE IF EXISTS `contacts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contacts` (
  `contactsID` int(11) NOT NULL AUTO_INCREMENT,
  `imageTop` smallint(4) NOT NULL DEFAULT '0',
  `titleTop` varchar(255) DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `text` text,
  `fileName` varchar(255) DEFAULT NULL,
  `fileID` smallint(4) DEFAULT NULL,
  `imageBottom` smallint(4) DEFAULT NULL,
  `titleBottom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`contactsID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacts`
--

LOCK TABLES `contacts` WRITE;
/*!40000 ALTER TABLE `contacts` DISABLE KEYS */;
INSERT INTO `contacts` VALUES (1,116,'<div class=\"consult__text1\">Как нас найти</div>\r\n<div class=\"consult__text2\">Контакты</div>','Ждем вас в гости ','<p>Каждый пациент Центра стоматологии &laquo;Mr.Dent&raquo; &mdash; ценный гость, который  получает самое качественное лечение и лучший сервис, которого достоин.  Обращайтесь, рады вам помочь</p>','Реквизиты ООО “Mr.Dent”',1,119,NULL);
/*!40000 ALTER TABLE `contacts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor`
--

DROP TABLE IF EXISTS `doctor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctor` (
  `doctorID` int(11) NOT NULL AUTO_INCREMENT,
  `titleTop` varchar(255) DEFAULT '0',
  `imageTop` smallint(4) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `titlTwo` varchar(255) DEFAULT NULL,
  `textTwo` text,
  `titleBottom` varchar(255) DEFAULT NULL,
  `imageBottom` smallint(4) DEFAULT NULL,
  `isActive` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`doctorID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor`
--

LOCK TABLES `doctor` WRITE;
/*!40000 ALTER TABLE `doctor` DISABLE KEYS */;
INSERT INTO `doctor` VALUES (3,'<div class=\"up__text1\">Наша лучшая</div>\r\n<p>&nbsp;</p>\r\n<div class=\"up__text2\">Команда <br />\r\nспециалистов</div>',121,'Мы гордимся своими врачами ','<p>В стоматологической клинике &ldquo;Mr.Dent&rdquo; в Воронеже мы собрали лучшую  команду стоматологов. Мы проводим тщательный отбор врачей на должность.  Проверяем квалификацию, опыт работы и репутацию.                     <br />\r\n<br />\r\nЧтобы оказывать пациентам услуги нового поколения,  наши врачи постоянно повышают свою квалификацию и приобретают новые  знания, посещая лекции и мастер-классы известных стоматологов.                     <br />\r\n<br />\r\nБлагодаря опыту и квалификации специалистов клиники в  Воронеже стали доступны высококачественные услуги экстра-класса. </p>','В нашей команде только лучшие','<p>Кариес относят к полиэтиологическим заболеваниям, поэтому спровоцировать его развитие могут следующие факторы:</p>',NULL,66,1);
/*!40000 ALTER TABLE `doctor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doctor_images`
--

DROP TABLE IF EXISTS `doctor_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doctor_images` (
  `doctor_imagesID` int(11) NOT NULL AUTO_INCREMENT,
  `doctorID` int(11) NOT NULL,
  `imageID` int(11) NOT NULL,
  `priority` int(11) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`doctor_imagesID`),
  KEY `FK_doctor_images_doctor` (`doctorID`),
  CONSTRAINT `FK_doctor_images_doctor` FOREIGN KEY (`doctorID`) REFERENCES `doctor` (`doctorID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doctor_images`
--

LOCK TABLES `doctor_images` WRITE;
/*!40000 ALTER TABLE `doctor_images` DISABLE KEYS */;
INSERT INTO `doctor_images` VALUES (1,3,53,1,NULL,NULL),(2,3,54,2,NULL,NULL),(3,3,55,3,NULL,NULL),(4,3,71,4,NULL,NULL);
/*!40000 ALTER TABLE `doctor_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_fb_messages`
--

DROP TABLE IF EXISTS `greeny_fb_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_fb_messages` (
  `messageID` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) NOT NULL DEFAULT 'none',
  `doctor` varchar(255) NOT NULL DEFAULT 'none',
  `title` varchar(255) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `userName` varchar(255) NOT NULL DEFAULT 'none',
  `userEmail` varchar(255) NOT NULL DEFAULT 'email@email',
  `userPhone` varchar(30) NOT NULL DEFAULT 'none',
  `sendDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `isModeration` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`messageID`)
) ENGINE=MyISAM AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_fb_messages`
--

LOCK TABLES `greeny_fb_messages` WRITE;
/*!40000 ALTER TABLE `greeny_fb_messages` DISABLE KEYS */;
INSERT INTO `greeny_fb_messages` VALUES (1,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 11:49:32',0),(2,'appointment','none','Сообщение с формы - Записаться на прием','Записаться\r\nЗаписаться\r\nЗаписаться','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 11:50:59',0),(3,'question','Иванов Иван Иванович','Сообщение с формы - Задайте вопрос врачу','вопрос\r\nвопрос\r\nвопрос','Попков Сергей Николаевич','email@email','+89545401561','2019-10-15 11:52:26',0),(4,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:32:31',0),(5,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:32:33',0),(6,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:32:35',0),(7,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:32:36',0),(8,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:32:38',0),(9,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:32:39',0),(10,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:32:40',0),(11,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:32:59',0),(12,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:33:01',0),(13,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:33:02',0),(14,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:33:05',0),(15,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:33:06',0),(16,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:33:08',0),(17,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:33:10',0),(18,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:33:12',0),(19,'appointment','none','Сообщение с формы - Записаться на прием','Сообщение\r\nСообщение\r\nСообщение','Попков Сергей Николаевич','popckovM5@yandex.ru','+89545401561','2019-10-15 12:33:14',0),(20,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:33:53',0),(21,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:33:56',0),(22,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:33:57',0),(23,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:33:59',0),(24,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:34:01',0),(25,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:34:04',0),(26,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:34:06',0),(27,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:34:14',0),(28,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:34:16',0),(29,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:34:18',0),(30,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:34:20',0),(31,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:34:22',0),(32,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:34:24',0),(33,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:34:26',0),(34,'consultation','none','Сообщение с формы - Консультация','консультация\r\nконсультация\r\nконсультация','Енухин Остапенко ','wwerf@yandex.ru','+845645645','2019-10-15 12:34:28',0),(35,'question','Иванов Иван Иванович','Сообщение с формы - Задайте вопрос врачу','вопрос\r\nвопрос\r\nвопрос','Попков Сергей Николаевич','email@email','+89545401561','2019-10-15 12:35:01',0),(36,'appointment','none','Сообщение с формы - Записаться на прием','1231231212 12e23 ','Енухин 111','popckovM5@yandex.ru','+89545401561','2019-10-15 12:47:41',0),(37,'consultation','none','Сообщение с формы - Консультация','drgdghrghegergergerg','Попков Сергей Николаевич','wwerf@yandex.ru','12','2019-10-16 13:15:48',0),(38,'appointment','none','Сообщение с формы - Записаться на прием','йц','йцв','popckovM5@yandex.ru','12','2019-10-18 15:10:33',0);
/*!40000 ALTER TABLE `greeny_fb_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_fieldsRegistry`
--

DROP TABLE IF EXISTS `greeny_fieldsRegistry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_fieldsRegistry` (
  `entryID` int(11) NOT NULL AUTO_INCREMENT,
  `tableName` varchar(128) NOT NULL,
  `fieldName` varchar(128) NOT NULL,
  `canChange` tinyint(1) NOT NULL DEFAULT '1',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `controlType` varchar(63) NOT NULL DEFAULT 'LineInput',
  `description` varchar(255) DEFAULT NULL,
  `isListingKey` tinyint(1) NOT NULL DEFAULT '0',
  `isMultiLanguage` tinyint(1) NOT NULL DEFAULT '0',
  `tip` varchar(255) DEFAULT NULL,
  `controlSettings` text,
  PRIMARY KEY (`entryID`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_fieldsRegistry`
--

LOCK TABLES `greeny_fieldsRegistry` WRITE;
/*!40000 ALTER TABLE `greeny_fieldsRegistry` DISABLE KEYS */;
INSERT INTO `greeny_fieldsRegistry` VALUES (1,'Users','userID',0,0,'HiddenInput','',0,0,'',NULL),(2,'Users','login',1,1,'LineInput','Логин',1,0,'Введите логин','{ \"blOrder\" : 0, \"blClass\" : \" \", \"blName\" : \"Основые данные пользователя\", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),(3,'Users','FIO',1,1,'LineInput','ФИО',0,0,'','{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),(4,'Users','email',1,1,'LineInput','E-mail',0,0,NULL,'{\r\n\"blOrder\" : 0,\r\n\"blName\" : \"\",\r\n\"elemOrder\" : 15,\r\n\"size\" : 60\r\n}'),(5,'Users','role',1,1,'SingleSelect','Тип пользователя',0,0,'','{ \"enum\" : false, \"referentTable\" : \"Roles\", \"listingKey\":\"name\", \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"emptyValue\" : 0, \"emptyText\" : \" - \" }'),(6,'Users','isActive',1,1,'CheckBox','Активность',0,0,'','{\"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"isCheckedByDef\": 0} '),(7,'Users','registrationDate',1,1,'DateMaskedInput','Дата регистрации',0,0,'','{ \"blOrder\" : 0, \"blClass\" : \"\", \"elemOrder\" : 50, \"size\" : 20, \"insertCurrentDateIfEmpty\" : true, \"ignoreUserInput\" : false, \"mask\" : \"99.99.9999 99:99:99\", \"dateFormat\" : \"d.m.Y H:i:s\" }'),(8,'UsersInfo','birthdate',1,1,'DateMaskedInput','Дата рождения',0,0,'','{\r\n	\"blOrder\" : 0, \r\n	\"blClass\" : \"\", \r\n	\"elemOrder\" : 25,\r\n        \"size\" : 15, \r\n	\"ifNull\" : \"null\", \r\n	\"ifNotNull\" : \"null\",\r\n	\"mask\" : \"99.99.9999\",\r\n	\"dateFormat\" : \"d.m.Y\",\r\n	\"minDate\" : \"01.01.1900\",\r\n	\"maxDate\" : \"01.01.2009\"\r\n}'),(9,'UsersInfo','userID',0,0,'HiddenInput','',0,0,'',NULL),(10,'UsersInfo','FIO',1,1,'LineInput','ФИО',1,0,NULL,'{ \"blOrder\" : 0, \"blName\" : \"Дополнительные данные пользователя\", \"blClass\" : \" \", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),(11,'UsersInfo','gender',1,1,'SingleSelect','Пол',0,0,NULL,'{ \"enum\" : \"true\", \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 27 }'),(12,'Roles','role',0,0,'HiddenInput','',0,0,'',NULL),(13,'Roles','name',1,1,'LineInput','Тип пользователя',1,0,'','{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),(14,'Roles','level',1,1,'LineInput','Уровень доступа',0,0,'','{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),(15,'simplePages','pageContent',1,1,'RichText','Содержание',0,0,'Содержание страницы','{\r\n  \"blOrder\" : 0,\r\n  \"blName\" : \"\",\r\n  \"elemOrder\" : 10\r\n}'),(16,'simplePages','simplePageID',0,0,'HiddenInput',NULL,0,0,NULL,NULL),(17,'greeny_settings','groupID',1,1,'SingleSelect','Группа настроек',0,0,'','{ \"referentTable\" : \"greeny_settingsGroups\", \"listingKey\" : \"desc\", \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10 }'),(18,'greeny_settings','name',1,1,'LineInput','Название',0,0,'','{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),(19,'greeny_settings','desc',1,1,'LineInput','Описание',1,0,'','{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),(20,'greeny_settings','value',1,1,'LineInput','Значение',0,0,'','{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 40, \"size\" : 60, \"class\" : \" \" }'),(21,'greeny_settingsGroups','groupID',0,0,'HiddenInput','',0,0,'',NULL),(22,'greeny_settingsGroups','parentID',1,1,'SingleSelect','Родительская группа',0,0,'','{ \"referentTable\" : \"greeny_settingsGroups\", \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10 }'),(23,'greeny_settingsGroups','name',1,1,'LineInput','Назване группы',0,0,'','{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),(24,'greeny_settingsGroups','desc',1,1,'LineInput','Описание группы',1,0,'','{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 30, \"size\" : 60, \"class\" : \" \" }'),(25,'greeny_settings','settingID',0,0,'HiddenInput','',0,0,'',NULL),(26,'Menu','itemID',0,0,'HiddenInput','',0,0,'',NULL),(27,'Menu','parentID',1,1,'SingleSelectCustom','Родительский пункт',0,0,'','{ \"referentTable\" : \"Menu\" , \"listingKey\" : \"name\", \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10, \"emptyText\" : \" - \" , \"parentKey\":\"parentID\" }'),(28,'Menu','name',1,1,'LineInput','Название',0,0,'','{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 10, \"size\" : 60, \"class\" : \" \" }'),(29,'Menu','alias',1,1,'LineInput','Алиас',1,0,'','{ \"blOrder\" : 0, \"blClass\" : \" \", \"elemOrder\" : 20, \"size\" : 60, \"class\" : \" \" }'),(30,'Menu','priority',1,1,'LineInput','Порядок ',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" :30, \"size\" : 60, \"className\" : \" \" }'),(31,'Menu','isActive',1,1,'CheckBox','Активность',0,0,'','{ \"checkedDefault\" : true, \"blOrder\" : 0, \"elemOrder\" :40, \"className\" : \" \" }'),(32,'Menu','menuListID',0,0,'HiddenInput','',0,0,'',NULL),(33,'Menu','isButton',1,1,'CheckBox','Является кнопкой в верхнем меню',0,0,'','{ \"checkedDefault\" : false, \"blOrder\" : 0, \"elemOrder\" :40, \"className\" : \" \" }'),(34,'MenuList','menuListID',1,1,'HiddenInput','',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(35,'MenuList','menuListName',1,1,'LineInput','Название',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \" }'),(36,'MenuList','menuListAlias',1,1,'LineInput','Алиас',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \" }'),(37,'MenuList','menuListComment',1,1,'TextArea','Комментарий',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"cols\" : 60, \"rows\" : 5, \"className\" : \" \" }'),(38,'reviews','reviewID',0,0,'HiddenInput','',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(39,'reviews','name',1,1,'LineInput','Имя',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 10, \"size\" : 60, \"className\" : \" \" }'),(40,'reviews','email',1,1,'LineInput','E-mail',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 20, \"size\" : 60, \"className\" : \" \" }'),(41,'reviews','review',1,1,'TextArea','Текст отзыва',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 30, \"cols\" : 60, \"rows\" : 5, \"className\" : \" \" }'),(42,'reviews','dateAdd',1,1,'DateInput','Дата добавления',0,0,'','{ \"dateFormat\" : \"d.m.Y\", \"yearRange\" : \"-10:+5\", \"insertCurrentDateIfEmpty\" : true, \"ignoreUserInput\" : false, \"blOrder\" : 10, \"blName\" : \" \", \"elemOrder\" : 60 }'),(43,'reviews','isApproved',1,1,'CheckBox','Утвержден',0,0,'','{ \"checkedDefault\" : false, \"blOrder\" : 0, \"elemOrder\" : 60, \"className\" : \" \" }'),(44,'reviews','imageID',1,1,'DamnUpload','Фотография',0,0,'','{ \"blOrder\" : 1, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1200, \"height\" : 1200 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : true, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/reviews/\", \"uploadScript\" : \"/admin/upload/\" }'),(45,'slider','sliderID',1,0,'HiddenInput','id',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(46,'slider','titleOne',1,1,'LineInput','Заголовок первый',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(47,'slider','titleTwo',1,1,'LineInput','Заголовок второй',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(48,'slider','buttonLink',1,1,'LineInput','Ссылка кнопки',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(49,'slider','buttonTitle',1,1,'LineInput','Название кнопки',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(50,'slider','isActive',1,1,'LineInput','Активность',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(51,'slider','photos',1,1,'DamnUpload','Фотографии',0,0,'','{ \"blOrder\" : 0, \r\n		    \"elemOrder\" : 50,\r\n\r\n			\"image\" : { \r\n				\"resize\" : { \r\n					\"strong\" : false,\r\n					\"width\" : 1000,\r\n					\"height\" : 1000 \r\n				}, \r\n\r\n				\"thumb\" : {\r\n					\"resize\" : { \r\n						\"strong\" :  true, \r\n						\"width\" : 208, \r\n						\"height\" : 112 }, \r\n					\"postfix\" : \"_small\" \r\n				} \r\n			}, \r\n\r\n            \"includeThumbCropDialog\" : true,\r\n\r\n			\"maxFileSize\" : 5048576,\r\n\r\n			\"filesType\" : \"image\",\r\n \r\n			\"maxFilesNumber\" : null,\r\n\r\n			\"linkingTable\" : \"sliderImages\",\r\n\r\n			\"allowedExtensions\" : \"*.jpg; *.gif; *.png\", \r\n\r\n			\"filesTable\" : \"greeny_images\",\r\n \r\n			\"filesTableKey\" : \"imageID\",\r\n \r\n			\"destinationDirectory\" : \"uploaded/sliders/[ID]\",\r\n \r\n			\"usePriority\"	:	true,\r\n\r\n			\"priorityField\"	:	\"priority\",	\r\n\r\n			\"uploadScript\" : \"/admin/upload/\" ,\r\n\r\n			\"textFields\" : { 		\r\n                \"title\" : { \r\n                    \"label\" : \"Подпись\", \r\n                    \"showLabel\" : true, \r\n                    \"containerType\": \"line\" \r\n                },\r\n                \"link\" : {\r\n                    \"label\" : \"Ссылка\", \r\n                    \"showLabel\" : true, \r\n                    \"containerType\": \"line\" \r\n                }\r\n            }\r\n     }'),(71,'greeny_nl_news','smallImageID',1,1,'DamnUpload','Фотография',0,0,NULL,'{ \"blOrder\" : 0, \r\n\"elemOrder\" : 25,\r\n \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 980, \"height\" : 400 }, \r\n\"thumb\" : { \"resize\" : { \"strong\" : true, \"width\" : 220, \"height\" : 165 },\r\n \"postfix\" : \"_small\" }}, \r\n\"includeThumbCropDialog\" : true, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1,\r\n \"linkingTable\" : \"\", \r\n\"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \r\n\"filesTable\" : \"greeny_images\", \r\n\"filesTableKey\" : \"imageID\", \r\n\"destinationDirectory\" : \"uploaded/news/[ID]/\", \r\n\"uploadScript\" : \"/admin/upload/\" }'),(73,'greeny_nl_news','publicationDate',1,1,'DateInput','Дата публикации',0,0,NULL,'{ \"dateFormat\" : \"d.m.Y\", \"yearRange\" : \"-10:+5\", \"insertCurrentDateIfEmpty\" : true, \"ignoreUserInput\" : false, \"blOrder\" : 1, \"blName\" : \"Настройки публикации\", \"elemOrder\" : 110 }'),(74,'greeny_nl_news','modificationDate',0,1,'DateMaskedInput','Дата последнего редактирования',0,0,NULL,'{ \"dateFormat\" : \"d.m.Y H:i\", \"mask\" : \"99.99.9999 99:99\", \"insertCurrentDateIfEmpty\" : true, \"ignoreUserInput\" : true, \"blOrder\" : 1, \"elemOrder\" : 120 }'),(75,'greeny_nl_news','isPublished',1,1,'CheckBox','Публиковать',0,0,NULL,'{ \"checkedDefault\" : true, \"blOrder\" : 100, \"elemOrder\" : 130 }'),(84,'advantage','advantageID',1,0,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(85,'advantage','title',1,1,'LineInput','Заголовок',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(86,'advantage','text',1,1,'RichText','Описание наших преимуществ',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 40, \"width\" : \"100%\", \"height\" : 200 }'),(87,'advantage','imageID',1,1,'DamnUpload','Загружить изображение',0,0,'','{ \"blOrder\" : 1, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1200, \"height\" : 1200 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : true, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/advantage/\", \"uploadScript\" : \"/admin/upload/\" }'),(88,'advantage','isApproved',1,1,'CheckBox','Активность',0,0,'','{ \"checkedDefault\" : false, \"blOrder\" : 0, \"elemOrder\" : 0, \"className\" : \" \" }'),(89,'certificate','certificateID',1,0,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(90,'certificate','name',1,1,'LineInput','Название',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(91,'certificate','summary',1,1,'TextArea','Описание',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"cols\" : 60, \"rows\" : 5, \"className\" : \" \" }'),(92,'certificate','imageID',1,1,'DamnUpload','Загружить изображение',0,0,'','{ \"blOrder\" : 1, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1200, \"height\" : 1200 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : true, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/certificate/\", \"uploadScript\" : \"/admin/upload/\" } '),(93,'certificate','isApproved',1,1,'CheckBox','Активность',0,0,'','{ \"checkedDefault\" : false, \"blOrder\" : 0, \"elemOrder\" : 0, \"className\" : \" \" }'),(94,'prices','pricesID',1,0,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(95,'prices','title',1,1,'LineInput','Название',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(96,'prices','description',1,1,'TextArea','Описание услуги',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"cols\" : 60, \"rows\" : 5, \"className\" : \" \" }'),(97,'prices','cost',1,1,'LineInput','Цена в Рублях',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(98,'prices','isApproved',1,1,'CheckBox','Активность',0,0,'','{ \"checkedDefault\" : false, \"blOrder\" : 0, \"elemOrder\" : 0, \"className\" : \" \" }'),(99,'slider_about','slider_aboutID',1,0,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(100,'slider_about','title',1,1,'LineInput','Заголовок',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(101,'slider_about','imageID',1,1,'DamnUpload','Изображение',0,0,'','{ \"blOrder\" : 1, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1200, \"height\" : 1200 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : true, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/sliderAbout/\", \"uploadScript\" : \"/admin/upload/\" }'),(102,'slider_about','isActive',1,1,'CheckBox','Активность',0,0,'','{ \"checkedDefault\" : false, \"blOrder\" : 0, \"elemOrder\" : 0, \"className\" : \" \" }'),(103,'lechenie_cariesa','lechenie_cariesaID',1,0,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(104,'lechenie_cariesa','title',1,1,'LineInput','Заголовок',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(105,'lechenie_cariesa','description',1,1,'TextArea','Описание',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"cols\" : 60, \"rows\" : 5, \"className\" : \" \" }'),(106,'lechenie_cariesa','isActive',1,1,'CheckBox','Активность',0,0,'','{ \"checkedDefault\" : false, \"blOrder\" : 0, \"elemOrder\" : 0, \"className\" : \" \" }'),(107,'reason_cariesa','reason_cariesaID',1,0,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(108,'reason_cariesa','description',1,1,'LineInput','Описание',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(109,'reason_cariesa','isActive',1,1,'CheckBox','Активность',0,0,'','{ \"checkedDefault\" : false, \"blOrder\" : 0, \"elemOrder\" : 0, \"className\" : \" \" }'),(110,'personal','personalID',1,0,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(111,'personal','name',1,1,'LineInput','ФИО Сотрудника',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(112,'personal','profession',1,1,'LineInput','Профессия',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(113,'personal','experience',1,1,'LineInput','Опыт (лет)',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(114,'personal','certificate',1,1,'LineInput','Количество сертификатов',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(115,'personal','reviews',1,1,'LineInput','Количество отзывов',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(116,'personal','position',1,1,'LineInput','Должность',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(117,'personal','category',1,1,'LineInput','Категория',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(118,'personal','description',1,1,'RichText','Описание',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"cols\" : 60, \"rows\" : 5, \"className\" : \" \" }'),(119,'personal','imageAva',1,1,'DamnUpload','Ava ',0,0,'',' { \"blOrder\" : 1, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1200, \"height\" : 1200 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : true, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/personal/\", \"uploadScript\" : \"/admin/upload/\" } '),(120,'personal','imageFull',1,1,'DamnUpload','Полное фото',0,0,'',' { \"blOrder\" : 1, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1200, \"height\" : 1200 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : true, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/personal/\", \"uploadScript\" : \"/admin/upload/\" } '),(121,'personal','isActive',1,1,'CheckBox','Активность',0,0,'','{ \"checkedDefault\" : false, \"blOrder\" : 0, \"elemOrder\" : 0, \"className\" : \" \" }'),(128,'contacts','contactsID',1,0,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(129,'contacts','title',1,1,'LineInput','Заголовок',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 30, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(130,'contacts','text',1,1,'RichText','Текст страницы',0,0,'','{\r\n\"blOrder\"	:	0,\r\n\"elemOrder\"	:	0,\r\n\"width\"	:	\"100%\",\r\n\"height\"	:	400,\r\n\"toolbarSet\"	:	\"\"\r\n}'),(131,'contacts','fileID',1,1,'DamnUpload','Реквизиты организации',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 50, \"maxFileSize\" : 5048576, \"filesType\" : \"file\",\r\n\"maxFilesNumber\" : 1,\r\n\"allowedExtensions\" : \"*.pdf; *.doc; *.docx; *.xls; *.xlsx\",\r\n\"filesTable\" : \"greeny_files\",\r\n\"filesTableKey\" : \"fileID\",\r\n\"destinationDirectory\" : \"uploaded/docs/\",\r\n\"uploadScript\" : \"/admin/upload/\"\r\n}\r\n\r\n'),(136,'main','mainID',1,1,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(137,'main','titleAboutClinic',1,1,'LineInput','Заголовок раздела \"О клинике\"',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(138,'main','textAboutClinic',1,1,'RichText','Текст \"О клинике\"',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),(139,'main','linkVideo',1,1,'LineInput','Ссылка на видео',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(140,'main','promotionTitle',1,1,'LineInput','Заголовок для продвижения',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(141,'main','promotionText',1,1,'RichText','Текст для продвижения',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),(142,'main','imageBottom',1,1,'DamnUpload','Картинка для формы отправки \"Записаться на прием\"',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 50, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/main/[ID]\", \"uploadScript\" : \"/admin/upload/\"}'),(143,'price','priceID',1,0,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(144,'price','titleTop',1,1,'RichText','Текст с верху для формы отправки \"Бесплатная консультация\"',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),(145,'price','imageTop',1,1,'DamnUpload','Изображение для формы с верху',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1086, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/price/[ID]\", \"uploadScript\" : \"/admin/upload/\" }'),(146,'price','titleBottom',1,1,'RichText','Текст с низу для формы отправки \"Записаться на прием\"',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),(147,'price','imageBottom',1,1,'DamnUpload','Изображение для формы с низу',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/price/[ID]\", \"uploadScript\" : \"/admin/upload/\" }'),(148,'services','servicesID',1,0,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(149,'services','titleTop',1,1,'RichText','Текст с верху для формы отправки \"Бесплатная консультация\"',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),(150,'services','imageTop',1,1,'DamnUpload','Изображение для формы с верху',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/services\", \"uploadScript\" : \"/admin/upload/\" }'),(151,'services','servicesTitle',1,1,'LineInput','Заголовок для \"Услуги клиники\"',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(152,'services','servicesText',1,1,'RichText','Текст с описанием услуг клиники',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),(153,'services','promotionTitle',1,1,'LineInput','Заголовок для продвижения',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(154,'services','promotionText',1,1,'RichText','Текст для продвижения',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),(155,'services','titleBottom',1,1,'RichText','Текст с низу для формы отправки \"Записаться на прием',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),(156,'services','imageBottom',1,1,'DamnUpload','Картинка для формы отправки \"Записаться на прием\"',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/services\", \"uploadScript\" : \"/admin/upload/\" }'),(157,'doctor','doctorID',1,0,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(158,'doctor','titleTop',1,1,'RichText','Текст с верху на фоне изображения',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 10, \"width\" : \"100%\", \"height\" : 200, \"toolbarSet\" : \"\" }'),(159,'doctor','imageTop',1,1,'DamnUpload','Изображение с верху',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 20, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1217, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/doctors\", \"uploadScript\" : \"/admin/upload/\"}'),(160,'doctor','title',1,1,'LineInput','Заголовок о нашем персонале',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 30, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(161,'doctor','text',1,1,'RichText','Описание нашей клиники',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 40, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),(162,'doctor','titlTwo',1,1,'LineInput','Загловок о нашей команде',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 60, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(163,'doctor','textTwo',1,1,'RichText','Описание нашей команды',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 70, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),(164,'doctor','titleBottom',1,1,'RichText','Текст с низу на фоне изображения',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 80, \"width\" : \"100%\", \"height\" : 200, \"toolbarSet\" : \"\" }'),(165,'doctor','imageBottom',1,1,'DamnUpload','Картинка для формы отправки \"Записаться на прием\"',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 90, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/doctors\", \"uploadScript\" : \"/admin/upload/\"}'),(166,'about_company','about_companyID',1,1,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 10 }'),(167,'about_company','titleTop',1,1,'RichText','Текст с верху на фоне изображения',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 20, \"width\" : \"100%\", \"height\" : 200, \"toolbarSet\" : \"\" }'),(168,'about_company','imageTop',1,1,'DamnUpload','Изображение с верху',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 30, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/about\", \"uploadScript\" : \"/admin/upload/\" }'),(169,'about_company','imageTeam',1,1,'DamnUpload','Фото команды',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 40, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/about\", \"uploadScript\" : \"/admin/upload/\" }'),(170,'about_company','titleTeam',1,1,'LineInput','Заголовок для фото команды',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 45, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(171,'about_company','textTeam',1,1,'RichText','Текст о команде',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 50, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),(172,'about_company','title',1,1,'LineInput','Заголовок для слайдера',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 55, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(173,'about_company','text',1,1,'RichText','Текст для слайдера',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 60, \"width\" : \"100%\", \"height\" : 200, \"toolbarSet\" : \"\" }'),(174,'about_company','certificateText',1,1,'RichText','Описание сертификатов',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 70, \"width\" : \"100%\", \"height\" : 200, \"toolbarSet\" : \"\" }'),(175,'about_company','titleBottom',1,1,'RichText','Текст с низу для формы отправки \"Записаться на прием\"',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 80, \"width\" : \"100%\", \"height\" : 200, \"toolbarSet\" : \"\" }'),(176,'about_company','imageBottom',1,1,'DamnUpload','Картинка для формы отправки \"Записаться на прием',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 90, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/about\", \"uploadScript\" : \"/admin/upload/\" }'),(177,'doctor','photos',1,1,'DamnUpload','Фотографии для Слайдера',0,0,'','{ \"blOrder\" : 0, \r\n		    \"elemOrder\" : 50,\r\n\r\n			\"image\" : { \r\n				\"resize\" : { \r\n					\"strong\" : false,\r\n					\"width\" : 1000,\r\n					\"height\" : 1000 \r\n				}, \r\n\r\n				\"thumb\" : {\r\n					\"resize\" : { \r\n						\"strong\" :  true, \r\n						\"width\" : 208, \r\n						\"height\" : 112 }, \r\n					\"postfix\" : \"_small\" \r\n				} \r\n			}, \r\n\r\n            \"includeThumbCropDialog\" : true,\r\n\r\n			\"maxFileSize\" : 5048576,\r\n\r\n			\"filesType\" : \"image\",\r\n \r\n			\"maxFilesNumber\" : null,\r\n\r\n			\"linkingTable\" : \"doctor_images\",\r\n\r\n			\"allowedExtensions\" : \"*.jpg; *.gif; *.png\", \r\n\r\n			\"filesTable\" : \"greeny_images\",\r\n \r\n			\"filesTableKey\" : \"imageID\",\r\n \r\n			\"destinationDirectory\" : \"uploaded/doctor/[ID]\",\r\n \r\n			\"usePriority\"	:	true,\r\n\r\n			\"priorityField\"	:	\"priority\",	\r\n\r\n			\"uploadScript\" : \"/admin/upload/\" ,\r\n\r\n			\"textFields\" : { 		\r\n		\r\n	}\r\n\r\n        }'),(178,'statistic','statisticID',1,1,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(179,'statistic','statisticNumber',1,1,'LineInput','Число',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(180,'statistic','statisticTitle',1,1,'RichText','Название',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 300, \"toolbarSet\" : \"\" }'),(181,'statistic','imageID',1,1,'DamnUpload','Изображение',0,0,'',' { \"blOrder\" : 0, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/statistic/[ID]\", \"uploadScript\" : \"/admin/upload/\" }'),(183,'about_company','photos',1,1,'DamnUpload','Фотографии для Слайдера',0,0,'','{ \"blOrder\" : 0, \r\n		    \"elemOrder\" : 65,\r\n\r\n			\"image\" : { \r\n				\"resize\" : { \r\n					\"strong\" : false,\r\n					\"width\" : 1000,\r\n					\"height\" : 1000 \r\n				}, \r\n\r\n				\"thumb\" : {\r\n					\"resize\" : { \r\n						\"strong\" :  true, \r\n						\"width\" : 208, \r\n						\"height\" : 112 }, \r\n					\"postfix\" : \"_small\" \r\n				} \r\n			}, \r\n\r\n            \"includeThumbCropDialog\" : true,\r\n\r\n			\"maxFileSize\" : 5048576,\r\n\r\n			\"filesType\" : \"image\",\r\n \r\n			\"maxFilesNumber\" : null,\r\n\r\n			\"linkingTable\" : \"sliderAbout\",\r\n\r\n			\"allowedExtensions\" : \"*.jpg; *.gif; *.png\", \r\n\r\n			\"filesTable\" : \"greeny_images\",\r\n \r\n			\"filesTableKey\" : \"imageID\",\r\n \r\n			\"destinationDirectory\" : \"uploaded/sliderAbout/\",\r\n \r\n			\"usePriority\"	:	true,\r\n\r\n			\"priorityField\"	:	\"priority\",	\r\n\r\n			\"uploadScript\" : \"/admin/upload/\" ,\r\n\r\n			\"textFields\" : { 		\r\n		\r\n	}\r\n\r\n        }'),(191,'contacts','imageTop',1,1,'DamnUpload','Картинка с Верху',0,0,'',' { \"blOrder\" : 0, \"elemOrder\" : 10, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1086, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/contacts/[ID]\", \"uploadScript\" : \"/admin/upload/\" }'),(192,'contacts','titleTop',1,1,'RichText','Текст с верху на фоне изображения',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 20, \"width\" : \"100%\", \"height\" : 200, \"toolbarSet\" : \"\" }'),(193,'contacts','imageBottom',1,1,'DamnUpload','Изображение для формы с низу',0,0,'',' { \"blOrder\" : 0, \"elemOrder\" : 100, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1015, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/contacts/[ID]\", \"uploadScript\" : \"/admin/upload/\" }'),(194,'contacts','titleBottom',1,1,'RichText','Текст с низу для формы отправки \"Записаться на прием\"',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 90, \"width\" : \"100%\", \"height\" : 200, \"toolbarSet\" : \"\" }'),(195,'contacts','files',1,1,'DamnUpload','Список документов',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 80, \"maxFileSize\" : 5048576, \"filesType\" : \"file\", \r\n\"maxFilesNumber\" : 100, \r\n\"linkingTable\" : \"contact_files\",\r\n\"allowedExtensions\" : \"*.pdf; *.doc; *.docx; *.xls; *.xlsx\",\r\n\"filesTable\" : \"greeny_files\", \r\n\"filesTableKey\" : \"fileID\", \r\n\"destinationDirectory\" : \"uploaded/docs/[ID]\", \r\n\"uploadScript\" : \"/admin/upload/\",\r\n\"textFields\" : { \"name\" : { \"name\" : \"Описание\", \"showLabel\" : true} }\r\n}'),(196,'Menu','smallImage',1,1,'DamnUpload','Маленькое изображение',0,0,'',' { \"blOrder\" : 0, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/menu/[ID]\", \"uploadScript\" : \"/admin/upload/\" }'),(199,'greeny_nl_news','news_blocks',1,1,'DependendTable','Информационные блоки для статьи',0,0,'','{ \"linkingTable\" : \"news_blocks\", \"blOrder\" : 50, \"elemOrder\" : 0, \"className\" : \" \", \"size\" : 10, \"orderBy\" : \"\", \"textFields\" : { \"title\" : { \"label\" : \"Заголовок\", \"showLabel\" : true, \"containerType\" : \"textarea\" } ,\"text\": { \"label\" : \"Текст\", \"showLabel\" : true, \"containerType\" : \"textarea\" }, \"priority\": { \"label\" : \"Приоритет\", \"showLabel\" : true, \"containerType\" : \"line\" } } } '),(200,'blue_blocks','blue_blocksID',1,1,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(201,'blue_blocks','parentPage',1,1,'SingleSelect','Страница',0,0,'',' { \"enum\" : false, \"referentTable\" : \"greeny_pageStructure\", \"listingKey\" : \"alias\", \"blOrder\" : 0, \"elemOrder\" : 0, \"emptyValue\" : 0, \"className\" : \" \", \"emptyText\" : \" - \" } '),(202,'blue_blocks','textTop',1,1,'RichText','Главный текст блока',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 200, \"toolbarSet\" : \"\" }'),(203,'blue_blocks','imageIconID',1,1,'DamnUpload','Изображение Иконки',0,0,'','{ \"blOrder\" : 0, \r\n\"elemOrder\" : 0, \r\n\"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \r\n\"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \r\n\"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \r\n\"maxFilesNumber\" : 1, \r\n\"linkingTable\" : \"\", \r\n\"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \r\n\"filesTable\" : \"greeny_images\", \r\n\"filesTableKey\" : \"imageID\", \r\n\"destinationDirectory\" : \"uploaded/blue_blocks/[ID]\", \r\n\"uploadScript\" : \"/admin/upload/\"}'),(204,'blue_blocks','imageIconTitle',1,1,'LineInput','Текст Изображения иконки',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(205,'blue_blocks','imageAvaTitle',1,1,'LineInput','Текст для картинки Доктора',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(206,'blue_blocks','imageAvaID',1,1,'DamnUpload','Изображение Доктора',0,0,'','{ \"blOrder\" : 0, \r\n\"elemOrder\" : 0,\r\n \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 },\r\n \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \r\n\"includeThumbCropDialog\" : false, \r\n\"maxFileSize\" : 5048576, \"filesType\" : \"image\",\r\n \"maxFilesNumber\" : 1,\r\n \"linkingTable\" : \"\",\r\n \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \r\n\"filesTable\" : \"greeny_images\",\r\n \"filesTableKey\" : \"imageID\", \r\n\"destinationDirectory\" : \"uploaded/blue_blocks/[ID]\",\r\n \"uploadScript\" : \"/admin/upload/\" }'),(207,'blue_blocks','buttonTitle',1,1,'LineInput','Текст кнопки',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(208,'blue_blocks','buttonLink',1,1,'LineInput','Ссылка кнопки',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(209,'blue_blocks','isActive',1,1,'CheckBox','Активность',0,0,'','{ \"checkedDefault\" : false, \"blOrder\" : 0, \"elemOrder\" : 0, \"className\" : \" \" }'),(212,'advantage','pageID',1,1,'SingleSelect','Страница ',0,0,'',' { \"enum\" : false, \"referentTable\" : \"greeny_pageStructure\", \"listingKey\" : \"alias\", \"blOrder\" : 0, \"elemOrder\" : 0, \"emptyValue\" : 0, \"className\" : \" \", \"emptyText\" : \" - \" } '),(213,'Menu','bigImage',1,1,'DamnUpload','Большая картинка для категории',0,0,'',' { \"blOrder\" : 0, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/menu/[ID]\", \"uploadScript\" : \"/admin/upload/\" }'),(214,'main','videoMask',1,1,'DamnUpload','Картинка маска для видео',0,0,'',' { \"blOrder\" : 0, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/main/[ID]\", \"uploadScript\" : \"/admin/upload/\" }'),(215,'blue_blocks','dependendDoctor',1,1,'SingleSelect','Доктор',0,0,'','{ \"enum\" : false, \"referentTable\" : \"personal\", \"listingKey\" : \"name\", \"blOrder\" : 0, \"elemOrder\" : 0, \"emptyValue\" : 0, \"className\" : \"\", \"emptyText\" : \" - \" }'),(216,'greeny_nl_news','bigImageID',1,1,'DamnUpload','Большое изображение',0,0,'',' { \"blOrder\" : 0, \"elemOrder\" : 27, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/news/[ID]\", \"uploadScript\" : \"/admin/upload/\" } '),(217,'contacts','fileName',1,1,'LineInput','Название файла реквизитов',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(218,'main','imageID',1,1,'DamnUpload','Картинка для раздела Сервис',0,0,'',' { \"blOrder\" : 0, \"elemOrder\" : 0, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/\", \"uploadScript\" : \"/admin/upload/main/[ID]\" } '),(221,'greeny_nl_news','newsID',0,0,'HiddenInput','',0,0,NULL,''),(222,'greeny_nl_news','sectionID',1,1,'SingleSelect','Категория',0,0,NULL,'{ \"enum\" : false, \"referentTable\" : \"greeny_nl_sections\", \"listingKey\" : \"caption\", \"blOrder\" : 0, \"elemOrder\" : 10, \"emptyValue\" : null, \"className\" : \" \", \"emptyText\" : \" - \", \"orderBy\":\"sectionID\" }'),(223,'greeny_nl_news','title',1,1,'LineInput','Заголовок',0,0,NULL,'{ \"blOrder\" : 0, \"elemOrder\" : 20, \"size\" : 60 }'),(224,'greeny_nl_news','summary',1,1,'TextArea','Краткое содержание',0,0,NULL,'{ \"blOrder\" : 0, \"elemOrder\" : 30, \"cols\" : 60, \"rows\" : 5 }'),(225,'greeny_nl_news','body',1,1,'RichText','Полный текст',0,0,NULL,'{ \"blOrder\" : 0, \"elemOrder\" : 40, \"width\" : \"100%\", \"height\" : 500 }'),(226,'greeny_nl_news','imageID',1,1,'DamnUpload','Фотография',0,0,NULL,'{ \"blOrder\" : 0, \"elemOrder\" : 25, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 980, \"height\" : 400 }, \"thumb\" : { \"resize\" : { \"strong\" : true, \"width\" : 220, \"height\" : 165 }, \"postfix\" : \"_small\" }}, \"includeThumbCropDialog\" : true, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/news/[ID]/\", \"uploadScript\" : \"/admin/upload/\" }'),(227,'greeny_nl_news','gallery',1,1,'DamnUpload','Галерея',0,0,NULL,'{ \"blOrder\" : 0, \"elemOrder\" : 45, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : true, \"width\" : 220, \"height\" : 165 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : true, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 100, \"linkingTable\" : \"greeny_nl_images\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/news/[ID]/\", \"uploadScript\" : \"/admin/upload/\" }'),(228,'greeny_nl_news','publicationDate',1,1,'DateInput','Дата публикации',0,0,NULL,'{ \"dateFormat\" : \"d.m.Y\", \"yearRange\" : \"-10:+5\", \"insertCurrentDateIfEmpty\" : true, \"ignoreUserInput\" : false, \"blOrder\" : 1, \"blName\" : \"Настройки публикации\", \"elemOrder\" : 60 }'),(229,'greeny_nl_news','modificationDate',0,1,'DateMaskedInput','Дата последнего редактирования',0,0,NULL,'{ \"dateFormat\" : \"d.m.Y H:i\", \"mask\" : \"99.99.9999 99:99\", \"insertCurrentDateIfEmpty\" : true, \"ignoreUserInput\" : true, \"blOrder\" : 1, \"elemOrder\" : 70 }'),(230,'greeny_nl_news','isPublished',1,1,'CheckBox','Публиковать',0,0,NULL,'{ \"checkedDefault\" : true, \"blOrder\" : 1, \"elemOrder\" : 50 }'),(231,'lechenie_zubov','lechenie_zubovID',1,1,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 10 }'),(232,'lechenie_zubov','titleTop',1,1,'RichText','Текст с верху на фоне изображения',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 20, \"width\" : \"100%\", \"height\" : 200, \"toolbarSet\" : \"\" }'),(233,'lechenie_zubov','imageTop',1,1,'DamnUpload','Изображение с верху',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 30, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/lechenie_zubov/[ID]\", \"uploadScript\" : \"/admin/upload/\" }'),(234,'lechenie_zubov','titleSlider',1,1,'LineInput','Заголовок слайдера',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 40, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(235,'lechenie_zubov','textSlider',1,1,'RichText','Текст слайдера',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 50, \"width\" : \"100%\", \"height\" : 300, \"toolbarSet\" : \"\" }'),(236,'lechenie_zubov','titleReason',1,1,'LineInput','Заголовок причиы заболевания',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 60, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(237,'lechenie_zubov','textReason',1,1,'RichText','Текст причиы заболевания',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 70, \"width\" : \"100%\", \"height\" : 300, \"toolbarSet\" : \"\" }'),(238,'lechenie_zubov','titleStep',1,1,'LineInput','Заголовок степени заболевания',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 80, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(239,'lechenie_zubov','textStep',1,1,'RichText','Текст степени заболевания',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 90, \"width\" : \"100%\", \"height\" : 300, \"toolbarSet\" : \"\" }'),(240,'lechenie_zubov','titleBottom',1,1,'RichText','Текст с низу для формы отправки \"Записаться на прием\"',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 120, \"width\" : \"100%\", \"height\" : 200, \"toolbarSet\" : \"\" }'),(241,'lechenie_zubov','imageBottom',1,1,'DamnUpload','Картинка для формы отправки с низу',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 110, \"image\" : { \"resize\" : { \"strong\" : false, \"width\" : 1000, \"height\" : 1000 }, \"thumb\" : { \"resize\" : { \"strong\" : false, \"width\" : 150, \"height\" : 150 }, \"postfix\" : \"_small\" } }, \"includeThumbCropDialog\" : false, \"maxFileSize\" : 5048576, \"filesType\" : \"image\", \"maxFilesNumber\" : 1, \"linkingTable\" : \"\", \"allowedExtensions\" : \"*.jpg; *.jpeg; *.gif; *.png\", \"filesTable\" : \"greeny_images\", \"filesTableKey\" : \"imageID\", \"destinationDirectory\" : \"uploaded/lechenie_zubov/[ID]\", \"uploadScript\" : \"/admin/upload/\" }'),(243,'lechenie_zubov','processLechenie',1,1,'DependendTable','Блоки для лечения',0,0,'','{ \"linkingTable\" : \"lechenieBlocks\", \"blOrder\" : 0, \"elemOrder\" : 85, \"className\" : \"\", \"size\" : 10, \"orderBy\" : \"\", \"textFields\" : { \"title\" : { \"label\" : \"Заголовок\", \"showLabel\" : true, \"containerType\" : \"line\" }, \"text\" : { \"label\" : \"Текст\", \"showLabel\" : true, \"containerType\" : \"textarea\" }, \"priority\" : { \"label\" : \"Приоритет\", \"showLabel\" : true, \"containerType\" : \"line\" } } }'),(244,'lechenie_zubov','lechenieReason',1,1,'DependendTable','Блоки для описания \"Причин заболевания\"',0,0,'','{ \"linkingTable\" : \"lechenie_reason\", \r\n\"blOrder\" : 0, \r\n\"elemOrder\" : 55, \r\n\"className\" : \"\", \r\n\"size\" : 10, \r\n\"orderBy\" : \"\", \r\n\"textFields\" : \r\n{ \r\n\"description\" : { \r\n\"label\" : \"Причины заболевания\", \r\n                    \"showLabel\" : true, \r\n                    \"containerType\": \"line\" \r\n }, \r\n\"priority\" : { \"label\" : \"Приоритет\", \"showLabel\" : true, \"containerType\" : \"line\" } } }\r\n'),(245,'lechenie_zubov','slider',1,1,'DamnUpload','Фотографии',0,0,'','{ \"blOrder\" : 0, \r\n		    \"elemOrder\" : 45,\r\n\r\n			\"image\" : { \r\n				\"resize\" : { \r\n					\"strong\" : false,\r\n					\"width\" : 1000,\r\n					\"height\" : 1000 \r\n				}, \r\n\r\n				\"thumb\" : {\r\n					\"resize\" : { \r\n						\"strong\" :  true, \r\n						\"width\" : 208, \r\n						\"height\" : 112 }, \r\n					\"postfix\" : \"_small\" \r\n				} \r\n			}, \r\n\r\n            \"includeThumbCropDialog\" : true,\r\n\r\n			\"maxFileSize\" : 5048576,\r\n\r\n			\"filesType\" : \"image\",\r\n \r\n			\"maxFilesNumber\" : null,\r\n\r\n			\"linkingTable\" : \"lechenie_slider\",\r\n\r\n			\"allowedExtensions\" : \"*.jpg; *.gif; *.png\", \r\n\r\n			\"filesTable\" : \"greeny_images\",\r\n \r\n			\"filesTableKey\" : \"imageID\",\r\n \r\n			\"destinationDirectory\" : \"uploaded/lechenieSlider/[ID]/\",\r\n \r\n			\"usePriority\"	:	true,\r\n\r\n			\"priorityField\"	:	\"priority\",	\r\n\r\n			\"uploadScript\" : \"/admin/upload/\" ,\r\n\r\n			\"textFields\" : { 		\r\n		\r\n	}\r\n\r\n        }'),(247,'lechenie_zubov','titleForm',1,1,'LineInput','Заголовок перед формой',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(248,'lechenie_zubov','textForm',1,1,'RichText','Текст перед формой',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),(249,'sliders','photos',1,1,'DamnUpload','Фотографии',0,0,'','{ \"blOrder\" : 0, \r\n		    \"elemOrder\" : 50,\r\n\r\n			\"image\" : { \r\n				\"resize\" : { \r\n					\"strong\" : false,\r\n					\"width\" : 1000,\r\n					\"height\" : 1000 \r\n				}, \r\n\r\n				\"thumb\" : {\r\n					\"resize\" : { \r\n						\"strong\" :  true, \r\n						\"width\" : 208, \r\n						\"height\" : 112 }, \r\n					\"postfix\" : \"_small\" \r\n				} \r\n			}, \r\n\r\n            \"includeThumbCropDialog\" : true,\r\n\r\n			\"maxFileSize\" : 5048576,\r\n\r\n			\"filesType\" : \"image\",\r\n \r\n			\"maxFilesNumber\" : 50,\r\n\r\n			\"linkingTable\" : \"slidersImages\",\r\n\r\n			\"allowedExtensions\" : \"*.jpg; *.gif; *.png\", \r\n\r\n			\"filesTable\" : \"greeny_images\",\r\n \r\n			\"filesTableKey\" : \"imageID\",\r\n \r\n			\"destinationDirectory\" : \"uploaded/sliders/[ID]\",\r\n \r\n			\"usePriority\"	:	true,\r\n\r\n			\"priorityField\"	:	\"priority\",	\r\n\r\n			\"uploadScript\" : \"/admin/upload/\" ,\r\n\r\n			\"textFields\" : { 		\r\n  	 \"title\" : { \r\n                    \"label\" : \"Заголовок\", \r\n                    \"showLabel\" : true, \r\n                    \"containerType\": \"line\" \r\n                },\r\n 	  \"text\" : { \r\n                    \"label\" : \"Текст\", \r\n                    \"showLabel\" : true, \r\n                    \"containerType\": \"text\",\r\n	      \"class\":\"richEditor\" \r\n                },\r\n                \"buttonTitle\" : { \r\n                    \"label\" : \"Кнопка\", \r\n                    \"showLabel\" : true, \r\n                    \"containerType\": \"line\" \r\n                },\r\n                \"link\" : {\r\n                    \"label\" : \"Ссылка\", \r\n                    \"showLabel\" : true, \r\n                    \"containerType\": \"line\" \r\n                }\r\n            }\r\n     }'),(250,'sliders','slidersID',1,1,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0 }'),(251,'sliders','title',1,1,'LineInput','Заголовок',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(252,'sliders','text',1,1,'RichText','Текст',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 0, \"width\" : \"100%\", \"height\" : 500, \"toolbarSet\" : \"\" }'),(253,'sliders','isActive',1,1,'CheckBox','Активность',0,0,'','{ \"checkedDefault\" : false, \"blOrder\" : 0, \"elemOrder\" : 0, \"className\" : \" \" }'),(254,'sliders','pageID',1,1,'SingleSelect','Страница родитель',0,0,'',' { \"enum\" : false, \"referentTable\" : \"greeny_pageStructure\", \"listingKey\" : \"caption\", \"blOrder\" : 0, \"elemOrder\" : 0, \"emptyValue\" : 0, \"className\" : \"\", \"emptyText\" : \" - \" } '),(255,'tabs','tabsID',1,1,'HiddenInput','ID',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 10 }'),(256,'tabs','pageID',1,1,'SingleSelect','Страница родитель',0,0,'','{ \"enum\" : false, \"referentTable\" : \"greeny_pageStructure\", \"listingKey\" : \"caption\", \"blOrder\" : 0, \"elemOrder\" : 20, \"emptyValue\" : 0, \"className\" : \" \", \"emptyText\" : \" - \" }'),(257,'tabs','title',1,1,'LineInput','Заголовок',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 30, \"size\" : 60, \"className\" : \" \", \"defaultValue\" : \"\" }'),(258,'tabs','text',1,1,'RichText','Текст',0,0,'','{ \"blOrder\" : 0, \"elemOrder\" : 40, \"width\" : \"100%\", \"height\" : 200, \"toolbarSet\" : \"\" }'),(259,'tabs','isActive',1,1,'CheckBox','Публиковать',0,0,'','{ \"checkedDefault\" : false, \"blOrder\" : 0, \"elemOrder\" : 50, \"className\" : \" \" }'),(260,'tabs','slider',1,1,'DamnUpload','Фотографии',0,0,'','{ \"blOrder\" : 0, \r\n		    \"elemOrder\" : 50,\r\n\r\n			\"image\" : { \r\n				\"resize\" : { \r\n					\"strong\" : false,\r\n					\"width\" : 1000,\r\n					\"height\" : 1000 \r\n				}, \r\n\r\n				\"thumb\" : {\r\n					\"resize\" : { \r\n						\"strong\" :  true, \r\n						\"width\" : 208, \r\n						\"height\" : 112 }, \r\n					\"postfix\" : \"_small\" \r\n				} \r\n			}, \r\n\r\n            \"includeThumbCropDialog\" : true,\r\n\r\n			\"maxFileSize\" : 5048576,\r\n\r\n			\"filesType\" : \"image\",\r\n \r\n			\"maxFilesNumber\" : 50,\r\n\r\n			\"linkingTable\" : \"tab_sliders\",\r\n\r\n			\"allowedExtensions\" : \"*.jpg; *.gif; *.png\", \r\n\r\n			\"filesTable\" : \"greeny_images\",\r\n \r\n			\"filesTableKey\" : \"imageID\",\r\n \r\n			\"destinationDirectory\" : \"uploaded/tab_sliders/[ID]\",\r\n \r\n			\"usePriority\"	:	true,\r\n\r\n			\"priorityField\"	:	\"priority\",	\r\n\r\n			\"uploadScript\" : \"/admin/upload/\" ,\r\n\r\n			\"textFields\" : { 		\r\n                \"title\" : { \r\n                    \"label\" : \"Подпись\", \r\n                    \"showLabel\" : true, \r\n                    \"containerType\": \"line\" \r\n                },\r\n                \"link\" : {\r\n                    \"label\" : \"Ссылка\", \r\n                    \"showLabel\" : true, \r\n                    \"containerType\": \"line\" \r\n                }\r\n            }\r\n     }');
/*!40000 ALTER TABLE `greeny_fieldsRegistry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_files`
--

DROP TABLE IF EXISTS `greeny_files`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_files` (
  `fileID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `src` varchar(255) NOT NULL,
  PRIMARY KEY (`fileID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_files`
--

LOCK TABLES `greeny_files` WRITE;
/*!40000 ALTER TABLE `greeny_files` DISABLE KEYS */;
INSERT INTO `greeny_files` VALUES (1,'/uploaded/docs/O-kompanii.docx'),(2,'/uploaded/docs/1/stryktyra.doc'),(3,'/uploaded/docs/1/O-kompanii.docx'),(4,'/uploaded/docs/1/-2019.doc2.docx'),(5,'/uploaded/docs/1/-2019.doc2_75.docx');
/*!40000 ALTER TABLE `greeny_files` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_images`
--

DROP TABLE IF EXISTS `greeny_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_images` (
  `imageID` int(11) NOT NULL AUTO_INCREMENT,
  `src` varchar(255) NOT NULL,
  `srcSmall` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`imageID`)
) ENGINE=InnoDB AUTO_INCREMENT=253 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_images`
--

LOCK TABLES `greeny_images` WRITE;
/*!40000 ALTER TABLE `greeny_images` DISABLE KEYS */;
INSERT INTO `greeny_images` VALUES (1,'/uploaded/sliders/1/slide1_492.png','/uploaded/sliders/1/slide1_492_small.png'),(2,'/uploaded/sliders/2/slide1_991.png','/uploaded/sliders/2/slide1_991_small.png'),(3,'/uploaded/sliders/3/slide1_507.png','/uploaded/sliders/3/slide1_507_small.png'),(4,'/uploaded/statistic/5/icon_tooth.png','/uploaded/statistic/5/icon_tooth_small.png'),(5,'/uploaded/statistic/6/icon_client.png','/uploaded/statistic/6/icon_client_small.png'),(6,'/uploaded/statistic/7/icon-tooth3.png','/uploaded/statistic/icon-tooth3_small.png'),(7,'/uploaded/statistic/8/icon-tooth2.png','/uploaded/statistic/icon-tooth2_small.png'),(9,'/uploaded/reviews/client__avatar1_643.png','/uploaded/reviews/client__avatar1_643_small.png'),(10,'/uploaded/reviews/client__avatar1_822.png','/uploaded/reviews/client__avatar1_822_small.png'),(11,'/uploaded/reviews/client__avatar1_179.png','/uploaded/reviews/client__avatar1_179_small.png'),(12,'/uploaded/reviews/client__avatar1_173.png','/uploaded/reviews/client__avatar1_173_small.png'),(13,'/uploaded/personal/ava_vrach_453.png','/uploaded/personal/ava_vrach_453_small.png'),(14,'/uploaded/personal/ava_vrach1.png','/uploaded/personal/ava_vrach1_small.png'),(16,'/uploaded/personal/ava_vrach3.png','/uploaded/personal/ava_vrach3_small.png'),(17,'/uploaded/personal/ava_vrach4.png','/uploaded/personal/ava_vrach4_small.png'),(18,'/uploaded/personal/ava_vrach5.png','/uploaded/personal/ava_vrach5_small.png'),(19,'/uploaded/news/1/news_img1.png','/uploaded/news/1/news_img1_small.png'),(20,'/uploaded/news/2/news_img2.png','/uploaded/news/2/news_img2_small.png'),(21,'/uploaded/news/3/news_img3.png','/uploaded/news/3/news_img3_small.png'),(22,'/uploaded/news/4/news_img4.png','/uploaded/news/4/news_img4_small.png'),(23,'/uploaded/news/5/news_img1.png','/uploaded/news/5/news_img1_small.png'),(24,'/uploaded/certificate/sertificat1.png','/uploaded/certificate/sertificat1_small.png'),(25,'/uploaded/certificate/sertificat3.png','/uploaded/certificate/sertificat3_small.png'),(26,'/uploaded/certificate/sertificat1_68.png','/uploaded/certificate/sertificat1_68_small.png'),(27,'/uploaded/certificate/sertificat3_20.png','/uploaded/certificate/sertificat3_20_small.png'),(28,'/uploaded/certificate/sertificat1_92.png','/uploaded/certificate/sertificat1_92_small.png'),(33,'/uploaded/personal/ava_vrach_143.png','/uploaded/personal/ava_vrach_143_small.png'),(34,'/uploaded/personal/vrach-card_630.png','/uploaded/personal/vrach-card_630_small.png'),(35,'/uploaded/personal/ava_vrach2.png','/uploaded/personal/ava_vrach2_small.png'),(38,'/uploaded/personal/vrach-card_799.png','/uploaded/personal/vrach-card_799_small.png'),(39,'/uploaded/personal/ava_vrach4_12.png','/uploaded/personal/ava_vrach4_12_small.png'),(40,'/uploaded/personal/vrach-card_854.png','/uploaded/personal/vrach-card_854_small.png'),(42,'/uploaded/advantage/icon-people.png','/uploaded/advantage/icon-people_small.png'),(43,'/uploaded/advantage/icon_tooth3.png','/uploaded/advantage/icon_tooth3_small.png'),(44,'/uploaded/advantage/tooth7.png','/uploaded/advantage/tooth7_small.png'),(46,'/uploaded/news/7/news_img2.png','/uploaded/news/7/news_img2_small.png'),(47,'/uploaded/news/8/img-20.png','/uploaded/news/8/img-20_small.png'),(48,'/uploaded/news/9/img-20.png','/uploaded/news/9/img-20_small.png'),(49,'/uploaded/news/10/img-20.png','/uploaded/news/10/img-20_small.png'),(53,'/uploaded/doctor/3/img12.png','/uploaded/doctor/3/img12_small.png'),(54,'/uploaded/doctor/3/img12_310.png','/uploaded/doctor/3/img12_310_small.png'),(55,'/uploaded/doctor/3/img12_915.png','/uploaded/doctor/3/img12_915_small.png'),(56,'/uploaded/main/1/form-image.png','/uploaded/main/1/form-image_small.png'),(58,'/uploaded/price/1/form-image4.png','/uploaded/price/1/form-image4_small.png'),(64,'/uploaded/services/form-image.png','/uploaded/services/form-image_small.png'),(66,'/uploaded/doctors/form-image.png','/uploaded/doctors/form-image_small.png'),(67,'/uploaded/about/img-16.png','/uploaded/about/img-16_small.png'),(68,'/uploaded/about/team2.png','/uploaded/about/team2_small.png'),(69,'/uploaded/about/form-image4.png','/uploaded/about/form-image4_small.png'),(71,'/uploaded/doctor/3/img12_31.png','/uploaded/doctor/3/img12_31_small.png'),(72,'/uploaded/personal/ava_vrach1_5.png','/uploaded/personal/ava_vrach1_5_small.png'),(73,'/uploaded/personal/vrach-card_749.png','/uploaded/personal/vrach-card_749_small.png'),(74,'/uploaded/advantage/tooth5.png','/uploaded/advantage/tooth5_small.png'),(75,'/uploaded/statistic5/icon_tooth.png','/uploaded/statistic5/icon_tooth_small.png'),(76,'/uploaded/statistic5/icon_tooth_36.png','/uploaded/statistic5/icon_tooth_36_small.png'),(77,'/uploaded/statistic/9/icon_tooth_149.png','/uploaded/statistic/9/icon_tooth_149_small.png'),(78,'/uploaded/statistic/10/icon_client.png','/uploaded/statistic/10/icon_client_small.png'),(79,'/uploaded/statistic/11/icon-tooth3.png','/uploaded/statistic/11/icon-tooth3_small.png'),(80,'/uploaded/statistic/12/icon-tooth2.png','/uploaded/statistic/12/icon-tooth2_small.png'),(88,'/uploaded/menu/19/round1.png','/uploaded/menu/19/round1_small.png'),(89,'/uploaded/menu/20/round2.png','/uploaded/menu/20/round2_small.png'),(90,'/uploaded/menu/21/round3.png','/uploaded/menu/21/round3_small.png'),(91,'/uploaded/menu/22/round4.png','/uploaded/menu/22/round4_small.png'),(92,'/uploaded/menu/23/round5.png','/uploaded/menu/23/round5_small.png'),(93,'/uploaded/menu/24/round6.png','/uploaded/menu/24/round6_small.png'),(94,'/uploaded/menu/25/round7.png','/uploaded/menu/25/round7_small.png'),(95,'/uploaded/menu/26/round8.png','/uploaded/menu/26/round8_small.png'),(96,'/uploaded/menu/27/round9.png','/uploaded/menu/27/round9_small.png'),(97,'/uploaded/menu/28/round10.png','/uploaded/menu/28/round10_small.png'),(98,'/uploaded/sliderAbout/slide2_408.png','/uploaded/sliderAbout/slide2_408_small.png'),(101,'/uploaded/sliderAbout/slide3.png','/uploaded/sliderAbout/slide3_small.png'),(102,'/uploaded/blue_blocks/1/tooth.png','/uploaded/blue_blocks/1/tooth_small.png'),(103,'/uploaded/blue_blocks/1/ava_vrach_223.png','/uploaded/blue_blocks/1/ava_vrach_223_small.png'),(104,'/uploaded/blue_blocks/2/tooth.png','/uploaded/blue_blocks/2/tooth_small.png'),(105,'/uploaded/blue_blocks/2/ava_vrach_869.png','/uploaded/blue_blocks/2/ava_vrach_869_small.png'),(106,'/uploaded/blue_blocks/3/tooth.png','/uploaded/blue_blocks/3/tooth_small.png'),(107,'/uploaded/blue_blocks/3/ava_vrach2.png','/uploaded/blue_blocks/3/ava_vrach2_small.png'),(108,'/uploaded/blue_blocks/4/tooth.png','/uploaded/blue_blocks/4/tooth_small.png'),(109,'/uploaded/blue_blocks/4/img-pain.png','/uploaded/blue_blocks/4/img-pain_small.png'),(111,'/uploaded/blue_blocks/5/tooth.png','/uploaded/blue_blocks/5/tooth_small.png'),(112,'/uploaded/blue_blocks/6/tooth.png','/uploaded/blue_blocks/6/tooth_small.png'),(116,'/uploaded/contacts/1/form-image3.png','/uploaded/contacts/1/form-image3_small.png'),(119,'/uploaded/contacts/1/form-image.png','/uploaded/contacts/1/form-image_small.png'),(120,'/uploaded/price/1/form-image3.png','/uploaded/price/1/form-image3_small.png'),(121,'/uploaded/doctors/team.png','/uploaded/doctors/team_small.png'),(123,'/uploaded/services/dantist1.png','/uploaded/services/dantist1_small.png'),(124,'/uploaded/advantage/icon-5.png','/uploaded/advantage/icon-5_small.png'),(125,'/uploaded/advantage/icon-6.png','/uploaded/advantage/icon-6_small.png'),(126,'/uploaded/sliderAbout/slide2_718.png','/uploaded/sliderAbout/slide2_718_small.png'),(127,'/uploaded/sliderAbout/slide3_318.png','/uploaded/sliderAbout/slide3_318_small.png'),(128,'/uploaded/main/1/video__mask.png','/uploaded/main/1/video__mask_small.png'),(130,'/uploaded/menu/20/img1.png','/uploaded/menu/20/img1_small.png'),(133,'/uploaded/news/6/img-14.png','/uploaded/news/6/img-14_small.png'),(134,'/uploaded/news/6/tab2.png','/uploaded/news/6/tab2_small.png'),(135,'/uploaded/news/11/Layer_867.png','/uploaded/news/11/Layer_867_small.png'),(136,'/uploaded/news/11/img-15.png','/uploaded/news/11/img-15_small.png'),(137,'/uploaded/news/11/tab2.png','/uploaded/news/11/tab2_small.png'),(138,'/uploaded/news/11/slide5.jpg','/uploaded/news/11/slide5_small.jpg'),(139,'/uploaded/news/11/sertificat3.png','/uploaded/news/11/sertificat3_small.png'),(140,'/uploaded/news/11/round9.png','/uploaded/news/11/round9_small.png'),(141,'/uploaded/news/11/round6.png','/uploaded/news/11/round6_small.png'),(142,'/uploaded/news/7/slide1_402.png','/uploaded/news/7/slide1_402_small.png'),(144,'/uploaded/news/5/slide5.jpg','/uploaded/news/5/slide5_small.jpg'),(145,'/uploaded/news/3/slide5.jpg','/uploaded/news/3/slide5_small.jpg'),(146,'/uploaded/therapy__img.png','/uploaded/therapy__img_small.png'),(147,'/uploaded/therapy__img_33.png','/uploaded/therapy__img_33_small.png'),(148,'/uploaded/menu/21/img2-2.png','/uploaded/menu/21/img2-2_small.png'),(149,'/uploaded/menu/22/img3.png','/uploaded/menu/22/img3_small.png'),(150,'/uploaded/menu/23/img1-1.png','/uploaded/menu/23/img1-1_small.png'),(151,'/uploaded/menu/24/img4.png','/uploaded/menu/24/img4_small.png'),(153,'/uploaded/menu/26/img7.png','/uploaded/menu/26/img7_small.png'),(154,'/uploaded/menu/27/img8.png','/uploaded/menu/27/img8_small.png'),(166,'/uploaded/news/12/tab2.png','/uploaded/news/12/tab2_small.png'),(167,'/uploaded/news/12/img-14.png','/uploaded/news/12/img-14_small.png'),(168,'/uploaded/news/8/video__mask.png','/uploaded/news/8/video__mask_small.png'),(169,'/uploaded/news/9/img-15.png','/uploaded/news/9/img-15_small.png'),(170,'/uploaded/news/10/img-14.png','/uploaded/news/10/img-14_small.png'),(171,'/uploaded/news/4/img-15.png','/uploaded/news/4/img-15_small.png'),(173,'/uploaded/news/2/img-15.png','/uploaded/news/2/img-15_small.png'),(174,'/uploaded/news/1/img-14.png','/uploaded/news/1/img-14_small.png'),(176,'/uploaded/lechenie_zubov/1/form-image.png','/uploaded/lechenie_zubov/1/form-image_small.png'),(177,'/uploaded/lechenie_zubov/2/background-2.png','/uploaded/lechenie_zubov/2/background-2_small.png'),(178,'/uploaded/lechenie_zubov/2/form-image.png','/uploaded/lechenie_zubov/2/form-image_small.png'),(179,'/uploaded/lechenie_zubov/3/background-2.png','/uploaded/lechenie_zubov/3/background-2_small.png'),(180,'/uploaded/lechenie_zubov/3/form-image.png','/uploaded/lechenie_zubov/3/form-image_small.png'),(181,'/uploaded/lechenie_zubov/4/background-2.png','/uploaded/lechenie_zubov/4/background-2_small.png'),(183,'/uploaded/lechenie_zubov/4/form-image.png','/uploaded/lechenie_zubov/4/form-image_small.png'),(184,'/uploaded/lechenie_zubov/1/background-2_63.png','/uploaded/lechenie_zubov/1/background-2_63_small.png'),(187,'/uploaded/lechenieSlider/1/img10.png','/uploaded/lechenieSlider/1/img10_small.png'),(189,'/uploaded/lechenieSlider/1/img12.png','/uploaded/lechenieSlider/1/img12_small.png'),(193,'/uploaded/sliders/1/img10_800.png','/uploaded/sliders/1/img10_800_small.png'),(198,'/uploaded/sliders/1/img10_406.png','/uploaded/sliders/1/img10_406_small.png'),(206,'/uploaded/tab_sliders/3/img12.png','/uploaded/tab_sliders/3/img12_small.png'),(207,'/uploaded/tab_sliders/3/img10_337.png','/uploaded/tab_sliders/3/img10_337_small.png'),(208,'/uploaded/tab_sliders/3/img8_936.png','/uploaded/tab_sliders/3/img8_936_small.png'),(214,'/uploaded/menu/19/img8_583.png','/uploaded/menu/19/img8_583_small.png'),(215,'/uploaded/menu/25/img4.png','/uploaded/menu/25/img4_small.png'),(217,'/uploaded/menu/28/img1_603.png','/uploaded/menu/28/img1_603_small.png'),(219,'/uploaded/lechenieSlider/1/team2.png','/uploaded/lechenieSlider/1/team2_small.png'),(223,'/uploaded/tab_sliders/1/tab2.png','/uploaded/tab_sliders/1/tab2_small.png'),(224,'/uploaded/tab_sliders/1/tab1.png','/uploaded/tab_sliders/1/tab1_small.png'),(225,'/uploaded/tab_sliders/1/tab3.png','/uploaded/tab_sliders/1/tab3_small.png'),(226,'/uploaded/tab_sliders/2/vrach-card_230.png','/uploaded/tab_sliders/2/vrach-card_230_small.png'),(227,'/uploaded/tab_sliders/2/video__mask.png','/uploaded/tab_sliders/2/video__mask_small.png'),(228,'/uploaded/tab_sliders/2/team2.png','/uploaded/tab_sliders/2/team2_small.png'),(229,'/uploaded/tab_sliders/4/tab3.png','/uploaded/tab_sliders/4/tab3_small.png'),(230,'/uploaded/tab_sliders/4/tab1.png','/uploaded/tab_sliders/4/tab1_small.png'),(231,'/uploaded/tab_sliders/4/tab2.png','/uploaded/tab_sliders/4/tab2_small.png'),(232,'/uploaded/sliders/2/img8_968.png','/uploaded/sliders/2/img8_968_small.png'),(233,'/uploaded/sliders/2/img7_276.png','/uploaded/sliders/2/img7_276_small.png'),(234,'/uploaded/sliders/2/img6.png','/uploaded/sliders/2/img6_small.png'),(235,'/uploaded/tab_sliders/5/tab3.png','/uploaded/tab_sliders/5/tab3_small.png'),(236,'/uploaded/tab_sliders/5/tab2.png','/uploaded/tab_sliders/5/tab2_small.png'),(237,'/uploaded/tab_sliders/5/tab1.png','/uploaded/tab_sliders/5/tab1_small.png'),(238,'/uploaded/tab_sliders/6/slide5.jpg','/uploaded/tab_sliders/6/slide5_small.jpg'),(239,'/uploaded/tab_sliders/6/slide4.jpg','/uploaded/tab_sliders/6/slide4_small.jpg'),(240,'/uploaded/tab_sliders/6/slide3_47.png','/uploaded/tab_sliders/6/slide3_47_small.png'),(243,'/uploaded/sliders/3/img10_608.png','/uploaded/sliders/3/img10_608_small.png'),(244,'/uploaded/sliders/3/img10_414.png','/uploaded/sliders/3/img10_414_small.png'),(245,'/uploaded/sliders/4/slide5_0.jpg','/uploaded/sliders/4/slide5_0_small.jpg'),(246,'/uploaded/sliders/4/slide4_30.jpg','/uploaded/sliders/4/slide4_30_small.jpg'),(247,'/uploaded/sliders/4/slide3_798.png','/uploaded/sliders/4/slide3_798_small.png'),(250,'/uploaded/sliders/5/slide1_258.png','/uploaded/sliders/5/slide1_258_small.png'),(251,'/uploaded/sliders/5/slide1_318.png','/uploaded/sliders/5/slide1_318_small.png'),(252,'/uploaded/sliders/5/slide1_929.png','/uploaded/sliders/5/slide1_929_small.png');
/*!40000 ALTER TABLE `greeny_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_log`
--

DROP TABLE IF EXISTS `greeny_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_log` (
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userID` int(11) DEFAULT NULL,
  `ip` int(11) NOT NULL,
  `type` enum('info','warning','error') NOT NULL,
  `message` varchar(255) NOT NULL
) ENGINE=ARCHIVE DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_log`
--

LOCK TABLES `greeny_log` WRITE;
/*!40000 ALTER TABLE `greeny_log` DISABLE KEYS */;
INSERT INTO `greeny_log` VALUES ('2019-09-16 14:42:15',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-17 07:39:47',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-17 08:58:10',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-17 09:45:45',1,2130706433,'info','New page created. pageID = 2'),('2019-09-17 09:46:58',1,2130706433,'info','New page created. pageID = 3'),('2019-09-17 09:48:00',1,2130706433,'info','New page created. pageID = 4'),('2019-09-17 09:51:07',1,2130706433,'info','New page created. pageID = 5'),('2019-09-17 09:51:50',1,2130706433,'info','New page created. pageID = 6'),('2019-09-17 13:38:08',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-18 07:45:49',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-18 08:10:40',1,2130706433,'info','Settings changed, groupID=1'),('2019-09-18 09:26:51',1,2130706433,'info','Page edit success (pageID=1)'),('2019-09-18 09:35:56',1,2130706433,'info','Page edit success (pageID=1)'),('2019-09-18 09:36:02',1,2130706433,'info','Page edit success (pageID=1)'),('2019-09-18 11:09:04',1,2130706433,'info','Page edit success (pageID=1)'),('2019-09-18 11:09:10',1,2130706433,'info','Page edit success (pageID=2)'),('2019-09-18 11:09:16',1,2130706433,'info','Page edit success (pageID=2)'),('2019-09-18 11:09:25',1,2130706433,'info','Page edit success (pageID=1)'),('2019-09-18 12:00:25',1,2130706433,'info','Settings changed, groupID=1'),('2019-09-19 07:14:56',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-19 10:13:53',1,2130706433,'info','Settings changed, groupID=1'),('2019-09-19 10:21:36',1,2130706433,'info','Settings changed, groupID=1'),('2019-09-19 13:12:13',1,2130706433,'info','New page created. pageID = 8'),('2019-09-19 13:14:27',1,2130706433,'info','New page created. pageID = 9'),('2019-09-19 13:16:10',1,2130706433,'info','New page created. pageID = 10'),('2019-09-19 13:16:20',1,2130706433,'info','Page edit success (pageID=9)'),('2019-09-19 13:17:23',1,2130706433,'info','New page created. pageID = 11'),('2019-09-19 13:18:38',1,2130706433,'info','New page created. pageID = 12'),('2019-09-19 13:19:19',1,2130706433,'info','New page created. pageID = 13'),('2019-09-19 13:19:48',1,2130706433,'info','New page created. pageID = 14'),('2019-09-19 13:20:32',1,2130706433,'info','New page created. pageID = 15'),('2019-09-19 13:21:04',1,2130706433,'info','New page created. pageID = 16'),('2019-09-19 13:21:45',1,2130706433,'info','New page created. pageID = 17'),('2019-09-19 14:01:17',1,2130706433,'info','New page created. pageID = 18'),('2019-09-19 14:01:38',1,2130706433,'info','Page edit success (pageID=18)'),('2019-09-20 07:54:50',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-20 11:06:14',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-20 11:47:01',1,2130706433,'info','Settings changed, groupID=1'),('2019-09-23 08:19:13',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-23 11:01:44',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-23 11:06:23',1,2130706433,'info','Settings changed, groupID=2'),('2019-09-23 11:11:31',1,2130706433,'info','Settings changed, groupID=2'),('2019-09-23 12:47:03',1,2130706433,'info','New page created. pageID = 21'),('2019-09-23 12:47:21',1,2130706433,'info','Page edit success (pageID=21)'),('2019-09-23 12:48:31',1,2130706433,'info','Page edit success (pageID=21)'),('2019-09-23 12:49:19',1,2130706433,'info','Page edit success (pageID=21)'),('2019-09-23 12:49:40',1,2130706433,'info','Page edit success (pageID=21)'),('2019-09-24 08:08:58',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-24 09:04:41',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-24 09:05:01',1,2130706433,'info','Page edit success (pageID=3)'),('2019-09-24 10:12:37',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-24 11:13:01',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-24 12:05:56',1,2130706433,'info','Page edit success (pageID=3)'),('2019-09-24 12:07:11',1,2130706433,'info','New page created. pageID = 22'),('2019-09-24 12:52:55',1,2130706433,'info','Page edit success (pageID=7)'),('2019-09-24 13:00:20',1,2130706433,'info','Page edit success (pageID=7)'),('2019-09-25 07:51:03',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-25 09:23:15',1,2130706433,'info','Page (pageID=8) deleted'),('2019-09-25 09:23:23',1,2130706433,'info','Page (pageID=9) deleted'),('2019-09-25 09:23:33',1,2130706433,'info','Page (pageID=10) deleted'),('2019-09-25 09:23:38',1,2130706433,'info','Page (pageID=11) deleted'),('2019-09-25 09:23:45',1,2130706433,'info','Page (pageID=12) deleted'),('2019-09-25 09:23:51',1,2130706433,'info','Page (pageID=14) deleted'),('2019-09-25 09:23:57',1,2130706433,'info','Page (pageID=15) deleted'),('2019-09-25 09:24:02',1,2130706433,'info','Page (pageID=13) deleted'),('2019-09-25 09:24:07',1,2130706433,'info','Page (pageID=16) deleted'),('2019-09-25 09:24:13',1,2130706433,'info','Page (pageID=17) deleted'),('2019-09-25 09:24:28',1,2130706433,'info','Page edit success (pageID=2)'),('2019-09-25 13:21:38',1,2130706433,'info','Page edit success (pageID=4)'),('2019-09-25 13:30:28',1,2130706433,'error','Error restoring page (pageID=8)SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'isDeleted\' cannot be null'),('2019-09-25 13:30:45',1,2130706433,'error','Error restoring page (pageID=8)SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'isDeleted\' cannot be null'),('2019-09-25 13:30:54',1,2130706433,'error','Error restoring page (pageID=9)SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'isDeleted\' cannot be null'),('2019-09-25 13:31:38',1,2130706433,'info','Page (pageID=8) restored'),('2019-09-25 13:31:38',1,2130706433,'info','Page (pageID=9) restored'),('2019-09-25 13:31:38',1,2130706433,'info','Page (pageID=10) restored'),('2019-09-25 13:31:38',1,2130706433,'info','Page (pageID=11) restored'),('2019-09-25 13:31:38',1,2130706433,'info','Page (pageID=12) restored'),('2019-09-25 13:31:38',1,2130706433,'info','Page (pageID=13) restored'),('2019-09-25 13:31:38',1,2130706433,'info','Page (pageID=14) restored'),('2019-09-25 13:31:38',1,2130706433,'info','Page (pageID=15) restored'),('2019-09-25 13:31:38',1,2130706433,'info','Page (pageID=16) restored'),('2019-09-25 13:31:38',1,2130706433,'info','Page (pageID=17) restored'),('2019-09-26 07:54:23',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-26 08:03:24',1,2130706433,'info','Page edit success (pageID=2)'),('2019-09-26 12:01:59',1,2130706433,'info','Settings changed, groupID=2'),('2019-09-26 12:05:07',1,2130706433,'info','Page edit success (pageID=2)'),('2019-09-27 07:50:58',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-27 08:00:07',1,2130706433,'info','Page (pageID=6) deleted'),('2019-09-27 08:01:39',1,2130706433,'error','Error creating page: SQLSTATE[HY000]: General error: 1364 Field \'fileID\' doesn\'t have a default value'),('2019-09-27 08:01:54',1,2130706433,'info','New page created. pageID = 23'),('2019-09-27 08:02:00',1,2130706433,'info','Page wiped (pageID=6)'),('2019-09-27 08:02:08',1,2130706433,'info','Page edit success (pageID=23)'),('2019-09-27 08:06:00',1,2130706433,'info','Page edit success (pageID=23)'),('2019-09-27 08:08:59',1,2130706433,'info','New page created. pageID = 24'),('2019-09-27 08:13:57',1,2130706433,'info','Page (pageID=24) deleted'),('2019-09-27 08:14:04',1,2130706433,'info','Page wiped (pageID=24)'),('2019-09-27 08:14:50',1,2130706433,'info','Page edit success (pageID=23)'),('2019-09-27 08:32:41',1,2130706433,'info','Page (pageID=22) deleted'),('2019-09-27 09:31:16',1,2130706433,'info','Page wiped (pageID=22)'),('2019-09-27 09:44:56',1,2130706433,'info','Page edit success (pageID=3)'),('2019-09-27 09:49:10',1,2130706433,'info','New page created. pageID = 25'),('2019-09-27 11:44:31',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-27 12:02:58',1,2130706433,'info','Page (pageID=25) deleted'),('2019-09-27 13:07:19',1,2130706433,'info','Page wiped (pageID=25)'),('2019-09-27 13:45:49',1,2130706433,'info','Page edit success (pageID=5)'),('2019-09-27 13:58:43',1,2130706433,'info','Page (pageID=3) deleted'),('2019-09-27 13:58:49',1,2130706433,'info','Page wiped (pageID=3)'),('2019-09-27 14:16:04',1,2130706433,'info','New page created. pageID = 26'),('2019-09-27 14:16:20',1,2130706433,'info','Page edit success (pageID=26)'),('2019-09-27 14:18:45',1,2130706433,'info','Page edit success (pageID=26)'),('2019-09-27 14:36:38',1,2130706433,'info','Page edit success (pageID=26)'),('2019-09-27 14:37:14',1,2130706433,'info','Page edit success (pageID=26)'),('2019-09-27 14:45:30',1,2130706433,'info','Page edit success (pageID=26)'),('2019-09-30 07:57:39',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-09-30 08:21:50',1,2130706433,'info','Page (pageID=1) deleted'),('2019-09-30 08:21:56',1,2130706433,'info','Page wiped (pageID=1)'),('2019-09-30 08:44:43',1,2130706433,'info','New page created. pageID = 27'),('2019-09-30 08:44:51',1,2130706433,'info','Page edit success (pageID=27)'),('2019-09-30 08:45:04',1,2130706433,'info','Page edit success (pageID=18)'),('2019-09-30 08:45:10',1,2130706433,'info','Page edit success (pageID=18)'),('2019-09-30 09:22:02',1,2130706433,'info','Page edit success (pageID=27)'),('2019-09-30 09:24:50',1,2130706433,'info','Page edit success (pageID=18)'),('2019-09-30 09:24:55',1,2130706433,'info','Page edit success (pageID=18)'),('2019-09-30 09:25:11',1,2130706433,'info','Page edit success (pageID=27)'),('2019-09-30 09:38:41',1,2130706433,'info','Page (pageID=4) deleted'),('2019-09-30 09:38:48',1,2130706433,'info','Page wiped (pageID=4)'),('2019-09-30 10:08:00',1,2130706433,'info','New page created. pageID = 28'),('2019-09-30 10:14:27',1,2130706433,'info','Page edit success (pageID=28)'),('2019-09-30 10:33:12',1,2130706433,'info','Page edit success (pageID=2)'),('2019-09-30 10:42:40',1,2130706433,'info','Page (pageID=2) deleted'),('2019-09-30 10:42:49',1,2130706433,'info','Page wiped (pageID=2)'),('2019-09-30 11:06:40',1,2130706433,'info','Page wiped (pageID=8)'),('2019-09-30 11:06:41',1,2130706433,'info','Page wiped (pageID=9)'),('2019-09-30 11:06:41',1,2130706433,'info','Page wiped (pageID=10)'),('2019-09-30 11:06:41',1,2130706433,'info','Page wiped (pageID=11)'),('2019-09-30 11:06:41',1,2130706433,'info','Page wiped (pageID=12)'),('2019-09-30 11:06:41',1,2130706433,'info','Page wiped (pageID=13)'),('2019-09-30 11:06:41',1,2130706433,'info','Page wiped (pageID=14)'),('2019-09-30 11:06:41',1,2130706433,'info','Page wiped (pageID=15)'),('2019-09-30 11:06:41',1,2130706433,'info','Page wiped (pageID=16)'),('2019-09-30 11:06:41',1,2130706433,'info','Page wiped (pageID=17)'),('2019-09-30 11:09:41',1,2130706433,'info','New page created. pageID = 29'),('2019-09-30 11:09:49',1,2130706433,'info','Page edit success (pageID=29)'),('2019-09-30 11:18:57',1,2130706433,'info','Page edit success (pageID=29)'),('2019-09-30 11:41:47',1,2130706433,'info','Page edit success (pageID=29)'),('2019-09-30 11:42:31',1,2130706433,'info','Page edit success (pageID=29)'),('2019-09-30 12:06:16',1,2130706433,'info','Page edit success (pageID=26)'),('2019-09-30 12:20:42',1,2130706433,'info','Page edit success (pageID=26)'),('2019-09-30 12:45:29',1,2130706433,'info','Page (pageID=18) deleted'),('2019-09-30 12:45:35',1,2130706433,'info','Page wiped (pageID=18)'),('2019-09-30 13:11:12',1,2130706433,'info','New page created. pageID = 30'),('2019-09-30 13:12:20',1,2130706433,'info','Page edit success (pageID=30)'),('2019-09-30 13:47:50',1,2130706433,'info','Page edit success (pageID=30)'),('2019-09-30 13:55:18',1,2130706433,'info','Page edit success (pageID=30)'),('2019-09-30 14:17:04',1,2130706433,'info','Page edit success (pageID=26)'),('2019-09-30 14:17:47',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-01 07:41:00',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-01 08:59:18',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-01 11:29:09',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-01 11:46:11',1,2130706433,'info','Settings changed, groupID=1'),('2019-10-01 11:46:34',1,2130706433,'info','Settings changed, groupID=4'),('2019-10-01 11:51:55',1,2130706433,'info','Page edit success (pageID=33)'),('2019-10-01 11:52:06',1,2130706433,'info','Page edit success (pageID=33)'),('2019-10-03 08:42:20',1,2130706433,'info','New page created. pageID = 34'),('2019-10-03 08:46:22',1,2130706433,'info','Page edit success (pageID=34)'),('2019-10-03 08:47:49',1,2130706433,'info','Page edit success (pageID=34)'),('2019-10-03 09:00:07',1,2130706433,'info','Page edit success (pageID=33)'),('2019-10-03 09:00:28',1,2130706433,'info','Page edit success (pageID=34)'),('2019-10-03 09:00:46',1,2130706433,'info','Page edit success (pageID=34)'),('2019-10-03 09:01:09',1,2130706433,'info','Page edit success (pageID=33)'),('2019-10-03 09:01:21',1,2130706433,'info','Page edit success (pageID=34)'),('2019-10-03 09:05:21',1,2130706433,'info','Page edit success (pageID=33)'),('2019-10-03 09:06:09',1,2130706433,'info','Page edit success (pageID=33)'),('2019-10-03 09:06:40',1,2130706433,'info','Page edit success (pageID=33)'),('2019-10-03 09:33:42',1,2130706433,'info','Page edit success (pageID=27)'),('2019-10-03 10:27:33',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-03 10:27:54',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-03 11:56:00',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-03 11:56:53',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-03 12:12:21',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-03 13:56:58',1,2130706433,'info','Page edit success (pageID=7)'),('2019-10-03 14:30:52',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-03 14:53:11',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-03 15:20:58',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-03 15:21:53',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-03 15:22:17',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-03 15:22:30',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-04 07:56:27',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-04 08:12:35',1,2130706433,'info','Page edit success (pageID=27)'),('2019-10-04 08:13:05',1,2130706433,'info','Page edit success (pageID=27)'),('2019-10-04 08:13:48',1,2130706433,'info','Page edit success (pageID=27)'),('2019-10-04 08:14:08',1,2130706433,'info','Page edit success (pageID=27)'),('2019-10-04 08:14:30',1,2130706433,'info','Page edit success (pageID=27)'),('2019-10-04 08:14:47',1,2130706433,'info','Page edit success (pageID=27)'),('2019-10-04 10:38:54',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-04 10:41:16',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-04 10:47:01',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-04 12:23:06',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-04 12:44:35',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-04 12:52:57',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-04 12:53:19',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-04 12:58:26',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-04 13:05:26',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-07 07:56:18',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-07 07:59:04',1,2130706433,'info','Page edit success (pageID=29)'),('2019-10-07 07:59:12',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-07 07:59:22',1,2130706433,'info','Page edit success (pageID=27)'),('2019-10-07 07:59:41',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-07 07:59:48',1,2130706433,'info','Page edit success (pageID=29)'),('2019-10-07 07:59:56',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-07 08:00:07',1,2130706433,'info','Page edit success (pageID=28)'),('2019-10-07 08:00:13',1,2130706433,'info','Page edit success (pageID=5)'),('2019-10-07 08:00:19',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-07 08:48:03',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-07 08:48:34',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-07 09:26:34',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-07 09:26:45',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-07 09:27:10',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-07 09:32:30',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-07 09:34:51',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-07 09:35:15',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-07 09:35:27',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-07 09:46:09',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-07 09:47:36',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-07 14:09:01',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-08 07:56:05',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-08 08:00:39',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-08 09:04:27',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-08 09:04:58',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-08 09:08:11',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-08 09:08:41',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-08 09:37:30',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-08 09:38:01',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-08 09:39:21',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-08 09:39:40',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-08 09:40:47',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-08 09:40:58',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-08 09:41:39',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-08 09:41:50',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-08 09:43:50',1,2130706433,'info','Page edit success (pageID=28)'),('2019-10-08 09:44:06',1,2130706433,'info','Page edit success (pageID=28)'),('2019-10-08 09:45:04',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-08 09:45:57',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-08 09:47:04',1,2130706433,'info','Page edit success (pageID=29)'),('2019-10-08 09:47:55',1,2130706433,'info','Page edit success (pageID=29)'),('2019-10-08 09:48:58',1,2130706433,'info','Page edit success (pageID=29)'),('2019-10-08 09:49:10',1,2130706433,'info','Page edit success (pageID=29)'),('2019-10-08 09:50:29',1,2130706433,'info','Page edit success (pageID=29)'),('2019-10-08 09:50:59',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-08 10:44:16',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-08 10:45:18',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-08 10:47:43',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-08 14:24:29',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-11 14:09:21',1,2130706433,'error','Error editing page (pageID=30): Файл /var/www/dent/uploaded/tmp/slide3_247.png не может быть перемещён. Недостаточно прав на запись в целевую директорию. /var/www/dent/uploaded/sliderAbout/'),('2019-10-11 14:09:32',1,2130706433,'error','Error editing page (pageID=30): Файл /var/www/dent/uploaded/tmp/slide2_72.png не может быть перемещён. Недостаточно прав на запись в целевую директорию. /var/www/dent/uploaded/sliderAbout/'),('2019-10-11 14:09:50',1,2130706433,'error','Error editing page (pageID=30): Файл /var/www/dent/uploaded/tmp/slide2_72.png не может быть перемещён. Недостаточно прав на запись в целевую директорию. /var/www/dent/uploaded/sliderAbout/'),('2019-10-11 14:10:55',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-11 14:31:22',1,2130706433,'info','Page edit success (pageID=27)'),('2019-10-14 08:52:22',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-14 08:56:37',1,2130706433,'info','Page edit success (pageID=33)'),('2019-10-14 08:56:45',1,2130706433,'info','Page edit success (pageID=33)'),('2019-10-14 08:59:07',1,2130706433,'info','Page (pageID=34) deleted'),('2019-10-14 11:46:24',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-14 11:46:30',1,2130706433,'error','Error restoring page (pageID=34)SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'parentID\' cannot be null'),('2019-10-14 11:46:42',1,2130706433,'error','Error restoring page (pageID=34)SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'parentID\' cannot be null'),('2019-10-14 11:46:46',1,2130706433,'error','Error restoring page (pageID=34)SQLSTATE[23000]: Integrity constraint violation: 1048 Column \'parentID\' cannot be null'),('2019-10-14 11:46:50',1,2130706433,'info','Page wiped (pageID=34)'),('2019-10-14 15:01:11',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-14 15:02:23',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-15 07:58:51',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-15 08:00:45',1,2130706433,'error','Error editing page (pageID=27): SQLSTATE[42000]: Syntax error or access violation: 1064 You have an error in your SQL syntax; check the manual that corresponds to your MySQL server version for the right syntax to use near \'WHERE `mainID` = 1\' at line 2'),('2019-10-15 08:01:30',1,2130706433,'info','Page edit success (pageID=27)'),('2019-10-15 08:40:30',1,2130706433,'info','Page edit success (pageID=29)'),('2019-10-15 08:41:24',1,2130706433,'info','Page edit success (pageID=29)'),('2019-10-15 08:42:11',1,2130706433,'info','Page edit success (pageID=29)'),('2019-10-15 09:19:07',1,2130706433,'info','Page edit success (pageID=29)'),('2019-10-15 09:35:11',1,2130706433,'info','Page edit success (pageID=29)'),('2019-10-15 09:36:53',1,2130706433,'info','New page created. pageID = 40'),('2019-10-15 09:37:49',1,2130706433,'info','New page created. pageID = 41'),('2019-10-15 09:38:21',1,2130706433,'info','New page created. pageID = 42'),('2019-10-15 09:39:02',1,2130706433,'info','New page created. pageID = 43'),('2019-10-15 09:40:38',1,2130706433,'info','New page created. pageID = 44'),('2019-10-15 09:41:06',1,2130706433,'info','New page created. pageID = 45'),('2019-10-15 09:41:25',1,2130706433,'info','New page created. pageID = 46'),('2019-10-15 09:41:47',1,2130706433,'info','New page created. pageID = 47'),('2019-10-15 09:42:08',1,2130706433,'info','New page created. pageID = 48'),('2019-10-15 09:42:34',1,2130706433,'info','New page created. pageID = 49'),('2019-10-15 09:58:10',1,2130706433,'info','Page edit success (pageID=43)'),('2019-10-15 10:12:21',1,2130706433,'info','New page created. pageID = 50'),('2019-10-15 10:55:23',1,2130706433,'info','New page created. pageID = 51'),('2019-10-15 11:45:26',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-15 12:55:48',1,2130706433,'info','New page created. pageID = 54'),('2019-10-15 12:56:07',1,2130706433,'info','Page edit success (pageID=54)'),('2019-10-15 12:58:35',1,2130706433,'info','New page created. pageID = 55'),('2019-10-15 12:59:07',1,2130706433,'info','Page edit success (pageID=55)'),('2019-10-15 14:10:22',1,2130706433,'info','Page edit success (pageID=55)'),('2019-10-15 14:25:42',1,2130706433,'info','Page edit success (pageID=51)'),('2019-10-15 14:27:07',1,2130706433,'info','Page edit success (pageID=51)'),('2019-10-15 15:18:23',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-16 08:05:54',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-16 09:10:29',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 09:11:16',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 09:13:06',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 09:15:57',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 09:51:48',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 09:54:47',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 10:01:12',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 10:01:34',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 10:48:05',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 11:15:00',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 12:13:39',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 12:16:53',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 12:17:35',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 12:18:10',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 12:34:05',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 12:36:40',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 12:37:38',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 12:45:40',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 12:46:44',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 12:46:57',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-16 12:48:05',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-17 07:52:57',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-17 08:33:39',1,2130706433,'info','Page edit success (pageID=27)'),('2019-10-17 08:34:20',1,2130706433,'info','Page edit success (pageID=27)'),('2019-10-17 13:07:37',1,2130706433,'info','Page edit success (pageID=55)'),('2019-10-18 08:13:21',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-18 10:00:56',1,2130706433,'info','Page edit success (pageID=29)'),('2019-10-18 10:01:15',1,2130706433,'info','Page edit success (pageID=29)'),('2019-10-18 10:19:45',1,2130706433,'info','Settings changed, groupID=2'),('2019-10-18 10:21:11',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-18 10:27:53',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-18 10:29:33',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-18 10:29:47',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-18 10:29:47',1,2130706433,'info','Page edit success (pageID=30)'),('2019-10-18 10:36:06',1,2130706433,'info','Page edit success (pageID=26)'),('2019-10-18 10:37:39',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-18 10:37:56',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-18 10:38:07',1,2130706433,'info','Page edit success (pageID=23)'),('2019-10-18 10:39:05',1,2130706433,'error','Error editing page (pageID=50): Файл /var/www/dent/uploaded/tmp/tooth5.png не может быть перемещён. Недостаточно прав на запись в целевую директорию. /var/www/dent/uploaded/lechenieSlider/1/'),('2019-10-18 10:39:14',1,2130706433,'error','Error editing page (pageID=50): Файл /var/www/dent/uploaded/tmp/tooth5.png не может быть перемещён. Недостаточно прав на запись в целевую директорию. /var/www/dent/uploaded/lechenieSlider/1/'),('2019-10-18 10:39:33',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-18 10:40:11',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-18 10:41:17',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-18 10:46:22',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-18 10:57:26',1,2130706433,'info','Page edit success (pageID=54)'),('2019-10-18 12:51:51',1,2130706433,'info','Page edit success (pageID=27)'),('2019-10-18 13:03:23',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-18 13:16:00',1,2130706433,'info','Page edit success (pageID=51)'),('2019-10-18 13:16:38',1,2130706433,'info','Page edit success (pageID=51)'),('2019-10-18 13:23:37',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-18 13:29:37',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-18 13:30:01',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-18 13:30:29',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-18 13:32:06',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-18 13:36:53',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-18 13:37:26',1,2130706433,'info','Page edit success (pageID=50)'),('2019-10-18 14:21:29',NULL,2130706433,'info','Authorisation, username=admin, userID=1'),('2019-10-18 14:22:22',1,2130706433,'info','Page edit success (pageID=51)'),('2019-10-21 08:09:11',NULL,2130706433,'info','Authorisation, username=admin, userID=1');
/*!40000 ALTER TABLE `greeny_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_modules`
--

DROP TABLE IF EXISTS `greeny_modules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_modules` (
  `moduleID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `description` varchar(127) NOT NULL DEFAULT '',
  `version` varchar(20) DEFAULT '',
  `rootAlias` varchar(50) DEFAULT NULL,
  `isVisible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`moduleID`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_modules`
--

LOCK TABLES `greeny_modules` WRITE;
/*!40000 ALTER TABLE `greeny_modules` DISABLE KEYS */;
INSERT INTO `greeny_modules` VALUES (1,'AnyEditor','Всё Редактор','',NULL,0),(11,'NewsLine','Новостная лента','3.2','newsline',1),(13,'Feedback','Обратная связь','1.2','feedback',1);
/*!40000 ALTER TABLE `greeny_modules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_modulesSettings`
--

DROP TABLE IF EXISTS `greeny_modulesSettings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_modulesSettings` (
  `settingID` int(11) NOT NULL AUTO_INCREMENT,
  `moduleID` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  `value` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) DEFAULT NULL,
  `order` mediumint(9) NOT NULL DEFAULT '0',
  `isVisible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`settingID`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_modulesSettings`
--

LOCK TABLES `greeny_modulesSettings` WRITE;
/*!40000 ALTER TABLE `greeny_modulesSettings` DISABLE KEYS */;
INSERT INTO `greeny_modulesSettings` VALUES (29,11,'rootAlias','newsline','Алиас во фрон-енде',0,1),(30,11,'ParentAlias','','Полный алиас родительского раздела',20,1),(31,11,'MessagesOnPageAdmin','10','По сколько сообщений отображать на странице в админке',20,1),(32,11,'MessagesOnPageFront','10','По сколько сообщений отображать на странице на фрон-енде',20,1),(36,13,'rootAlias','feedback','Алиас во фрон-енде',0,1),(37,13,'Email','name@mail.ru','Адреса (через запятую), на которые отправлять сообщения (если не указан, сообщения не будут отправляться)',10,1),(38,13,'MessagesOnPage','10','По сколько сообщений отображать на странице',5,1);
/*!40000 ALTER TABLE `greeny_modulesSettings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_nl_images`
--

DROP TABLE IF EXISTS `greeny_nl_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_nl_images` (
  `newsImageID` int(11) NOT NULL AUTO_INCREMENT,
  `imageID` int(11) NOT NULL,
  `newsID` int(11) NOT NULL,
  PRIMARY KEY (`newsImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_nl_images`
--

LOCK TABLES `greeny_nl_images` WRITE;
/*!40000 ALTER TABLE `greeny_nl_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `greeny_nl_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_nl_news`
--

DROP TABLE IF EXISTS `greeny_nl_news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_nl_news` (
  `newsID` int(11) NOT NULL AUTO_INCREMENT,
  `sectionID` int(11) DEFAULT NULL,
  `smallImageID` smallint(4) DEFAULT NULL,
  `bigImageID` smallint(4) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `summary` varchar(1000) DEFAULT NULL,
  `body` text NOT NULL,
  `publicationDate` datetime NOT NULL,
  `modificationDate` datetime DEFAULT NULL,
  `isPublished` tinyint(1) DEFAULT '1',
  `isDeleted` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`newsID`),
  KEY `FK_SECTION` (`sectionID`),
  CONSTRAINT `FK_SECTION` FOREIGN KEY (`sectionID`) REFERENCES `greeny_nl_sections` (`sectionID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_nl_news`
--

LOCK TABLES `greeny_nl_news` WRITE;
/*!40000 ALTER TABLE `greeny_nl_news` DISABLE KEYS */;
INSERT INTO `greeny_nl_news` VALUES (1,1,19,174,'Заголовок Первый','Употребление чая повышает риск эрозии зубной эмали','<p style=\"text-align: center;\">Употребление чая повышает риск эрозии зубной эмали</p>\r\n<p style=\"text-align: left;\">Если зубы не болят, они ведь в порядке, правда?<br />\r\nНе всегда.<br />\r\nСтоматологи предупреждают, что если боль в зубе вас беспокоит, уже  поздно бить тревогу. Боль появляется, когда задет зубной нерв. В этом  случае врач установит пломбу, в запущенных случаях ее придется удалить  нерв из корневого канала зуба.<br />\r\nВот почему важна регулярная проверка состояния ваших зубов.<br />\r\nМиф: &laquo;Подождите, пока не заболит&raquo; должен быть в конце концов развенчан, ведь даже разрушенный зуб не всегда болит.<br />\r\nА знаете, что действительно причиняет боль? Цена на услуги стоматолога, когда вы придете к нему слишком поздно.</p>\r\n<p style=\"text-align: left;\"><strong>Факт: сахар является причиной кариеса</strong></p>\r\n<p style=\"text-align: left;\">Здесь  все гораздо глубже, чем кажется: действительно, сахар может  способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме того, разрушительным оказывает не только сахар из конфет; сахара  есть, например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются бактерии, живущие в ротовой полости, в результате образуется  кислота, которая разрушает зубы.<br />\r\nВот почему так важно пользоваться  зубной щеткой, зубной нитью и антибактериальным ополаскивателем для  полости рта. Зубы необходимо чистить как минимум дважды в день (по 2  минуты каждый раз), один раз в день полоскать рот и один раз  использовать зубную нить. Если этого не делать, сначала образуется  зубной налет, ну а если вы любите углеводную пищу, со временем налет  превращается в кислоту. Кислота разъедает зубную эмаль, образовывается  дыра в зубе, в нее проникают бактерии, а их уже невозможно вычистить  щеткой или зубной нитью.<br />\r\nВ этом случае кариес не заставит себя долго  ждать, вот почему конфеты вредны для ваших зубов, но также не забывайте  про регулярный уход за полостью рта.</p>\r\n<p style=\"text-align: left;\">Здесь  все гораздо глубже, чем кажется: действительно, сахар может  способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме того, разрушительным оказывает не только сахар из конфет; сахара  есть, например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются бактерии, живущие в ротовой полости, в результате образуется  кислота, которая разрушает зубы.<br />\r\nВот почему так важно пользоваться  зубной щеткой, зубной нитью и антибактериальным ополаскивателем для  полости рта. Зубы необходимо чистить как минимум дважды в день (по 2  минуты каждый раз), один раз в день полоскать рот и один раз  использовать зубную нить. Если этого не делать, сначала образуется  зубной налет, ну а если вы любите углеводную пищу, со временем налет  превращается в кислоту. Кислота разъедает зубную эмаль, образовывается  дыра в зубе, в нее проникают бактерии, а их уже невозможно вычистить  щеткой или зубной нитью.<br />\r\nВ этом случае кариес не заставит себя долго  ждать, вот почему конфеты вредны для ваших зубов, но также не забывайте  про регулярный уход за полостью рта.</p>\r\n<p style=\"text-align: left;\">Здесь  все гораздо глубже, чем кажется: действительно, сахар может  способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме того, разрушительным оказывает не только сахар из конфет; сахара  есть, например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются бактерии, живущие в ротовой полости, в результате образуется  кислота, которая разрушает зубы.<br />\r\nВот почему так важно пользоваться  зубной щеткой, зубной нитью и антибактериальным ополаскивателем для  полости рта. Зубы необходимо чистить как минимум дважды в день (по 2  минуты каждый раз), один раз в день полоскать рот и один раз  использовать зубную нить. Если этого не делать, сначала образуется  зубной налет, ну а если вы любите углеводную пищу, со временем налет  превращается в кислоту. Кислота разъедает зубную эмаль, образовывается  дыра в зубе, в нее проникают бактерии, а их уже невозможно вычистить  щеткой или зубной нитью.<br />\r\nВ этом случае кариес не заставит себя долго  ждать, вот почему конфеты вредны для ваших зубов, но также не забывайте  про регулярный уход за полостью рта.</p>','2019-09-19 00:00:00','2019-10-15 12:31:22',1,0),(2,1,20,173,'Заголовок Второй','Употребление чая повышает риск эрозии зубной эмали','<p style=\"text-align: center;\">Употребление чая повышает риск эрозии зубной эмали</p>\r\n<p>Если зубы не болят, они ведь в порядке, правда?<br />\r\nНе всегда.<br />\r\nСтоматологи предупреждают, что если боль в зубе вас беспокоит, уже  поздно бить тревогу. Боль появляется, когда задет зубной нерв. В этом  случае врач установит пломбу, в запущенных случаях ее придется удалить  нерв из корневого канала зуба.<br />\r\nВот почему важна регулярная проверка состояния ваших зубов.<br />\r\nМиф: &laquo;Подождите, пока не заболит&raquo; должен быть в конце концов развенчан, ведь даже разрушенный зуб не всегда болит.<br />\r\nА знаете, что действительно причиняет боль? Цена на услуги стоматолога, когда вы придете к нему слишком поздно.</p>\r\n<p><strong>Факт: сахар является причиной кариеса</strong></p>\r\n<p>Здесь  все гораздо глубже, чем кажется: действительно, сахар может  способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме того, разрушительным оказывает не только сахар из конфет; сахара  есть, например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются бактерии, живущие в ротовой полости, в результате образуется  кислота, которая разрушает зубы.<br />\r\nВот почему так важно пользоваться  зубной щеткой, зубной нитью и антибактериальным ополаскивателем для  полости рта. Зубы необходимо чистить как минимум дважды в день (по 2  минуты каждый раз), один раз в день полоскать рот и один раз  использовать зубную нить. Если этого не делать, сначала образуется  зубной налет, ну а если вы любите углеводную пищу, со временем налет  превращается в кислоту. Кислота разъедает зубную эмаль, образовывается  дыра в зубе, в нее проникают бактерии, а их уже невозможно вычистить  щеткой или зубной нитью.<br />\r\nВ этом случае кариес не заставит себя долго  ждать, вот почему конфеты вредны для ваших зубов, но также не забывайте  про регулярный уход за полостью рта.</p>\r\n<p>Здесь  все гораздо глубже, чем кажется: действительно, сахар может  способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме того, разрушительным оказывает не только сахар из конфет; сахара  есть, например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются бактерии, живущие в ротовой полости, в результате образуется  кислота, которая разрушает зубы.<br />\r\nВот почему так важно пользоваться  зубной щеткой, зубной нитью и антибактериальным ополаскивателем для  полости рта. Зубы необходимо чистить как минимум дважды в день (по 2  минуты каждый раз), один раз в день полоскать рот и один раз  использовать зубную нить. Если этого не делать, сначала образуется  зубной налет, ну а если вы любите углеводную пищу, со временем налет  превращается в кислоту. Кислота разъедает зубную эмаль, образовывается  дыра в зубе, в нее проникают бактерии, а их уже невозможно вычистить  щеткой или зубной нитью.<br />\r\nВ этом случае кариес не заставит себя долго  ждать, вот почему конфеты вредны для ваших зубов, но также не забывайте  про регулярный уход за полостью рта.</p>\r\n<p>Здесь  все гораздо глубже, чем кажется: действительно, сахар может  способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме того, разрушительным оказывает не только сахар из конфет; сахара  есть, например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются бактерии, живущие в ротовой полости, в результате образуется  кислота, которая разрушает зубы.<br />\r\nВот почему так важно пользоваться  зубной щеткой, зубной нитью и антибактериальным ополаскивателем для  полости рта. Зубы необходимо чистить как минимум дважды в день (по 2  минуты каждый раз), один раз в день полоскать рот и один раз  использовать зубную нить. Если этого не делать, сначала образуется  зубной налет, ну а если вы любите углеводную пищу, со временем налет  превращается в кислоту. Кислота разъедает зубную эмаль, образовывается  дыра в зубе, в нее проникают бактерии, а их уже невозможно вычистить  щеткой или зубной нитью.<br />\r\nВ этом случае кариес не заставит себя долго  ждать, вот почему конфеты вредны для ваших зубов, но также не забывайте  про регулярный уход за полостью рта.</p>','2019-09-20 00:00:00','2019-10-15 12:30:39',1,0),(3,1,21,145,'Заголовок Третий','Употребление чая повышает риск эрозии зубной эмали','<pre style=\"background-color: rgb(43, 43, 43); color: rgb(169, 183, 198); font-family: \" dejavu=\"\" sans=\"\" font-size:=\"\" text-align:=\"\">\r\nУпотребление чая повышает риск эрозии зубной эмали</pre>\r\n<p>Если зубы не болят, они ведь в порядке, правда?<br />\r\nНе всегда.<br />\r\nСтоматологи предупреждают, что если боль в зубе вас беспокоит, уже  поздно бить тревогу. Боль появляется, когда задет зубной нерв. В этом  случае врач установит пломбу, в запущенных случаях ее придется удалить  нерв из корневого канала зуба.<br />\r\nВот почему важна регулярная проверка состояния ваших зубов.<br />\r\nМиф: &laquo;Подождите, пока не заболит&raquo; должен быть в конце концов развенчан, ведь даже разрушенный зуб не всегда болит.<br />\r\nА знаете, что действительно причиняет боль? Цена на услуги стоматолога, когда вы придете к нему слишком поздно.</p>\r\n<p><strong>Факт: сахар является причиной кариеса</strong></p>\r\n<p>Здесь  все гораздо глубже, чем кажется: действительно, сахар может  способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме того, разрушительным оказывает не только сахар из конфет; сахара  есть, например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются бактерии, живущие в ротовой полости, в результате образуется  кислота, которая разрушает зубы.<br />\r\nВот почему так важно пользоваться  зубной щеткой, зубной нитью и антибактериальным ополаскивателем для  полости рта. Зубы необходимо чистить как минимум дважды в день (по 2  минуты каждый раз), один раз в день полоскать рот и один раз  использовать зубную нить. Если этого не делать, сначала образуется  зубной налет, ну а если вы любите углеводную пищу, со временем налет  превращается в кислоту. Кислота разъедает зубную эмаль, образовывается  дыра в зубе, в нее проникают бактерии, а их уже невозможно вычистить  щеткой или зубной нитью.<br />\r\nВ этом случае кариес не заставит себя долго  ждать, вот почему конфеты вредны для ваших зубов, но также не забывайте  про регулярный уход за полостью рта.</p>\r\n<p>Здесь  все гораздо глубже, чем кажется: действительно, сахар может  способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме того, разрушительным оказывает не только сахар из конфет; сахара  есть, например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются бактерии, живущие в ротовой полости, в результате образуется  кислота, которая разрушает зубы.<br />\r\nВот почему так важно пользоваться  зубной щеткой, зубной нитью и антибактериальным ополаскивателем для  полости рта. Зубы необходимо чистить как минимум дважды в день (по 2  минуты каждый раз), один раз в день полоскать рот и один раз  использовать зубную нить. Если этого не делать, сначала образуется  зубной налет, ну а если вы любите углеводную пищу, со временем налет  превращается в кислоту. Кислота разъедает зубную эмаль, образовывается  дыра в зубе, в нее проникают бактерии, а их уже невозможно вычистить  щеткой или зубной нитью.<br />\r\nВ этом случае кариес не заставит себя долго  ждать, вот почему конфеты вредны для ваших зубов, но также не забывайте  про регулярный уход за полостью рта.</p>\r\n<p>Здесь  все гораздо глубже, чем кажется: действительно, сахар может  способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме того, разрушительным оказывает не только сахар из конфет; сахара  есть, например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются бактерии, живущие в ротовой полости, в результате образуется  кислота, которая разрушает зубы.<br />\r\nВот почему так важно пользоваться  зубной щеткой, зубной нитью и антибактериальным ополаскивателем для  полости рта. Зубы необходимо чистить как минимум дважды в день (по 2  минуты каждый раз), один раз в день полоскать рот и один раз  использовать зубную нить. Если этого не делать, сначала образуется  зубной налет, ну а если вы любите углеводную пищу, со временем налет  превращается в кислоту. Кислота разъедает зубную эмаль, образовывается  дыра в зубе, в нее проникают бактерии, а их уже невозможно вычистить  щеткой или зубной нитью.<br />\r\nВ этом случае кариес не заставит себя долго  ждать, вот почему конфеты вредны для ваших зубов, но также не забывайте  про регулярный уход за полостью рта.</p>','2019-09-22 00:00:00','2019-10-14 17:47:23',1,0),(4,1,22,171,'Заголовок Четвертый','Употребление чая повышает риск эрозии зубной эмали','<p style=\"text-align: center;\">Употребление чая повышает риск эрозии зубной эмали</p>\r\n<p>Если зубы не болят, они ведь в порядке, правда?</p>\r\n<p>Не всегда.<br />\r\nСтоматологи  предупреждают, что если боль в зубе вас беспокоит, уже  поздно бить  тревогу. Боль появляется, когда задет зубной нерв. В этом  случае врач  установит пломбу, в запущенных случаях ее придется удалить  нерв из  корневого канала зуба.<br />\r\nВот почему важна регулярная проверка состояния ваших зубов.<br />\r\nМиф: &laquo;Подождите, пока не заболит&raquo; должен быть в конце концов развенчан, ведь даже разрушенный зуб не всегда болит.<br />\r\nА знаете, что действительно причиняет боль? Цена на услуги стоматолога, когда вы придете к нему слишком поздно.</p>\r\n<p><strong>Факт: сахар является причиной кариеса</strong></p>\r\n<p>Здесь   все гораздо глубже, чем кажется: действительно, сахар может   способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме  того, разрушительным оказывает не только сахар из конфет; сахара  есть,  например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются  бактерии, живущие в ротовой полости, в результате образуется  кислота,  которая разрушает зубы.<br />\r\nВот почему так важно пользоваться  зубной  щеткой, зубной нитью и антибактериальным ополаскивателем для  полости  рта. Зубы необходимо чистить как минимум дважды в день (по 2  минуты  каждый раз), один раз в день полоскать рот и один раз  использовать  зубную нить. Если этого не делать, сначала образуется  зубной налет, ну а  если вы любите углеводную пищу, со временем налет  превращается в  кислоту. Кислота разъедает зубную эмаль, образовывается  дыра в зубе, в  нее проникают бактерии, а их уже невозможно вычистить  щеткой или зубной  нитью.<br />\r\nВ этом случае кариес не заставит себя долго  ждать, вот  почему конфеты вредны для ваших зубов, но также не забывайте  про  регулярный уход за полостью рта.</p>\r\n<p>Здесь  все гораздо глубже, чем  кажется: действительно, сахар может  способствовать возникновению  кариеса, но не сам сахар является причиной.<br />\r\nКроме того,  разрушительным оказывает не только сахар из конфет; сахара  есть,  например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются  бактерии, живущие в ротовой полости, в результате образуется  кислота,  которая разрушает зубы.<br />\r\nВот почему так важно пользоваться  зубной  щеткой, зубной нитью и антибактериальным ополаскивателем для  полости  рта. Зубы необходимо чистить как минимум дважды в день (по 2  минуты  каждый раз), один раз в день полоскать рот и один раз  использовать  зубную нить. Если этого не делать, сначала образуется  зубной налет, ну а  если вы любите углеводную пищу, со временем налет  превращается в  кислоту. Кислота разъедает зубную эмаль, образовывается  дыра в зубе, в  нее проникают бактерии, а их уже невозможно вычистить  щеткой или зубной  нитью.<br />\r\nВ этом случае кариес не заставит себя долго  ждать, вот  почему конфеты вредны для ваших зубов, но также не забывайте  про  регулярный уход за полостью рта.</p>\r\n<p>Здесь  все гораздо глубже, чем  кажется: действительно, сахар может  способствовать возникновению  кариеса, но не сам сахар является причиной.</p>','2019-09-23 00:00:00','2019-10-15 12:29:47',1,0),(5,1,23,144,'Заголовок Пятый','Употребление чая повышает риск эрозии зубной эмали','<p style=\"text-align: center;\">Употребление чая повышает риск эрозии зубной эмали</p>\r\n<p><strong>Факт: сахар является причиной кариеса</strong></p>\r\n<p>Здесь   все гораздо глубже, чем кажется: действительно, сахар может   способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме  того, разрушительным оказывает не только сахар из конфет; сахара  есть,  например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются  бактерии, живущие в ротовой полости, в результате образуется  кислота,  которая разрушает зубы.<br />\r\nВот почему так важно пользоваться  зубной  щеткой, зубной нитью и антибактериальным ополаскивателем для  полости  рта. Зубы необходимо чистить как минимум дважды в день (по 2  минуты  каждый раз), один раз в день полоскать рот и один раз  использовать  зубную нить. Если этого не делать, сначала образуется  зубной налет, ну а  если вы любите углеводную пищу, со временем налет  превращается в  кислоту. Кислота разъедает зубную эмаль, образовывается  дыра в зубе, в  нее проникают бактерии, а их уже невозможно вычистить  щеткой или зубной  нитью.<br />\r\nВ этом случае кариес не заставит себя долго  ждать, вот  почему конфеты вредны для ваших зубов, но также не забывайте  про  регулярный уход за полостью рта.</p>\r\n<p>Здесь  все гораздо глубже, чем  кажется: действительно, сахар может  способствовать возникновению  кариеса, но не сам сахар является причиной.<br />\r\nКроме того,  разрушительным оказывает не только сахар из конфет; сахара  есть,  например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются  бактерии, живущие в ротовой полости, в результате образуется  кислота,  которая разрушает зубы.<br />\r\nВот почему так важно пользоваться  зубной  щеткой, зубной нитью и антибактериальным ополаскивателем для  полости  рта. Зубы необходимо чистить как минимум дважды в день (по 2  минуты  каждый раз), один раз в день полоскать рот и один раз  использовать  зубную нить. Если этого не делать, сначала образуется  зубной налет, ну а  если вы любите углеводную пищу, со временем налет  превращается в  кислоту. Кислота разъедает зубную эмаль, образовывается  дыра в зубе, в  нее проникают бактерии, а их уже невозможно вычистить  щеткой или зубной  нитью.<br />\r\nВ этом случае кариес не заставит себя долго  ждать, вот  почему конфеты вредны для ваших зубов, но также не забывайте  про  регулярный уход за полостью рта.</p>\r\n<p>Здесь  все гораздо глубже, чем  кажется: действительно, сахар может  способствовать возникновению  кариеса, но не сам сахар является причиной.</p>','2019-09-23 00:00:00','2019-10-14 17:26:11',1,0),(6,1,134,133,'Заголовок 666','Употребление чая повышает риск эрозии зубной эмали','<div class=\"article__part2-wrap\">Среди самых частых проблем, с которыми сталкиваются современные люди, можно выделить:\r\n<ul class=\"article__part2-list\">\r\n    <li>Чувствительность зубов;</li>\r\n    <li>Потемнение эмали, цветной налет;</li>\r\n    <li>Кариес;</li>\r\n    <li>Склонность к воспалениям десен.</li>\r\n</ul>\r\nКаждая из этих проблем требует обязательного вмешательства стоматолога &mdash; профессиональная гигиена полости рта, обработка антибактериальными составами, покрытие зубов специальными средствами для укрепления эмали и, конечно, своевременное лечение кариеса зубов помогают избежать развития осложнений. Но в каждом из перечисленных случаев большую роль играет и выбор средств для домашнего ухода.Каждая из этих проблем требует обязательного вмешательства стоматолога &mdash; профессиональная гигиена полости рта, обработка антибактериальными составами, покрытие зубов специальными средствами для укрепления эмали и, конечно, своевременное лечение кариеса зубов помогают избежать развития осложнений. Но в каждом из перечисленных случаев большую роль играет и выбор средств для домашнего ухода.Каждая из этих проблем требует обязательного вмешательства стоматолога &mdash; профессиональная гигиена полости рта, обработка антибактериальными составами, покрытие зубов специальными средствами для укрепления эмали и, конечно, своевременное лечение кариеса зубов помогают избежать развития осложнений. Но в каждом из перечисленных случаев большую роль играет и выбор средств для домашнего ухода.Каждая из этих проблем требует обязательного вмешательства стоматолога &mdash; профессиональная гигиена полости рта, обработка антибактериальными составами, покрытие зубов специальными средствами для укрепления эмали и, конечно, своевременное лечение кариеса зубов помогают избежать развития осложнений. Но в каждом из перечисленных случаев большую роль играет и выбор средств для домашнего ухода.Итак:</div>','2019-09-24 00:00:00','2019-10-14 16:58:46',1,0),(7,1,46,142,'Заголовок 777','Употребление чая повышает риск эрозии зубной эмали','<p style=\"text-align: center;\">Употребление чая повышает риск эрозии зубной эмали</p>\r\n<p><strong>Факт: сахар является причиной кариеса</strong></p>\r\n<p>Здесь    все гораздо глубже, чем кажется: действительно, сахар может    способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме   того, разрушительным оказывает не только сахар из конфет; сахара   есть,  например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже   питаются  бактерии, живущие в ротовой полости, в результате образуется   кислота,  которая разрушает зубы.<br />\r\nВот почему так важно пользоваться   зубной  щеткой, зубной нитью и антибактериальным ополаскивателем для   полости  рта. Зубы необходимо чистить как минимум дважды в день (по 2   минуты  каждый раз), один раз в день полоскать рот и один раз   использовать  зубную нить. Если этого не делать, сначала образуется   зубной налет, ну а  если вы любите углеводную пищу, со временем налет   превращается в  кислоту. Кислота разъедает зубную эмаль, образовывается   дыра в зубе, в  нее проникают бактерии, а их уже невозможно вычистить   щеткой или зубной  нитью.<br />\r\nВ этом случае кариес не заставит себя долго   ждать, вот  почему конфеты вредны для ваших зубов, но также не  забывайте  про  регулярный уход за полостью рта.</p>\r\n<p>Здесь  все  гораздо глубже, чем  кажется: действительно, сахар может  способствовать  возникновению  кариеса, но не сам сахар является причиной.<br />\r\nКроме  того,  разрушительным оказывает не только сахар из конфет; сахара  есть,   например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются   бактерии, живущие в ротовой полости, в результате образуется  кислота,   которая разрушает зубы.</p>','2019-09-24 00:00:00','2019-10-14 17:18:13',1,0),(8,1,47,168,'Заголовок 888','Употребление чая повышает риск эрозии зубной эмали','<p style=\"text-align: center;\">Употребление чая повышает риск эрозии зубной эмали</p>\r\n<p><strong>Факт: сахар является причиной кариеса</strong></p>\r\n<p>Здесь    все гораздо глубже, чем кажется: действительно, сахар может    способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме   того, разрушительным оказывает не только сахар из конфет; сахара   есть,  например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже   питаются  бактерии, живущие в ротовой полости, в результате образуется   кислота,  которая разрушает зубы.<br />\r\nВот почему так важно пользоваться   зубной  щеткой, зубной нитью и антибактериальным ополаскивателем для   полости  рта. Зубы необходимо чистить как минимум дважды в день (по 2   минуты  каждый раз), один раз в день полоскать рот и один раз   использовать  зубную нить. Если этого не делать, сначала образуется   зубной налет, ну а  если вы любите углеводную пищу, со временем налет   превращается в  кислоту. Кислота разъедает зубную эмаль, образовывается   дыра в зубе, в  нее проникают бактерии, а их уже невозможно вычистить   щеткой или зубной  нитью.<br />\r\nВ этом случае кариес не заставит себя долго   ждать, вот  почему конфеты вредны для ваших зубов, но также не  забывайте  про  регулярный уход за полостью рта.</p>\r\n<p>Здесь  все  гораздо глубже, чем  кажется: действительно, сахар может  способствовать  возникновению  кариеса, но не сам сахар является причиной.<br />\r\nКроме  того,  разрушительным оказывает не только сахар из конфет; сахара  есть,   например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаются   бактерии, живущие в ротовой полости, в результате образуется  кислота,   которая разрушает зубы.</p>','2019-09-24 00:00:00','2019-10-15 12:28:15',1,0),(9,1,48,169,'Заголовок 999','Употребление чая повышает риск эрозии зубной эмали','<p style=\"text-align: center;\">Употребление чая повышает риск эрозии зубной эмали</p>\r\n<p><strong>Факт: сахар является причиной кариеса</strong></p>\r\n<p>Здесь    все гораздо глубже, чем кажется: действительно, сахар может    способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме   того, разрушительным оказывает не только сахар из конфет; сахара   есть,  например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже   питаются  бактерии, живущие в ротовой полости, в результате образуется   кислота,  которая разрушает зубы.<br />\r\nВот почему так важно пользоваться   зубной  щеткой, зубной нитью и антибактериальным ополаскивателем для   полости  рта. Зубы необходимо чистить как минимум дважды в день (по 2   минуты  каждый раз), один раз в день полоскать рот и один раз   использовать  зубную нить. Если этого не делать, сначала образуется   зубной налет, ну а  если вы любите углеводную пищу, со временем налет   превращается в  кислоту. Кислота разъедает зубную эмаль, образовывается   дыра в зубе, в  нее проникают бактерии, а их уже невозможно вычистить   щеткой или зубной  нитью.<br />\r\nВ этом случае кариес не заставит себя долго   ждать, вот  почему конфеты вредны для ваших зубов, но также не  забывайте  про  регулярный уход за полостью рта.</p>\r\n<p>&nbsp;</p>','2019-09-24 00:00:00','2019-10-15 12:28:44',1,0),(10,1,49,170,'Заголовок 000','Употребление чая повышает риск эрозии зубной эмали','<p style=\"text-align: center;\">Употребление чая повышает риск эрозии зубной эмали</p>\r\n<p><strong>Факт: сахар является причиной кариеса</strong></p>\r\n<p>Здесь    все гораздо глубже, чем кажется: действительно, сахар может    способствовать возникновению кариеса, но не сам сахар является причиной.<br />\r\nКроме   того, разрушительным оказывает не только сахар из конфет; сахара   есть,  например, в хлебе, бобовых, фруктах и картофеле &ndash; ими тоже  питаю</p>','2019-09-24 00:00:00','2019-10-15 12:29:25',1,0),(11,1,135,136,'Тестовая Новость','цукцукцук','<p>цукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцк цукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцкцукцук цукцук цукцукцукцу куцк</p>','2019-10-14 00:00:00','2019-10-14 17:03:10',1,0),(12,1,166,167,'Как выбрать зубную пасту?','С юных лет все мы привыкли к тому, что чистка зубов — часть ежеутреннего ритуала по уходу за собой. ','<h3>Чистим и лечим</h3>\r\n<p>Среди самых частых проблем, с которыми сталкиваются современные люди, можно выделить:</p>\r\n<ul class=\"article__part2-list\">\r\n    <li>Чувствительность зубов;</li>\r\n    <li>Потемнение эмали, цветной налет;</li>\r\n    <li>Кариес;</li>\r\n    <li>Склонность к воспалениям десен.</li>\r\n</ul>\r\n<p>Каждая из этих проблем требует обязательного вмешательства стоматолога &mdash; профессиональная гигиена полости рта, обработка антибактериальными составами, покрытие зубов специальными средствами для укрепления эмали и, конечно, своевременное лечение кариеса зубов помогают избежать развития осложнений. Но в каждом из перечисленных случаев большую роль играет и выбор средств для домашнего ухода. Итак:</p>\r\n<p>Каждая из этих проблем требует обязательного вмешательства стоматолога &mdash; профессиональная гигиена полости рта, обработка антибактериальными составами, покрытие зубов специальными средствами для укрепления эмали и, конечно, своевременное лечение кариеса зубов помогают избежать развития осложнений. Но в каждом из перечисленных случаев большую роль играет и выбор средств для домашнего ухода. Итак:</p>\r\n<p>Каждая из этих проблем требует обязательного вмешательства стоматолога &mdash; профессиональная гигиена полости рта, обработка антибактериальными составами, покрытие зубов специальными средствами для укрепления эмали и, конечно, своевременное лечение кариеса зубов помогают избежать развития осложнений. Но в каждом из перечисленных случаев большую роль играет и выбор средств для домашнего ухода. Итак:Каждая из этих проблем требует обязательного вмешательства стоматолога &mdash; профессиональная гигиена полости рта, обработка антибактериальными составами, покрытие зубов специальными средствами для укрепления эмали и, конечно, своевременное лечение кариеса зубов помогают избежать развития осложнений. Но в каждом из перечисленных случаев большую роль играет и выбор средств для домашнего ухода. Итак:</p>','2019-10-15 00:00:00','2019-10-15 12:27:11',1,0);
/*!40000 ALTER TABLE `greeny_nl_news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_nl_sections`
--

DROP TABLE IF EXISTS `greeny_nl_sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_nl_sections` (
  `sectionID` int(11) NOT NULL AUTO_INCREMENT,
  `caption` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  PRIMARY KEY (`sectionID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_nl_sections`
--

LOCK TABLES `greeny_nl_sections` WRITE;
/*!40000 ALTER TABLE `greeny_nl_sections` DISABLE KEYS */;
INSERT INTO `greeny_nl_sections` VALUES (1,'новости','novosti');
/*!40000 ALTER TABLE `greeny_nl_sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_pageStructure`
--

DROP TABLE IF EXISTS `greeny_pageStructure`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_pageStructure` (
  `pageID` int(11) NOT NULL AUTO_INCREMENT,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `caption` varchar(255) NOT NULL,
  `pageTypeID` int(11) DEFAULT NULL,
  `contentID` int(11) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `keywords` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(255) NOT NULL,
  `external` varchar(255) DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  `isDeleted` tinyint(1) DEFAULT '0',
  `isSection` tinyint(1) NOT NULL DEFAULT '0',
  `isLogin` tinyint(1) NOT NULL DEFAULT '0',
  `isNoindex` tinyint(1) NOT NULL DEFAULT '0',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`pageID`),
  KEY `pageTypeID` (`pageTypeID`),
  CONSTRAINT `greeny_pageStructure_ibfk_1` FOREIGN KEY (`pageTypeID`) REFERENCES `greeny_pageTypes` (`pageTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_pageStructure`
--

LOCK TABLES `greeny_pageStructure` WRITE;
/*!40000 ALTER TABLE `greeny_pageStructure` DISABLE KEYS */;
INSERT INTO `greeny_pageStructure` VALUES (5,0,'Отзывы',1,5,'reviews',NULL,NULL,50,'reviews',NULL,1,0,0,0,0,'2019-10-07 08:00:13'),(23,0,'Контакты',5,1,'contacts','Контакты','contacts, Контакты, Реквизиты организации, Документы',40,'contacts',NULL,1,0,0,0,0,'2019-10-08 08:00:39'),(26,0,'Врачи',3,3,'doctors','Список всех докторов Клиники','doctors',70,'doctors',NULL,1,0,0,0,0,'2019-10-07 07:59:56'),(27,0,'Главная страница',6,1,'Mr.Dent','Главная страница сайта','index, Главная страница',100,'index',NULL,1,0,0,0,0,'2019-10-07 07:59:22'),(28,0,'Цены',7,1,NULL,'Цены на услуги',NULL,60,'price',NULL,1,0,0,0,0,'2019-10-07 08:00:06'),(29,0,'Услуги',4,2,'services','Страница услуг с разделами','services, Услуги',80,'services',NULL,1,0,1,0,0,'2019-10-15 09:35:11'),(30,0,'О Клинике',8,1,'about_company','Страница о клинике','about_company, Страница о клинике',90,'about_company',NULL,1,0,0,0,0,'2019-10-08 09:50:59'),(39,0,'Новостная лента',NULL,0,NULL,NULL,NULL,0,'newsline',NULL,1,0,0,0,0,'2019-10-15 09:33:34'),(40,29,'Консультация',NULL,0,'Консультация','Консультация','Консультация,  consultation',0,'consultation',NULL,1,0,0,0,0,'2019-10-15 09:36:53'),(41,29,'Гигиена',NULL,0,'Гигиена',NULL,'gigiena, Гигиена',0,'gigiena',NULL,1,0,0,0,0,'2019-10-15 09:37:49'),(42,29,'Имплантация',NULL,0,'Имплантация',NULL,'Имплантация,  implant',0,'implant',NULL,1,0,0,0,0,'2019-10-15 09:38:21'),(43,29,'Терапевтическая стоматология',NULL,0,NULL,NULL,'ortoped,  Терапевтическая стоматология',0,'ortoped',NULL,1,0,1,0,0,'2019-10-15 09:58:09'),(44,29,'Ортопедическая стоматология',NULL,0,'Ортопедическая стоматология',NULL,'Ортопедическая стоматология, prosthetic',0,'prosthetic',NULL,1,0,0,0,0,'2019-10-15 09:40:38'),(45,29,'Хирургическая стоматология',NULL,0,NULL,'hirurg, Хирургическая стоматология',NULL,0,'hirurg',NULL,1,0,0,0,0,'2019-10-15 09:41:06'),(46,29,'Ортодонтия',NULL,0,NULL,'ortodont, Ортодонтия',NULL,0,'ortodont',NULL,1,0,0,0,0,'2019-10-15 09:41:25'),(47,29,'Лечение дёсен',NULL,0,NULL,'Лечение дёсен, desny',NULL,0,'desny',NULL,1,0,0,0,0,'2019-10-15 09:41:47'),(48,29,'Диагностика',NULL,0,NULL,'Диагностика, diagnos',NULL,0,'diagnos',NULL,1,0,0,0,0,'2019-10-15 09:42:08'),(49,29,'Отбеливание',NULL,0,NULL,'white, Отбеливание',NULL,0,'white',NULL,1,0,0,0,0,'2019-10-15 09:42:34'),(50,43,'Лечение кариеса',11,1,'Лечение-кариеса','страница Лечение кариеса','Лечение кариеса, lechenie_caries',10,'lechenie_caries',NULL,1,0,0,0,0,'2019-10-15 10:12:21'),(51,43,'Реставрация зуба',11,2,'Реставрация-зуба',NULL,NULL,0,'tooth_restoration',NULL,1,0,0,0,0,'2019-10-15 10:55:23'),(53,0,'Обратная связь',NULL,0,NULL,NULL,NULL,0,'feedback',NULL,1,0,0,0,0,'2019-10-15 11:48:58'),(54,43,'Лечение пульпита',11,3,'Лечение-пульпита','pulpit','Лечение пульпита, pulpit',0,'pulpit',NULL,1,0,0,0,0,'2019-10-15 12:56:07'),(55,43,'Лечение периодонтита',11,4,'Лечение-периодонтита',NULL,'Лечение периодонтита, periodontit',0,'periodontit',NULL,1,0,0,0,0,'2019-10-15 12:58:34');
/*!40000 ALTER TABLE `greeny_pageStructure` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_pageTypes`
--

DROP TABLE IF EXISTS `greeny_pageTypes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_pageTypes` (
  `pageTypeID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  `handlerType` enum('module','table') NOT NULL DEFAULT 'module',
  `constantName` varchar(125) NOT NULL DEFAULT '',
  `noCreatePage` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Запрещает создание новых страниц данного типа из админки',
  PRIMARY KEY (`pageTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_pageTypes`
--

LOCK TABLES `greeny_pageTypes` WRITE;
/*!40000 ALTER TABLE `greeny_pageTypes` DISABLE KEYS */;
INSERT INTO `greeny_pageTypes` VALUES (1,'simplePages','Обычная страница','table','SIMPLE_PAGE_ID',0),(3,'doctor','Страница доктора','table','CATALOG_PAGE',0),(4,'services','Страница Услуг','table','SERVICES_PAGE',0),(5,'contacts','Страница Контактов','table','CONTACTS_PAGE',0),(6,'main','Главная страница','table','MAIN_PAGE',0),(7,'price','Страница Цены на услуги','table','PRICE_PAGE',0),(8,'about_company','Страница О Клинике','table','ABOUT_COMPANY_PAGE',0),(9,'novosti','Новость','table','NOVOSTI_PAGE',0),(10,'greeny_nl_news','Лента новостей','table','NEWSLINE_ID',0),(11,'lechenie_zubov','Страница Лечение зубов','table','LECHENIE_ZUBOV_PAGE',0);
/*!40000 ALTER TABLE `greeny_pageTypes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_pwdRecoveryTokens`
--

DROP TABLE IF EXISTS `greeny_pwdRecoveryTokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_pwdRecoveryTokens` (
  `userID` int(11) NOT NULL,
  `token` varchar(32) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_pwdRecoveryTokens`
--

LOCK TABLES `greeny_pwdRecoveryTokens` WRITE;
/*!40000 ALTER TABLE `greeny_pwdRecoveryTokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `greeny_pwdRecoveryTokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_routes`
--

DROP TABLE IF EXISTS `greeny_routes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_routes` (
  `routeID` int(11) NOT NULL AUTO_INCREMENT,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `pattern` varchar(64) NOT NULL,
  `controller` varchar(64) NOT NULL,
  `action` varchar(64) NOT NULL DEFAULT 'Index',
  `defaultController` varchar(64) DEFAULT NULL,
  `defaultAction` varchar(64) DEFAULT NULL,
  `allowedGroups` varchar(128) NOT NULL DEFAULT '0',
  PRIMARY KEY (`routeID`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_routes`
--

LOCK TABLES `greeny_routes` WRITE;
/*!40000 ALTER TABLE `greeny_routes` DISABLE KEYS */;
INSERT INTO `greeny_routes` VALUES (1,0,'*','0','1','DefaultController','Index','0'),(2,1,'admin','AdminController','Index',NULL,NULL,'-1'),(3,2,'upload','UploadAdminController','Index',NULL,NULL,'-1'),(4,2,'cropthumbnail','CropThumbnailAdminController','Index',NULL,NULL,'-1'),(5,2,'structure','StructureController','2',NULL,NULL,'-1'),(6,2,'anyeditor','AnyEditorController','2',NULL,NULL,'-1'),(7,2,'pwdrecovery','PasswordRecovery','2',NULL,NULL,'-1'),(8,2,'routing','RoutingController','Index',NULL,NULL,'-1'),(9,8,'edit','RoutingController','Edit',NULL,NULL,'-1'),(10,8,'add','RoutingController','Add',NULL,NULL,'-1'),(11,8,'delete','RoutingController','Delete',NULL,NULL,'-1'),(12,2,'*','ModuleLinkController','Index',NULL,NULL,'-1'),(13,1,'ajax','AjaxController','1',NULL,NULL,'0'),(14,1,'logout','FrontController','Logout',NULL,NULL,'0'),(15,1,'contacts','ContactsController','Index',NULL,NULL,'0'),(16,1,'getcaptcha','CaptchaGoogleController','GetCaptcha',NULL,NULL,'0'),(17,1,'sitemap','SitemapController','Index',NULL,NULL,'0'),(18,1,'*','SimplePageController','Index',NULL,'Index','0'),(19,1,'','IndexController','Index',NULL,'Index','0'),(20,2,'modules','ModulesController','2',NULL,NULL,'0'),(21,2,'profile','ProfileController','2',NULL,NULL,'0'),(22,2,'settings','SettingsController','Index',NULL,NULL,'0'),(23,2,'users','UsersController','Index',NULL,NULL,'0'),(24,2,'fieldsregistry','FieldsregistryController','Index',NULL,NULL,'0'),(25,2,'maintenance','MaintenanceController','Index',NULL,NULL,'0'),(26,2,'editor','EditorController','Index',NULL,NULL,'0'),(27,2,'custommenu','CustomMenuAdminController','Index',NULL,NULL,'0'),(28,1,'getimage','ImagesController','Index',NULL,NULL,'0'),(29,2,'cache','CacheControlController','2',NULL,NULL,'0'),(30,2,'reviews','ReviewAdminController','Index',NULL,NULL,'0'),(31,1,'/','IndexController','Index',NULL,NULL,'0'),(34,1,'services','ServicesController','Index',NULL,NULL,'0'),(37,1,'about_company','AboutController','Index',NULL,NULL,'0'),(38,1,'price','PriceController','Index',NULL,NULL,'0'),(39,1,'lechenie_zubov','LechenieZubovController','Index',NULL,NULL,'0'),(40,1,'doctors','DoctorsController','Index',NULL,NULL,'0'),(42,1,'contacts','ContactsController','Index',NULL,NULL,'0'),(58,1,'newsline','NewsLineControllerFront','Index',NULL,NULL,'0'),(59,2,'NewsLine','NewsLineControllerAdmin','Index',NULL,NULL,'0'),(60,34,'ortoped','ServicesController','Ortoped',NULL,NULL,'0'),(61,60,'*','ServicesController','Ortoped',NULL,NULL,'0'),(64,1,'feedback','FeedbackControllerFront','Index',NULL,NULL,'0'),(65,2,'Feedback','FeedbackControllerAdmin','Index',NULL,NULL,'0');
/*!40000 ALTER TABLE `greeny_routes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_settings`
--

DROP TABLE IF EXISTS `greeny_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_settings` (
  `settingID` int(11) NOT NULL AUTO_INCREMENT,
  `groupID` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  `isVisible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`settingID`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_settings`
--

LOCK TABLES `greeny_settings` WRITE;
/*!40000 ALTER TABLE `greeny_settings` DISABLE KEYS */;
INSERT INTO `greeny_settings` VALUES (1,4,'fromEmail','Почтовый адрес отправителя','popckovM5@yandex.ru',1),(2,4,'fromName','Имя отправителя','Енухин',1),(3,1,'siteName','Название сайта','Mr.Dent стоматология',1),(4,1,'version','Версия CMS','2.0',0),(5,2,'defaultTitle','Заголовок страницы по умолчанию (если не задан в параметрах страницы)','Стандартный заголовок',1),(6,2,'defaultDescription','Мета-описание страницы по умолчанию (если не задан в параметрах страницы)','Описание страницы',1),(7,2,'defaultKeywords','Ключевые слова страницы по умолчанию (если не задан в параметрах страницы)','Ключевые слова',1),(8,1,'LOGIN_FRONT_END','Авторизация на фронт-енде','0',1),(9,3,'maintenanceEnabled','Режим обслуживания ','0',1),(10,3,'maintenancePageText','Содержимое страницы обслуживания ','<h1>Сайт находится на техническом обслуживании</h1>',1),(11,1,'telephone','Телефон - будет отображаться в заголовке','8 920 225 3399',1),(12,1,'addressTown','Адрес','г. Воронеж,',1),(13,1,'officeTitle','Часы работы','Часы работы:  ',1),(14,1,'email','Email ','popckovM5@yandex.ru',1),(15,1,'addressStreet','Улица города','ул.Название, д.1, оф.2',1),(16,1,'officeHourse','Часы работы','с 8.00 до 20.00 (Пн.-Сб.) ',1),(19,2,'vkontakte','Контакт','https://vkontakte',1),(20,2,'facebook','Facebook','https://facebook',1),(21,2,'twitter','Twitter','https://twitter',1),(22,2,'YandexMap','Яндекс карта для главной','https://yandex.ua/map-widget/v1/?um=constructor%3Acba5c5b98d71d61f9a1f93de3f46fdfd168eefd035f476d9b3540980e95e312d&source=constructor',1),(24,2,'YandexMapContacts','Карта Yandex для контактов','https://yandex.ru/map-widget/v1/?um=constructor%3Acbafbb3a9e04ee4ab5a1f942c4e0635e6332e83edb6abf414a526a1acb9ea373&source=constructor',1);
/*!40000 ALTER TABLE `greeny_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `greeny_settingsGroups`
--

DROP TABLE IF EXISTS `greeny_settingsGroups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `greeny_settingsGroups` (
  `groupID` int(11) NOT NULL AUTO_INCREMENT,
  `parentID` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL,
  `desc` varchar(255) NOT NULL,
  PRIMARY KEY (`groupID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `greeny_settingsGroups`
--

LOCK TABLES `greeny_settingsGroups` WRITE;
/*!40000 ALTER TABLE `greeny_settingsGroups` DISABLE KEYS */;
INSERT INTO `greeny_settingsGroups` VALUES (1,0,'general','Общие настройки'),(2,0,'front','Настройки фронт-энда'),(3,0,'admin','Настройки админки'),(4,1,'mailer','Почтовая рассылка');
/*!40000 ALTER TABLE `greeny_settingsGroups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lechenieBlocks`
--

DROP TABLE IF EXISTS `lechenieBlocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lechenieBlocks` (
  `lechenieBlocksID` int(11) NOT NULL AUTO_INCREMENT,
  `lechenie_zubovID` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `priority` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`lechenieBlocksID`),
  KEY `FK_lechenieBlocks_lechenie_zubov` (`lechenie_zubovID`),
  CONSTRAINT `FK_lechenieBlocks_lechenie_zubov` FOREIGN KEY (`lechenie_zubovID`) REFERENCES `lechenie_zubov` (`lechenie_zubovID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lechenieBlocks`
--

LOCK TABLES `lechenieBlocks` WRITE;
/*!40000 ALTER TABLE `lechenieBlocks` DISABLE KEYS */;
INSERT INTO `lechenieBlocks` VALUES (1,1,'В процессе лечения кариеса','В процессе лечения кариеса, зуб фторируется для предотвращения повторного кариозного процесса, который может оказаться губительным для полости зуба и не даёт прогрессировать процессу до глубоких слоёв дентина и пульпы зуба, что является профилактикой осложнения кариеса в виде пульпитов и периодонтитов!',3),(2,1,'Лечение осложнённого кариеса','В процессе лечения кариеса, зуб фторируется для предотвращения повторного кариозного процесса, который может оказаться губительным для полости зуба и не даёт прогрессировать процессу до глубоких слоёв дентина и пульпы зуба, что является профилактикой осложнения кариеса в виде пульпитов и периодонтитов!',2),(3,1,'Осложнённые формы кариеса','В процессе лечения кариеса, зуб фторируется для предотвращения повторного кариозного процесса, который может оказаться губительным для полости зуба и не даёт прогрессировать процессу до глубоких слоёв дентина и пульпы зуба, что является профилактикой осложнения кариеса в виде пульпитов и периодонтитов!',4),(4,1,'1','23213123',1),(5,2,'Кардинально улучшите внешний эстетический','Кардинально улучшите внешний эстетический вид передних зубов, линию улыбки от клыка до клыка. Быстро и красиво.',1),(6,2,'Восстановите функциональность','Вы получаете возможность исправить сложные дефекты зубного ряда - патологию эмали, кривые зубы и скорректировать прикус без ортодонтии и коронок ( если есть потребность).',2),(7,2,'Исправите как верхние так и нижние зубы.','Индивидуальную, красивую форму, размер и цвет зубов. Никто не заметит что зубы сделаны - это и есть качество исполнения! Эксклюзивность\r\n\r\nработы.',3),(8,2,' Вы преобразитесь внешне,','Вы преобразитесь внешне, получите эмоциональный и психологический комфорт - ПРОВЕРЕНО!',4),(9,2,'Комплексно и полностью решить свой ',' И все это прочно, долгосрочно и из лучшего реставрационного  нанокомпозита - это не пломбы и реставрация в обывательском понимании - это творческая, сложная, кропотливая беспрерывная работа, это ВИНИРЫ! МЫ ЗНАЕМ, КАК ЭТО СДЕЛАТЬ!',5);
/*!40000 ALTER TABLE `lechenieBlocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lechenie_reason`
--

DROP TABLE IF EXISTS `lechenie_reason`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lechenie_reason` (
  `lechenie_reasonID` int(11) NOT NULL AUTO_INCREMENT,
  `lechenie_zubovID` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `priority` smallint(4) DEFAULT NULL,
  `isActive` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`lechenie_reasonID`),
  KEY `FK_lechenie_reason_lechenie_zubov` (`lechenie_zubovID`),
  CONSTRAINT `FK_lechenie_reason_lechenie_zubov` FOREIGN KEY (`lechenie_zubovID`) REFERENCES `lechenie_zubov` (`lechenie_zubovID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lechenie_reason`
--

LOCK TABLES `lechenie_reason` WRITE;
/*!40000 ALTER TABLE `lechenie_reason` DISABLE KEYS */;
INSERT INTO `lechenie_reason` VALUES (1,1,'Наследственная предрасположенность;',1,1),(2,1,'Отсутствие ежедневной гигиены;',2,1),(3,1,'Избыточное потребление сахарозы;',3,1),(4,1,'Деминерализация зуба;',4,1),(5,1,'Сниженный иммунитет;',5,1),(6,1,'Низкое содержание фтора в воде;',6,1),(7,1,'Образование кармана зуба;',7,1),(8,1,'Повышенные физические нагрузки;',8,1),(9,1,'Период беременности;',9,1),(10,1,'Аномалии прикуса, так как неправильное расположение зубов затрудняет проведение гигиенических мероприятий',10,1),(12,2,'1',1,1),(13,2,'2',2,1),(14,2,'3',3,1),(15,2,'4',4,1),(16,2,'5',5,1),(17,2,'6',6,1);
/*!40000 ALTER TABLE `lechenie_reason` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lechenie_slider`
--

DROP TABLE IF EXISTS `lechenie_slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lechenie_slider` (
  `lechenie_sliderID` int(11) NOT NULL AUTO_INCREMENT,
  `lechenie_zubovID` int(11) NOT NULL,
  `imageID` int(11) NOT NULL,
  `priority` int(11) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`lechenie_sliderID`),
  KEY `FK_lechenie_slider_lechenie_zubov` (`lechenie_zubovID`),
  CONSTRAINT `FK_lechenie_slider_lechenie_zubov` FOREIGN KEY (`lechenie_zubovID`) REFERENCES `lechenie_zubov` (`lechenie_zubovID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lechenie_slider`
--

LOCK TABLES `lechenie_slider` WRITE;
/*!40000 ALTER TABLE `lechenie_slider` DISABLE KEYS */;
INSERT INTO `lechenie_slider` VALUES (3,1,187,0,NULL,NULL),(5,1,189,0,NULL,NULL),(7,1,219,0,NULL,NULL);
/*!40000 ALTER TABLE `lechenie_slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lechenie_zubov`
--

DROP TABLE IF EXISTS `lechenie_zubov`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lechenie_zubov` (
  `lechenie_zubovID` int(11) NOT NULL AUTO_INCREMENT,
  `titleTop` varchar(255) DEFAULT NULL,
  `imageTop` smallint(4) DEFAULT NULL,
  `titleSlider` varchar(255) NOT NULL,
  `textSlider` text,
  `titleReason` varchar(255) NOT NULL,
  `textReason` text,
  `titleStep` varchar(255) NOT NULL,
  `textStep` text,
  `titleForm` varchar(255) DEFAULT NULL,
  `textForm` text,
  `titleBottom` varchar(255) DEFAULT NULL,
  `imageBottom` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`lechenie_zubovID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lechenie_zubov`
--

LOCK TABLES `lechenie_zubov` WRITE;
/*!40000 ALTER TABLE `lechenie_zubov` DISABLE KEYS */;
INSERT INTO `lechenie_zubov` VALUES (1,'<div class=\"up__text1\">В клинике Mr.Dent найдут</div>\r\n<p>&nbsp;</p>\r\n<div class=\"up__text2\">Быстрое и эффективное решение проблем с Лечение кариеса</div>',184,'Лечение кариеса зубов','<p>С такой проблемой, как кариес зубов, сталкивался хоть раз практически каждый человек в своей жизни.                     <br />\r\n<br />\r\nЗаболевание представляет собой деструктивный  процесс, который вызывает постепенное разрушение тканей зуба с  образованием кариозной полости.                     <br />\r\n<br />\r\nНекоторые люди стараются провести лечение кариеса на  начальных стадиях. Другие &ndash; запускают патологический процесс, что  вызывает развитие опасных осложнений.</p>','Причины возникновения кариеса','<p>Кариес относят к полиэтиологическим заболеваниям, поэтому спровоцировать его развитие могут следующие факторы:</p>','Степени кариеса','В стоматологии выделяют следующие стадии развития заболевания','Если сразу не обратиться к врачу?','<p>Дальнейшее разрушение зуба приведет к пульпиту, тогда вас уже &quot;заставит&quot; обратиться к стоматологу нестерпимая зубная боль. При этом стоимость лечения будет выше, поскольку придется обрабатывать и пломбировать каналы зуба.</p>',NULL,176),(2,'<div class=\"up__text1\">В клинике Mr.Dent найдут</div>\r\n<p>&nbsp;</p>\r\n<div class=\"up__text2\">Быстрое и эффективное решение проблем с зубами любой сложности</div>\r\n<p>&nbsp;</p>',177,'Реставрация зуба','<p>С такой проблемой, Реставрация зуба, сталкивался хоть раз практически каждый человек в своей жизни.                     <br />\r\n<br />\r\nЗаболевание  представляет собой деструктивный  процесс, который вызывает постепенное  разрушение тканей зуба с  образованием кариозной полости.                      <br />\r\n<br />\r\nНекоторые люди стараются провести лечение кариеса на   начальных стадиях. Другие &ndash; запускают патологический процесс, что   вызывает развитие опасных осложнений.</p>','Причины реставрация зуба','<p>Причины Реставрация зуба может быть кариес относят к полиэтиологическим заболеваниям, поэтому спровоцировать его развитие могут следующие факторы:</p>','Степени реставрация зуба','<p>Степени реставрация зубаСтепени реставрация зуба Степени реставрация зуба Степени реставрация зуба Степени реставрация зуба</p>',NULL,NULL,NULL,178),(3,'<div class=\"up__text1\">В клинике Mr.Dent найдут</div>\r\n<p>&nbsp;</p>\r\n<div class=\"up__text2\">Быстрое и эффективное решение проблем с Лечение пульпита</div>',179,'Лечение пульпита','<p>Лечение пульпита С такой проблемой, Лечение пульпита, сталкивался хоть раз практически каждый человек в своей жизни.                     <br />\r\n<br />\r\nЗаболевание   представляет собой деструктивный  процесс, который вызывает  постепенное  разрушение тканей зуба с  образованием кариозной полости.                       <br />\r\n<br />\r\nНекоторые люди стараются провести лечение  кариеса на   начальных стадиях. Другие &ndash; запускают патологический  процесс, что   вызывает развитие опасных осложнений.</p>','Причины Лечение пульпита','<p>Причины Лечение пульпита может быть кариес относят к полиэтиологическим  заболеваниям, поэтому спровоцировать его развитие могут следующие  факторы:</p>','Степени Лечение пульпита','Степени Лечение пульпита - 1\r\nСтепени Лечение пульпита - 2\r\nСтепени Лечение пульпита - 3',NULL,NULL,NULL,180),(4,'<div class=\"up__text1\">Лечение периодонтита</div>\r\n<p>&nbsp;</p>\r\n<div class=\"up__text2\">Быстрое и эффективное решение проблем с зубами любой сложности</div>\r\n<p>&nbsp;</p>',181,'Лечение периодонтита','<p>С такой проблемой, Лечение периодонтита, сталкивался хоть раз практически каждый человек в своей жизни.                     <br />\r\n<br />\r\nЗаболевание   представляет собой деструктивный  процесс, который вызывает  постепенное  разрушение тканей зуба с  образованием кариозной полости.                       <br />\r\n<br />\r\nНекоторые люди стараются провести лечение  кариеса на   начальных стадиях. Другие &ndash; запускают патологический  процесс, что   вызывает развитие опасных осложнений.</p>','Причины Лечение периодонтита','<p>Причины Реставрация зуба может быть кариес относят к  полиэтиологическим заболеваниям, поэтому спровоцировать его развитие  могут следующие факторы:</p>','Степени Лечение периодонтита','Степени Лечение периодонтита- 1\r\nСтепени Лечение периодонтита - 2\r\nСтепени Лечение периодонтита - 3',NULL,NULL,NULL,183);
/*!40000 ALTER TABLE `lechenie_zubov` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `main`
--

DROP TABLE IF EXISTS `main`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `main` (
  `mainID` int(11) NOT NULL AUTO_INCREMENT,
  `titleAboutClinic` varchar(255) DEFAULT NULL,
  `textAboutClinic` text,
  `videoMask` smallint(4) DEFAULT NULL,
  `imageID` smallint(4) DEFAULT NULL,
  `linkVideo` varchar(255) DEFAULT NULL,
  `promotionTitle` text,
  `promotionText` text,
  `imageBottom` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`mainID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `main`
--

LOCK TABLES `main` WRITE;
/*!40000 ALTER TABLE `main` DISABLE KEYS */;
INSERT INTO `main` VALUES (1,'О клинике Mr.Dent','<p class=\"aboutblock__text\">Все сотрудники &laquo;Клиники&raquo;- профессионалы и единомышленники.</p>\r\n<p class=\"aboutblock__text\">Благоприятная и доброжелательная обстановка в  процессе лечения, желание услышать и понять проблему пациента &ndash; стоят во  главе угла.</p>\r\n<p class=\"aboutblock__text\">Это помогает легко определить план лечения, который  устроит и доктора и клиента. На первом месте в стоматологии &mdash;  внимательное отношение к пациентам, забота о них и честность.</p>',128,147,'https://www.youtube.com/embed/OXuIqaeg0SI','Текст для продвижения','<p>Представляем вам полный спектр стоматологических услуг, который  оказывает для вас Центр стоматологии &laquo;Mr.Dent&raquo;. Узнайте подробную  информацию об интересующих стоматологических услугах</p>',56);
/*!40000 ALTER TABLE `main` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news_blocks`
--

DROP TABLE IF EXISTS `news_blocks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_blocks` (
  `news_blocksID` int(11) NOT NULL AUTO_INCREMENT,
  `newsID` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `text` text NOT NULL,
  `priority` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`news_blocksID`),
  KEY `FK_news_blocks_greeny_nl_news` (`newsID`),
  CONSTRAINT `FK_news_blocks_greeny_nl_news` FOREIGN KEY (`newsID`) REFERENCES `greeny_nl_news` (`newsID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_blocks`
--

LOCK TABLES `news_blocks` WRITE;
/*!40000 ALTER TABLE `news_blocks` DISABLE KEYS */;
INSERT INTO `news_blocks` VALUES (5,6,'Если зубы болезненно реагируют на горячую или холодную пищу, их «ломит», то их тип — чувствительные. ','Если верхний плотный слой эмали изнашивается, то обнажается уязвимый внутренний слой ткани, дентин (отсюда и неприятные ощущения). Посетите стоматолога: он проведет специальные манипуляции, закрывающие «бреши» в эмали, например, обработает участок зуба фтором или даже поставит пломбу. Вам подойдет специальная паста для чувствительных зубов, она укрепит уязвимую эмаль благодаря содержанию солей калия и стронция.',1),(6,6,'В вашей ротовой полости развилось воспаление, на деснах есть ранки, стоматитные афты? Вам подойдет антибактериальная паста. ','Лучше всего себя показали щадящие антисептические средства: они не только отлично справляются с патогенными микробами, но и помогают снять раздражение и воспаление десен. В состав часто входят экстракты лекарственных растений: шалфея, календулы, тысячелистника, ромашки.',2),(7,6,'Бывает, что у человека крепкая эмаль, но темная. В этом случае подойдет отбеливающая паста. ','Пользоваться ей можно 1–2 раза в неделю (чаще нельзя, пострадает эмаль). Наиболее сильные составы содержат пероксид водорода или пероксид карбамида, но применять эти средства можно только тем, у кого здоровые зубы.',3),(8,11,'123','вап',2),(9,11,'1234','вапвапавпавп',1),(10,7,'1','2',1),(12,5,'1','2',1),(13,12,'Если зубы болезненно реагируют на горячую или холодную пищу, их «ломит», то их тип — чувствительные. ','Если верхний плотный слой эмали изнашивается, то обнажается уязвимый внутренний слой ткани, дентин (отсюда и неприятные ощущения). Посетите стоматолога: он проведет специальные манипуляции, закрывающие «бреши» в эмали, например, обработает участок зуба фтором или даже поставит пломбу. Вам подойдет специальная паста для чувствительных зубов, она укрепит уязвимую эмаль благодаря содержанию солей калия и стронция.',4),(14,12,'В вашей ротовой полости развилось воспаление, на деснах есть ранки, стоматитные афты? Вам подойдет антибактериальная паста. ','Лучше всего себя показали щадящие антисептические средства: они не только отлично справляются с патогенными микробами, но и помогают снять раздражение и воспаление десен. В состав часто входят экстракты лекарственных растений: шалфея, календулы, тысячелистника, ромашки.',3),(15,12,'Бывает, что у человека крепкая эмаль, но темная. В этом случае подойдет отбеливающая паста. ','Пользоваться ей можно 1–2 раза в неделю (чаще нельзя, пострадает эмаль). Наиболее сильные составы содержат пероксид водорода или пероксид карбамида, но применять эти средства можно только тем, у кого здоровые зубы.',2),(16,12,'Пасты, относящиеся к типу противокариозных, содержат в составе в основном производные фтора и кальция. ','Фтор используют, чтобы «заживлять» зубную эмаль (он способен закрывать микротрещинки, препятствуя развитию кариозного процесса). Содержание этого вещества не должно быть слишком высоким: не больше 150 мг на 100 г пасты для взрослых и 50 мг на 100 г детской. Некоторым людям фтор противопоказан — проконсультируйтесь с врачом!',1);
/*!40000 ALTER TABLE `news_blocks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal`
--

DROP TABLE IF EXISTS `personal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal` (
  `personalID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `profession` varchar(255) NOT NULL DEFAULT '0',
  `experience` smallint(4) NOT NULL DEFAULT '0',
  `certificate` smallint(4) NOT NULL DEFAULT '0',
  `reviews` smallint(4) NOT NULL,
  `position` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `imageAva` smallint(4) DEFAULT NULL,
  `imageFull` smallint(4) DEFAULT NULL,
  `isActive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`personalID`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal`
--

LOCK TABLES `personal` WRITE;
/*!40000 ALTER TABLE `personal` DISABLE KEYS */;
INSERT INTO `personal` VALUES (5,'Светланова Ирина 111','Гигиенист',16,6,4,'Терапевт-стоматолог','высшая','<p>Окончила Воронежский государственный медицинский университет,  квалификация: &laquo;врач-стоматолог&raquo; по специальности &laquo;стоматология&raquo;.  Сертифицированный доктор в терапевтической стоматологии.<br />\r\n<br />\r\nСпециалист в области эндодонтического лечения.  Профессионал в терапии и пародонтологии. Постоянно приобретает новые  умения и навыки в работе.<br />\r\n<br />\r\nВнимание каждому пациенту и создание максимально  комфортных условий лечения &ndash; вот основные принципы работы доктора.</p>',33,34,1),(6,'Светланова Ирина 333','стоматолог',5,5,5,'врач','высшая','Окончила Воронежский государственный медицинский университет, квалификация: «врач-стоматолог» по специальности «стоматология». Сертифицированный доктор в терапевтической стоматологии.\r\n\r\nВнимание каждому пациенту и создание максимально комфортных условий лечения – вот основные принципы работы доктора.\r\n\r\nСпециалист в области эндодонтического лечения. Профессионал в терапии и пародонтологии. Постоянно приобретает новые умения и навыки в работе.',35,38,1),(7,'Светланова Ирина 222','гигиенист',10,10,10,'врач','высшая','Окончила Воронежский государственный медицинский университет, квалификация: «врач-стоматолог» по специальности «стоматология». Сертифицированный доктор в терапевтической стоматологии.\r\nОкончила Воронежский государственный медицинский университет, квалификация: «врач-стоматолог» по специальности «стоматология». Сертифицированный доктор в терапевтической стоматологии.\r\nОкончила Воронежский государственный медицинский университет, квалификация: «врач-стоматолог» по специальности «стоматология». Сертифицированный доктор в терапевтической стоматологии.\r\nОкончила Воронежский государственный медицинский университет, квалификация: «врач-стоматолог» по специальности «стоматология». Сертифицированный доктор в терапевтической стоматологии.\r\n',39,40,1),(8,'Иванов Иван Иванович','стоматолог',10,10,10,'Главный врач клиники','высшая','<br>\r\n<p>Окончила Воронежский государственный медицинский университет,  квалификация: &laquo;врач-стоматолог&raquo; по специальности &laquo;стоматология&raquo;.  Сертифицированный доктор в терапевтической стоматологии.<br />\r\n<br />\r\nСпециалист в области эндодонтического лечения.  Профессионал в терапии и пародонтологии. Постоянно приобретает новые  умения и навыки в работе.<br />\r\n<br />\r\nВнимание каждому пациенту и создание максимально  комфортных условий лечения &ndash; вот основные принципы работы доктора.</p>',72,73,1);
/*!40000 ALTER TABLE `personal` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price`
--

DROP TABLE IF EXISTS `price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price` (
  `priceID` int(11) NOT NULL AUTO_INCREMENT,
  `titleTop` varchar(255) DEFAULT NULL,
  `imageTop` smallint(4) DEFAULT NULL,
  `titleBottom` varchar(255) DEFAULT NULL,
  `imageBottom` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`priceID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price`
--

LOCK TABLES `price` WRITE;
/*!40000 ALTER TABLE `price` DISABLE KEYS */;
INSERT INTO `price` VALUES (1,'<p>Стоимость услуг !</p>',120,NULL,58);
/*!40000 ALTER TABLE `price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prices`
--

DROP TABLE IF EXISTS `prices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prices` (
  `pricesID` int(11) NOT NULL AUTO_INCREMENT,
  `pageID` smallint(4) DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `cost` int(11) DEFAULT NULL,
  `isApproved` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pricesID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prices`
--

LOCK TABLES `prices` WRITE;
/*!40000 ALTER TABLE `prices` DISABLE KEYS */;
INSERT INTO `prices` VALUES (5,0,'Лечение кариеса','Анестезия, изоляция, формирование полости, постановка пломбы из композитных материалов, шлифовка и полировка, осмотры',7000,1),(6,0,'Реставрация зуба','Анестезия, изоляция, формирование полости, постановка пломбы из композитных материалов, шлифовка и полировка, осмотры',4168,1),(7,0,'Лечение<br> периодонтита','Анестезия, изоляция, формирование полости, постановка пломбы из композитных материалов, шлифовка и полировка, осмотры',7000,1),(8,0,'Лечение <br>периодонтита','Анестезия, изоляция, формирование полости, постановка пломбы из композитных материалов, шлифовка и полировка, осмотры',7777,1),(9,0,'Лечение кариеса-2','Анестезия, изоляция, формирование полости, постановка пломбы из композитных материалов, шлифовка и полировка, осмотры',6666,1),(10,0,'Лечение кариеса-3','Анестезия, изоляция, формирование полости, постановка пломбы из композитных материалов, шлифовка и полировка, осмотры',8888,1),(11,0,'Лечение периодонтита-2','Анестезия, изоляция, формирование полости, постановка пломбы из композитных материалов, шлифовка и полировка, осмотры',4569,1);
/*!40000 ALTER TABLE `prices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `reviewID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) NOT NULL,
  `email` varchar(64) NOT NULL,
  `review` text NOT NULL,
  `imageID` int(11) DEFAULT NULL,
  `dateAdd` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `userIp` varchar(16) DEFAULT NULL,
  `isApproved` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`reviewID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (1,'Елена Борисовна 111','E-mail','Все сотрудники «Клиники»- профессионалы и единомышленники. Благоприятная и доброжелательная обстановка в процессе лечения, желание услышать и понять проблему пациента – стоят во главе угла.',9,'2019-09-17 21:00:00',NULL,1),(2,'Елена Борисовна 222','E-mail','Все сотрудники «Клиники»- профессионалы и единомышленники. Благоприятная и доброжелательная обстановка в процессе лечения, желание услышать и понять проблему пациента – стоят во главе угла.',10,'2019-09-16 21:00:00',NULL,1),(3,'Елена Борисовна 333','E-mail','Все сотрудники «Клиники»- профессионалы и единомышленники. Благоприятная и доброжелательная обстановка в процессе лечения, желание услышать и понять проблему пациента – стоят во главе угла. ',11,'2019-09-15 21:00:00',NULL,1),(4,'Елена Борисовна 444','E-mail','Все сотрудники «Клиники»- профессионалы и единомышленники. Благоприятная и доброжелательная обстановка в процессе лечения, желание услышать и понять проблему пациента – стоят во главе угла. ',12,'2019-09-17 21:00:00',NULL,1);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `servicesID` int(11) NOT NULL AUTO_INCREMENT,
  `titleTop` varchar(255) DEFAULT NULL,
  `imageTop` smallint(4) DEFAULT NULL,
  `servicesTitle` varchar(255) DEFAULT NULL,
  `servicesText` text,
  `promotionTitle` varchar(255) DEFAULT NULL,
  `promotionText` text,
  `titleBottom` varchar(255) DEFAULT NULL,
  `imageBottom` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`servicesID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (2,'<div class=\"consult__text1\">Наш врач поможет Вам выявить проблему.</div>\r\n<div class=\"consult__text2\">Запишитесь на консультацию</div>\r\n<div class=\"consult__text3\">Это бесплатно!</div>',123,'Услуги клиники ','<p>Представляем вам полный спектр стоматологических услуг, который  оказывает для вас Центр стоматологии &laquo;Mr.Dent&raquo;. Узнайте подробную  информацию об интересующих стоматологических услугах</p>','Текст для продвижения ','<p>Представляем вам полный спектр стоматологических услуг, который  оказывает для вас Центр стоматологии &laquo;Mr.Dent&raquo;. Узнайте подробную  информацию об интересующих стоматологических услугах </p>',NULL,64);
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `simplePages`
--

DROP TABLE IF EXISTS `simplePages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `simplePages` (
  `simplePageID` int(11) NOT NULL AUTO_INCREMENT,
  `pageContent` text NOT NULL,
  PRIMARY KEY (`simplePageID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `simplePages`
--

LOCK TABLES `simplePages` WRITE;
/*!40000 ALTER TABLE `simplePages` DISABLE KEYS */;
INSERT INTO `simplePages` VALUES (5,'<p style=\"text-align: center;\">Отзывы</p>\r\n<p style=\"text-align: center;\">ыОтзывыОтзывыОтзывыОтзывыОтзывыОтзывыОтзывыОтзывы</p>');
/*!40000 ALTER TABLE `simplePages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider`
--

DROP TABLE IF EXISTS `slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider` (
  `sliderID` int(11) NOT NULL AUTO_INCREMENT,
  `titleOne` varchar(255) NOT NULL,
  `titleTwo` varchar(255) NOT NULL,
  `buttonLink` varchar(255) NOT NULL,
  `buttonTitle` varchar(50) NOT NULL,
  `isActive` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`sliderID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider`
--

LOCK TABLES `slider` WRITE;
/*!40000 ALTER TABLE `slider` DISABLE KEYS */;
INSERT INTO `slider` VALUES (1,'Бесплатная консультация ','и план лечения','#','Записаться ',1),(2,'Далеко-далеко	 ','за высокими горами','#','Звонок ',1);
/*!40000 ALTER TABLE `slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliderAbout`
--

DROP TABLE IF EXISTS `sliderAbout`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliderAbout` (
  `sliderAboutID` int(11) NOT NULL AUTO_INCREMENT,
  `about_companyID` int(11) NOT NULL,
  `imageID` int(11) NOT NULL,
  `priority` int(11) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`sliderAboutID`),
  KEY `FK_sliderAbout_about_company` (`about_companyID`),
  CONSTRAINT `FK_sliderAbout_company` FOREIGN KEY (`about_companyID`) REFERENCES `about_company` (`about_companyID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliderAbout`
--

LOCK TABLES `sliderAbout` WRITE;
/*!40000 ALTER TABLE `sliderAbout` DISABLE KEYS */;
INSERT INTO `sliderAbout` VALUES (6,1,98,1,NULL,NULL),(9,1,101,4,NULL,NULL),(10,1,126,0,NULL,NULL),(11,1,127,0,NULL,NULL);
/*!40000 ALTER TABLE `sliderAbout` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliders` (
  `slidersID` int(11) NOT NULL AUTO_INCREMENT,
  `pageID` int(11) DEFAULT '0',
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `isActive` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`slidersID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliders`
--

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` VALUES (1,50,'Слайдер для страницы Лечение зубов от кариеса','<p>текст</p>',1),(2,51,'Слайдер для реставрация зуба',NULL,1),(3,54,'Слайдер для Лечение пульпита',NULL,1),(4,55,'Слайдер для Лечение периодонтита',NULL,1),(5,27,'Слайдер для Главной страницы',NULL,1);
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slidersImages`
--

DROP TABLE IF EXISTS `slidersImages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slidersImages` (
  `slidersImagesID` int(11) NOT NULL AUTO_INCREMENT,
  `slidersID` int(11) NOT NULL,
  `imageID` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `buttonTitle` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`slidersImagesID`),
  KEY `FK__slidersImages_sliders` (`slidersID`),
  CONSTRAINT `FK_slidersImages_sliders` FOREIGN KEY (`slidersID`) REFERENCES `sliders` (`slidersID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slidersImages`
--

LOCK TABLES `slidersImages` WRITE;
/*!40000 ALTER TABLE `slidersImages` DISABLE KEYS */;
INSERT INTO `slidersImages` VALUES (3,1,193,NULL,NULL,NULL,NULL,0),(8,1,198,NULL,NULL,NULL,NULL,0),(9,2,232,'',NULL,NULL,NULL,0),(10,2,233,'',NULL,NULL,NULL,0),(11,2,234,'',NULL,NULL,NULL,0),(14,3,243,'',NULL,NULL,NULL,0),(15,3,244,'',NULL,NULL,NULL,0),(16,4,245,'',NULL,NULL,NULL,0),(17,4,246,'',NULL,NULL,NULL,0),(18,4,247,'',NULL,NULL,NULL,0),(21,5,250,NULL,'Slider 1  <br>Lorem ipsum dolor sit amet,<br> consectetur adipisicing elit.','Записаться','#Записаться',0),(22,5,251,NULL,'Slider 2 <br> Lorem ipsum dolor sit amet,<br> consectetur adipisicing elit.','Консультация ','#Консультация ',0),(23,5,252,NULL,'Slider 3 <br> Lorem ipsum dolor sit amet,<br> consectetur adipisicing elit.','Звонок','#Звонок',0);
/*!40000 ALTER TABLE `slidersImages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statistic`
--

DROP TABLE IF EXISTS `statistic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statistic` (
  `statisticID` int(11) NOT NULL AUTO_INCREMENT,
  `statisticNumber` varchar(100) NOT NULL,
  `statisticTitle` varchar(100) NOT NULL,
  `imageID` smallint(4) DEFAULT NULL,
  PRIMARY KEY (`statisticID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statistic`
--

LOCK TABLES `statistic` WRITE;
/*!40000 ALTER TABLE `statistic` DISABLE KEYS */;
INSERT INTO `statistic` VALUES (9,'5','Лет успешной <br />\r\nработы',77),(10,'2,302','Довольных <br />\r\nклиентов',78),(11,'5,613','Установленных <br />\r\nимплантов</p>',79),(12,'12','Международных <br />\r\nсертификатов',80);
/*!40000 ALTER TABLE `statistic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tab_sliders`
--

DROP TABLE IF EXISTS `tab_sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tab_sliders` (
  `tab_slidersID` int(11) NOT NULL AUTO_INCREMENT,
  `tabsID` int(11) NOT NULL,
  `imageID` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tab_slidersID`),
  KEY `FK__tab_sliders_tabs` (`tabsID`),
  CONSTRAINT `FK_tab_sliders_tabs` FOREIGN KEY (`tabsID`) REFERENCES `tabs` (`tabsID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tab_sliders`
--

LOCK TABLES `tab_sliders` WRITE;
/*!40000 ALTER TABLE `tab_sliders` DISABLE KEYS */;
INSERT INTO `tab_sliders` VALUES (8,3,206,NULL,NULL,0),(9,3,207,NULL,NULL,0),(10,3,208,NULL,NULL,0),(15,1,223,NULL,NULL,0),(16,1,224,NULL,NULL,0),(17,1,225,NULL,NULL,0),(18,2,226,NULL,NULL,0),(19,2,227,NULL,NULL,0),(20,2,228,NULL,NULL,0),(21,4,229,NULL,NULL,0),(22,4,230,NULL,NULL,0),(23,4,231,NULL,NULL,0),(24,5,235,NULL,NULL,0),(25,5,236,NULL,NULL,0),(26,5,237,NULL,NULL,0),(27,6,238,NULL,NULL,0),(28,6,239,NULL,NULL,0),(29,6,240,NULL,NULL,0);
/*!40000 ALTER TABLE `tab_sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tabs`
--

DROP TABLE IF EXISTS `tabs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tabs` (
  `tabsID` int(11) NOT NULL AUTO_INCREMENT,
  `pageID` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` text,
  `isActive` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`tabsID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tabs`
--

LOCK TABLES `tabs` WRITE;
/*!40000 ALTER TABLE `tabs` DISABLE KEYS */;
INSERT INTO `tabs` VALUES (1,50,'Темное пятно','<p>Кариес в стадии белого или темного пятна. Это начальный этап патологии, во время которого отсутствует анатомическое разрушение эмали. Однако на поверхности зуба образуется пятнышко. Лечение кариеса в стадии пятна предполагает использование методик, основанных на минимальном вмешательстве в структуру зуба.</p>',1),(2,50,'Поверхностный кариес','<p class=\"tab__block-text\">Lorem ipsum dolor sit amet, consectetur  adipisicing elit. Quisquam deleniti illo recusandae, nostrum harum  molestiae eligendi ab, atque quasi similique dolorem architecto ea. Eos  repellendus reprehenderit dicta? Voluptatum, quisquam, suscipit.</p>',1),(3,50,'Средний кариес','<p>Кариес в стадии белого или темного пятна. Это начальный этап патологии,  во время которого отсутствует анатомическое разрушение эмали. Однако на  поверхности зуба образуется пятнышко. Лечение кариеса в стадии пятна  предполагает использование методик, основанных на минимальном  вмешательстве в структуру зуба.</p>',1),(4,50,'Глубокий кариес','<p>Это начальный этап патологии,  во время которого отсутствует анатомическое разрушение эмали. Однако на  поверхности зуба образуется пятнышко. Лечение кариеса в стадии пятна  предполагает использование методик, основанных на минимальном  вмешательстве в структуру зуба.</p>',1),(5,51,'Реставрация зуба 1','<p>Реставрация зуба Реставрация зуба Реставрация зуба Реставрация зуба</p>',1),(6,51,'Реставрация зуба 2','<p>Реставрация зуба Реставрация зуба Реставрация зуба</p>',1);
/*!40000 ALTER TABLE `tabs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-10-21 11:11:48
