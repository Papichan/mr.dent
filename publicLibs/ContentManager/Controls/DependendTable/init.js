function DependendTableInit(initObject)
{
	var name = initObject.name;
	var dependendTable = $("." + name + " table");
	var initDependendTableRow = $("." + name + " table .default").clone().removeAttr("class");
	$("." + name + " table .default").remove();

	$("#" + name + "_add").live('click', function() {
//		var defaultRow = $("." + name + " table .default");
//		defaultRow.clone().removeAttr("class").appendTo(defaultRow.parent()).show();
//		console.log(initObject);
		initDependendTableRow.clone().appendTo(dependendTable).show();
		return false;
	});
    $(".up").bind("click", trup);
    $(".down").bind("click", trdown);
}

function trup(){
    var cur = $(this).parents('tr');
    var prev = cur.prev();

    prev.before($(cur));
    return false;
}

function trdown(){
    var cur = $(this).parents('tr');
    var next = cur.next();

    next.after($(cur));
    return false;
}