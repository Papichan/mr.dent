/**
*  Инициализатор контрола
*/
function FormConstructorInit(initObject)
{
    $('.controlsSelect').change(function(){
        var select = $(this);
        
        // Очищаем раздел настроек
        var settings = $(this).parent().siblings('.fc_settings');
        settings.html('');
        
        // Получаем выбранный контрол
        var controlName = select.val();
        
        var foundControl = null;
        
        for (var i = 0; i < initObject.controls.length; i++)
        {
            var control = initObject.controls[i];
            if (control.name == controlName)
            {
                foundControl = control;
                break;
            }
        }

        if (foundControl != null)
        {
            if (typeof(foundControl.settings) != 'undefined' && foundControl.settings.length > 0)
            {
                for (var i = 0; i < foundControl.settings.length; i++)
                {
                    setting = foundControl.settings[i];
                    settings.append('<div>' + setting.description + ': <input type="text" name="' + controlName + 'Settings['+ setting.name +']" value="'+ setting.defaultValue +'" size="20" /></div>')
                }
            }
        }
    });
}