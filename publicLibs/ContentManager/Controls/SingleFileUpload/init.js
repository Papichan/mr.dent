/**
*  Инициализатор контрола SingleFileUpload
*/
function SingleFileUploadInit(initObject)
{
    var controlName = initObject.name;
    var swfu;
    /* SWFUpload */

    var settings = {
		flash_url : initObject.libsPath + "SWFUpload/swfupload.swf",
		upload_url: "/uploadTextFile.php",
		file_size_limit : "100 MB",
		file_types : "*.doc; *.txt; *.rtf",
		file_types_description : "Текстовый файл",
		file_upload_limit : 100,
		file_queue_limit : 0,

		custom_settings : {
        					progressTarget : "fsUploadProgress_" + controlName,
        					cancelButtonId : "btnCancel_" + controlName
        				},

		debug: true,

		// Button settings
		button_image_url : initObject.libsPath + "SWFUpload/images/XPButtonUploadText_61x22.png",
		button_placeholder_id : "spanButtonPlaceHolder_" + controlName,
		button_width: 61,
		button_height: 22,

		uploadedFilesInputsPlaceHolder : "uploadedFilesInputsPlaceHolder_" + controlName,
		uploadedFilesArrayPrefix : "" + controlName + "__",

		// The event handler functions are defined in handlers.js
		file_queued_handler : fileQueued,
	    file_queue_error_handler : fileQueueError,
		file_dialog_complete_handler : fileDialogComplete,
		upload_start_handler : uploadStart,
		upload_progress_handler : uploadProgress,
		upload_error_handler : uploadError,
		upload_success_handler : uploadSuccess,
		upload_complete_handler : uploadComplete,
		queue_complete_handler : function (numFilesUploaded)
            {
            	var status = document.getElementById("divStatus_" + controlName);
            	trueUploadedFilesNumber = numFilesUploaded*1 - errorUploadsCnt*1;
            	if (trueUploadedFilesNumber > 0)
            	{
            	    status.innerHTML = "Файл загружен. Нажмите «Сохранить»";
            	}
            }	// Queue plugin event
	};

	try
	{
	     swfu = new SWFUpload(settings);
	}
	catch (e)
	{
	    $.jGrowl(e);
	}

    /* SWFUpload */
}

