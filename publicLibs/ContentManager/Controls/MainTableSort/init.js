function MainTableSortInit(initObject)
{
    $(".up").bind("click", trup);
    $(".down").bind("click", trdown);
}

function trup() {
    var cur = $(this).parents('tr');
    var prev = cur.prev();

    prev.before($(cur));
    return false;
}

function trdown(){
    var cur = $(this).parents('tr');
    var next = cur.next();

    next.after($(cur));
    return false;
}