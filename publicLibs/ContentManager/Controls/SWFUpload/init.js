/**
*  Инициализатор контрола
*/
function SWFUploadInit(initObject)
{
    var controlName = initObject.name;
    var maxFilesNumber = 1;
    if (typeof initObject.maxFilesNumber != 'undefined')
        maxFilesNumber = initObject.maxFilesNumber;

	if (typeof initObject.includeThumbCropDialog != "undefined" && initObject.includeThumbCropDialog)
	{
		var o = new Object();
		if (typeof initObject.thumbWidth != "undefined")
			o.thumbWidth = initObject.thumbWidth;
		if (typeof initObject.thumbHeight != "undefined")
			o.thumbHeight = initObject.thumbHeight;
		if (typeof initObject.thumbCropScript != "undefined")
			o.formAction = initObject.thumbCropScript;

		$.CropThumbnailDialog.Init(o);

		$("#"+initObject.name+"_controlContainer .thumbCropDialogActivator").bind(
			"click",
			o,
			function(e)
			{
				e.preventDefault();
				var a = $(this).siblings("a:eq(0)");
				if (typeof e.data != "undefined")
					$.CropThumbnailDialog.Init(o);
				$.CropThumbnailDialog.Show(a.attr("href"), a.children().attr("src"));
			}
		);
	}

    var swfu;

    var settings = {
		flash_url : initObject.libsPath + "SWFUpload/swfupload.swf",
		upload_url: initObject.uploadScript,
		file_size_limit : Math.ceil(Number(initObject.maxFileSize) / (1024 * 1024)) + " MB",
		file_types : initObject.allowedExtensions,
		file_types_description : initObject.filesType == "image"? "Изображения":"Файлы",
		file_upload_limit : maxFilesNumber,
		file_queue_limit : 0,

		custom_settings : {
        					progressTarget : "fsUploadProgress_" + controlName,
        					cancelButtonId : "btnCancel_" + controlName
        				},

		debug: false,

		// Button settings
		button_image_url : initObject.libsPath +  "SWFUpload/images/XPButtonUploadText_61x22.png",
		button_placeholder_id : "spanButtonPlaceHolder_" + controlName,
		button_width: 61,
		button_height: 22,

		uploadedFilesInputsPlaceHolder : "uploadedFilesInputsPlaceHolder_" + controlName,
//		uploadedFilesArrayPrefix : controlName + "__",
		uploadedFilesArrayPrefix : initObject.tableName+"["+controlName+"__files]",

		// The event handler functions are defined in handlers.js
		file_queued_handler : fileQueued,
	    file_queue_error_handler : fileQueueError,
		file_dialog_complete_handler : fileDialogComplete,
		upload_start_handler : uploadStart,
		upload_progress_handler : uploadProgress,
		upload_error_handler : uploadError,
		upload_success_handler : uploadSuccess,
		upload_complete_handler : uploadComplete,
		queue_complete_handler : function (numFilesUploaded)
            {
            	var status = document.getElementById("divStatus_" + controlName);
            	//status.innerHTML = numFilesUploaded + " file" + (numFilesUploaded === 1 ? "" : "s") + " uploaded.";
            	trueUploadedFilesNumber = numFilesUploaded*1 - errorUploadsCnt*1;
                status.innerHTML = "Загружено файлов: " + trueUploadedFilesNumber;
            }	// Queue plugin event
		,
		button_window_mode: SWFUpload.WINDOW_MODE.OPAQUE
	};

	try
	{
	     swfu = new SWFUpload(settings);
	}
	catch (e)
	{
	    $.jGrowl(e);
	}
}