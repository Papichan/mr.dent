/**
 * cachePost - A client cache plugin for jQuery, using jCache and caching Post ajax queries
 * Should come in handy when data needs to be cached in client to improve performance.
 * Author: 	George K
 *			georgekalashnikov@gmail.com
 * License : MIT license

Usage:
	as usual post query
    $.cachePost(path, data, callback, type)
 */
(function($)
{
    $.cachePost = function(path, data, callback, type)
    {
        if (typeof $.jCache == "undefined")
			throw new Exception("This plugin is depended on jCache plugin");
		
		var key = path;
		if (data != null) 
			key += "?" + $.param(data);

		var cache = $.jCache.getItem(key);
		if (typeof cache == "undefined")
		{
			$.post(path, 
					data,
					function(data, status)
					{
						$.jCache.setItem(key, {data:data, status:status});
						if  (typeof callback != "undefined")
							callback(data, status);
					},
					type);
		}
		else
		{
			if  (typeof callback != "undefined")
				callback(cache.data, cache.status);
		}
    };
})(jQuery);