<?php
/**
 * Стандартный контроллер
 * 
 * @version 1.0
 *
 */
abstract class Controller
{
    /**
     * Вызывается сразу после создания контроллера
     * Возможно переопределить функцию и вернуть сразу свой 
     * ActionResult, сообщающий, например, что контроллер в
     * разработке или редиректящий на другой контроллер результат,
     * например, на авторизацию
     *
     * @return ActionResult
     */
    public function Init()
    {
        return new EmptyActionResult();
    }

    public function Index($args = null)
    {
        return new EmptyActionResult();
    }
    
    /**
     * Вызывается после срабатывания действия.
     * Служит для добавления данных в массив ViewData
     *
     */
    public function Complete()
    {
        
    }
}
?>