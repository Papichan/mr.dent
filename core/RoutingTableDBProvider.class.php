<?php
/**
 * Провайдер таблицы маршрутизации из базы данных
 *
 * @author grom
 */
class RoutingTableDBProvider implements IRoutingTableProvider
{
	const cacheFileName = 'RoutingTable';

	private $cachedTree;
	/**
	 * @var DBTableManager
	 */
	private $routingTableManager;

	public function __construct()
	{
		$this->routingTableManager = new DBTableManager(TablesNames::$ROUTING_TABLE_NAME);
	}

	public function &GetRoutingTree()
	{
		$tree = ObjectsCache::GetCache(self::cacheFileName);
		if (empty($tree))
		{
			$altree = $this->_getTree();

			// ненужные поля можно удалить из дерева
//			$altree->Iterate(function(&$node) {unset($node['routeID'], $node['parentID']); });
			$tree = $altree->ToArray();

			ObjectsCache::Cache($tree, self::cacheFileName);
		}

		return $tree;
	}

	public function GetRoute($routeID)
	{
		return $this->routingTableManager->SelectFirst(array('routeID' => (int)$routeID));
	}

	public function GetLinearTree()
	{
		$altree = $this->_getTree();

		return $altree->Linear();
	}

	/**
	 * Сохраняет существующий или добавляет новый маршрут
	 * @param array $route маршрут
	 */
	public function SaveRoute($route)
	{
		$this->CleanCache(); // чистим кэш маршрутов
		if (isset($route['routeID']))
		{
			return $this->routingTableManager->Update($route, $route['routeID']);
		}
		else
		{
			return $this->routingTableManager->Insert($route);
		}
	}

	public function DeleteRouteRecursive($routeID)
	{
		$this->CleanCache();
		$IDs = array((int)$routeID);
		$altree = $this->_getTree();
		$children = $altree->GetChildrenRecursive($routeID);

		foreach ($children as $child)
			$IDs[] = (int)$child['routeID'];

		$query = '
			DELETE FROM `' . TablesNames::$ROUTING_TABLE_NAME . '`
			WHERE `routeID` IN (' . implode(', ', $IDs) . ')
		';

		return DB::AffectedRows(DB::Query($query));
//		return $this->routingTableManager->Delete(array('routeID' => (int)$routeID));
	}

	public function CleanCache()
	{
		ObjectsCache::KillCache(self::cacheFileName);
	}

	private function _getTree()
	{
		$plainTable = $this->routingTableManager->Select();

		return new ALTree($plainTable, 'routeID', 'parentID', 'pattern', 0, 'fullPath', '/', 'children', NULL, 'level');
	}
}
