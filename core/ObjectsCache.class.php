<?php
/**
 * Класс для сериализации данных в виде php структур и сохранения
 * 
 * @version 2.0
 *
 */
class ObjectsCache
{
    /**
     * Кеширование объекта
     *
     * @param object $object
     * @param string $fileName
     */
    public static function Cache($object, $fileName)
    {
  		file_put_contents(AppSettings::$OBJECTS_CACHE_PATH . $fileName, serialize($object));
    }

    /**
     * Получение закешированного объекта
     *
     * @param string $fileName
     */
    public static function GetCache($fileName)
    {
    	if (!File::Exists(AppSettings::$OBJECTS_CACHE_PATH . $fileName))
    		return null;
    	return unserialize(file_get_contents(AppSettings::$OBJECTS_CACHE_PATH . $fileName));
    }
    
    /**
     * Очистка файла кеша
     *
     * @param string $fileName
     *
     */
    public static function KillCache($fileName, $quiet = false)
    {
    	try
    	{
    		File::Delete(AppSettings::$OBJECTS_CACHE_PATH . $fileName);
    	}
    	catch (Exception $e)
    	{
    		if (!$quiet)
    			throw $e;
    	}
    }
    
	/**
     * Очистка файла кеша по имени файла, 
     * удовлетворяющему паттерну c wildcards
     *
     * @param string $fileNamePattern
     *
     */
    public static function KillCacheByPattern($fileNamePattern, $quiet = false)
    {
    	$list = glob(AppSettings::$OBJECTS_CACHE_PATH .$fileNamePattern);
    	
    	if (!empty($list))
    	for ($i = count($list) - 1; $i >= 0; $i--)
    	{
	    	try
	    	{
	    		File::Delete($list[$i]);
	    	}
	    	catch (Exception $e)
	    	{
	    		if (!$quiet)
	    			throw $e;
	    	}
    	}
    }
}
?>