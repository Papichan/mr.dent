<?php

/**
 * Класс для хранения данных, использующихся представлением
 *
 * @version 1.0
 */
class ViewData
{
    private static $viewData = null;
    
    /**
     * Добавляет новую переменную
     *
     * @param string $name
     * @param mixed $value
     */
    public static function Assign($name, $value)
    {
        if (is_null(self::$viewData))
            self::$viewData = array();
        
        self::$viewData[$name] = $value;
    }
    
    /**
     * Добавляет новое значение к существующей переменной
     *
     * @param string $name
     * @param mixed $value
     */
    public static function Append($name, $value)
    {
        if (is_null(self::$viewData))
            self::$viewData = array();
            
        // Переменная с таким именем есть. Добавлем к ней новое значение
        if (isset(self::$viewData[$name]))
        {
            // Массив. Доавляем в него новый элемент
            if (is_array(self::$viewData[$name]))
                self::$viewData[$name][] = $value;
            // Не массив. Создаем его с 2-мя значениями
            else 
                self::$viewData[$name] = array(self::$viewData[$name], $value);
        }
        // Переменной с таким именем нет. Создаем ее
        else 
        {
            self::$viewData[$name] = array($value);
        }
    }
    
    /**
     * Удаляет переменную
     *
     * @param string $name
     */
    public static function Delete($name)
    {
        if (isset(self::$viewData[$name]))
            unset(self::$viewData[$name]);
    }
    
    /**
     * Возвращает весь массив переменных, либо значение конкретной переменной
     *
     * @param string $name
     * @return unknown
     */
    public static function Get($name = null)
    {
        if (!is_null($name))
            return isset(self::$viewData[$name]) ? self::$viewData[$name] : null;
        else 
            return self::$viewData;
    }
}

?>