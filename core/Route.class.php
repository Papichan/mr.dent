<?php
/**
 * Класс маршрут
 * 
 * @version 1.0
 *
 */
class Route
{
    private $controllerName;
    private $actionName;
    private $args = null;

    public function __construct($controllerName, $actionName, $args = null)
    {
        $this -> controllerName = $controllerName;
        $this -> actionName = $actionName;
        if (!is_null($args))
            $this -> args = &$args;
    }

    public function GetControllerName()
    {
        return $this -> controllerName;
    }

    public function GetActionName()
    {
        return $this -> actionName;
    }

    public function &GetArgs()
    {
        return $this -> args;
    }
}
?>