<?php
class RoutingTableProvider implements IRoutingTableProvider
{
    /**
     * Таблица маршрутизации
     *
     * @var RoutingTable
     */
    private $routingTable;

    public function __construct()
    {
       $this -> routingTable = new RoutingTable();
    }

    public function &GetRoutingTree()
    {
        return $this -> routingTable -> GetRoutingTable();
    }
}
?>