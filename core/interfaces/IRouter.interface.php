<?php
/**
 * Интерфейс маршрутизатора
 *
 * @version 1.0
 */
interface IRouter
{
    /**
     * Получение пути по запросу
     * 
     * @return Route
     * 
     */
    public function GetRoute();
    
    /**
     * Получение URL по интерфейсу контроллера
     *
     * @param string $controllerName
     * @param string $actionName
     * @param array $args
     * @return string
     */
    public function GetURL($controllerName, $actionName = null, $args = null);
}
?>