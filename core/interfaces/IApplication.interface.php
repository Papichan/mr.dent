<?php
/**
 * Интерфейс класса-приложения
 * 
 * @version 1.0
 *
 */
interface IApplication
{
    /**
     * Функция запуска приложения
     *
     */
    public function Run();
}
?>