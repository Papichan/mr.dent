<?php
/**
 * Класс, содержащий функцию автозагрузки классов, а также
 * вспомогательные функции для поиска классов
 * использует spl_autoload_register
 *
 * @version 1.0
 *
 */
class Autoload
{
    /**
     * Массив соответствий имён классов и путей к ним
     *
     * @var array
     */
    private static $classesPaths = null;

    /**
     * Имя файла, в который сохраняется $classesPaths
     *
     * @var string
     */
    private static $cacheFile = 'AutoloadClassesPaths';

    /**
     * Внутренний флаг, указывающий на то, что
     * массив путей к классам изменился и его необходимо
     * пересохранить
     *
     * @var bool
     */
    private static $savePaths = false;

    /**
     * Флаг чувствительности к регистру имени класса при поиске
     *
     * @var bool
     */
    private static $caseSensitive = true;

    /**
     * Флаг, разрешающий поиск в папках
     *
     * @var bool
     */
    public static $searchFolders = true;

    /**
     * Функция вызывается для регистрации функции Autoload::Load
     * в качестве функции автозагрузки
     *
     */
    public static function Register()
    {
        if (!spl_autoload_register('Autoload::Load'))
        {
            throw new Exception('Не могу подключить Autoload функцию', 1001);
        }

        $cache = ObjectsCache::GetCache(self::$cacheFile);

        if (is_array($cache) && !empty($cache))
        {
            self::$classesPaths = $cache;
        }
    }

    /**
     * Убрать Autoload::Load из очереди функций-автозагрузчиков
     *
     */
    public static function Unregister()
    {
        if (!spl_autoload_unregister('Autoload::Load'))
        {
            throw new Exception('Не могу отключить Autoload функцию', 1002);
        }
    }

    /**
     * Функция, регистрируемая в spl_register_autoload
     * выполняет поиск и подключение файла с искомым классом
     *
     * @param string $fullClassName
     */
    public static function Load($fullClassName)
    {
        $namespaces = explode('::', $fullClassName);
        $className = array_pop($namespaces);
        //if (count($namespaces) != 1 || $namespaces[0] != __NAMESPACE__)
        //  return;

        $pathCached = true;
        if (isset(self::$classesPaths[$className]) && File::Exists(self::$classesPaths[$className]))
        {
            $path = self::$classesPaths[$className];
        }
        if (empty($path) && self::$searchFolders)
        {
            $pathCached = false;
            // Обход основных папок приложения и сохранение полученного результата в индекс
            $path = self::FindClass(IncPaths::$CORE_PATH, $className, self::GetCaseSensitivity());
        }

        if (empty($path) && self::$searchFolders)
            $path = self::FindClass(IncPaths::$APPLICATION_PATH, $className, self::GetCaseSensitivity());
        if (empty($path) && self::$searchFolders)
            $path = self::FindClass(IncPaths::$LIBS_PATH, $className, self::GetCaseSensitivity());
        if (empty($path) && self::$searchFolders)
            $path = self::FindClass(IncPaths::$MODULES_PATH, $className);

        if (!empty($path) && self::$searchFolders)
        {
            self::$classesPaths[$className] = $path;
            if (!$pathCached)
                self::$savePaths = true;
        }

        if (!empty($path))
            require_once($path);
    }

    /**
     * Ищет класс в указанной папке
     * можно задавать флаг и искать рекрсивно,
     * можно искать в независимости от регистра
     *
     * @param string $path
     * @param string $className
     * @param bool $recursive
     * @param bool $caseSensitive
     * @return string
     */
    public static function FindClass($path, $className, $recursive = true, $caseSensitive = true)
    {
        if (!is_dir($path))
        {
            echo 'Path for autoload "' . $path . '" is not a directory';
            return;
        }
        $className = str_replace("\\", '/', $className);
        if (!$caseSensitive)
            $className = strtolower($className);

        $handle = opendir($path);

        if ($path{strlen($path) - 1} != '/')
            $path .= '/';

        $classPath = null;
        while (($file      = readdir($handle)) !== false && empty($classPath))
        {
            if ($file != '.' && $file != '..' && $file != '.svn')
            {
                if (is_file($path . $file))
                {
                    $filename = $file;
                    if (!$caseSensitive)
                        $filename = strtolower($file);

                    if ($filename == $className . '.class.php' || $filename == $className . '.interface.php' || $filename == $className . '.abstract.php')
                    {
                        $classPath = $path . $file;
                    }
                }
                else if ($recursive && is_dir($path . $file))
                {
                    $classPath = self::FindClass($path . $file . '/', $className);
                }
            }
        }
        closedir($handle);
        if (!$classPath)
        {
            if (is_file($path . "/" . $className . ".php"))
            {
                require $path . "/" . $className . ".php";
            }
        }
        return $classPath;
    }

    /**
     * Функция возвращает имя закешированного (уже найденного)
     * класса, можно искать регистронезависим, тогда фукнция вернёт правильное
     * имя класса
     *
     * @param string $className
     * @param bool $caseSensitive
     * @return string
     */
    public static function FindCachedClass($className, $caseSensitive = true)
    {
        if (!$caseSensitive)
        {
            $classes = array_keys(self::$classesPaths);
            $cnt = count($classes);
            for ($i = 0; $i < $cnt; $i++)
            {
                $className = strtolower($className);
                if (strtolower($classes[$i]) == $className)
                    return $classes[$i];
            }
        }
        else
        {
            if (isset(self::$classesPaths[$className]))
                return $className;
        }

        return null;
    }

    /**
     * Пробует подключить файл по указанному пути.
     * В случае, если файла нет, возвращается false
     *
     * @param string $path
     * @return bool
     */
    public static function TryLoad($path)
    {
        // Если такого файла нет, возвращаем false
        if (!file_exists($path))
            return false;

        // Загружаем файл
        require_once($path);
        return true;
    }

    /**
     * Вызывается в конце работы приложения для сохранения
     * найденных путей.
     * Пути сохраняются в файле self::$cacheFile
     *
     */
    public static function SavePaths()
    {
        if (self::$savePaths && !empty(self::$classesPaths))
            ObjectsCache::Cache(self::$classesPaths, self::$cacheFile);
    }

    /**
     * Установка флага чувствительности к регистру
     * Если выставить false, то Автолоад будет искать названия классов
     * по именам файлов независимо от регистра
     *
     * @param bool $caseSensitive
     */
    public static function SetCaseSensitivity($caseSensitive)
    {
        // Поскольку могут передать не булевский тип
        self::$caseSensitive = ($caseSensitive) ? true : false;
    }

    /**
     * Получение флага чквствительности к регистру
     *
     * @return bool
     */
    public static function GetCaseSensitivity()
    {
        return self::$caseSensitive;
    }
}
?>