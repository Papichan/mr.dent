<?php

/**
 * Результат работы контроллера в виде редиректа на другой адрес
 *
 * @version 2.0
 *
 */
class RedirectActionResult extends ActionResult
{
    /**
     * Путь редиректа
     *
     * @var string
     */
    private $redirectTo;
    private $permanent;

    public function __construct($redirectTo, $permanent = false)
    {
        $this->redirectTo = $redirectTo;
        $this->permanent    = $permanent;
    }

    public function ExecuteResult()
    {
        if(!empty($this->redirectTo))
        {
            if(!headers_sent())
            {
                if ($this->permanent)
                {
                    header('HTTP/1.1 301 Moved Permanently');
                }
                header('Location: ' . $this->redirectTo);
            }
        }
        else throw new Exception('Не задан путь редиректа');
    }

}

?>