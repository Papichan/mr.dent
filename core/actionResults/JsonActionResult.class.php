<?php
/**
 * Результат работы контроллера в виде JSON
 * 
 * @version 1.0
 *
 */
class JsonActionResult extends ActionResult 
{
    /**
     * Объект, который необходимо преобразовать в JSON
     *
     * @var object
     */
    private $objectToEncode;

    /**
     * Готовая JSON строка
     *
     * @var string
     */
    private $jsonString;

    public function __construct($result)
    {
        if (is_string($result))
            $this -> jsonString = &$result;
        else 
            $this -> objectToEncode = &$result;
    }

    public function ExecuteResult()
    {
        if (empty($this -> jsonString))
        {
            echo JSON::Encode($this -> objectToEncode);
        }
        else echo $this -> jsonString;
    }
}
?>