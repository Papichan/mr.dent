<?php
/**
 * Стандартный результат работы контроллера
 *
 */
class XlsActionResult extends ActionResult 
{
    /**
     * file path
     *
     * @var string
     */
    private $file;
    
    /**
     * Удалить после отдачи
     *
     * @var boolean
     */
    private $removeAfterExecution = false;

    /**
     * Заголовок тпиа содержимого
     *
     * @var string
     */
    public $contentTypeHeader = 'Content-Type: application/vnd.ms-excel; charset=UTF-8';

    public function __construct($file)
    {
        $this -> file = $file;
    }

    public function ExecuteResult()
    {
    	// Определяем тип данных
    	if (!empty($this -> contentTypeHeader) && !headers_sent())
    	{
    		header("Pragma: public");
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header($this -> contentTypeHeader);
			header("Content-Type: application/force-download");
			header("Content-Type: application/download");
			$path = pathinfo($this -> file);
			header('Content-Disposition: inline; filename="'.$path['filename'].'.'.$path['extension'].'"');
    	}
        readfile($this -> file);
        if ($this -> removeAfterExecution && is_writable($this -> file))
        	unlink($this -> file);
    }
    
    public function RemoveAfterExecution()
    {
    	$this -> removeAfterExecution = true;
    }
}
?>