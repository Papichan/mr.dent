<?php
/**
 * Представление с шаблонизатором
 * 
 * @version 1.0
 *
 */
class ViewResult extends ActionResult 
{
    /**
     * Шаблонизатор
     *
     * @var Template
     */
    protected $tpl;
    
    /**
     * Путь к шаблонам
     */
    public $tplsPath;
    
    /*
     * Название модуля
     */
    protected $moduleName = null;
    
    /*
     * Папка нахождения файлов
     */
    protected $publicFilesPath = null;

    /**
     * Заголовок тпиа содержимого
     *
     * @var string
     */
    public $contentTypeHeader = 'Content-Type: text/html; charset=utf-8';

    public function __construct($tplPath)
    {
        $this -> tpl = new Template($tplPath);
        i18n::registerSmartyMultyLangFuncion($this->tpl);
    }
    
    public function ExecuteResult()
    {
    	// Определяем тип данных
    	if (!empty($this -> contentTypeHeader) && !headers_sent())
			header($this -> contentTypeHeader);

        $viewData = ViewData::Get();
        foreach ($viewData as $key => &$value)
            $this -> tpl -> assign_by_ref($key, $value);

        if (!empty($this -> tpl))
            echo $this -> tpl -> Parse();
    }
    
    public function &GetTemplate()
    {
        return $this -> tpl;
    }
}
?>