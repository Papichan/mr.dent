<?php
/**
 * Стандартный результат работы контроллера
 *
 */
class ContentActionResult extends ActionResult 
{
    /**
     * Выходная строка
     *
     * @var string
     */
    private $contents;

    /**
     * Заголовок тпиа содержимого
     *
     * @var string
     */
    public $contentTypeHeader = 'Content-Type: text/html; charset=utf-8';

    public function __construct($contents)
    {
        $this -> contents = $contents;
    }

    public function ExecuteResult()
    {
    	// Определяем тип данных
    	if (!empty($this -> contentTypeHeader) && !headers_sent())
			header($this -> contentTypeHeader);
        echo $this -> contents;
    }
}
?>