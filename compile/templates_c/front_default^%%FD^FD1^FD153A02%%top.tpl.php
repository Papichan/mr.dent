<?php /* Smarty version 2.6.20, created on 2019-10-18 12:37:47
         compiled from top.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'json_encode', 'top.tpl', 37, false),)), $this); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ru">
    <head>
        <?php if (( isset ( $this->_tpl_vars['PAGE']['isNoindex'] ) && $this->_tpl_vars['PAGE']['isNoindex'] )): ?><meta name="robots" content="noindex,nofollow"><?php endif; ?>
            <title><?php if (isset ( $this->_tpl_vars['PAGE']['title'] )): ?><?php echo $this->_tpl_vars['PAGE']['title']; ?>
<?php endif; ?></title>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- Старые меты
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <meta name="viewport" content="width=device-width" />
        -->
            <?php if (isset ( $this->_tpl_vars['PAGE']['description'] )): ?><meta name="description" content="<?php echo $this->_tpl_vars['PAGE']['description']; ?>
" /><?php endif; ?>
            <?php if (isset ( $this->_tpl_vars['PAGE']['keywords'] )): ?><meta name="keywords" content="<?php echo $this->_tpl_vars['PAGE']['keywords']; ?>
" /><?php endif; ?>

            <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['STYLES_ALIAS']; ?>
style.css" media="all"/>
            <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['STYLES_ALIAS']; ?>
myStyles.css" />

            <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['STYLES_ALIAS']; ?>
normalize.css" media="all"/>
            <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['STYLES_ALIAS']; ?>
slick.css" />
            <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['STYLES_ALIAS']; ?>
slick-theme.css" media="all"/>
            <link rel="stylesheet" type="text/css" href="<?php echo $this->_tpl_vars['STYLES_ALIAS']; ?>
main.min.css" />
                
                
            <?php if (isset ( $this->_tpl_vars['STYLE_FILE'] )): ?>
                <?php $_from = $this->_tpl_vars['STYLE_FILE']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['FILE']):
?>
                    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['FILE']; ?>
" type="text/css" />
                <?php endforeach; endif; unset($_from); ?>
            <?php endif; ?>

            <?php if (isset ( $this->_tpl_vars['JS_VARS'] )): ?>
                <script type="text/javascript">
                    <?php $_from = $this->_tpl_vars['JS_VARS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['JS_VAR']):
?>
                        <?php $_from = $this->_tpl_vars['JS_VAR']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['JS_VAR_NAME'] => $this->_tpl_vars['JS_VAR_VALUE']):
?>
                                <?php if (is_array ( $this->_tpl_vars['JS_VAR_VALUE'] ) || is_object ( $this->_tpl_vars['JS_VAR_VALUE'] )): ?>
            var <?php echo $this->_tpl_vars['JS_VAR_NAME']; ?>
 = <?php echo json_encode($this->_tpl_vars['JS_VAR_VALUE']); ?>
;
                                <?php else: ?>
            var <?php echo $this->_tpl_vars['JS_VAR_NAME']; ?>
 = "<?php echo $this->_tpl_vars['JS_VAR_VALUE']; ?>
";
                            <?php endif; ?>
                        <?php endforeach; endif; unset($_from); ?>
                    <?php endforeach; endif; unset($_from); ?>
                </script>
            <?php endif; ?>
    </head>