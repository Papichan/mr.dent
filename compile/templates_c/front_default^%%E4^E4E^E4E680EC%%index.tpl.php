<?php /* Smarty version 2.6.20, created on 2019-10-18 15:26:16
         compiled from services/index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'debug', 'services/index.tpl', 1, false),)), $this); ?>
<?php echo smarty_function_debug(array(), $this);?>


<main>

    <!-- ФОРМА ОТПРАВКИ ВЕРХ  -->
    <section  class="consult consult__services">
        <div class="content-wrapper consult__wrapper">
            <div class="consult__img">
                <img  src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['topImage']; ?>
" alt="">
            </div>
            <div class="consult__left">
                <ul class="breadcrumbs consult__breadcrubms">
                    <li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
                    <li><a class="current-link">Услуги</a></li>
                </ul>
                <div class="consult__text">
                   <?php echo $this->_tpl_vars['PAGE_CONTENT']['titleTop']; ?>

                </div>
            </div>
            <div class="consult__right">
                <div id="appointment" class="form-wrapper consult__form">
                    <!-- Загрузка формы отправки "Бесплатная консультация" -->
                    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-consultation.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                </div>
            </div>
        </div>
    </section>





    <!-- ВЫВОД ВСЕХ РАЗДЕЛОВ С ИХ ПОДРАЗДЕЛАМИ ДЛЯ СТРАНИЦЫ "УСЛУГИ" -->
    <section class="uslugi content-wrapper">
        <h2 class="title uslugi__title"><?php echo $this->_tpl_vars['PAGE_CONTENT']['servicesTitle']; ?>
</h2>
        <p class="uslugi__descr"><?php echo $this->_tpl_vars['PAGE_CONTENT']['servicesText']; ?>
</p>
        <span class="uslugi__razdel-title block_mobile">Профессиональная гигиена полости рта</span>
        <div class="uslugi__wrapper ">

            
                        <!-- Вывод происходит по 4 коротких блока, каждый 5-й блок имеет уникальные стили, растягиваясь на весь экран -->    
                        <!-- порядок блоков берется из порядка в Админка->меню --> 
                        <?php $this->assign('itterator', 1); ?><!-- Итератор для счета количества выведенных разделоов с услугами -->
                        <?php $this->assign('blok', 0); ?><!-- Итератор для счте вывода <div> -->

                        <?php $_from = $this->_tpl_vars['SERVICES_MENU']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['menu']):
?><!-- Циклвывода разделов с услугами -->
                            
                            <!-- Условие сработает если блок является пятым (у него уникальные стили) -->
                            <?php if (( $this->_tpl_vars['itterator'] == 5 )): ?>
                                <div class="uslugi__razdel uslugi__razdel--wide uslugi__razdel<?php echo $this->_tpl_vars['itterator']++; ?>
 ">
                                    <img src="<?php echo $this->_tpl_vars['menu']['bigImage']; ?>
" alt="">
                                    <span class="uslugi__razdel-title block_desktop"><?php echo $this->_tpl_vars['menu']['name']; ?>
</span>
                                    <ul class="uslugi__razdel-list">
                                        <?php $_from = $this->_tpl_vars['menu']['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['line']):
?>
                                            <!-- Вывод блоков по 3 штуки в колонку -->
                                            <?php if (( $this->_tpl_vars['blok'] == 0 )): ?> <div> <?php endif; ?>
                                                <li><a href="<?php echo $this->_tpl_vars['menu']['alias']; ?>
/<?php echo $this->_tpl_vars['line']['alias']; ?>
"><?php echo $this->_tpl_vars['line']['name']; ?>
</a></li>
                                                <?php $this->assign('blok', $this->_tpl_vars['blok']+1); ?>
                                            <?php if (( $this->_tpl_vars['blok'] == 3 )): ?> </div> <?php $this->assign('blok', 0); ?> <?php endif; ?>
                                        <?php endforeach; endif; unset($_from); ?>
                                    </ul>
                                </div>
                                <?php $this->assign('itterator', 1); ?>
                            <!-- Вывод по 4 обычных блока -->    
                            <?php else: ?><!-- Если раздел не пятый то для него другие стили -->
                                 <div class="uslugi__razdel uslugi__razdel<?php echo $this->_tpl_vars['itterator']++; ?>
">
                                     <img src="<?php echo $this->_tpl_vars['menu']['bigImage']; ?>
" alt="">
                                     <span class="uslugi__razdel-title block_desktop"><?php echo $this->_tpl_vars['menu']['name']; ?>
</span>
                                     <ul class="uslugi__razdel-list">
                                         <?php $_from = $this->_tpl_vars['menu']['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['line']):
?>
                                             <li><a href="<?php echo $this->_tpl_vars['menu']['alias']; ?>
/<?php echo $this->_tpl_vars['line']['alias']; ?>
"><?php echo $this->_tpl_vars['line']['name']; ?>
</a></li>
                                         <?php endforeach; endif; unset($_from); ?>
                                     </ul>
                                 </div>
                            <?php endif; ?>
                            
                        <?php endforeach; endif; unset($_from); ?>

        </div>
    </section>
        
        

        
<!--  Текст для продвижения -->
<section class="promotion">
    <h2 class="visually-hidden"><?php echo $this->_tpl_vars['PAGE_CONTENT']['promotionTitle']; ?>
</h2>
    <div class="content-wrapper promotion__wrapper">
        <h2 class="promotion__title"><?php echo $this->_tpl_vars['PAGE_CONTENT']['promotionTitle']; ?>
</h2>
        <p class="promotion__text"><?php echo $this->_tpl_vars['PAGE_CONTENT']['promotionText']; ?>
</p>
    </div>
</section>



    
<!-- ФОРМА ОТПРАВКИ НИЗ -->
<section id="appointment" class="form">
    <div class=" content-wrapper">
        <img src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['bottomImage']; ?>
" alt="">
        <div class="form-wrapper">
            <!-- Загрузка формы отправки "Записаться на прием" -->
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-appointment.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        </div>
    </div>
</section>
        
        

</main>