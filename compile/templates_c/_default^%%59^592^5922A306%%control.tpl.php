<?php /* Smarty version 2.6.20, created on 2019-10-03 17:08:06
         compiled from /var/www/dent/libs/ContentManager/Controls/DamnUpload/control.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', '/var/www/dent/libs/ContentManager/Controls/DamnUpload/control.tpl', 71, false),)), $this); ?>
<div style="width:100%; float:left;" id="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
_controlContainer">
	<div id="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
_filesDropTarget" class="damnupload-files-drop-target" style="display: none;">
	</div>
	<div id="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
_filesContainer" class="damnupload-files-container">
		<?php if (! empty ( $this->_tpl_vars['localVars']['FILES'] )): ?>
		<?php $_from = $this->_tpl_vars['localVars']['FILES']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['I'] => $this->_tpl_vars['FILE']):
?>
			<div class="file-block<?php if (isset ( $this->_tpl_vars['localVars']['blockClass'] )): ?> <?php echo $this->_tpl_vars['localVars']['blockClass']; ?>
<?php endif; ?>">
				<?php if (preg_match ( '/\.jpg$|\.jpeg$|\.gif$|\.png$|\.svg$/i' , $this->_tpl_vars['FILE']['src'] ) != false): ?>
					<?php if (! empty ( $this->_tpl_vars['FILE']['srcSmall'] )): ?>
					<a href="<?php echo $this->_tpl_vars['FILE']['src']; ?>
" class="fancy" rel="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
"><img src="<?php echo $this->_tpl_vars['FILE']['srcSmall']; ?>
" alt="" /></a>
					<br/>

						<?php if (! empty ( $this->_tpl_vars['localVars']['includeThumbCropDialog'] )): ?>
							<a href="#" class="thumbCropDialogActivator jsLink" title="Открыть диалог редактирования уменьшенной копии">Обрезать вручную</a>
						<?php endif; ?>

					<?php else: ?>
					<img src="<?php echo $this->_tpl_vars['FILE']['src']; ?>
" alt="" />
					<?php endif; ?>
				<?php else: ?>
					<?php if (preg_match ( '/\.flv$/i' , $this->_tpl_vars['FILE']['src'] ) != false): ?>
					<object id="flvPlayer_<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
_<?php echo $this->_tpl_vars['I']; ?>
" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="600" height="330">
						<param name="movie" value="/publicLibs/VideoPlayerStandart.swf" />
						<param name="flashvars" value="flv=<?php echo $this->_tpl_vars['FILE']['src']; ?>
" />
						<param name="wmode" value="opaque" />
						<!--[if !IE]>-->
						<object type="application/x-shockwave-flash" data="/publicLibs/VideoPlayerStandart.swf" width="600" height="330">
							<param name="flashvars" value="flv=<?php echo $this->_tpl_vars['FILE']['src']; ?>
" />
							<param name="wmode" value="opaque" />
						<!--<![endif]-->
						  <p>Установите Flash-плеер</p>
						<!--[if !IE]>-->
						</object>
						<!--<![endif]-->
					</object>
					<?php else: ?>
						<?php if (preg_match ( '/\.mp3$/i' , $this->_tpl_vars['FILE']['src'] ) != false): ?>
						<object id="mp3Player_<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
_<?php echo $this->_tpl_vars['I']; ?>
" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="300" height="20">
							<param name="movie" value="/publicLibs/MP3PlayerStandart.swf" />
							<param name="flashvars" value="mp3=<?php echo $this->_tpl_vars['FILE']['src']; ?>
" />
							<param name="wmode" value="opaque" />
							<!--[if !IE]>-->
							<object type="application/x-shockwave-flash" data="/publicLibs/MP3PlayerStandart.swf" width="300" height="20">
								<param name="flashvars" value="mp3=<?php echo $this->_tpl_vars['FILE']['src']; ?>
" />
								<param name="wmode" value="opaque" />
							<!--<![endif]-->
							  <p>Установите Flash-плеер</p>
							<!--[if !IE]>-->
							</object>
							<!--<![endif]-->
						</object>
						<?php else: ?>
						<a href="<?php echo $this->_tpl_vars['FILE']['src']; ?>
" class="openInNewWin"><?php echo $this->_tpl_vars['FILE']['src']; ?>
</a>
						<?php endif; ?>
					<?php endif; ?>
				<?php endif; ?>
				<?php if (! empty ( $this->_tpl_vars['localVars']['includeTitle'] )): ?>
				<br />
				<input type="text" name="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__title[<?php echo $this->_tpl_vars['FILE'][$this->_tpl_vars['localVars']['FILE_ID_KEY']]; ?>
]" id="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__title[<?php echo $this->_tpl_vars['FILE'][$this->_tpl_vars['localVars']['FILE_ID_KEY']]; ?>
]" value="<?php echo $this->_tpl_vars['FILE']['title']; ?>
" title="Название изображения" style="width: 200px" />
				<?php endif; ?>
				<?php if (! empty ( $this->_tpl_vars['localVars']['textFields'] )): ?>
				<?php $_from = $this->_tpl_vars['localVars']['textFields']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['fieldName'] => $this->_tpl_vars['fieldInfo']):
?>
					<?php if (! empty ( $this->_tpl_vars['fieldInfo']['containerType'] ) && $this->_tpl_vars['fieldInfo']['containerType'] == 'text'): ?>
						<div <?php if (! empty ( $this->_tpl_vars['fieldInfo']['containerClass'] )): ?>class="<?php echo $this->_tpl_vars['fieldInfo']['containerClass']; ?>
"<?php endif; ?>>
							<?php if ($this->_tpl_vars['fieldInfo']['showLabel']): ?><span><?php echo $this->_tpl_vars['fieldInfo']['label']; ?>
 </span><?php endif; ?>
							<textarea name="<?php echo $this->_tpl_vars['localVars']['tableName']; ?>
[<?php echo $this->_tpl_vars['localVars']['formIndex']; ?>
][<?php echo $this->_tpl_vars['localVars']['FIELD_NAME']; ?>
][<?php echo $this->_tpl_vars['fieldName']; ?>
][<?php echo $this->_tpl_vars['FILE'][$this->_tpl_vars['localVars']['FILE_ID_KEY']]; ?>
]" id="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['fieldName']; ?>
[<?php echo $this->_tpl_vars['FILE'][$this->_tpl_vars['localVars']['FILE_ID_KEY']]; ?>
]" style="width:185px; height: 230px" title="<?php echo $this->_tpl_vars['fieldInfo']['label']; ?>
"<?php if (! empty ( $this->_tpl_vars['fieldInfo']['styles'] )): ?> style="<?php echo $this->_tpl_vars['fieldInfo']['styles']; ?>
"<?php endif; ?>><?php echo $this->_tpl_vars['FILE'][$this->_tpl_vars['fieldName']]; ?>
</textarea>
						</div>
					<?php else: ?>
						<div <?php if (! empty ( $this->_tpl_vars['fieldInfo']['containerClass'] )): ?>class="<?php echo $this->_tpl_vars['fieldInfo']['containerClass']; ?>
"<?php endif; ?>>
							<?php if ($this->_tpl_vars['fieldInfo']['showLabel']): ?><span><?php echo $this->_tpl_vars['fieldInfo']['label']; ?>
 </span><?php endif; ?>
							<input type="text" name="<?php echo $this->_tpl_vars['localVars']['tableName']; ?>
[<?php echo $this->_tpl_vars['localVars']['formIndex']; ?>
][<?php echo $this->_tpl_vars['localVars']['FIELD_NAME']; ?>
][<?php echo $this->_tpl_vars['fieldName']; ?>
][<?php echo $this->_tpl_vars['FILE'][$this->_tpl_vars['localVars']['FILE_ID_KEY']]; ?>
]" id="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['fieldName']; ?>
[<?php echo $this->_tpl_vars['FILE'][$this->_tpl_vars['localVars']['FILE_ID_KEY']]; ?>
]" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['FILE'][$this->_tpl_vars['fieldName']])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
" title="<?php echo $this->_tpl_vars['fieldInfo']['label']; ?>
"<?php if (! empty ( $this->_tpl_vars['fieldInfo']['styles'] )): ?> style="<?php echo $this->_tpl_vars['fieldInfo']['styles']; ?>
"<?php endif; ?> />
						</div>
					<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
				<?php endif; ?>
				<?php if (! empty ( $this->_tpl_vars['localVars']['usePriority'] )): ?>
				<br />
				<label for="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__priority[<?php echo $this->_tpl_vars['FILE'][$this->_tpl_vars['localVars']['FILE_ID_KEY']]; ?>
]">Проиритет:</label> <input type="text" name="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__priority[<?php echo $this->_tpl_vars['FILE'][$this->_tpl_vars['localVars']['FILE_ID_KEY']]; ?>
]" id="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__priority[<?php echo $this->_tpl_vars['FILE'][$this->_tpl_vars['localVars']['FILE_ID_KEY']]; ?>
]" value="<?php echo $this->_tpl_vars['FILE']['priority']; ?>
" style="width: 50px" />
				<?php endif; ?>
				<br />
				<label for="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
_<?php echo $this->_tpl_vars['I']; ?>
__delete">Удалить <input type="checkbox" id="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
_<?php echo $this->_tpl_vars['I']; ?>
__delete" class="delete" name="<?php echo $this->_tpl_vars['localVars']['tableName']; ?>
[<?php echo $this->_tpl_vars['localVars']['formIndex']; ?>
][<?php echo $this->_tpl_vars['localVars']['FIELD_NAME']; ?>
__delete][]" value="<?php echo $this->_tpl_vars['FILE'][$this->_tpl_vars['localVars']['FILE_ID_KEY']]; ?>
" /></label>

			</div>

		<?php endforeach; endif; unset($_from); ?>
		<?php endif; ?>
	</div>
	<div style="clear: both;">  </div>
	<div id="fsUploadProgress_<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
" class="damnupload-progress">
		<span class="legend">Загружаемые файлы</span>
	</div>


	<span style="display: none;" id="uploadedFilesInputsPlaceHolder_<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
"></span>

	<span class="fileinput btn">
		<span class="plus">+</span>
		<span class="text">Добавить файлы...</span>
		<input type="file" title="<?php echo $this->_tpl_vars['localVars']['TIP']; ?>
" class="damnupload" id="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
_fileInput" <?php if ($this->_tpl_vars['localVars']['FILE_TYPE'] == 'image'): ?>accept="image/*"<?php endif; ?> <?php if ($this->_tpl_vars['localVars']['MAX_FILES_NUMBER'] > 1): ?>multiple="multiple"<?php endif; ?>/>
	</span>
</div>
<div style="clear:left;"></div>