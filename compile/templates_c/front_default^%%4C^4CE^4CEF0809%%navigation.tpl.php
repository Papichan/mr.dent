<?php /* Smarty version 2.6.20, created on 2019-10-03 10:44:13
         compiled from navigation.tpl */ ?>
		<ul class="main-nav" id="main-nav">
			<?php $_from = $this->_tpl_vars['MAIN_MENU']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['MENU_ITEM']):
?>
				<li class="<?php if (( $this->_tpl_vars['MENU_ACTIVE_ALIAS'] == $this->_tpl_vars['MENU_ITEM']['alias'] )): ?>active<?php endif; ?><?php if (empty ( $this->_tpl_vars['MENU_ITEM']['children'] )): ?> no-drop<?php endif; ?>">
					<?php if (empty ( $this->_tpl_vars['MENU_ITEM']['children'] )): ?>
						<a href="<?php echo $this->_tpl_vars['MENU_ITEM']['fullAlias']; ?>
" <?php if ($this->_tpl_vars['MENU_ITEM']['external']): ?> target="_blank"<?php endif; ?>><?php echo $this->_tpl_vars['MENU_ITEM']['caption']; ?>
</a>
					<?php else: ?>
						<a href="<?php echo $this->_tpl_vars['MENU_ITEM']['fullAlias']; ?>
"><?php echo $this->_tpl_vars['MENU_ITEM']['caption']; ?>
</a>
						<ul>
							<?php $_from = $this->_tpl_vars['MENU_ITEM']['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['menu']):
?>
								<li class="<?php if ($this->_tpl_vars['menu']['pageID'] == $this->_tpl_vars['PAGE']['pageID']): ?>active<?php endif; ?>">
									<a href="<?php echo $this->_tpl_vars['menu']['fullAlias']; ?>
" <?php if ($this->_tpl_vars['menu']['external']): ?> target="_blank"<?php endif; ?>><?php echo $this->_tpl_vars['menu']['caption']; ?>
</a>
								</li>
							<?php endforeach; endif; unset($_from); ?>
							<li class="last"></li>
						</ul>
					<?php endif; ?>
				</li>
			<?php endforeach; endif; unset($_from); ?>
		</ul>