<?php /* Smarty version 2.6.20, created on 2019-10-14 18:02:10
         compiled from contacts/index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'debug', 'contacts/index.tpl', 1, false),)), $this); ?>
<?php echo smarty_function_debug(array(), $this);?>


<main>
    <!-- ФОРМА ОТПРАВКИ ВЕРХ -->
    <section  class="consult  consult__contacts">

        <div class="content-wrapper consult__wrapper">
            <div class="consult__img">
                <img  src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['topImage']; ?>
" alt="">
            </div>
            <div class="consult__left">
                <ul class="breadcrumbs consult__breadcrubms">
                    <li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
                    <li><a class="current-link">Контакты</a></li>
                </ul>
                <div class="consult__text">
                    <?php echo $this->_tpl_vars['PAGE_CONTENT']['titleTop']; ?>

                </div>
            </div>
            <div class="consult__right">
                <div id="appointment" class="form-wrapper consult__form">
                    <!-- Загрузка формы отправки "Бесплатная консультация" -->
                    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-consultation.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            </div>
        </div>
        </div>
    </section>



    <!-- ТЕКСТ -->
    <section class="promotion promotion__contacts">
        <h2 class="visually-hidden"><?php echo $this->_tpl_vars['PAGE_CONTENT']['title']; ?>
</h2>
        <div class="content-wrapper promotion__wrapper">
            <h2 class="promotion__title"><?php echo $this->_tpl_vars['PAGE_CONTENT']['title']; ?>
</h2>
            <p class="promotion__text"><?php echo $this->_tpl_vars['PAGE_CONTENT']['text']; ?>
</p>
        </div>
    </section>



    <!-- О НАС, КОНТАКТЫ, РЕКВИЗИТЫ, ТЕЛЕФОН. -->
    <section  class="column2 card-doctor column2__contacts">
        <h2 class="visually-hidden">Карта проезда</h2>
        <div class="content-wrapper  column2__wrapper">
            <div class="column2__left">
                <iframe src="<?php echo $this->_tpl_vars['SETTINGS']['front']['YandexMapContacts']; ?>
" width="100%" height="439" frameborder="0"></iframe>
                <!--<iframe src="https://yandex.ua/map-widget/v1/?um=constructor%3Acba5c5b98d71d61f9a1f93de3f46fdfd168eefd035f476d9b3540980e95e312d&amp;source=constructor" width="100%" height="439" frameborder="0"></iframe> -->
            </div>

            <div class="column2__right">
                <p>Звоните сейчас</p>
                <br>
                <div class="column2__tel1">
                    <a href="tel:<?php echo $this->_tpl_vars['SETTINGS']['general']['telephone']; ?>
"><?php echo $this->_tpl_vars['SETTINGS']['general']['telephone']; ?>
</a>
                </div> <br>
                <div class="column2__tel2">
                    <a href="<?php echo $this->_tpl_vars['SETTINGS']['general']['telephone']; ?>
">Перезвоните мне</a>
                </div> <br>
                <p > Наш адрес:</p><br>

                <p style="font-size: 17px"> <?php echo $this->_tpl_vars['SETTINGS']['general']['addressTown']; ?>
 <?php echo $this->_tpl_vars['SETTINGS']['general']['addressStreet']; ?>
 </p>
                <br>
                <br>
                <p> Реквизиты организации:</p><br>
                <div class="link-download">
                    <a download href="<?php echo $this->_tpl_vars['PAGE_CONTENT']['path']; ?>
"><?php echo $this->_tpl_vars['PAGE_CONTENT']['fileName']; ?>
</a>
                </div>
            </div>
        </div>
    </section>



    <!-- ФОРМА ЗАДАЙТЕ ВОПРОС ВРАЧУ -->
    <section id="doctors" class="blue-block">
        <div class="content-wrapper blue-block__wrapper">
            <div class="psevdo-wrap">
                <div class="blue__left-block block_desktop"><?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['imageIconTitle']; ?>
</div>
                <div class="blue__left-block block_tablet"><?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['imageIconTitle']; ?>
</div>
            </div>
            <div class="blue__right-block blue__text-contacts">
                <div class="blue__right-block-img">
                    <img src="<?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['dependendDoctor'][0]['imageAva']; ?>
" width="178" height="178" alt="">
                    <p><?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['dependendDoctor'][0]['name']; ?>
</p>

                    <span><?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['dependendDoctor'][0]['position']; ?>
</span>

                </div>
                <div class="blue__text blue__text-contacts">
                    <div id="appointment" class="">
                        <!-- Загрузка формы отправки "Задайте вопрос врачу" -->
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-question.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                </div>
            </div>
        </div>
        </div>
    </section>


    <!-- Дополнительные документы -->
    <section class="download">
        <h2 class="download__title"> Дополнительные документы</h2>
        <div class="content-wrapper download__wrapper">
            <?php if (! empty ( $this->_tpl_vars['PAGE_CONTENT']['files'] )): ?>
                <?php $_from = $this->_tpl_vars['PAGE_CONTENT']['files']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['file']):
?>
                    <div class="link-download">
                        <a download href="<?php echo $this->_tpl_vars['file']['path']; ?>
"><?php echo $this->_tpl_vars['file']['name']; ?>
</a>
                    </div>
                <?php endforeach; endif; unset($_from); ?>
            </div>
            <?php endif; ?>
        </div>
    </section>


    <!-- ФОРМА ОТПРАВКИ НИЗ -->
    <section id="appointment" class="form">
        <div class=" content-wrapper">
            <img src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['bottomImage']; ?>
" alt="">
            <div class="form-wrapper">
                <!-- Загрузка формы отправки "Записаться на прием" -->
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-appointment.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            </div>
        </div>
    </section>

</main>