<?php /* Smarty version 2.6.20, created on 2019-10-16 15:51:20
         compiled from doctors/index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'debug', 'doctors/index.tpl', 1, false),)), $this); ?>
<?php echo smarty_function_debug(array(), $this);?>

<main id="doctors">

    <!-- Верх -->
    <section class="up doctors">
        <div class="doctors-wrapper">
            <div class="up__back">
                <div class="up__img">
                    <img src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['topImage']; ?>
" alt="">
                </div>
                <div class="up__wrapper content-wrapper">
                    <ul class="breadcrumbs up__breadcrubms">
                        <li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
                        <li class="current-link"><a>Врачи</a></li>
                    </ul>
                    <div class="up__text">
                        <?php echo $this->_tpl_vars['PAGE_CONTENT']['titleTop']; ?>

                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Мы гордимся своими врачами -->
    <section class="column2">
        <h2 class="column2__title"><?php echo $this->_tpl_vars['PAGE_CONTENT']['title']; ?>
</h2>
        <div class="content-wrapper  column2__wrapper">

            <div class="column2__left">
                <?php if (! empty ( $this->_tpl_vars['PAGE_CONTENT']['slider'] )): ?>
                    <div class="column2__slider">
                        <?php $_from = $this->_tpl_vars['PAGE_CONTENT']['slider']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['slid']):
?>
                        <div>
                            <div class="column2__left-slide">
                                <img width="100%" src="<?php echo $this->_tpl_vars['slid']['image']; ?>
" alt="">
                            </div>
                        </div>
                        <?php endforeach; endif; unset($_from); ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="column2__right">
                <p align="left">
                    <?php echo $this->_tpl_vars['PAGE_CONTENT']['text']; ?>

                </p>
            </div>

        </div>
    </section>



    <!-- ГОЛУБОЙ БЛОК "О команде" -->
    <?php if (! empty ( $this->_tpl_vars['BLUE_BLOCKS'][0] )): ?>
    <section class="blue-block blue-block__doctors" >
        <div class="content-wrapper blue-block__wrapper">
            <div class="psevdo-wrap">
                <img src="<?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['imageIcon']; ?>
" alt="">
                <div class="blue__left-block"><?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['imageIconTitle']; ?>
</div>
            </div>
            <div class="blue__right-block">
                <div class="blue__right-block-img">
                    <img src="<?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['dependendDoctor'][0]['imageAva']; ?>
" width="190" height="190" alt="">
                    <p><?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['dependendDoctor'][0]['name']; ?>
</p>

                    <span><?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['dependendDoctor'][0]['position']; ?>
</span>

                </div>
                <div class="blue__text">
                    <p align="left">
                        <?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['textTop']; ?>

                    <div class="blue__link-wrapper">

                    </div>
                </div>

            </div>
        </div>
    </section>
    <?php endif; ?>


    
    <!-- В нашей команде только лучшие! -->
    <section class="reasons reasons__doctors">
        <div class="content-wrapper ">
            <div class=" reasons__wrapper">
                <div class="">
                    <h2 class="reasons__title"><?php echo $this->_tpl_vars['PAGE_CONTENT']['titlTwo']; ?>
</h2>
                    <p class="reasons__text"><?php echo $this->_tpl_vars['PAGE_CONTENT']['textTwo']; ?>
</p>
                    <div class="reasons__block-wrapper">

                        <!-- Цикл вывода Блоков -->
                        <?php if (! empty ( $this->_tpl_vars['BEST_TEAM'] )): ?>
                            <?php $_from = $this->_tpl_vars['BEST_TEAM']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['block']):
?>
                                <div class="reasons__block">
                                    <p class="reasons__block-text1"><?php echo $this->_tpl_vars['block']['title']; ?>
</p>
                                    <p class="reasons__block-text2"><?php echo $this->_tpl_vars['block']['text']; ?>
</p>
                                    <img src="<?php echo $this->_tpl_vars['block']['image']; ?>
" alt="">
                                </div>
                            <?php endforeach; endif; unset($_from); ?>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
    </section>


                        

    <!-- Наши специалисты -->
    <?php if (! empty ( $this->_tpl_vars['PERSONALS'] )): ?>
    <section id="personal" class="personal">
        <h2 class="personal__title">Наши специалисты</h2>
        <div class=" personal__wrapper  content-wrapper">

            <?php $_from = $this->_tpl_vars['PERSONALS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['person']):
?>
            <div class="personal__item">
                <div class="personal__av">
                    <img  src="<?php echo $this->_tpl_vars['person']['imageAva']; ?>
" alt="">
                </div>
                <div class="personal__text-wrapper">
                    <p class="personal__name"><?php echo $this->_tpl_vars['person']['name']; ?>
 <br> <span class="personal__spec"><?php echo $this->_tpl_vars['person']['profession']; ?>
</span></p>

                    <div class="personal__button">
                        <a href="<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
card-doctor/<?php echo $this->_tpl_vars['person']['personalID']; ?>
" class="button button--blue">Подробнее <span>>></span></a>
                    </div>
                </div>
            </div>
            <?php endforeach; endif; unset($_from); ?>

        </div>
    </section>
    <?php endif; ?>




    <!-- ФОРМА ОТПРАВКИ НИЗ -->
    <section id="appointment" class="form">
        <div class=" content-wrapper">
            <img src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['bottomImage']; ?>
" alt="">
            <div class="form-wrapper">
                <!-- Загрузка формы отправки "Записаться на прием" -->
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-appointment.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            </div>
        </div>
    </section>

</main>