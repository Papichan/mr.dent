<?php /* Smarty version 2.6.20, created on 2019-10-18 14:21:35
         compiled from /var/www/dent/modules/NewsLine/templates/front/index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', '/var/www/dent/modules/NewsLine/templates/front/index.tpl', 15, false),)), $this); ?>

<!-- СТАРОЕ
<div class="block-hold">
	<?php if (! empty ( $this->_tpl_vars['news'] )): ?>
		<ul class="list-news">
			<?php $_from = $this->_tpl_vars['news']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
			<li>
				<a <?php if (! $this->_tpl_vars['item']['isBody']): ?>href="<?php echo $this->_tpl_vars['item']['fullAlias']; ?>
"<?php endif; ?> class="figure">
					<?php if (! empty ( $this->_tpl_vars['item']['srcSmall'] )): ?>
						<img src="<?php echo $this->_tpl_vars['item']['srcSmall']; ?>
" width="220" />
					<?php else: ?>
						<img src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
noimage.jpg" alt="image" width="220" height="165" />
					<?php endif; ?>
				</a>
					<span class="date"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']['publicationDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %m %Y", "", 'rus') : smarty_modifier_date_format($_tmp, "%e %m %Y", "", 'rus')); ?>
</span>
					<a class="title-news" <?php if (! $this->_tpl_vars['item']['isBody']): ?>href="<?php echo $this->_tpl_vars['item']['fullAlias']; ?>
"<?php endif; ?>><?php echo $this->_tpl_vars['item']['title']; ?>
</a>
					<p><?php echo $this->_tpl_vars['item']['summary']; ?>
</p>
				<?php if (! $this->_tpl_vars['item']['isBody']): ?><a href="<?php echo $this->_tpl_vars['item']['fullAlias']; ?>
">Подробнее</a><?php endif; ?>
			</li>
			<?php endforeach; endif; unset($_from); ?>
		</ul>
		<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "paginator.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<?php endif; ?>
</div>
-->

<main>


	<!-- Хлеб Крошки -->
	<section class="up  up__news">
		<div class=" content-wrapper up__article">

			<ul class="breadcrumbs up__breadcrubms">
				<li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
				<li><a href="/<?php echo $this->_tpl_vars['PAGE']['alias']; ?>
/">Статьи</a></li>

			</ul>

		</div>
	</section>




	<!-- ВЫВОД ВСЕХ НОВОСТЕЙ -->
	<?php if (! empty ( $this->_tpl_vars['news'] )): ?>
	<section class="news-list content-wrapper">
		<h2 class="news-list__title">Статьи</h2>

		<div class="news-list__wrapper">

			<?php $_from = $this->_tpl_vars['news']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['item']):
?>
			<div class="news-list__item">
				<div class="news-list__item-img">
					<img width="382" height="260" src="<?php echo $this->_tpl_vars['item']['src']; ?>
" alt="">
				</div>

				<div class="news-list__item-text">
					<h3 class="news-list__item-title"><?php echo $this->_tpl_vars['item']['title']; ?>
</h3>
					<p><?php echo $this->_tpl_vars['item']['summary']; ?>
</p>
					<div class="news-list__item-button">
						<a href="<?php echo $this->_tpl_vars['item']['fullAlias']; ?>
" class="button button--transparent">Подробнее <span>>></span></a>
					</div>
				</div>
			</div>
			<?php endforeach; endif; unset($_from); ?>

		</div>
	</section>
	<?php endif; ?>



	
<!-- ПАГИНАЦИЯ -->
  <div class="news__pages">
    <!--
    <ul>
      <li class="page-current"><a href="">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
    </ul>
  -->
  <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'paginator.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
  </div>
  



	<!-- ФОРМА ОТПРАВКИ НИЗ-->
	<section id="appointment" class="form">
		<div class=" content-wrapper">
                    <img src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
form-image.png" alt="">
                    <div class="form-wrapper">
		        <!-- Загрузка формы отправки "Записаться на прием" -->
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-appointment.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    </div>
		</div>
	</section>

</main>