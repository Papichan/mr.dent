<?php /* Smarty version 2.6.20, created on 2019-10-07 12:04:28
         compiled from price/index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'debug', 'price/index.tpl', 1, false),)), $this); ?>
<?php echo smarty_function_debug(array(), $this);?>


<main >

    <!-- ФОРМА ОТПРАВКИ ВЕРХ -->
    <section  class="consult  consult__contacts">
        <div class="content-wrapper consult__wrapper">
            <div class="consult__img">

                <img src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['topImage']; ?>
" alt="">
            </div>
            <div class="consult__left">
                <ul class="breadcrumbs consult__breadcrubms">
                    <li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
                    <li><a class="current-link">Цены</a></li>
                </ul>
                <div class="consult__text">
                    <div class="consult__text1"> </div>
                    <div class="consult__text2"><?php echo $this->_tpl_vars['PAGE_CONTENT']['titleTop']; ?>
</div>
                </div>
            </div>
            <div class="consult__right">
                <div id="appointment" class="form-wrapper consult__form">
                    <!-- Загрузка формы отправки "Бесплатная консультация" -->
                    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-consultation.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            </div>
        </div>
        </div>
    </section>



<!-- СПИСОК ЦЕН НА УСЛУГИ -->
    <section class="price-block price-block--white">
        <div class="content-wrapper">
            <h2 class="price-block__title">Стоимость услуг</h2>
            <div class="price-block__wrapper">
                <table class="price-block__table">
                    <tr class="price-block__row-title">
                        <th class="price-block__column1 price-block__title1 ">Услуга</th>
                        <th class="price-block__title2">Описание</th>
                        <th class="price-block__title3">Стоимость</th>
                    </tr> <!--ряд с ячейками заголовков-->
                    <?php $_from = $this->_tpl_vars['PRICES']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['price']):
?>
                        <tr class="price-block__row">
                            <td class="price-block__column1"><?php echo $this->_tpl_vars['price']['title']; ?>
</td>
                            <td class="price-block__column2"><?php echo $this->_tpl_vars['price']['description']; ?>
</td>
                            <td class="price-block__column3">от <?php echo $this->_tpl_vars['price']['cost']; ?>
 ₽</td>
                        </tr>
                    <?php endforeach; endif; unset($_from); ?>
                </table>

            </div>

        </div>
    </section>



<!-- ФОРМА ОТПРАВКИ "Записаться на прием" С НИЗУ -->
    <section id="appointment" class="form  form__chair">
        <div class=" content-wrapper">
            <img src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['bottomImage']; ?>
" alt="">
            <div class="form-wrapper">
                <!-- Загрузка формы отправки "Записаться на прием" -->
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-appointment.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            </div>
        </div>
    </section>
</main>
