<?php /* Smarty version 2.6.20, created on 2019-10-18 14:20:20
         compiled from /var/www/dent/modules/NewsLine/templates/front/item.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', '/var/www/dent/modules/NewsLine/templates/front/item.tpl', 12, false),)), $this); ?>

<!--=========================================================== ======================================================-->
<!--
<div class="group-info">
	<?php if (! empty ( $this->_tpl_vars['news']['src'] )): ?>
		<figure class="baner-hold">
			<img src="<?php echo $this->_tpl_vars['news']['src']; ?>
" alt="image" width="480" height="320" />
		</figure>
	<?php endif; ?>
	<?php echo $this->_tpl_vars['news']['body']; ?>

</div>
<?php echo ((is_array($_tmp=$this->_tpl_vars['news']['publicationDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e %m %Y", "", 'rus') : smarty_modifier_date_format($_tmp, "%e %m %Y", "", 'rus')); ?>

<br/><br/>
<?php if (! empty ( $this->_tpl_vars['news']['gallery'] )): ?>
	<div class="block-hold">
		<h2>ФОТО</h2>
		<ul class="photo">
			<?php $_from = $this->_tpl_vars['news']['gallery']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['photo']):
?>
				<li>
					<a data-rel="1" href="<?php echo $this->_tpl_vars['photo']['src']; ?>
" class="link-photo fancybox">
						<span class="text-center">
							<span class="text-inner">
								<span class="text-last">
									<img src="<?php echo $this->_tpl_vars['photo']['srcSmall']; ?>
" alt="image" width="90" height="59" />
								</span>
							</span>
						</span>
					</a>
				</li>
			<?php endforeach; endif; unset($_from); ?>
			<li class="last"></li>
		</ul>
	</div>
<?php endif; ?>

<a href="/<?php echo $this->_tpl_vars['ROOT_ALIAS']; ?>
/<?php if ($this->_tpl_vars['news']['alias'] !== null): ?><?php echo $this->_tpl_vars['news']['alias']; ?>
/<?php endif; ?>">Вернуться в раздел</a>
-->
<!--=========================================================== ======================================================-->

<main>

	<h1 class="visually-hidden"> Центр стоматологии «Mr.Dent»</h1>
	<!-- ХЛЕБ.КРОШКИ -->
	<div class="content-wrapper up__article">
		<ul class="breadcrumbs up__breadcrubms">
			<li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
			<li><a href="<?php echo $this->_tpl_vars['BREADCRUMBS'][0]['alias']; ?>
">Статьи<span style="color: #42c6d1;">></span></a></li>
			<li><a class="current-link"><?php echo $this->_tpl_vars['news']['title']; ?>
</a></li>
		</ul>
	</div>

                
	<!-- НАЧАЛО КАРТИНКА(БЛОШАЯ) -->
	<article class="article  content-wrapper ">
		<div class="content-wrapper article__background">
			<img width="100%" height="auto" src="<?php echo $this->_tpl_vars['news']['bigImage']; ?>
" alt="">
                        <!--<img width="100%" height="auto" src="/getimage/<?php echo $this->_tpl_vars['news']['bigImageID']; ?>
/315x215/" alt="">-->
		</div>






		<!-- СТАТЬЯ ЗАГОЛОВОК -->
		<!--
		<section class="article__part1 ">
			<h2 class="article__title"><?php echo $this->_tpl_vars['news']['title']; ?>
</h2>
			<p>
				Среди самых частых проблем, с которыми сталкиваются современные люди, можно выделить:
			</p>
		</section>
-->

		<!-- СТАТЬЯ -->
		<section class="article__part2 ">
			<h2 class="article__title"><?php echo $this->_tpl_vars['news']['title']; ?>
</h2>
			<div ><img src="<?php echo $this->_tpl_vars['news']['smallImage']; ?>
" width="534" height="377" alt="" class="article__left-img">
<!--
				<div class="article__part2-wrap">Среди самых частых проблем, с которыми сталкиваются современные люди, можно выделить:
					<ul class="article__part2-list">
						<li>Чувствительность зубов;</li>
						<li>Потемнение эмали, цветной налет;</li>
						<li>Кариес;</li>
						<li>Склонность к воспалениям десен.</li>
					</ul>
					<?php echo $this->_tpl_vars['news']['body']; ?>

				</div>
-->
				<?php echo $this->_tpl_vars['news']['body']; ?>

			</div>
		</section>


		<!-- БЛОКИ СТАТЬИ -->
		<section class="article__part3">
			<?php if (! empty ( $this->_tpl_vars['news']['blocks'] )): ?>
				<?php $_from = $this->_tpl_vars['news']['blocks']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['block']):
?>
					<div class="article__block">
						<h3 class="article__block-title"><?php echo $this->_tpl_vars['block']['title']; ?>
</h3>
						<p><?php echo $this->_tpl_vars['block']['text']; ?>
</p>
					</div>
				<?php endforeach; endif; unset($_from); ?>
			<?php endif; ?>
		</section>



                <!-- JS СОЦ.СЕТИ и Переход по страницам -->
		<section class="article__down">
                        <!-- JavaScript СОЦ СЕТИ -->
			<div class="article__down-left">Поделиться:   <script type="text/javascript">
					<?php echo '
						(function() {
						if (window.pluso)if (typeof window.pluso.start == "function") return;
						if (window.ifpluso==undefined) { window.ifpluso = 1;
							var d = document, s = d.createElement(\'script\'), g = \'getElementsByTagName\';
							s.type = \'text/javascript\'; s.charset=\'UTF-8\'; s.async = true;
							s.src = (\'https:\' == window.location.protocol ? \'https\' : \'http\')  + \'://share.pluso.ru/pluso-like.js\';
							var h=d[g](\'body\')[0];
							h.appendChild(s);
						}})();</script>
					'; ?>

                            <div class="pluso" data-background="transparent"
                                 data-options="medium,round,line,horizontal,nocounter,theme=04"
                                 data-services="vkontakte,google,facebook">
                            </div>
                        </div>
			<!-- Переход по страницам -->
                        <div class="article__down-right">
                            
                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'paginator.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                            <!--
				<a href="" class="article__prev"><img width="62" height="62" src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
slide-left.png" alt=""></a>
				<a href="" class="article__next"><img width="62" height="62" src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
slide-right.png" alt=""></a>
			
                            </div>
		</section>
	</article>





        
        
	<!-- ФОРМА ОТПРАВКИ НИЗ -->
	<section id="appointment" class="form">
		<div class=" content-wrapper">
			<img src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
form-image.png" alt="">
			<div class="form-wrapper">
				 <!-- Загрузка формы отправки "Записаться на прием" -->
                                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-appointment.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			</div>
		</div>
	</section>



	<!-- НОВОСТИ -->
	<?php if (isset ( $this->_tpl_vars['TEN_NEWS'] )): ?>
		<section class="news">
			<h2 class="news__title">Новости</h2>
			<div  class="news__wrapper news__slider content-wrapper">

				<?php $_from = $this->_tpl_vars['TEN_NEWS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['new']):
?>
					<div>
						<div class="news__item">
							<a href="<?php echo $this->_tpl_vars['new']['fullAlias']; ?>
">
								<div class="news__item-img">
									<img src="<?php echo $this->_tpl_vars['new']['srcSmall']; ?>
" alt="">
								</div>
								<div class="news__item-title">
									<a href="<?php echo $this->_tpl_vars['new']['fullAlias']; ?>
"><?php echo $this->_tpl_vars['new']['summary']; ?>
</a></div>
								<div class="news__item-data"><?php echo ((is_array($_tmp=$this->_tpl_vars['news']['publicationDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e.%m.%Y") : smarty_modifier_date_format($_tmp, "%e.%m.%Y")); ?>
</div>
							</a>
						</div>
					</div>
				<?php endforeach; endif; unset($_from); ?>

			</div>
			<div class="news__button">
				<a href="<?php echo $this->_tpl_vars['BREADCRUMBS'][0]['alias']; ?>
" class="button button--pink">Все новости <span>>></span></a>
			</div>
		</section>
	<?php endif; ?>

</main>