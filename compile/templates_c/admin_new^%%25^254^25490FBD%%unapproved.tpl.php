<?php /* Smarty version 2.6.20, created on 2019-10-16 15:55:09
         compiled from reviews/unapproved.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'reviews/unapproved.tpl', 97, false),)), $this); ?>
<?php echo '
<script type="text/javascript">
    $(document).ready(function(){
        $(\'.approveBtn\').click(ApproveReview);
        $(\'.deleteBtn\').click(DeleteReview);
    });

    function ApproveReview(event)
    {
        var reviewID = $(this).siblings(\'input[name=reviewID]\').val();
        $.post(
            \''; ?>
<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
<?php echo 'unapproved/\',
            {
                reviewID : reviewID,
                action : "approve"
            },
            ModerateReviewResponse,
            \'json\'
        );
    }

    function DeleteReview(event)
    {
        var reviewID = $(this).siblings(\'input[name=reviewID]\').val();
        $.post(
            \''; ?>
<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
<?php echo 'unapproved/\',
            {
                reviewID : reviewID,
                action : "delete"
            },
            ModerateReviewResponse,
            \'json\'
        );
    }

    function ModerateReviewResponse(data, status)
    {
        if (!standartAJAXResponseHandler(data, status)) return;
        if (data.msg == "ok")
        {
            $.jGrowl(data.text);
            RemoveReviewRow(data.reviewID);
        };
    }

    function RemoveReviewRow(reviewID)
    {
//		alert(\'RemoveReviewRow \' + reviewID);
//		alert($(\'input[name=reviewID][value=\'+ reviewID +\']\').parent().parent())
        $(\'input[name=reviewID][value=\'+ reviewID +\']\').parent().parent().fadeOut(\'slow\',
            function()
            {
                $(this).remove();
                $(\'#moderationTable tbody tr\').removeClass(\'even\').removeClass(\'odd\');

                $(\'#moderationTable tbody tr:even\').addClass(\'even\');
                $(\'#moderationTable tbody tr:odd\').addClass(\'odd\');

//                $(\'#moderationTable tbody tr\').each(function(i)
//                {
//                    $(this).children(\'td:first\').text(i+1);
//                });
            }
        );
    }
</script>
'; ?>


<h1>Новые отзывы</h1>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'paginator.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<table class="highlightable" id="moderationTable">
    <thead>
        <tr>
            <th>№</th>
            <th>Имя</th>
            <th>Заголовок</th>
			<th>Отзыв</th>
            <th>Дата</th>
            <th style="width: 80px;">Действие</th>
        </tr>
    </thead>
    <tbody>
    <?php $_from = $this->_tpl_vars['REVIEWS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['I'] => $this->_tpl_vars['REVIEW']):
?>
        <tr>
            <td style="width: 20px;"><?php echo $this->_tpl_vars['REVIEW']['reviewID']; ?>
</td>
            <td>
                <?php echo $this->_tpl_vars['REVIEW']['name']; ?>

            </td>
            <td>
                <?php echo $this->_tpl_vars['REVIEW']['title']; ?>

            </td>
            <td>
                <?php echo $this->_tpl_vars['REVIEW']['review']; ?>

            </td>
            <td style="width: 120px;">
                <?php echo ((is_array($_tmp=$this->_tpl_vars['REVIEW']['dateAdd'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d.%m.%Y %H:%M") : smarty_modifier_date_format($_tmp, "%d.%m.%Y %H:%M")); ?>

            </td>
            <td>
                <input type="hidden" name="reviewID" value="<?php echo $this->_tpl_vars['REVIEW']['reviewID']; ?>
" />
                <span class="approveBtn"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES_ALIAS']; ?>
icons/accept.png" title="Одобрить" /></span>
                <span class="deleteBtn"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES_ALIAS']; ?>
icons/cancel.png" title="Удалить" /></span>
            </td>
        </tr>
    <?php endforeach; endif; unset($_from); ?>
    </tbody>
</table>                                                