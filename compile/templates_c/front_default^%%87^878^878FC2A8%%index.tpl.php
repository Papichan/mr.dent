<?php /* Smarty version 2.6.20, created on 2019-10-18 15:25:41
         compiled from about/index.tpl */ ?>
<main>

    
    
    
    <!-- ФОРМА ОТПРАВКИ ВЕРХ -->
    <section class="up up__company">
        <div class=" content-wrapper">
            <div class="up__img">
                <img src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['topImage']; ?>
" alt="">
            </div>
            <div class="up__wrapper">
                <div class="up__back">
                    <ul class="breadcrumbs up__breadcrubms">
                        <li><a href="index.html">Главная <span style="color: #42c6d1;">></span></a></li>
                        <li><a href="">О компании</a></li>
                    </ul>
                    <div class="up__text">
                        <?php echo $this->_tpl_vars['PAGE_CONTENT']['titleTop']; ?>

                    </div>
                    <div class="up__button">
                        <a href="#" class="button button--blue">Записаться на прием <span>>></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>




                    
                    
<!-- СТАТИСТИКА КЛИНИКИ -->
<?php if (isset ( $this->_tpl_vars['STATISTIC'] )): ?>
    <section class="statistic">
        <h2 class="statistic__title content-wrapper">Клиника в цифрах</h2>
        <div class="statistic__wrapper content-wrapper">
            <?php $_from = $this->_tpl_vars['STATISTIC']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['stat']):
?>
                <div class="statistic__block">
                    <div class="statistic__img">
                        <img src="<?php echo $this->_tpl_vars['stat']['img']; ?>
" alt="">
                    </div>
                    <div class="statistic__block-wrap">
                        <p class="statistic__number"><?php echo $this->_tpl_vars['stat']['nubmer']; ?>
</p>
                        <p class="statistic__text"><?php echo $this->_tpl_vars['stat']['title']; ?>
</p>
                    </div>
                </div>
            <?php endforeach; endif; unset($_from); ?>
        </div>
    </section>
<?php endif; ?>






<!-- ФОТО КОМАНДЫ -->
<section  class="column2 card-doctor column2__company">
    <h2 class="column2__title"><?php echo $this->_tpl_vars['PAGE_CONTENT']['titleTeam']; ?>
</h2>
    <div class="content-wrapper  column2__wrapper">
        <div class="column2__left">
            <img  src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['teamImage']; ?>
" alt="">
        </div>
        <div class="column2__right">
            <p class="column2__name"><?php echo $this->_tpl_vars['PAGE_CONTENT']['titleTeam']; ?>
</p>
            <?php echo $this->_tpl_vars['PAGE_CONTENT']['textTeam']; ?>

            <br><br>
        </div>
    </div>
</section>


            
            
            
            
<!-- ГОЛУБОЙ БЛОК БОЛИТ ЗУБ? -->
    <?php if (! empty ( $this->_tpl_vars['BLUE_BLOCKS'] )): ?>
<section class="blue-block blue-block2">
    <div class="content-wrapper blue-block__wrapper">
        <div class="psevdo-wrap">
            <img src="<?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['imageIcon']; ?>
" alt="">
            <div class="blue__left-block">Болит зуб?</div>
        </div>
        <div class="blue__right-block">
            <img src="<?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['imageAva']; ?>
" width="190" height="190" alt="">
            <div class="blue__text">
                <?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['textTop']; ?>

                <div class="blue__link-wrapper">
                    <div class="blue__link-number"><p>Бесплатная консультация</p>
                        <a href="tel:<?php echo $this->_tpl_vars['SETTINGS']['general']['telephone']; ?>
"><?php echo $this->_tpl_vars['SETTINGS']['general']['telephone']; ?>
</a>
                    </div>
                    <div class="blue__button">
                        <a href="<?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['buttonLink']; ?>
" class="button--blue"><?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['buttonTitle']; ?>
 <span>>></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>





<!-- ТЕКСТ "С любовью к своей работе" -->
<section class="promotion  promotion__company">
    <h2 class="visually-hidden"><?php echo $this->_tpl_vars['PAGE_CONTENT']['title']; ?>
</h2>
    <div class="content-wrapper promotion__wrapper">
        <h2 class="promotion__title"><?php echo $this->_tpl_vars['PAGE_CONTENT']['title']; ?>
</h2>
        <br>
        <p class="promotion__text" >
            <?php echo $this->_tpl_vars['PAGE_CONTENT']['text']; ?>

        </p>
    </div>
</section>


        
        
        
        
        
<!-- СЛАЙДЕР -->
    <?php if (! empty ( $this->_tpl_vars['SLIDER_ABOUT'] )): ?>
<section id="photo-slider" class="photo-slider">
    <div class="sl-ps content-wrapper photo-slider__wrapper photo__slider ">
        <?php $_from = $this->_tpl_vars['SLIDER_ABOUT']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['slider']):
?>
        <div>
            <div class="photo__slide">
                <div class="prev"></div>
                <div class="photo__slide-img">
                    <img width="100%" height="439" src="<?php echo $this->_tpl_vars['slider']['sliderImage']; ?>
" alt="">
                </div>
                <div class="next"></div>
            </div>
        </div>
        <?php endforeach; endif; unset($_from); ?>
    </div>
    <div class="sl-count">Фото: <span class="sl-count__num">1</span> / <span class="sl-count__total"></span></div>
</section>
    <?php endif; ?>



<!-- Преимущества нашей клиники -->
<section  class="reasons reasons__company">
    <div class="content-wrapper ">
        <div class=" reasons__wrapper">
            <div class="">
                <h2 class="reasons__title">Преимущества нашей клиники</h2>
                <p class="reasons__text"></p>
                <div class="reasons__block-wrapper">
                    <?php if (! empty ( $this->_tpl_vars['ADVANTAGE'] )): ?>
                        <?php $_from = $this->_tpl_vars['ADVANTAGE']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['article']):
?>
                    <div class="reasons__block">

                        <p class="reasons__block-text1"><?php echo $this->_tpl_vars['article']['title']; ?>
</p>
                        <p class="reasons__block-text2"><?php echo $this->_tpl_vars['article']['text']; ?>
</p>
                        <img src="<?php echo $this->_tpl_vars['article']['image']; ?>
" alt="">

                    </div>
                        <?php endforeach; endif; unset($_from); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
</section>




<!-- ВЫВОД ВСЕХ СЕРТИФИКАТОВ В ЦИКЛЕ -->
    <section id="sertificat" class="sertificat ">
        <div>
            <h2 class="sertificat__title">Награждены сертификатами</h2>
            <p class="content-wrapper">
                <?php echo $this->_tpl_vars['PAGE_CONTENT']['certificateText']; ?>

            </p>
        </div>
        <div class="sl-sf sertificat__wrapper sertificat__slider ">
            <?php if (! empty ( $this->_tpl_vars['CERTIFICATE'] )): ?>
                <?php $_from = $this->_tpl_vars['CERTIFICATE']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['sertif']):
?>
                    <div>
                        <div class="sertificat__item">
                            <div class="sertificat__av">
                                <img width="157" height="217" src="<?php echo $this->_tpl_vars['sertif']['image']; ?>
" alt="">
                            </div>
                            <div class="sertificat__text-wrapper">
                                <p class="sertificat__name"><?php echo $this->_tpl_vars['sertif']['name']; ?>
 <br> <span class="sertificat__spec"><?php echo $this->_tpl_vars['sertif']['summary']; ?>
</span></p>
                                <div class="sertificat__slide-block">
                                    <div class="sertificat__button">
                                        <a href="#sertificat1" class="simplebox  button button--blue">Увеличить <span>>></span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; endif; unset($_from); ?>
            <?php endif; ?>
        </div>
        <div class="sl-count"><span class="sl-count__num">1</span> / <span class="sl-count__total"></span></div>
    </section>
            


            

<!-- ФОРМА ОТПРАВКИ НИЗ -->
<section id="appointment" class="form  form__chair">
    <div class=" content-wrapper">
        <img src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['bottomImage']; ?>
" alt="">
        <div class="form-wrapper">
            <!-- Загрузка формы отправки "Записаться на прием" -->
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-appointment.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        </div>
    </div>
</section>

</main>