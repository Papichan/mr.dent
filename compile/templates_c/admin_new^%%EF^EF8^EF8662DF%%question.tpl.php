<?php /* Smarty version 2.6.20, created on 2019-10-15 15:28:40
         compiled from /var/www/dent/modules/Feedback/templates/admin/question.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', '/var/www/dent/modules/Feedback/templates/admin/question.tpl', 24, false),)), $this); ?>
<h1>Вопрос врачу</h1>

<div>

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'paginator.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <?php if (! empty ( $this->_tpl_vars['MESSAGES'] )): ?>
        <form action="<?php echo $this->_tpl_vars['MODULE_ALIAS']; ?>
" method="post">
            <table width="100%" class="highlightable">
                <tr>
                    <th width="5%">Номер ID</th>
                    <th width="10%">Врач</th>
                    <th width="25%">Пользователь</th>
                    <th width="30%">Сообщение</th>
                    <th width="15%">Телефон</th>
                    <th width="15%">Дата отправки</th>
                    <th width="10%">Опубликован</th>
                </tr>
                <?php $_from = $this->_tpl_vars['MESSAGES']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['I'] => $this->_tpl_vars['MESSAGE']):
?>
                    <tr class="<?php if ($this->_tpl_vars['I']%2): ?>odd<?php else: ?>even<?php endif; ?>">
                        <td style="text-align:center;"><a href="<?php echo $this->_tpl_vars['MODULE_ALIAS']; ?>
message/<?php echo $this->_tpl_vars['MESSAGE']['messageID']; ?>
/" title="Просмотреть сообщение"><?php echo $this->_tpl_vars['MESSAGE']['messageID']; ?>
</a></td>
                        <td style="text-align:center;"><?php echo $this->_tpl_vars['MESSAGE']['doctor']; ?>
</td>
                        <td><?php echo $this->_tpl_vars['MESSAGE']['userName']; ?>
 <br> <?php if ($this->_tpl_vars['MESSAGE']['userEmail']): ?>[<a href="mailto:<?php echo $this->_tpl_vars['MESSAGE']['userEmail']; ?>
"><?php echo $this->_tpl_vars['MESSAGE']['userEmail']; ?>
</a>]<?php endif; ?></td>
                        <td><?php echo ((is_array($_tmp=$this->_tpl_vars['MESSAGE']['text'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 60) : smarty_modifier_truncate($_tmp, 60)); ?>
</td>
                        <td style="text-align:center;"><?php echo $this->_tpl_vars['MESSAGE']['userPhone']; ?>
</td>
                        <td style="text-align:center;"><?php echo $this->_tpl_vars['MESSAGE']['sendDate']; ?>
</td>
                        <td style="text-align:center;" class="<?php if ($this->_tpl_vars['MESSAGE']['isModeration']): ?>success<?php else: ?>error<?php endif; ?>"><?php if ($this->_tpl_vars['MESSAGE']['isModeration']): ?>Да<?php else: ?>Нет<?php endif; ?></td>
                    </tr>
                <?php endforeach; endif; unset($_from); ?>
            </table>

        </form>
    <?php else: ?>
        Сообщений нет
    <?php endif; ?>
</div>