<?php /* Smarty version 2.6.20, created on 2019-10-03 11:40:49
         compiled from structure/page_form.tpl */ ?>
<h1><?php echo $this->_tpl_vars['HEADER']; ?>
</h1>
<?php if (! empty ( $this->_tpl_vars['RIGHTBLOCK_ENABLED'] )): ?>
    <div id="rightblock">
        <?php if (isset ( $this->_tpl_vars['MODULE_LINK'] )): ?>
            <div class="linkButton">
                <a href="<?php echo $this->_tpl_vars['MODULE_LINK']; ?>
"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
12-em-pencil.png" alt="Переидти к молулю" class="pictogramm" />[ <span style="text-decoration:underline">Управление модулем</span> ]</a>
            </div>
        <?php endif; ?>

        <?php if (! empty ( $this->_tpl_vars['DELETE_ENABLED'] ) || ! empty ( $this->_tpl_vars['CLONE_ENABLED'] )): ?>
            <div class="linkButton">
                <?php if (! empty ( $this->_tpl_vars['DELETE_ENABLED'] )): ?>
                    <a href="<?php echo $this->_tpl_vars['DELETE_SCRIPT']; ?>
" onclick="return AreYouSure();" title="Переместить в корзину | Удалить"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
delete_icon.png" alt="Переместить в корзину | Удалить" class="pictogramm" />[ <span style="text-decoration:underline">Удалить страницу</span> ]</a>
                    <?php endif; ?>
                    <?php if (! empty ( $this->_tpl_vars['CLONE_ENABLED'] )): ?>
                    <br/><br/>
                    <a href="<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
clonepage/<?php echo $this->_tpl_vars['PAGE']['pageID']; ?>
/"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
icons/page_copy.png" alt="Переместить в корзину | Удалить" class="pictogramm" />[ <span style="text-decoration:underline">Клонировать страницу</span> ]</a>
                    <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>

<form action="" method="post" enctype="multipart/form-data" id="pageForm">
    <?php if (isset ( $this->_tpl_vars['PAGE']['pageID'] )): ?>
        <div> <input type="hidden" name="pageID" value="<?php echo $this->_tpl_vars['PAGE']['pageID']; ?>
" /></div>
        <?php endif; ?>
    <div class="formBlockHeader">Служебные параметры</div>

    <div class="formBlock">
        <div class="formElement">
            <label for="page_caption">Название страницы <span class="smallLightGrey" style="display:none; margin-left:10px;">(<span id="page_caption_count"></span>)</span>
                <span class="helper" title="Служебный параметр. Название будет отображаться в списках.">?</span>
            </label>
            <br />
            <input type="text" id="page_caption" name="page_caption" class="required" size="80" value="<?php echo $this->_tpl_vars['PAGE']['caption']; ?>
" maxlength="255" /><sup style="color:red">*</sup>
        </div>

        <div class="formElement">
            <label for="page_alias">Псевдо-URL страницы <?php if (! empty ( $this->_tpl_vars['ALIAS_READONLY'] )): ?><span class="smallLightGrey">(Неизменяем)</span><?php endif; ?><span class="smallLightGrey" style="display:none; margin-left:10px;">(англ, <span id="page_alias_count"></span>)</span>
                <span class="helper" title="Адрес страницы, набираемый в адресной строке браузера.">?</span>
            </label>
            <br />
            <input type="text" id="page_alias" name="page_alias" size="80" title="выводится в адресе сайта" value="<?php echo $this->_tpl_vars['PAGE']['alias']; ?>
" maxlength="255" <?php if (! empty ( $this->_tpl_vars['ALIAS_READONLY'] )): ?>readonly="readonly"<?php endif; ?> />
        </div>
        <div class="formElement">
            <label for="page_external">Внешняя ссылка<?php if (! empty ( $this->_tpl_vars['ALIAS_READONLY'] )): ?><span class="smallLightGrey">(Неизменяем)</span><?php endif; ?><span class="smallLightGrey" style="display:none; margin-left:10px;">(англ, <span id="page_alias_count"></span>)</span>
                <span class="helper" title="Адрес страницы, набираемый в адресной строке браузера.">?</span>
            </label>
            <br />
            <input type="text" id="page_external" name="page_external" size="80" title="выводится в адресе сайта" value="<?php echo $this->_tpl_vars['PAGE']['external']; ?>
" maxlength="255" <?php if (! empty ( $this->_tpl_vars['ALIAS_READONLY'] )): ?>readonly="readonly"<?php endif; ?> />
        </div>

        <div class="formElement">
            <label for="page_parerntID">Родительский раздел <span class="helper" title="Какому разделу принадлежит страница.">?</span>
            </label>
            <br />
            <select name="page_parentID" id="page_parerntID" style="width:507px;">
                <option value="0"> - </option>
                <?php $_from = $this->_tpl_vars['PARENT_OPTION']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['OPTION']):
?>
                    <option value="<?php echo $this->_tpl_vars['OPTION']['value']; ?>
"<?php if ($this->_tpl_vars['OPTION']['value'] == $this->_tpl_vars['PAGE']['parentID']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['OPTION']['text']; ?>
</option>
                <?php endforeach; endif; unset($_from); ?>
            </select>
        </div>

        <div class="formElement">
            <label for="page_pageTypeID">Тип страницы <span class="helper" title="От типа страницы зависит её содержимое.  К странице может быть подключён модуль.  В зависимости от типа меняется форма редактирования   содержимого (появляется ниже).">?</span>
            </label><br />
            <select name="page_pageTypeID" id="page_pageTypeID" <?php if (! empty ( $this->_tpl_vars['DISABLE_PAGE_TYPES_OPTION'] )): ?>disabled="disabled"<?php endif; ?>>
                <option value="0">  </option>
                <?php $_from = $this->_tpl_vars['PAGE_TYPES_OPTION']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['OPTION']):
?>
                    <option value="<?php echo $this->_tpl_vars['OPTION']['pageTypeID']; ?>
"<?php if ($this->_tpl_vars['PAGE']['pageTypeID'] == $this->_tpl_vars['OPTION']['pageTypeID']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['OPTION']['description']; ?>
</option>
                <?php endforeach; endif; unset($_from); ?>
            </select><span id="loadBar" style="display:none"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
loading.gif" style="margin:0 3px -3px 5px" alt="Загрузка"/>Загрузка</span>
        </div>

        <div style="height:30px; margin-top:25px">
            <div class="formInlineElement">
                <input type="hidden" name="page_isSection" value="0" />
                <input type="checkbox" title="Поставьте галку, чтобы страница  стала родительским разделом." name="page_isSection" id="page_isSection" style="cursor:pointer" value="1" <?php if ($this->_tpl_vars['PAGE']['isSection']): ?>checked="checked"<?php endif; ?> />
                <label for="page_isSection"  title="Поставьте галку, чтобы страница  стала родительским разделом.">Раздел</label>
            </div>

            <div class="formInlineElement">
                <input type="hidden" name="page_isActive" value="0" />
                <input type="checkbox" title="Уберите галку, если хотите, чтобы страница не отображалась на сайте" name="page_isActive" id="page_isActive" style="cursor:pointer" value="1" <?php if ($this->_tpl_vars['PAGE']['isActive']): ?>checked="checked"<?php endif; ?>/>
                <label for="page_isActive" title="Уберите галку, если хотите, чтобы страница не отображалась на сайте" >Активна?</label>
            </div>

            <div class="formInlineElement">
                <label for="page_priority">Порядок
                </label>
                <input type="text" size="5" value="<?php echo $this->_tpl_vars['PAGE']['priority']; ?>
" name="page_priority" id="page_priority" />
                <span class="helper" title="Число определяет порядок вывода страницы в списках.  Чем больше число, тем выше страница в списке.">?</span>
            </div>
        </div>


        <div style="height:30px; margin-top:10px">
            <?php if (! empty ( $this->_tpl_vars['SHOW_MAINMENU_CHECKBOX'] )): ?>
                <div class="formInlineElement">
                    <input type="checkbox" title="Поставьте галку, чтобы страница попала в главное меню." name="showInMainMenu" id="showInMainMenu" style="cursor:pointer" value="1" <?php if (! empty ( $this->_tpl_vars['SHOW_PAGE_IN_MAIN_MENU'] )): ?>checked="checked"<?php endif; ?> />
                    <label for="showInMainMenu"  title="Поставьте галку, чтобы страница попала в главное меню.">Включить в главное меню</label>
                </div>
            <?php endif; ?>

            <div class="formInlineElement">
                <input type="hidden" name="page_isLogin" value="0" />
                <input type="checkbox" title="Уберите галку, если хотите, чтобы страница была доступна всем" name="page_isLogin" id="page_isLogin" style="cursor:pointer" value="1" <?php if ($this->_tpl_vars['PAGE']['isLogin']): ?>checked="checked"<?php endif; ?>/>
                <label for="page_isLogin" title="Уберите галку, если хотите, чтобы страница была доступна всем" >Запаролить</label>
            </div>

            <div class="formInlineElement">
                <input type="hidden" name="page_isNoindex" value="0" />
                <input type="checkbox" title="Уберите галку, если хотите, чтобы страница была доступна всем" name="page_isNoindex" id="page_isNoindex" style="cursor:pointer" value="1" <?php if ($this->_tpl_vars['PAGE']['isNoindex']): ?>checked="checked"<?php endif; ?>/>
                <label for="page_isNoindex" title="Уберите галку, если хотите, чтобы страница была доступна всем" >noindex</label>
            </div>
        </div>

    </div>

    <div id="contentFormContainer" <?php if (! empty ( $this->_tpl_vars['HIDE_CONTENT_FORM'] )): ?>style="display:none;"<?php endif; ?>>
        <?php if (! isset ( $this->_tpl_vars['FORM_BLOCKS'] )): ?>
            <div class="formBlockHeader">Содержимое</div>
        <?php else: ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'formBlocks.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        <?php endif; ?>
    </div>

    <div class="formBlockHeader">Мета-информация</div>
    <div class="formBlock">
        <div class="formElement">
            <label for="page_title">Заголовок окна браузера
                <span class="smallLightGrey" style="display:none; margin-left:10px;">(<span id="page_title_count"></span>)</span>
                <span class="helper" title="Для поисковых систем.  Выводится в заголовке окна браузера">?</span>
            </label><br />
            <input type="text" id="page_title" name="page_title" size="95" value="<?php echo $this->_tpl_vars['PAGE']['title']; ?>
" maxlength="255" />
        </div>

        <div class="formElement">
            <label for="page_description">Описание
                <span class="smallLightGrey" style="display:none; margin-left:10px;">(<span id="page_description_count"></span>)</span>
                <span class="helper" title="Для поисковых систем.  Выводится в META-теге description">?</span>
            </label><br />
            <input type="text" id="page_description" name="page_description" size="95" value="<?php echo $this->_tpl_vars['PAGE']['description']; ?>
" maxlength="255" />
        </div>

        <div class="formElement">
            <label for="page_keywords">Ключевые слова через запятую
                <span class="smallLightGrey" style="display:none; margin-left:10px;">(<span id="page_keywords_count"></span>)</span>
                <span class="helper" title="Для поисковых систем.  Выводится в META-теге keywords">?</span>
            </label><br />
            <input type="text" id="page_keywords" name="page_keywords" size="95" value="<?php echo $this->_tpl_vars['PAGE']['keywords']; ?>
" maxlength="255" />
        </div>
        <div class="formElement">
            <label for="page_modified">Дата посдеднего редактирования
            </label><br />
            <input type="text" readonly="true"  size="95" value="<?php echo $this->_tpl_vars['PAGE']['modified']; ?>
" maxlength="255" />
        </div>
    </div>

    <div style="clear: both; text-align: center;"><input type="submit" class="handyButton" value="Сохранить" name="<?php echo $this->_tpl_vars['SUBMIT_NAME']; ?>
" /></div>
</form>

<div id="submitResult1" style="float:left; display:none;">Страница успешно сохранена. Вы можете <a href="">продолжить редактирование</a>.</div>
<div id="submitResult2" style="float:left; display:none;">Страница успешно сохранена.<br/>Необходима перезагрузка формы.<br/>Она произойдёт автоматически или вы можете <a href="">перезагрузить её сами</a>.</div>