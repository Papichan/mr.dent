<?php /* Smarty version 2.6.20, created on 2019-10-17 13:01:09
         compiled from /var/www/dent/modules/GreenyShop/templates/admin/orders.tpl */ ?>
<h1>Список заказов</h1>
<div id="filters">
	<form method="post" action="<?php echo $this->_tpl_vars['MODULE_ALIAS']; ?>
orders" id="filtersForm">
		<div class="filtersFormControl">
			<label for="filter_FIO">ФИО клиента:</label>
			<input type="text" name="filter[FIO]" id="filter_FIO" value="<?php if (isset ( $this->_tpl_vars['FILTERS']['FIO'] )): ?><?php echo $this->_tpl_vars['FILTERS']['FIO']; ?>
<?php endif; ?>" title="Часть фамилии имени или отчества.&lt;br/&gt;Введите больше 3-х символов." />
		</div>
		<div class="filtersFormControl">
			<label for="filter_creationDateLeft">Дата создания от:</label>
			<input type="text" name="filter[creationDateLeft]" id="filter_creationDateLeft" value="<?php if (isset ( $this->_tpl_vars['FILTERS']['creationDateLeft'] )): ?><?php echo $this->_tpl_vars['FILTERS']['creationDateLeft']; ?>
<?php endif; ?>" title="Начальная дата диапазона" style="width:75px" />
			<label for="filter_creationDateRight">до:</label>
			<input type="text" name="filter[creationDateRight]" id="filter_creationDateRight" value="<?php if (isset ( $this->_tpl_vars['FILTERS']['creationDateRight'] )): ?><?php echo $this->_tpl_vars['FILTERS']['creationDateRight']; ?>
<?php endif; ?>" title="Конечная дата диапазона"  style="width:75px" />
		</div>
		<div class="filtersFormControl">
			<label for="filter_statuses">Статусы:</label>
			<select name="filter[statuses][]" id="filter_statuses"  multiple="multiple">
				<?php $_from = $this->_tpl_vars['STATUSES']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['STATUS']):
?>
				<option value="<?php echo $this->_tpl_vars['STATUS']['statusID']; ?>
" <?php if (isset ( $this->_tpl_vars['STATUS']['selected'] )): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['STATUS']['description']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
		</div>
		<div style="float:left"><input type="submit" name="doFilter" value=" &gt;&gt;&gt; " id="doFilter" title="Отфильтровать"/></div>		
	</form>	
</div>

<div id="clearFilters"><a href="#">[ Снять фильтры ]</a></div>

<?php if ($this->_tpl_vars['ORDERS']): ?>
	<?php if (isset ( $this->_tpl_vars['PAGINATOR'] )): ?>
	<div class="paginator">
		<span><?php echo $this->_tpl_vars['PAGINATOR']['itemsOnPage']; ?>
</span> 
		<a href="<?php echo $this->_tpl_vars['PAGINATOR']['firstLink']; ?>
">1</a>  
		<a href="<?php echo $this->_tpl_vars['PAGINATOR']['secondLink']; ?>
">2</a> 
		<span><?php echo $this->_tpl_vars['PAGINATOR']['current']; ?>
</span>/<span><?php echo $this->_tpl_vars['PAGINATOR']['total']; ?>
</span>
		<?php if ($this->_tpl_vars['PAGINATOR']['nextLink']): ?> <a href="<?php echo $this->_tpl_vars['PAGINATOR']['nextLink']; ?>
">След</a><?php endif; ?>
	</div>
	<?php endif; ?>
	<div style="clear:left;"></div>

	<form action="<?php echo $this->_tpl_vars['MODULE_ALIAS']; ?>
groupoperation" method="post">
	<table id="orders" class="highlightable">
	<tr>
		<th style="width:3%">Номер заказа</th>
		<th style="width:15%">Дата создания</th>
		<th style="width:5%">Дата доставки</th>
		<th style="width:25%">Статус</th>
		<th style="width:25%">Покупатель</th>
		<th style="width:25%">Метка</th>
		<th style="width:1%" title="Групповые операции"></th>
		<th style="width:1%" title="Откть в новом окне"><img src="/images/icons/bullet_go.png" alt="Открыть в новом окне"/></th>
	</tr>
	<?php $_from = $this->_tpl_vars['ORDERS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ORDER']):
?>
	<tr class="<?php echo $this->_tpl_vars['ORDER']['status']; ?>
">
		<td><a href="<?php echo $this->_tpl_vars['ORDER']['LINK']; ?>
"><?php echo $this->_tpl_vars['ORDER']['orderID']; ?>
</a></td>
		<td><?php echo $this->_tpl_vars['ORDER']['creationTime']; ?>
</td>
		<td><?php echo $this->_tpl_vars['ORDER']['deliveryDate']; ?>
</td>
		<td><?php echo $this->_tpl_vars['ORDER']['statusDesc']; ?>
</td>
		<td><?php echo $this->_tpl_vars['ORDER']['FIO']; ?>
</td>
		<td><?php echo $this->_tpl_vars['ORDER']['tip']; ?>
</td>
		<td><input type="checkbox" name="groupOperation[]" value="<?php echo $this->_tpl_vars['ORDER']['orderID']; ?>
" /></td>
		<td><a href="<?php echo $this->_tpl_vars['ORDER']['LINK']; ?>
" class="openInNewWin" title="Открыть в новом окне"><img src="/images/icons/bullet_go.png" alt="Открыть в новом окне"  title="Открыть в новом окне" /></a></td>
	</tr>
	<?php endforeach; endif; unset($_from); ?>
	</table>

	<div style="margin-top: 20px; float:right;">
		<a href="#" id="selectAll">Выделить всё</a> |
		<a href="#" id="deselectAll">Снять выделение</a> |
		<a href="#" id="invertSelection">Инвертировать выделение</a>
	</div>
	
	
	<?php if (isset ( $this->_tpl_vars['PAGINATOR'] )): ?>
	<div class="paginator">
		<span><?php echo $this->_tpl_vars['PAGINATOR']['itemsOnPage']; ?>
</span> 
		<a href="<?php echo $this->_tpl_vars['PAGINATOR']['firstLink']; ?>
">1</a>  
		<a href="<?php echo $this->_tpl_vars['PAGINATOR']['secondLink']; ?>
">2</a> 
		<span><?php echo $this->_tpl_vars['PAGINATOR']['current']; ?>
</span>/<span><?php echo $this->_tpl_vars['PAGINATOR']['total']; ?>
</span>
		<?php if ($this->_tpl_vars['PAGINATOR']['nextLink']): ?> <a href="<?php echo $this->_tpl_vars['PAGINATOR']['nextLink']; ?>
">След</a><?php endif; ?>
	</div>
	<?php endif; ?>
	
	<div style="clear:left; margin-top:30px; float:left;">
		<label for="withSelected">Отмеченные заказы:</label>
		<select id="withSelected" name="groupAction">
			<option value=""> - </option>
			<option value="changeStatus">Изменить статус</option>
			<option value="delete">Удалить</option>
		</select>
		<select id="changeStatus" style="display:none" name="newStatus">
			<?php $_from = $this->_tpl_vars['STATUSES']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['STATUS']):
?>
			<option value="<?php echo $this->_tpl_vars['STATUS']['statusID']; ?>
"><?php echo $this->_tpl_vars['STATUS']['description']; ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
		</select>
		<input type="submit" name="doGroupOperation" value="Вперёд" />
	</div>	
	</form>
<?php else: ?>
Заказов нет.
<?php endif; ?>