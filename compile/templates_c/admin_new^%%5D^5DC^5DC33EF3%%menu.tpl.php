<?php /* Smarty version 2.6.20, created on 2019-10-03 12:10:57
         compiled from modules/menu.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'lower', 'modules/menu.tpl', 3, false),)), $this); ?>
<ul>
	<?php $_from = $this->_tpl_vars['MODULES_MENU']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['MENU_MODULE']):
?>
		<li<?php if ($this->_tpl_vars['ACTION_ALIAS'] == ((is_array($_tmp=$this->_tpl_vars['MENU_MODULE']['name'])) ? $this->_run_mod_handler('lower', true, $_tmp) : smarty_modifier_lower($_tmp))): ?> class="active"<?php endif; ?>>
			<a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
<?php echo $this->_tpl_vars['MENU_MODULE']['name']; ?>
/"><?php echo $this->_tpl_vars['MENU_MODULE']['description']; ?>
 <?php echo $this->_tpl_vars['MENU_MODULE']['name']; ?>
</a>
			<a href="<?php echo $this->_tpl_vars['SECTION_ALIAS']; ?>
edit/<?php echo $this->_tpl_vars['MENU_MODULE']['name']; ?>
/" title="Редактировать настройки модуля  '<?php echo $this->_tpl_vars['MENU_MODULE']['description']; ?>
'" style="font-size: 80%;">[ edit ]</a>
		</li>
	<?php endforeach; endif; unset($_from); ?>
	<li <?php if ($this->_tpl_vars['ACTION_ALIAS'] == 'install'): ?> class="active"<?php endif; ?>>
		<span style="color:gray;font-size:80%;letter-spacing:-1px;">(+)</span> <a href="<?php echo $this->_tpl_vars['SECTION_ALIAS']; ?>
install/" title="Установить модуль">Установить модуль</a>
	</li>
</ul>