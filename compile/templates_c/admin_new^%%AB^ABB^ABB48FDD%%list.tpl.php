<?php /* Smarty version 2.6.20, created on 2019-10-03 12:10:48
         compiled from /var/www/dent/modules/Articles/templates/admin/list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', '/var/www/dent/modules/Articles/templates/admin/list.tpl', 28, false),array('modifier', 'truncate', '/var/www/dent/modules/Articles/templates/admin/list.tpl', 50, false),array('modifier', 'date_format', '/var/www/dent/modules/Articles/templates/admin/list.tpl', 52, false),)), $this); ?>
<h1>Список статей</h1>

<?php echo '
<script type="text/javascript">
    $(document).ready(function(){
        $(\'.deleteCheckBox\').change(EnableDisableDeleteButton);
    });
    
    function EnableDisableDeleteButton()
    {
        var checkedNumber = $(\'.deleteCheckBox:checked\').length;
        
        if (checkedNumber == 0)
            $(\'#deleteItems\').attr(\'disabled\', true);
        else
            $(\'#deleteItems\').attr(\'disabled\', false);
    }
</script>
'; ?>


<div style="text-align: right; margin: -30px 0 5px 0;">
<form action="" method="post">
    <input type="button" id="goAddNews" class="goToBtn" value="Добавить статью" />
    <input type="hidden" id="goAddNews_link" value="<?php echo $this->_tpl_vars['MODULE_ALIAS']; ?>
add/" />
</form>
</div>

<?php if (count($this->_tpl_vars['ARTICLES_LIST']) == 0): ?>
    Список статей пуст
<?php else: ?>
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'paginator.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <form action="<?php echo $this->_tpl_vars['MODULE_ALIAS']; ?>
deletes/" method="POST" onsubmit="return AreYouSure()">
    <table id="entriesList" class="highlightable" style="clear: left;">
    	<thead>
    		<tr>
    			<th width="5%">№</th>
    			<th width="20%">Заголовок статьи</th>
    			<th width="30%">Категория</th>
    			<th width="15%">Дата публикации</th>
    			<th width="15%">Дата изменения</th>
    			<th width="10%" style="text-align:center;"><span>Опубликована</span></th>
    			<th width="5%"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
delete.png" title="Поставьте галочку, если хотите удалить статью" /></th>
    		</tr>
    	</thead>
    	<tbody>
            <?php $_from = $this->_tpl_vars['ARTICLES_LIST']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['I'] => $this->_tpl_vars['ARTICLES_ITEM']):
?>
    		<tr>
                <td style="display: none;"><a href="<?php echo $this->_tpl_vars['MODULE_ALIAS']; ?>
edit/<?php echo $this->_tpl_vars['ARTICLES_ITEM']['articleID']; ?>
/"></a></td>
    			<td><?php echo $this->_tpl_vars['ARTICLES_ITEM']['articleID']; ?>
</td>
    			<td><?php echo ((is_array($_tmp=$this->_tpl_vars['ARTICLES_ITEM']['title'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 50) : smarty_modifier_truncate($_tmp, 50)); ?>
</td>
    			<td><?php echo ((is_array($_tmp=$this->_tpl_vars['ARTICLES_ITEM']['caption'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 60) : smarty_modifier_truncate($_tmp, 60)); ?>
</td>
    			<td><?php echo smarty_modifier_date_format($this->_tpl_vars['ARTICLES_ITEM']['publicationDate'], "%d.%m.%Y %H:%M"); ?>
</td>
    			<td><?php echo smarty_modifier_date_format($this->_tpl_vars['ARTICLES_ITEM']['modificationDate'], "%d.%m.%Y %H:%M"); ?>
</td>
    			<td class="skip"><?php if ($this->_tpl_vars['ARTICLES_ITEM']['isPublished'] == 1): ?><span style="color: green;">Да</span><?php else: ?><span style="color: red;">Нет</span><?php endif; ?></td>
    			<td class="skip"><input type="checkbox" name="toDelete[]" class="deleteCheckBox" value="<?php echo $this->_tpl_vars['ARTICLES_ITEM']['articleID']; ?>
" /></td>
    		</tr>
    		<?php endforeach; endif; unset($_from); ?>
    	</tbody>
    </table>
    
    <div class="deleteBtn" style="margin-top: 10px; text-align: right;">
    	<input type="submit" id="deleteItems" name="doDelete" value="Удалить отмеченные" />
    </div>
    </form>
<?php endif; ?>