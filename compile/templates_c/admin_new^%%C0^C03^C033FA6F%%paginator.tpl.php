<?php /* Smarty version 2.6.20, created on 2019-10-14 17:47:08
         compiled from paginator.tpl */ ?>

<?php if (isset ( $this->_tpl_vars['PAGINATOR'] )): ?>
     <div class="paginator">
          <ul class="number-page">
               <?php if (( $this->_tpl_vars['PAGINATOR']['pageCurrent'] > 1 )): ?>
                    <li class="prev"><a href="?page=<?php echo $this->_tpl_vars['PAGINATOR']['pageCurrent']-1; ?>
">пред</a></li>
               <?php endif; ?>
               <?php if (( ( $this->_tpl_vars['PAGINATOR']['pageCurrent']-3 ) >= 1 )): ?>
                    <li><a href="?page=1">1</a></li>
                    <?php if (( $this->_tpl_vars['PAGINATOR']['pageCurrent']-3 != 1 )): ?><li><a href="?page=<?php echo $this->_tpl_vars['PAGINATOR']['pageCurrent']-3; ?>
">...</a></li><?php endif; ?>
               <?php endif; ?>
               <?php if (( $this->_tpl_vars['PAGINATOR']['pageCurrent'] == 1 )): ?>
                    <?php $this->assign('foo_start', 1); ?>
               <?php else: ?>
                    <?php $this->assign('foo_start', $this->_tpl_vars['PAGINATOR']['pageCurrent']-2); ?>
               <?php endif; ?>
               <?php unset($this->_sections['foo']);
$this->_sections['foo']['name'] = 'foo';
$this->_sections['foo']['start'] = (int)$this->_tpl_vars['foo_start'];
$this->_sections['foo']['loop'] = is_array($_loop=$this->_tpl_vars['PAGINATOR']['pageCurrent']+3) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['foo']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['foo']['show'] = true;
$this->_sections['foo']['max'] = $this->_sections['foo']['loop'];
if ($this->_sections['foo']['start'] < 0)
    $this->_sections['foo']['start'] = max($this->_sections['foo']['step'] > 0 ? 0 : -1, $this->_sections['foo']['loop'] + $this->_sections['foo']['start']);
else
    $this->_sections['foo']['start'] = min($this->_sections['foo']['start'], $this->_sections['foo']['step'] > 0 ? $this->_sections['foo']['loop'] : $this->_sections['foo']['loop']-1);
if ($this->_sections['foo']['show']) {
    $this->_sections['foo']['total'] = min(ceil(($this->_sections['foo']['step'] > 0 ? $this->_sections['foo']['loop'] - $this->_sections['foo']['start'] : $this->_sections['foo']['start']+1)/abs($this->_sections['foo']['step'])), $this->_sections['foo']['max']);
    if ($this->_sections['foo']['total'] == 0)
        $this->_sections['foo']['show'] = false;
} else
    $this->_sections['foo']['total'] = 0;
if ($this->_sections['foo']['show']):

            for ($this->_sections['foo']['index'] = $this->_sections['foo']['start'], $this->_sections['foo']['iteration'] = 1;
                 $this->_sections['foo']['iteration'] <= $this->_sections['foo']['total'];
                 $this->_sections['foo']['index'] += $this->_sections['foo']['step'], $this->_sections['foo']['iteration']++):
$this->_sections['foo']['rownum'] = $this->_sections['foo']['iteration'];
$this->_sections['foo']['index_prev'] = $this->_sections['foo']['index'] - $this->_sections['foo']['step'];
$this->_sections['foo']['index_next'] = $this->_sections['foo']['index'] + $this->_sections['foo']['step'];
$this->_sections['foo']['first']      = ($this->_sections['foo']['iteration'] == 1);
$this->_sections['foo']['last']       = ($this->_sections['foo']['iteration'] == $this->_sections['foo']['total']);
?>
                    <?php if (( ( $this->_sections['foo']['index'] >= 1 ) && ( $this->_sections['foo']['index'] <= $this->_tpl_vars['PAGINATOR']['pageCount'] ) )): ?>
                         <?php if (( $this->_sections['foo']['index'] == $this->_tpl_vars['PAGINATOR']['pageCurrent'] )): ?>
                              <li class="active">
                                   <a><?php echo $this->_sections['foo']['index']; ?>
</a>
                              </li>
                         <?php else: ?>
                              <li>
                                   <a href="?page=<?php echo $this->_sections['foo']['index']; ?>
"><?php echo $this->_sections['foo']['index']; ?>
</a>
                              </li>
                         <?php endif; ?>
                    <?php endif; ?>
               <?php endfor; endif; ?>
               <?php if (( ( $this->_tpl_vars['PAGINATOR']['pageCurrent']+3 ) <= $this->_tpl_vars['PAGINATOR']['pageCount'] )): ?>
                    <?php if (( $this->_tpl_vars['PAGINATOR']['pageCurrent']+3 != $this->_tpl_vars['PAGINATOR']['pageCount'] )): ?><li><a href="?page=<?php echo $this->_tpl_vars['PAGINATOR']['pageCurrent']+3; ?>
">...</a></li><?php endif; ?>
                    <li><a href="?page=<?php echo $this->_tpl_vars['PAGINATOR']['pageCount']; ?>
"><?php echo $this->_tpl_vars['PAGINATOR']['pageCount']; ?>
</a></li>
               <?php endif; ?>
               <?php if (( $this->_tpl_vars['PAGINATOR']['pageCurrent'] < ( $this->_tpl_vars['PAGINATOR']['pageCount'] ) )): ?>
                    <li class="next"><a href="?page=<?php echo $this->_tpl_vars['PAGINATOR']['pageCurrent']+1; ?>
">след</a></li>
               <?php endif; ?>
          </ul>
     </div>
<?php endif; ?>



