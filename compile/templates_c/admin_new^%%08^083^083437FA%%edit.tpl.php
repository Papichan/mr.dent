<?php /* Smarty version 2.6.20, created on 2019-10-03 12:11:05
         compiled from modules/edit.tpl */ ?>
<h1>Настройки модуля "<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
" </h1>
<div style="float:right;"><a href="<?php echo $this->_tpl_vars['SECTION_ALIAS']; ?>
uninstall/<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
/" onclick="return AreYouSure('<?php if (isset ( $this->_tpl_vars['AYS_MESSAGE'] )): ?><?php echo $this->_tpl_vars['AYS_MESSAGE']; ?>
<?php endif; ?>')">[ удалить модуль ]</a></div>

<div>
    <?php if ($this->_tpl_vars['MODULE_SETTINGS']): ?>
    <form action="<?php echo $this->_tpl_vars['SECTION_ALIAS']; ?>
edit/<?php echo $this->_tpl_vars['MODULE_NAME']; ?>
/" method="post">
            <ol>
                    <?php $_from = $this->_tpl_vars['MODULE_SETTINGS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['SETTING_NAME'] => $this->_tpl_vars['SETTING']):
?>
                    <?php if ($this->_tpl_vars['SETTING']['isVisible']): ?>
                    <li>[ <strong><?php echo $this->_tpl_vars['SETTING_NAME']; ?>
</strong> ] <?php echo $this->_tpl_vars['SETTING']['description']; ?>

                            <br />
                            <input type="text" size="70" name="settings[<?php echo $this->_tpl_vars['SETTING_NAME']; ?>
]" value="<?php echo $this->_tpl_vars['SETTING']['value']; ?>
" />
                    </li>
                    <?php endif; ?>
                    <?php endforeach; endif; unset($_from); ?>
            </ol>
            <div style="clear:left; margin:25px 0 0 150px;"><input type="submit" name="doEdit" value="Обновить настройки" /></div>
    </form>
    <?php else: ?>
    Настроек нет. Нечего тут настраивать.
    <?php endif; ?>
</div>