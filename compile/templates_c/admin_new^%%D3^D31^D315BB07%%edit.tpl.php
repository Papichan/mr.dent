<?php /* Smarty version 2.6.20, created on 2019-10-03 11:42:29
         compiled from routing/edit.tpl */ ?>
<form method="POST" action="">
	<input type="hidden" name="routeID" value="<?php if (! empty ( $this->_tpl_vars['ROUTE']['routeID'] )): ?><?php echo $this->_tpl_vars['ROUTE']['routeID']; ?>
<?php endif; ?>" />
	<div>
		<label>
			Родительская ветка:<br />
			<select name="parentID">
				<option value="0">[Корень]</option>
				<?php $_from = $this->_tpl_vars['LINEAR_TREE']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ITEM']):
?>
					<option value="<?php echo $this->_tpl_vars['ITEM']['routeID']; ?>
" <?php if (! empty ( $this->_tpl_vars['ROUTE']['parentID'] ) && ( $this->_tpl_vars['ROUTE']['parentID'] == $this->_tpl_vars['ITEM']['routeID'] )): ?>selected="selected"<?php endif; ?>><?php unset($this->_sections['sect']);
$this->_sections['sect']['name'] = 'sect';
$this->_sections['sect']['loop'] = is_array($_loop=$this->_tpl_vars['ITEM']['level']+1) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['sect']['show'] = true;
$this->_sections['sect']['max'] = $this->_sections['sect']['loop'];
$this->_sections['sect']['step'] = 1;
$this->_sections['sect']['start'] = $this->_sections['sect']['step'] > 0 ? 0 : $this->_sections['sect']['loop']-1;
if ($this->_sections['sect']['show']) {
    $this->_sections['sect']['total'] = $this->_sections['sect']['loop'];
    if ($this->_sections['sect']['total'] == 0)
        $this->_sections['sect']['show'] = false;
} else
    $this->_sections['sect']['total'] = 0;
if ($this->_sections['sect']['show']):

            for ($this->_sections['sect']['index'] = $this->_sections['sect']['start'], $this->_sections['sect']['iteration'] = 1;
                 $this->_sections['sect']['iteration'] <= $this->_sections['sect']['total'];
                 $this->_sections['sect']['index'] += $this->_sections['sect']['step'], $this->_sections['sect']['iteration']++):
$this->_sections['sect']['rownum'] = $this->_sections['sect']['iteration'];
$this->_sections['sect']['index_prev'] = $this->_sections['sect']['index'] - $this->_sections['sect']['step'];
$this->_sections['sect']['index_next'] = $this->_sections['sect']['index'] + $this->_sections['sect']['step'];
$this->_sections['sect']['first']      = ($this->_sections['sect']['iteration'] == 1);
$this->_sections['sect']['last']       = ($this->_sections['sect']['iteration'] == $this->_sections['sect']['total']);
?>&nbsp;&nbsp;&nbsp;&nbsp;<?php endfor; endif; ?><?php if (empty ( $this->_tpl_vars['ITEM']['pattern'] )): ?>(Пустая строка)<?php else: ?><?php echo $this->_tpl_vars['ITEM']['pattern']; ?>
<?php endif; ?></option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
		</label>
	</div>
	<div>
		<label>
			Паттерн:<br />
			<input type="text" class="route-text" name="pattern" value="<?php if (! empty ( $this->_tpl_vars['ROUTE']['pattern'] )): ?><?php echo $this->_tpl_vars['ROUTE']['pattern']; ?>
<?php endif; ?>" />
		</label>
	</div>
	<div>
		<label>
			Контроллер:<br />
			<input type="text" class="route-text" name="controller" value="<?php if (! empty ( $this->_tpl_vars['ROUTE']['controller'] )): ?><?php echo $this->_tpl_vars['ROUTE']['controller']; ?>
<?php endif; ?>" />
		</label>
	</div>
	<div>
		<label>
			Действие:<br />
			<input type="text" class="route-text" name="action" value="<?php if (! empty ( $this->_tpl_vars['ROUTE']['action'] )): ?><?php echo $this->_tpl_vars['ROUTE']['action']; ?>
<?php else: ?>Index<?php endif; ?>" />
		</label>
	</div>
	<div>
		<label>
			Контроллер по-умолчанию:<br />
			<input type="text" class="route-text" name="defaultController" value="<?php if (! empty ( $this->_tpl_vars['ROUTE']['defaultController'] )): ?><?php echo $this->_tpl_vars['ROUTE']['defaultController']; ?>
<?php endif; ?>" />
		</label>
	</div>
	<div>
		<label>
			Действие по-умолчанию:<br />
			<input type="text" class="route-text" name="defaultAction" value="<?php if (! empty ( $this->_tpl_vars['ROUTE']['defaultAction'] )): ?><?php echo $this->_tpl_vars['ROUTE']['defaultAction']; ?>
<?php endif; ?>" />
		</label>
	</div>
	<div>
		<input type="submit" name="saveRoute" value="<?php if ($this->_tpl_vars['mode'] == 'edit'): ?>Сохранить<?php else: ?>Добавить<?php endif; ?>" />
	</div>
</form>