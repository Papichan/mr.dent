<?php /* Smarty version 2.6.20, created on 2019-10-04 11:08:00
         compiled from /var/www/dent/modules/NewsLine/templates/admin/menu.tpl */ ?>

        <ul>
            <li>
                <a href="<?php echo $this->_tpl_vars['MODULE_ALIAS']; ?>
sections/" title="Редактор категорий">Категории</a>
            </li>
            <?php if (isset ( $this->_tpl_vars['SECTIONS'] )): ?>
            <ul class="sections">
                <?php $_from = $this->_tpl_vars['SECTIONS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['SECTION']):
?>
                <li>
                    <a href="<?php echo $this->_tpl_vars['MODULE_ALIAS']; ?>
list/<?php echo $this->_tpl_vars['SECTION']['alias']; ?>
" title="Показать новости из категории «<?php echo $this->_tpl_vars['SECTION']['caption']; ?>
»">
                        <span style="<?php if (( isset ( $this->_tpl_vars['CUR_SECTION'] ) && ( $this->_tpl_vars['CUR_SECTION'] == $this->_tpl_vars['SECTION']['sectionID'] ) )): ?>font-weight: bold;<?php endif; ?>">
                            <?php echo $this->_tpl_vars['SECTION']['caption']; ?>

                        </span>
                    </a>
                </li>
                <?php endforeach; endif; unset($_from); ?>
            </ul>
            <?php endif; ?>
            <li>
                <a href="<?php echo $this->_tpl_vars['MODULE_ALIAS']; ?>
" title="Показать новости из всех категорий">Все новости</a>
            </li>
            <li>
                <a href="<?php echo $this->_tpl_vars['MODULE_ALIAS']; ?>
recycled/" title="Список удаленных новостей">Корзина (<?php echo $this->_tpl_vars['DELETED_NEWS_NUMBER']; ?>
)</a>
            </li>
        </ul>