<?php /* Smarty version 2.6.20, created on 2019-10-14 12:03:24
         compiled from modules/install.tpl */ ?>

			<h1>Управление модулями</h1>
			
			<div>
				<?php if (! empty ( $this->_tpl_vars['HAS_UNINSTALLED_MODULES'] )): ?>
					Список неустановленных модулей:<br/><br/>
					<?php $_from = $this->_tpl_vars['UNINSTALLED_MODULES']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['I'] => $this->_tpl_vars['UNINSTALLED_MODULE']):
?>
						
							<form action="<?php echo $this->_tpl_vars['SECTION_ALIAS']; ?>
install/<?php echo $this->_tpl_vars['UNINSTALLED_MODULE']; ?>
/" method="post">
								<div><label><?php echo $this->_tpl_vars['I']+1; ?>
. <?php echo $this->_tpl_vars['UNINSTALLED_MODULE']; ?>
</label>
								<input type="submit" class="submitLink" name="doInstall" value="[ установить ]" />
								</div>
							</form>
					<?php endforeach; endif; unset($_from); ?>
				<?php endif; ?>

				<?php if (! empty ( $this->_tpl_vars['NO_UNINSTALLED_MODULES'] )): ?>
					Все модули установлены.
				<?php endif; ?>
			</div>