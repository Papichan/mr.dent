<?php /* Smarty version 2.6.20, created on 2019-10-21 12:20:27
         compiled from header.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'header.tpl', 19, false),)), $this); ?>
<!--================= НАЧАЛО ОБЩЕГО HEADER ================-->
<header class="header ">
   
    <div class="header__first  header--closed header--no-js  content-wrapper">
        <div class="header__modal-overlay">

            
            <!-- ЭТО ОКНО ДЛЯ СЖАТОЙ ВЕРСИИ САЙТА (ОТОБРАЖАЕТСЯ В МАЛЕНЬКОМ ОКНЕ) -->
            <div class="header__modal">
                <div class="header__modal__back-wrapper">
                    <div class="header__modal__back">Главное меню</div>
                    <button  class="header__modal-toggle2" aria-label="Назад"></button>
                </div>
                <div class="header__modal-menu">
                    <ul>
                        
                        <!-- ЦИКЛ ВЫВОДА ГЛАВНОГО МЕНЮ ДЛЯ ВЕРСИИ МАЛЬНЬКОГО ОКНА БРАУЗЕРА -->
                        <!-- ОДНО ИСКЛЮЧЕНИЕ У РАЗДЕЛА "УСЛУГИ" ЕСТЬ УНИКАЛЬНЫЕ СТИЛИ -->
                        <?php $this->assign('count', count($this->_tpl_vars['MAIN_MENU'])); ?>
                        <?php unset($this->_sections['mySection']);
$this->_sections['mySection']['name'] = 'mySection';
$this->_sections['mySection']['start'] = (int)1;
$this->_sections['mySection']['loop'] = is_array($_loop=$this->_tpl_vars['count']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['mySection']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['mySection']['show'] = true;
$this->_sections['mySection']['max'] = $this->_sections['mySection']['loop'];
if ($this->_sections['mySection']['start'] < 0)
    $this->_sections['mySection']['start'] = max($this->_sections['mySection']['step'] > 0 ? 0 : -1, $this->_sections['mySection']['loop'] + $this->_sections['mySection']['start']);
else
    $this->_sections['mySection']['start'] = min($this->_sections['mySection']['start'], $this->_sections['mySection']['step'] > 0 ? $this->_sections['mySection']['loop'] : $this->_sections['mySection']['loop']-1);
if ($this->_sections['mySection']['show']) {
    $this->_sections['mySection']['total'] = min(ceil(($this->_sections['mySection']['step'] > 0 ? $this->_sections['mySection']['loop'] - $this->_sections['mySection']['start'] : $this->_sections['mySection']['start']+1)/abs($this->_sections['mySection']['step'])), $this->_sections['mySection']['max']);
    if ($this->_sections['mySection']['total'] == 0)
        $this->_sections['mySection']['show'] = false;
} else
    $this->_sections['mySection']['total'] = 0;
if ($this->_sections['mySection']['show']):

            for ($this->_sections['mySection']['index'] = $this->_sections['mySection']['start'], $this->_sections['mySection']['iteration'] = 1;
                 $this->_sections['mySection']['iteration'] <= $this->_sections['mySection']['total'];
                 $this->_sections['mySection']['index'] += $this->_sections['mySection']['step'], $this->_sections['mySection']['iteration']++):
$this->_sections['mySection']['rownum'] = $this->_sections['mySection']['iteration'];
$this->_sections['mySection']['index_prev'] = $this->_sections['mySection']['index'] - $this->_sections['mySection']['step'];
$this->_sections['mySection']['index_next'] = $this->_sections['mySection']['index'] + $this->_sections['mySection']['step'];
$this->_sections['mySection']['first']      = ($this->_sections['mySection']['iteration'] == 1);
$this->_sections['mySection']['last']       = ($this->_sections['mySection']['iteration'] == $this->_sections['mySection']['total']);
?>
                                <?php if (( $this->_tpl_vars['MAIN_MENU'][$this->_sections['mySection']['index']]['alias'] == 'services' )): ?>
                                    <li class="header__modal-menu-item menu1">
                                        <a data-toggle="tab2" href="#menu2">
                                            <?php echo $this->_tpl_vars['MAIN_MENU'][$this->_sections['mySection']['index']]['caption']; ?>

                                        </a>
                                    </li> 
                                <?php else: ?>
                                    <li>
                                        <a href="<?php echo $this->_tpl_vars['MAIN_MENU'][$this->_sections['mySection']['index']]['fullAlias']; ?>
">
                                            <?php echo $this->_tpl_vars['MAIN_MENU'][$this->_sections['mySection']['index']]['caption']; ?>

                                        </a>
                                    </li>
                                <?php endif; ?>
                        <?php endfor; endif; ?>
    
                    </ul>
                </div>
            </div>
            
             
                        
                        
<!--============================= МЕНЮ ДЛЯ МАЛЕНЬКОГО ОКНА БРАУЗЕРА ========================-->

            <!-- ВЫПОДАЮЩИЕ ПОДМЕНЮ ДЛЯ РАЗДЕЛА = УСЛУГИ (ВЫПАДАЮЩИЙ СПИСОК ВСЕХ РАЗДЕЛОВ СТРАНИЦЫ УСЛУГ) -->
            <div id="menu2" class="menu2 menu-main">
                <div class="header__modal__back-wrapper">
                    <button  class="header__modal-toggle4" aria-label="Назад"></button>
                    <div class="header__modal__back">Услуги</div>   
                </div>
                <ul>
                    <?php $this->assign('count', 2); ?>
                    <!-- !!! ПРОБЛЕМКА ВЫВОД ЕСЛИ НЕТУ TAB КАК ? !!! -->
                    <?php $_from = $this->_tpl_vars['SERVICES_MENU']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['submenu']):
?>
                        <?php if (( $this->_tpl_vars['count'] == 2 )): ?>
                            <li><a href="/services/<?php echo $this->_tpl_vars['submenu']['alias']; ?>
"><?php echo $this->_tpl_vars['submenu']['name']; ?>
</a></li>
                            <?php $this->assign('count', $this->_tpl_vars['count']+1); ?>
                        <?php else: ?>
                            <li><a data-toggle="tab2" href="#menu<?php echo $this->_tpl_vars['count']++; ?>
"><?php echo $this->_tpl_vars['submenu']['name']; ?>
</a></li>
                        <?php endif; ?>
                    <?php endforeach; endif; unset($_from); ?>
                </ul>
            </div>

                
            <!-- ЦИКЛ ДЛЯ ВЫВОДА ВСЕХ ПОДРАЗДЕЛОВ СТРАНИЦЫ УСЛУГ -->        
            <?php $this->assign('count', 2); ?> 
            <?php $this->assign('styleMenu', 4); ?> 
            
            <?php $_from = $this->_tpl_vars['SERVICES_MENU']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['submenu']):
?>
                 <div id="menu<?php echo $this->_tpl_vars['count']++; ?>
" class=" <?php if (( $this->_tpl_vars['styleMenu'] == 4 )): ?>menu4<?php echo $this->_tpl_vars['styleMenu']++; ?>
<?php endif; ?>  menu-main">
                    <div class="header__modal__back-wrapper">
                        <button  class="header__modal-toggle3" aria-label="Назад"></button>
                        <div class="header__modal__back"><?php echo $this->_tpl_vars['submenu']['name']; ?>
</div>
                    </div>
                    <ul>
                    <!-- У КАЖДОГО ПОДРАЗДЕЛА ЕСТЬ СВОЙ ВЫПАДАЮЩИЙ СПИСОК -->    
                    <?php $_from = $this->_tpl_vars['submenu']['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['menu']):
?>
                        <li ><a href="/services/<?php echo $this->_tpl_vars['submenu']['alias']; ?>
/<?php echo $this->_tpl_vars['menu']['alias']; ?>
"><?php echo $this->_tpl_vars['menu']['name']; ?>
</a></li >
                    <?php endforeach; endif; unset($_from); ?>
                    </ul>
                </div>
            <?php endforeach; endif; unset($_from); ?>
             
        </div>
<!--============== КОНЕЦ МЕНЮ ДЛЯ МАЛЕНЬКОГО ОКНА БРАУЗЕРА ==============-->
                
                    




                    
<!--============================= ВЫВОД МЕНЮ ОБЫЧНОГО РАЗМЕРА ========================-->       

        <!-- ЛОГО, АДРЕССА ТЕЛЕФОНЫ ГОРОД -->
        <div class="header__first-logo">
            <a href="/">
                <img class="logo1 " src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
logo.svg" width="110" height="95" alt="Логотип">
                <img class="logo2 none" src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
logo.svg" width="77" height="67" alt="Логотип">
            </a>
            <div class="header__second header__first-address">
                <?php echo $this->_tpl_vars['SETTINGS']['general']['addressTown']; ?>
<br> <?php echo $this->_tpl_vars['SETTINGS']['general']['addressStreet']; ?>

            </div>
            <div class="header__second header__first-tel">
                <div class="header__first-number">
                    <a class="telephone" href="tel:<?php echo $this->_tpl_vars['SETTINGS']['general']['telephone']; ?>
"><?php echo $this->_tpl_vars['SETTINGS']['general']['telephone']; ?>
</a>
                </div>
                <div class="header__first-order">
                    <a href="#">Заказать звонок</a>
                </div>
            </div>
            <button class="header__toggle" id="toggle-button" type="button" aria-label="Меню"> </button>
        </div>


                

        <div class="header__tablet">
            <div class="header__first-middle">
                
                <!-- ВЫВОД ГОРОД/УЛИЦА ЧАСЫ/ДНИ/РАБОТЫ -->
                <div class="header__first-work">
                    <div class="header__first-address">
                        <?php echo $this->_tpl_vars['SETTINGS']['general']['addressTown']; ?>
 <?php echo $this->_tpl_vars['SETTINGS']['general']['addressStreet']; ?>

                    </div>
                    <div class="header__first-hours">
                        <?php echo $this->_tpl_vars['SETTINGS']['general']['officeTitle']; ?>
 <?php echo $this->_tpl_vars['SETTINGS']['general']['officeHourse']; ?>

                    </div>
                </div>
                    
                    

                    
                <!-- ВЫВОД HEADER  -->
                <?php if (! empty ( $this->_tpl_vars['MAIN_MENU'] )): ?>
                        <nav class="main-nav">
                            <ul class="main-nav__list">

                                <!-- ВЫВОД ВСЕГО МЕНЮ (Имеет 2 исключения для УСЛУГИ и для ДОКТОРА у них уникальные стили) -->
                                <?php $_from = $this->_tpl_vars['MAIN_MENU']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['menu']):
?>

                                        <!-- УНИКАЛЬНЫЙ ЦИКЛ ВЫВОДА ТОЛЬКО ДЛЯ МЕНЮ = УСЛУГИ -->
                                        <?php if (( $this->_tpl_vars['menu']['fullAlias'] == '/services/' )): ?>
                                            <li class="item__services hover-shadow"><a href="<?php echo $this->_tpl_vars['menu']['fullAlias']; ?>
"><?php echo $this->_tpl_vars['menu']['caption']; ?>
</a>

                                                <div class="submenu services__submenu">
                                                    <div class="submenu__wrapper">

                                                        <!-- ЦИКЛ ПОКАЗАТЬ ВСЕ ПОДКАТЕГОРИИ СТРАНИЦЫ = УСЛУГ -->
                                                        <ul class="submenu__list">
                                                            <?php $_from = $this->_tpl_vars['SERVICES_MENU']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['service']):
?>
                                                                
                                                                <!-- Исключение для блока = gigiena (Уникальные css стили) -->
                                                                <?php if ($this->_tpl_vars['service']['alias'] == 'gigiena'): ?>
                                                                    <li class="submenu__list-item submenu__list-item--current">
                                                                        <a data-toggle="tab" href="#<?php echo $this->_tpl_vars['service']['alias']; ?>
">
                                                                            <?php echo $this->_tpl_vars['service']['name']; ?>

                                                                        </a>
							            </li>
                                                                <?php else: ?>    
                                                                    <li class="submenu__list-item">
                                                                        <a data-toggle="tab" href="#<?php echo $this->_tpl_vars['service']['alias']; ?>
">
                                                                            <?php echo $this->_tpl_vars['service']['name']; ?>

                                                                        </a>
                                                                    </li>
                                                                <?php endif; ?>
                                                            <?php endforeach; endif; unset($_from); ?>
                                                        </ul>

                                                        
                                                        
                                                        <!-- Пример работы --><!--
                                                        <ul id="therapy" class="submenu-2__list">
                                                            <li class="submenu-2__list-item"><a href="lechenie_zubov.html">Лечение кариеса</a></li>
                                                            <li class="submenu-2__list-item"><a href="#">Реставрация зубов</a></li>
                                                            <li class="submenu-2__list-item"><a href="#">Лечение пульпита</a></li>
                                                            <li class="submenu-2__list-item"><a href="#">Лечение периодонтита</a></li>
                                                        </ul>
                                                                              
                                                        <ul id="gigiena" class="submenu-2__list   active">
                                                            <li class="submenu-2__list-item" >
                                                                <a data-toggle="block" href="#uzi">
                                                                    Ультразвуковая чистка
                                                                </a>
                                                            </li>
                                                            <li class="submenu-2__list-item airflow">
                                                                <a data-toggle="block" class="airflow" href="#airflow">
                                                                    Ультразвуковая чистка + AirFlow
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <!-- -->
                                                        
                                                        <?php $this->assign('count', 1); ?> 
                                                        <!-- ЦИКЛ ПОКАЗАТЬ ВСЕ РАЗДКЛЫ ПОДКАТЕГОРИЙ СТРАНИЦЫ = УСЛУГ --> 
                                                        <div class="submenu-2">
                                                            <?php $_from = $this->_tpl_vars['SERVICES_MENU']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['service']):
?>
                                                                <!-- Исключение для блока = gigiena (уникальные css стили) -->
                                                                <?php if ($this->_tpl_vars['service']['alias'] == 'gigiena'): ?>
                                                                    <ul id="<?php echo $this->_tpl_vars['service']['alias']; ?>
" class="submenu-2__list active">
                                                                        <?php $_from = $this->_tpl_vars['service']['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['submenu']):
?>
                                                                            <li class="submenu-2__list-item">
                                                                                <a href="/services/<?php echo $this->_tpl_vars['service']['alias']; ?>
/<?php echo $this->_tpl_vars['submenu']['alias']; ?>
">
                                                                                    <?php echo $this->_tpl_vars['submenu']['name']; ?>

                                                                                </a>
                                                                            </li>
                                                                        <?php endforeach; endif; unset($_from); ?>
                                                                    </ul>
                                                                <?php else: ?>                                                                 
                                                                    <ul id="<?php echo $this->_tpl_vars['service']['alias']; ?>
" class="submenu-2__list">
                                                                        <?php $_from = $this->_tpl_vars['service']['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['submenu']):
?>
                                                                            <li class="submenu-2__list-item">
                                                                                <a href="/services/<?php echo $this->_tpl_vars['service']['alias']; ?>
/<?php echo $this->_tpl_vars['submenu']['alias']; ?>
">
                                                                                    <?php echo $this->_tpl_vars['submenu']['name']; ?>

                                                                                </a>
                                                                            </li>
                                                                        <?php endforeach; endif; unset($_from); ?>
                                                                    </ul>
                                                                <?php endif; ?>
                                                            <?php endforeach; endif; unset($_from); ?>
                                                        </div>        
                                                    </div>               
                           
                                                    <!-- Вывод текста и картинок для разделов категорий страницы "Услуги" -->    
                                                    <div class="submenu-3">

                                                        <div id="uzi" class="submenu__block">
                                                            <img width="340" height="192" src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
image1.png" alt="">
                                                            <p>
                                                                Lorem ipsum dolor sit amet, consectetur adipisicing elit. 
                                                                Sit accusantium qui fugiat consequatur laboriosam voluptas autem sint, 
                                                                asperiores possimus nostrum delectus soluta molestiae accusamus eveniet velit, 
                                                                dignissimos omnis in maxime!
                                                            </p>
                                                        </div>

                                                        <div id="airflow" class="submenu__block active">
                                                            <img width="340" height="192" src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
image1.png" alt="">
                                                            <p>
                                                                Гигиеническая чистка зубов Air Flow представляет собой процедуру, при которой 
                                                                аппарат распыляет на зубы смесь из воды, воздуха и кристаллов соды под давлением.
                                                                <br>
                                                                <br>
                                                                Степень такого давления регулирует врач-стоматолог. При помощи 
                                                                данного прибора различные типы отложений удаляются не только с поверхности зуба, 
                                                                но и из межзубных промежутков.
                                                            </p>
                                                        </div>
                                                       
                                                    </div> 
                                                    <!-- -->    
                                                                         
                                       
                                                </div>
                                            </li>
                                                 
                                        <?php elseif (( ( $this->_tpl_vars['menu']['fullAlias'] != '/doctors/' ) )): ?>
                                            <!-- УНИКАЛЬНЫЕ СТИЛИ ДЛЯ СТРАНИЦЫ ДОКТОРОВ -->
                                            <li class="doctors main-nav__list-item"><a href="<?php echo $this->_tpl_vars['menu']['fullAlias']; ?>
"><?php echo $this->_tpl_vars['menu']['caption']; ?>
</a></li>
                                        <?php else: ?>
                                            <!-- ЕСЛИ НЕ УСЛУГИ И НЕ ДОКТОРА ТО ВЫВОД ВСЕГО ОСТАЛЬНОГО -->
                                            <li class="main-nav__list-item"><a href="<?php echo $this->_tpl_vars['menu']['fullAlias']; ?>
"><?php echo $this->_tpl_vars['menu']['caption']; ?>
</a>  </li>
                                        <?php endif; ?>
                                <?php endforeach; endif; unset($_from); ?>

                    </ul>
                </nav>
                <?php endif; ?>
             
            </div>


                
            <!-- ТЕЛЕФОН, ЗАКАЗАТЬ ЗВОНОК -->    
            <div class="header__first-tel">
                <div class="header__first-number">
                    <a class="telephone" href="tel:<?php echo $this->_tpl_vars['SETTINGS']['general']['telephone']; ?>
"><?php echo $this->_tpl_vars['SETTINGS']['general']['telephone']; ?>
</a>
                </div>
                <div class="header__first-order">
                    <a href="#">Заказать звонок</a>
                </div>
            </div>
                
        </div>
    </div>
</header>
<!--================================================= КОНЕЦ HEADER ================================================-->