<?php /* Smarty version 2.6.20, created on 2019-10-08 13:53:14
         compiled from lechenieZubov/index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'debug', 'lechenieZubov/index.tpl', 1, false),array('modifier', 'date_format', 'lechenieZubov/index.tpl', 315, false),)), $this); ?>
<?php echo smarty_function_debug(array(), $this);?>


<main>

    <!-- Форма отправки Верх -->
    <h1 class="visually-hidden"> Центр стоматологии «Mr.Dent»</h1>

    <section class="up">

        <div class="content-wrapper">
            <div class="up__back">
                <div class="up__img">
                    <img src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
background-2.png" alt="">
                </div>
                <div class="up__wrapper">
                    <ul class="breadcrumbs up__breadcrubms">
                        <li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
                        <li><a href="<?php echo $this->_tpl_vars['BREADCRUMBS'][0]['alias']; ?>
">Услуги<span style="color: #42c6d1;">></span></a></li>
                        <li><a class="current-link">Лечение зубов</a></li>
                    </ul>
                    <div class="up__text">
                        <div class="up__text1">В клинике Mr.Dent найдут </div><br>
                        <div class="up__text2">Быстрое и эффективное решение проблем с зубами любой сложности</div>

                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- Лечение кариеса зубов -->
    <section class="column2">
        <h2 class="column2__title">Лечение кариеса зубов</h2>
        <div class="content-wrapper  column2__wrapper">
            <div class="column2__left">
                <div class="column2__slider">
                    <div>
                        <div class="column2__left-slide">
                            <img width="100%" src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
img10.png" alt="">
                            <a href="#"><img src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
icon__play.png" alt=""></a>
                        </div>
                    </div>
                    <div>
                        <div class="column2__left-slide">
                            <img width="100%" src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
img10.png" alt="">
                        </div>
                    </div>
                    <div>
                        <div class="column2__left-slide">
                            <img width="100%" src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
img10.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <div class="column2__right">
                <p>С такой проблемой, как кариес зубов, сталкивался хоть раз практически каждый человек в своей жизни.
                    <br>
                    <br>
                    Заболевание представляет собой деструктивный процесс, который вызывает постепенное разрушение тканей зуба с образованием кариозной полости.
                    <br>
                    <br>
                    Некоторые люди стараются провести лечение кариеса на начальных стадиях. Другие – запускают патологический процесс, что вызывает развитие опасных осложнений.</p>
            </div>
        </div>
    </section>


    <!-- БОЛИТ ЗУБ ? -->
    <section class="blue-block blue-block2">
        <div class="content-wrapper blue-block__wrapper">
            <div class="psevdo-wrap">
                <img src="<?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['imageIcon']; ?>
" alt="">
                <div class="blue__left-block">Болит зуб?</div>
            </div>
            <div class="blue__right-block">
                <img src="<?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['imageAva']; ?>
" width="190" height="190" alt="">
                <div class="blue__text">
                    <?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['textTop']; ?>

                    <div class="blue__link-wrapper">
                        <div class="blue__link-number"><p>Бесплатная консультация</p>
                            <a href="tel:89202253399">8 920 225 3399</a>
                        </div>
                        <div class="blue__button">

                            <a href="<?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['buttonLink']; ?>
" class="button--blue"><?php echo $this->_tpl_vars['BLUE_BLOCKS'][0]['buttonTitle']; ?>
 <span>>></span></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>



    <!-- Причины возникновения кариеса -->
    <section class="reasons" id="reasons">
        <div class="content-wrapper ">
            <div class=" reasons__wrapper">
                <div class="">
                    <h2 class="reasons__title">Причины возникновения кариеса</h2>
                    <p class="reasons__text">Кариес относят к полиэтиологическим заболеваниям, поэтому спровоцировать его развитие могут следующие факторы:</p>
                    <div class="reasons__block-wrapper">
                        <div class="reasons__block">
                            <?php $_from = $this->_tpl_vars['REASONS_LEFT']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['reason']):
?>
                                <ul>
                                    <li><?php echo $this->_tpl_vars['reason']['description']; ?>
</li>
                                </ul>
                            <?php endforeach; endif; unset($_from); ?>
                        </div>
                        <div class="reasons__block ">
                            <?php $_from = $this->_tpl_vars['REASONS_RIGHT']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['reason']):
?>
                                <ul>
                                    <li><?php echo $this->_tpl_vars['reason']['description']; ?>
</li>
                                </ul>
                            <?php endforeach; endif; unset($_from); ?>
                        </div>
                    </div>
                </div>
            </div>
    </section>





    <!-- СТЕПЕНИ КАРИЕСА -->
    <section class="tab-block">
        <h2 class="tab-block__title">Степени кариеса</h2>
        <p class="tab-block__descr">В стоматологии выделяют следующие стадии развития заболевания</p>
        <div class="content-wrapper">

            <div class="tab-block__wrapper">
                <ul class="tab-block__list">
                    <li class="tab-block--active"><a data-toggle="t1" href="#karies1">Темное пятно</a></li>
                    <li><a data-toggle="t1" href="#karies2">Поверхностный кариес</a></li>
                    <li><a data-toggle="t1" href="#karies3">Средний кариес</a></li>
                    <li><a data-toggle="t1" href="#karies4">Глубокий кариес</a></li>
                </ul>
                <div  class="tab-block__content">
                    <div id="karies1" class="tab-block__tab tab-block--current">
                        <p class="tab__block-text" align="justify">Кариес в стадии белого или темного пятна. Это начальный этап патологии, во время которого отсутствует анатомическое разрушение эмали. Однако на поверхности зуба образуется пятнышко. Лечение кариеса в стадии пятна предполагает использование методик, основанных на минимальном вмешательстве в структуру зуба.</p>
                        <div class="tab-block__tab-img">
                            <img width="254"  height="190" src="img/tab1.png" alt="">
                            <img width="254"  height="190" src="img/tab2.png" alt="">

                            <img width="254"  height="190" src="img/tab3.png" alt="">

                        </div>
                    </div>
                    <div id="karies2" class="tab-block__tab">
                        <p class="tab__block-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam deleniti illo recusandae, nostrum harum molestiae eligendi ab, atque quasi similique dolorem architecto ea. Eos repellendus reprehenderit dicta? Voluptatum, quisquam, suscipit.</p>
                        <div class="tab-block__tab-img"></div>
                    </div>
                    <div id="karies3" class="tab-block__tab">
                        <p class="tab__block-text">Кариес в стадии белого или темного пятна. Это начальный этап патологии, во время которого отсутствует анатомическое разрушение эмали. Однако на поверхности зуба образуется пятнышко. Лечение кариеса в стадии пятна предполагает использование методик, основанных на минимальном вмешательстве в структуру зуба.</p>
                        <div class="tab-block__tab-img"></div>
                    </div>
                    <div id="karies4" class="tab-block__tab">
                        <p class="tab__block-text">Кариес в стадии белого или темного пятна. Это начальный этап патологии, во время которого отсутствует анатомическое разрушение эмали. Однако на поверхности зуба образуется пятнышко. Лечение кариеса в стадии пятна предполагает использование методик, основанных на минимальном вмешательстве в структуру зуба.</p>
                        <div class="tab-block__tab-img"></div>
                    </div>
                </div>
                <div class="tab-block__alarm">
                    <div class="tab-block__alarm-title">Если сразу не обратиться к врачу?</div>
                    <p class="tab-block__alarm-text">
                        Дальнейшее разрушение зуба приведет к пульпиту, тогда вас уже "заставит" обратиться к стоматологу нестерпимая зубная боль. При этом стоимость лечения будет выше, поскольку придется обрабатывать и пломбировать каналы зуба.</p>
                </div>
                <div id="tab-form" class="tab-block__form">
                    <div class="tab-block__form-container">
                        <!-- Загрузка формы отправки "Записаться на бесплатную консультацию" -->
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-free-consultation.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- Процесс лечения кариеса -->
    <?php if (! empty ( $this->_tpl_vars['LECHENIE_ZUBOV'] )): ?>
    <section class="spoiler">
        <h2 class="spoiler__title">Процесс лечения кариеса</h2>
        <div class="content-wrapper">
            <div class="spoiler__wrapper">


                <?php $this->assign('itterator', 1); ?>
                <?php $_from = $this->_tpl_vars['LECHENIE_ZUBOV']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['article']):
?>
                <div class="spoiler__content">
                    <div class="spoiler__click">
                        <div class="spoiler__click-number"><?php echo $this->_tpl_vars['itterator']++; ?>
</div>
                        <div class="spoiler__click-name"><?php echo $this->_tpl_vars['article']['title']; ?>
</div>
                    </div>
                    <div class="spoiler__text" align="left"><?php echo $this->_tpl_vars['article']['description']; ?>
</div>

                </div>
                <?php endforeach; endif; unset($_from); ?>

            </div>
        </div>
        </h2>
    </section>
    <?php endif; ?>



    <!-- ЛЕЧЕНИЕ -->
    <section class="blue-block">
        <div class="content-wrapper blue-block__wrapper">
            <div class="psevdo-wrap">
                <img src="<?php echo $this->_tpl_vars['BLUE_BLOCKS'][1]['imageIcon']; ?>
" alt="">
                <div class="blue__left-block">Боитесь?<br>Лечите во сне</div>
            </div>
            <div class="blue__right-block">
                <img src="<?php echo $this->_tpl_vars['BLUE_BLOCKS'][1]['imageAva']; ?>
" width="190" height="190" alt="">
                <div class="blue__text">
                    <?php echo $this->_tpl_vars['BLUE_BLOCKS'][1]['textTop']; ?>

                    <div class="blue__link-wrapper">
                        <div class="blue__button">
                            <a href="<?php echo $this->_tpl_vars['BLUE_BLOCKS'][1]['buttonLink']; ?>
" class="button--blue"><?php echo $this->_tpl_vars['BLUE_BLOCKS'][1]['buttonTitle']; ?>
 <span>>></span></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>




    <!-- НАШИ СПЕЦЫ -->
    <?php if (! empty ( $this->_tpl_vars['PERSONAL'] )): ?>
    <section id="personal" class="personal">
        <h2 class="personal__title">Наши специалисты</h2>
        <div class="sl-fb personal__wrapper personal__slider content-wrapper">

            <?php $_from = $this->_tpl_vars['PERSONAL']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['person']):
?>
            <div>
                <div class="personal__item">
                    <div class="personal__av">
                        <img width="77" height="77" src="<?php echo $this->_tpl_vars['person']['imageAva']; ?>
" alt="">
                    </div>
                    <div class="personal__text-wrapper">
                        <p class="personal__name"><?php echo $this->_tpl_vars['person']['name']; ?>
 <br> <span class="personal__spec"><?php echo $this->_tpl_vars['person']['profession']; ?>
</span></p>
                        <div class="personal__slide-block">
                            <div class="personal__button">
                                <a href="###" class="button button--blue">Записаться <span>>></span></a>
                            </div>
                            <div class="personal__link">
                                <a href="/doctors/card-doctor/<?php echo $this->_tpl_vars['person']['personalID']; ?>
">Информация о враче <span>>></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; endif; unset($_from); ?>

        </div>
        <div class="sl-count"><span class="sl-count__num">1</span> / <span class="sl-count__total"></span></div>

    </section>
    <?php endif; ?>




    <!-- ЦЕНЫ -->
    <section class="price-block">
        <div class="content-wrapper">
            <h2 class="price-block__title">Стоимость услуг</h2>
            <div class="price-block__wrapper">
                <table class="price-block__table">
                    <tr class="price-block__row-title">
                        <th class="price-block__column1 price-block__title1 ">Услуга</th>
                        <th class="price-block__title2">Описание</th>
                        <th class="price-block__title3">Стоимость</th>
                    </tr> <!--ряд с ячейками заголовков-->
                    <?php $_from = $this->_tpl_vars['PRICES']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['price']):
?>
                    <tr class="price-block__row">
                        <td class="price-block__column1"><?php echo $this->_tpl_vars['price']['title']; ?>
</td>
                        <td class="price-block__column2"><?php echo $this->_tpl_vars['price']['description']; ?>
</td>
                        <td class="price-block__column3">от <?php echo $this->_tpl_vars['price']['cost']; ?>
 ₽</td>
                    </tr> <!--ряд с ячейками тела таблицы-->
                    <?php endforeach; endif; unset($_from); ?>
                </table>
                <div class="price-block__button">
                    <a href="/price/" class="button"> Все цены <span>>></span></a>
                </div>
            </div>

        </div>
    </section>



    <!--ОТЗЫВ -->
    <section id="review" class="review review--white">
        <h2 class="review__title">Отзывы клиентов</h2>
        <div class="review__wrapper review__slider content-wrapper">

            <?php $_from = $this->_tpl_vars['REVIEWS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['review']):
?>
            <div>
                <div class="review__item">
                    <div class="prev"></div>
                    <div class="review__av">
                        <img width="172" height="172" src="<?php echo $this->_tpl_vars['review']['image']; ?>
" alt="">
                    </div>
                    <div class="review__text">
                        <div class="review__data"><?php echo ((is_array($_tmp=$this->_tpl_vars['review']['dateAdd'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e.%m.%Y") : smarty_modifier_date_format($_tmp, "%e.%m.%Y")); ?>
</div>
                        <div class="review__name"><?php echo $this->_tpl_vars['review']['name']; ?>
</div>
                        <div class="review__text"><?php echo $this->_tpl_vars['review']['review']; ?>
</div>
                    </div>
                    <div class="next"></div>
                </div>
            </div>
            <?php endforeach; endif; unset($_from); ?>


        </div>
        <div class="review__button">
            <a href="/reviews/" class="button button--pink">Все отзывы <span>>></span></a>
        </div>
    </section>



    <!-- СТАТЬИ -->
    <section class="news news__lechenie">
        <h2 class="news__title">Статьи по теме</h2>
        <div  class="news__wrapper news__slider content-wrapper">

            <?php $_from = $this->_tpl_vars['NEWS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['new']):
?>
            <div>
                <div class="news__item">
                    <a href="/newsline/<?php echo $this->_tpl_vars['new']['alias']; ?>
/<?php echo $this->_tpl_vars['new']['newsID']; ?>
">
                        <div class="news__item-img">
                            <img src="<?php echo $this->_tpl_vars['new']['srcSmall']; ?>
" alt="">
                        </div>
                        <div class="news__item-title">
                            <a href="/newsline/<?php echo $this->_tpl_vars['new']['alias']; ?>
/<?php echo $this->_tpl_vars['new']['newsID']; ?>
"><?php echo $this->_tpl_vars['new']['summary']; ?>
</a></div>
                        <div class="news__item-data"><?php echo ((is_array($_tmp=$this->_tpl_vars['new']['publicationDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e.%m.%Y") : smarty_modifier_date_format($_tmp, "%e.%m.%Y")); ?>
</div>
                    </a>
                </div>
            </div>
            <?php endforeach; endif; unset($_from); ?>

        </div>
        <div class="news__button">
            <a href="/newsline/" class="button button--pink">Все статьи <span>>></span></a>
        </div>
    </section>


    <!-- Форма Отправки Низ -->
    <section id="appointment" class="form">
        <div class=" content-wrapper">
            <img src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
form-image.png" alt="">
            <div class="form-wrapper">
                <!-- Форма отправки "Записаться на прием" -->
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-appointment.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            </div>
        </div>
    </section>
</main>