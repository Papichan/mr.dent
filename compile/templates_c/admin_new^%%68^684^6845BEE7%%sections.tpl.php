<?php /* Smarty version 2.6.20, created on 2019-10-03 12:10:48
         compiled from /var/www/dent/modules/Articles/templates/admin/sections.tpl */ ?>
<h1>Категории</h1>

<form action="<?php echo $this->_tpl_vars['MODULE_ALIAS']; ?>
sections/" method="post">
    <ul>
        <li style="background-image:none;">
            <div class="formElement">
                <label class="formElementLabel" for="sectionID">
                   Список категорий
                </label>
                <select id="sections" multiple name="sections[]">
                    <?php $_from = $this->_tpl_vars['SECTIONS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['SECTION']):
?>
                       <option value="<?php echo $this->_tpl_vars['SECTION']['asectionID']; ?>
"><?php echo $this->_tpl_vars['SECTION']['caption']; ?>
</option>
                    <?php endforeach; endif; unset($_from); ?>
                </select>

                <br />

                <input class="btn" type="submit" name="doDel" value="Удалить" />
            </div>
        </li>
        <li style="background-image:none;">
            <div class="formElement">
                <label class="formElementLabel" for="sectionID">
                   Категория
                   <span style="color: red;">*</span>
                </label>
                <input type="text" name="section" id="section" />
                <input class="btn" type="submit" name="doAdd" value="Добавить" alt="" />
                <input class="btn" type="submit" name="doSave" value="Сохранить" alt="" disabled="disabled"/>
            </div>
        </li>
    </ul>
    <input type="hidden" name="messageID" value="" alt="">
</form>