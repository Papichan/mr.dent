<?php /* Smarty version 2.6.20, created on 2019-10-04 18:05:24
         compiled from reviews/list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'reviews/list.tpl', 40, false),)), $this); ?>
<h1>Отзывы</h1>

<?php if (! empty ( $this->_tpl_vars['REVIEWS'] )): ?>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'paginator_new.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<table class="highlightable" id="freviewsTable">
	<colgroup>
		<col>
		<col>
		<col>
		<col>
		<col style="width: 10px;">
		<col>
	</colgroup>
    <thead>
        <tr>
            <th>№</th>
            <th>Имя</th>
            <th>IP</th>
			<th>Дата</th>
			<th>Язык</th>
            <th>Утвержден</th>
            <th style="width: 10px;"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
icons/delete_icon.png" /></th>
        </tr>
    </thead>
    <tbody>
    <?php $_from = $this->_tpl_vars['REVIEWS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['REVIEW']):
?>
        <tr>
            <td style="display: none;"><a href="<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
edit/<?php echo $this->_tpl_vars['REVIEW']['reviewID']; ?>
/" /></td>
            <td style="width: 20px;"><?php echo $this->_tpl_vars['REVIEW']['reviewID']; ?>
</td>
            <td>
                <?php echo $this->_tpl_vars['REVIEW']['name']; ?>

            </td>
            <td>
                <?php echo $this->_tpl_vars['REVIEW']['userIp']; ?>

            </td>
            <td>
                <?php echo ((is_array($_tmp=$this->_tpl_vars['REVIEW']['dateAdd'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d.%m.%Y %H:%M") : smarty_modifier_date_format($_tmp, "%d.%m.%Y %H:%M")); ?>

            </td>
            <td>
                <?php if (empty ( $this->_tpl_vars['REVIEW']['lang'] )): ?>RU<?php else: ?>EN<?php endif; ?>
			</td>
						<td>
							<?php if ($this->_tpl_vars['REVIEW']['isApproved'] == 1): ?><span style="color: green">Да</span><?php else: ?><span style="color: red">Нет</span><?php endif; ?>
            </td>
            <td class="deleteEntry">
                <a href="<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
delete/<?php echo $this->_tpl_vars['REVIEW']['reviewID']; ?>
/" title="Удалить"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
delete_icon.png" alt="Удалить" style="border: 0;" /></a>
            </td>
        </tr>
    <?php endforeach; endif; unset($_from); ?>
    </tbody>
</table>
	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'paginator_new.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<?php else: ?>
	Отзывов пока нет
<?php endif; ?>