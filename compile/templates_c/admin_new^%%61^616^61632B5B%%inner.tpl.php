<?php /* Smarty version 2.6.20, created on 2019-10-04 11:07:04
         compiled from custommenu/inner.tpl */ ?>

<h1>Управление меню: <?php echo $this->_tpl_vars['PARENT_NAME']['menuListName']; ?>
</h1>

<div style="text-align: right; margin: -30px 0 5px 0;">
    <input type="button" id="goAddTheme" class="goToBtn" value="Добавить пункт" />
    <input type="hidden" id="goAddTheme_link" value="<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
showinner/<?php echo $this->_tpl_vars['PARENT_NAME']['menuListID']; ?>
/addinner/" />
</div>
<?php if (! empty ( $this->_tpl_vars['INNER_MENU'] )): ?>
    <ul class="ul-treefree ul-dropfree">
    <?php $_from = $this->_tpl_vars['INNER_MENU']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['I'] => $this->_tpl_vars['THEME']):
?>
        <li><?php echo $this->_tpl_vars['THEME']['name']; ?>

            &emsp;
            <a href="/admin/custommenu/showinner/<?php echo $this->_tpl_vars['PARENT_NAME']['menuListID']; ?>
/edit/<?php echo $this->_tpl_vars['THEME']['itemID']; ?>
" title="Редактировать">
				<img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
icons/pencil.png" />
            </a>
            &emsp;
            <a class="deleteEntry" href="<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
showinner/<?php echo $this->_tpl_vars['PARENT_NAME']['menuListID']; ?>
/del/<?php echo $this->_tpl_vars['THEME']['itemID']; ?>
/" title="Удалить">
				<img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
icons/delete_icon.png" />
            </a>
        <?php if (! empty ( $this->_tpl_vars['THEME']['children'] )): ?>
            <ul  class="ul-treefree ">
                <?php $_from = $this->_tpl_vars['THEME']['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['child']):
?>
                    <li ><?php echo $this->_tpl_vars['child']['name']; ?>

                        &emsp;
                        <a href="/admin/custommenu/showinner/<?php echo $this->_tpl_vars['PARENT_NAME']['menuListID']; ?>
/edit/<?php echo $this->_tpl_vars['child']['itemID']; ?>
" title="Редактировать">
							<img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
icons/pencil.png" />
                        </a>
                        &emsp;
                        <a class="deleteEntry" href="<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
showinner/<?php echo $this->_tpl_vars['PARENT_NAME']['menuListID']; ?>
/del/<?php echo $this->_tpl_vars['child']['itemID']; ?>
/" title="Удалить">
							<img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
icons/delete_icon.png" />
						</a>
                    </li>
                <?php endforeach; endif; unset($_from); ?>
            </ul>
        <?php endif; ?>
        </li>
      <?php endforeach; endif; unset($_from); ?>
    </ul>
<?php endif; ?>


