<?php /* Smarty version 2.6.20, created on 2019-10-03 17:08:06
         compiled from /var/www/dent/libs/ContentManager/Controls/SingleSelect/control.tpl */ ?>
<select name="<?php echo $this->_tpl_vars['localVars']['tableName']; ?>
[<?php echo $this->_tpl_vars['localVars']['formIndex']; ?>
][<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
]" id="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
_<?php echo $this->_tpl_vars['localVars']['formIndex']; ?>
" class="select2 <?php if ($this->_tpl_vars['localVars']['CLASS']): ?><?php echo $this->_tpl_vars['localVars']['CLASS']; ?>
<?php endif; ?>" <?php if (! empty ( $this->_tpl_vars['localVars']['READONLY'] )): ?> readonly="readonly"<?php endif; ?><?php if ($this->_tpl_vars['localVars']['TIP']): ?> title="<?php echo $this->_tpl_vars['localVars']['TIP']; ?>
"<?php endif; ?> style="width:400px;">
	<?php $_from = $this->_tpl_vars['localVars']['OPTIONS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['OPTION']):
?>
		<option value="<?php echo $this->_tpl_vars['OPTION']['value']; ?>
"<?php if ($this->_tpl_vars['OPTION']['selected']): ?> selected="selected"<?php endif; ?> <?php if (! empty ( $this->_tpl_vars['OPTION']['disabled'] )): ?> disabled="disabled"<?php endif; ?> <?php if (isset ( $this->_tpl_vars['OPTION']['level'] ) && $this->_tpl_vars['OPTION']['level'] == 2): ?> class="level1"<?php endif; ?>><?php echo $this->_tpl_vars['OPTION']['text']; ?>
</option>
	<?php endforeach; endif; unset($_from); ?>
</select>