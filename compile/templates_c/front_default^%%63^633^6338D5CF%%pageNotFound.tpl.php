<?php /* Smarty version 2.6.20, created on 2019-10-03 10:44:13
         compiled from error/pageNotFound.tpl */ ?>
<h1>Страница не найдена.</h1>

<?php if (! empty ( $this->_tpl_vars['errorInfo'] )): ?>
   <p><?php echo $this->_tpl_vars['errorInfo']; ?>
</p>
<?php else: ?>
    <p>Запрашиваемой страницы не существует на сайте. Используйте навигационное меню.</p>
<?php endif; ?>