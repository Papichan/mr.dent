<?php /* Smarty version 2.6.20, created on 2019-10-03 13:42:06
         compiled from anyeditor/items.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'anyeditor/items.tpl', 21, false),)), $this); ?>
<h1><?php if (! empty ( $this->_tpl_vars['HEADER'] )): ?><?php echo $this->_tpl_vars['HEADER']; ?>
<?php endif; ?></h1>

<?php if (isset ( $this->_tpl_vars['ALLOW_ADDITION'] ) && $this->_tpl_vars['ALLOW_ADDITION'] == 1): ?>
<div style="text-align: right; margin: -30px 0 5px 0;">
    <input type="button" id="goAddItem" class="goToBtn" value="Добавить <?php echo $this->_tpl_vars['ITEM_NAME']; ?>
" />
    <input type="hidden" id="goAddItem_link" value="<?php echo $this->_tpl_vars['ADD_ENTRY_ALIAS']; ?>
" />
</div>
<?php endif; ?>

<table id="itemsList"></table>
<div id="pager"></div>

<?php echo '
<script type="text/javascript">
$(document).ready(function(){
    $(\'#itemsList\')
	.jqGrid({
        url : \''; ?>
<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
items/?table=<?php echo $this->_tpl_vars['TABLE_NAME']; ?>
<?php echo '\',
        datatype: \'json\',
        mtype: \'GET\',
        colNames:['; ?>
<?php $_from = $this->_tpl_vars['TABLE_HEADERS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['I'] => $this->_tpl_vars['HEADER']):
?>'<?php echo $this->_tpl_vars['HEADER']['caption']; ?>
'<?php if ($this->_tpl_vars['I'] < count($this->_tpl_vars['TABLE_HEADERS']) -1): ?>,<?php endif; ?><?php endforeach; endif; unset($_from); ?><?php echo '],
        colModel:[ 
            '; ?>
<?php $_from = $this->_tpl_vars['TABLE_HEADERS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['I'] => $this->_tpl_vars['HEADER']):
?>
            {
                name:'<?php echo $this->_tpl_vars['HEADER']['name']; ?>
', 
                index:'<?php echo $this->_tpl_vars['HEADER']['name']; ?>
'
                <?php if (! isset ( $this->_tpl_vars['HEADER']['searchable'] ) || $this->_tpl_vars['HEADER']['searchable'] == true): ?>
                , search:true
                , searchoptions:{sopt:['cn','eq']}
                <?php else: ?>
                , search:false
                <?php endif; ?>
                <?php if (! isset ( $this->_tpl_vars['HEADER']['sortable'] ) || $this->_tpl_vars['HEADER']['sortable'] == true): ?>
                , sortable:true
                <?php else: ?>
                , sortable:false
                <?php endif; ?>
                <?php if (! empty ( $this->_tpl_vars['HEADER']['width'] )): ?>, width: <?php echo $this->_tpl_vars['HEADER']['width']; ?>
<?php endif; ?>
                <?php if (! empty ( $this->_tpl_vars['HEADER']['align'] )): ?>, align: '<?php echo $this->_tpl_vars['HEADER']['align']; ?>
'<?php endif; ?>
            }<?php if ($this->_tpl_vars['I'] < count($this->_tpl_vars['TABLE_HEADERS']) -1): ?>,<?php endif; ?>
            <?php endforeach; endif; unset($_from); ?><?php echo '
        ],
		ondblClickRow: function(id)
		{
			window.location.href = editURL + id;
        },
        '; ?>
<?php if (! empty ( $this->_tpl_vars['ITEMS_ON_PAGE'] )): ?>rowNum: <?php echo $this->_tpl_vars['ITEMS_ON_PAGE']; ?>
, <?php endif; ?><?php echo '
        pager: $(\'#pager\'),
		toppager: true,
        height: "100%",
        //width: $(document).width() - 100,
        autowidth: true,
        viewrecords: true
    })
	.navGrid(\'#pager\',
		{view:false, del:false, add:false, edit:false, searchtext:"Поиск", refresh:false, refreshtext:"Обновить"}, 
		{}, //  default settings for edit
		{}, //  default settings for add
		{},  // delete instead that del:false we need this
		{closeOnEscape:true, multipleSearch:true, closeAfterSearch:true}, // search options
		{} /* view parameters*/
	)
	.navButtonAdd
	(
		\'#pager\',
		{
			caption:"Редактировать", 
			onClickButton: AEGoToEditRow, 
			position:"last"
		}
	)
	.navButtonAdd
	(
		\'#pager\',
		{
			caption:"Добавить", 
			onClickButton: AEGoToAddRow, 
			position:"last"
		}
	)
	.navButtonAdd
	(
		\'#pager\',
		{
			caption:"Удалить", 
			onClickButton: AEDeleteRow, 
			position:"last"
		}
	);
    //$(\'#itemsList\').navGrid(\'#pager\', {search:true});
});

var editURL = \''; ?>
<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
edit/?table=<?php echo $this->_tpl_vars['TABLE_NAME']; ?>
&rowID=<?php echo '\';
function AEGoToEditRow()
{
    var rowID = $(\'tr[aria-selected=true]\').attr(\'id\');
    if (typeof(rowID) != "undefined")
    {
        window.location.href = editURL + rowID;
    }
    else
    {
        $.jGrowl(\'Необходимо выбрать запись для редактирования\');
    }
}

var addURL = \''; ?>
<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
add/?table=<?php echo $this->_tpl_vars['TABLE_NAME']; ?>
<?php echo '\';
function AEGoToAddRow()
{
    window.location.href = addURL;
}

var removeURL = \''; ?>
<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
delete/?table=<?php echo $this->_tpl_vars['TABLE_NAME']; ?>
&rowID=<?php echo '\';
function AEDeleteRow()
{
    var rowID = $(\'tr[aria-selected=true]\').attr(\'id\');
    if (typeof(rowID) != "undefined")
    {
        $.post(
            removeURL + rowID,
            { async: true },
            AEDeleteRowSuccess,
            \'json\'
        );
    }
    else
    {
        $.jGrowl(\'Необходимо выбрать запись для удаления\');
    }
    
}

function AEDeleteRowSuccess(data, status)
{
    if (!standartAJAXResponseHandler(data, status)) return;
	if (data.msg == "ok")
	{
	    $(\'tr#\' + data.rowID).remove();
		$.jGrowl("Запись удалена");
		//if ($("#entriesList tbody").length == 0) list.replaceWith( "Таблица пуста" );
	}
	else if(data.dependentEntries)
	{
		$(\'#dialog\').jqm({modal: false});
		var dependentItemsContainer = $("#dialog div").eq(1).empty();
		var link = 0;
		for (var i = 0; i < data.dependentEntries.length; i++)
		{
			dependentItemsContainer.append($("<h2>"+data.dependentEntries[i].tableName+"</h2>"));
			for (var l = 0; l < data.dependentEntries[i].itemsLinks.length; l++)
			{
				link = data.dependentEntries[i].itemsLinks[l];
				dependentItemsContainer.append($("<div>- <a href=\'"+link+"\'>"+link+"</a></div>"));
			}
		}
		$(\'#dialog\').jqmShow();
	}
}
</script>
'; ?>



<div id="dialog" class="jqmWindow">
    <div style="width:100%;text-align:right"><a href="#" class="jqmClose">х</a></div>
    Данная запись не может быть удалена, поскольку имеются связанные записи в других таблицах:
    <div></div>
</div>