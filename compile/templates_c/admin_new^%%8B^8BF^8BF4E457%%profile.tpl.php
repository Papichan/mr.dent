<?php /* Smarty version 2.6.20, created on 2019-10-03 12:10:46
         compiled from profile/profile.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'profile/profile.tpl', 35, false),)), $this); ?>
<h1>Профиль [<a href="<?php echo $this->_tpl_vars['SECTION_ALIAS']; ?>
edit/" title="Редактировать профиль"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
edit.png" alt="редактировать" class="noborder" /></a>]</h1>

<div class="profile_row">
    <div class="profile_rowLeft">&nbsp;</div>
    <div class="profile_rowRight" style="font-size: 200%;"><?php echo $this->_tpl_vars['USER']['login']; ?>
</div>
</div>

<div class="profile_row">
    <div class="profile_rowLeft">Тип пользователя:</div>
    <div class="profile_rowRight"><?php echo $this->_tpl_vars['USER']['roleInfo']['name']; ?>
</div>
</div>

<div class="profile_row">
    <div class="profile_rowLeft">Электронная почта:</div>
    <div class="profile_rowRight"><?php echo $this->_tpl_vars['USER']['email']; ?>
</div>
</div>

<div class="profile_row">
    <div class="profile_rowLeft">Дата регистрации:</div>
    <div class="profile_rowRight"><?php echo $this->_tpl_vars['USER']['registrationDate']; ?>
</div>
</div>

<div class="profile_row" style="margin-top: 10px;">
    <div class="profile_rowLeft">&nbsp;</div>
    <div class="profile_rowRight"><h2 style="margin: 0;">Информация профиля</h2></div>
</div>

<div class="profile_row">
    <div class="profile_rowLeft">ФИО:</div>
    <div class="profile_rowRight"><?php echo $this->_tpl_vars['USER']['info']['FIO']; ?>
</div>
</div>

<div class="profile_row">
    <div class="profile_rowLeft">Дата рождения:</div>
    <div class="profile_rowRight"><?php echo ((is_array($_tmp=$this->_tpl_vars['USER']['info']['birthdate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%d.%m.%Y") : smarty_modifier_date_format($_tmp, "%d.%m.%Y")); ?>
</div>
</div>

<div class="profile_row">
    <div class="profile_rowLeft">Пол:</div>
    <div class="profile_rowRight"><?php echo $this->_tpl_vars['USER']['info']['gender']; ?>
</div>
</div>

<div style="clear: both;"></div>