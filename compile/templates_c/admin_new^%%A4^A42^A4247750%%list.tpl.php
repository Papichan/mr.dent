<?php /* Smarty version 2.6.20, created on 2019-10-03 11:40:47
         compiled from structure/list.tpl */ ?>
<h1>Система управления сайтом <?php echo $this->_tpl_vars['SETTINGS']['general']['siteName']; ?>
</h1>

<form action="<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
directAccess" method="post">
<ul>
    <li>
        Для быстрого перехода к редактированию страницы, введите её полный <abbr title="путь, например:  http://site.ru/page/1/">URL</abbr>-адрес:
        <br />
        <input type="text" name="directAccess" value="" style="width: 85%;" /> <input type="submit"  class="handyButton" name="go" value=" Go " />
    </li>
    <li>
        Или найдите её в меню слева
        <br />
        &larr;
    </li>
</ul>
</form>

<div id="pagesList">
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'structure/pages_level.tpl', 'smarty_include_vars' => array('PAGES_TREE' => $this->_tpl_vars['PAGES_TREE'],'LEVEL' => 0)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
</div>

<script type="text/javascript">
<?php echo '$(document).ready(function() {if (!$.support.boxModel)	$("#changeBrowser").slideToggle();});'; ?>

</script>
<div id="changeBrowser" style="display:none;">
<p style="font-size:1.2em;color:#c00">Вы используете устаревший браузер!</p>

<p style="font-size:1.2em"> Для комфортной работы в системе нужен один из следующих <span class="helper" title="Программа для просмотра сайтов">браузеров</span>:
</p>
	<ul>
		<li><a href="http://www.google.com/chrome">Google Chrome</a> </li>
		<li><a href="http://getfirefox.com">Mozilla Firefox v3+</a>  </li>
	</ul>
</div>

<noscript>
	<div style="color:#C00; font-size:1.3em">Внимание, JavaScript должен быть включён в браузере!</div>
</noscript>

<p>
&laquo;<a href="http://www.BitPlatform.ru">Битплатформа</a>&raquo; &mdash; платформа для разработки сайтов и веб-приложений. Разработана и поддерживается компанией <a href="http://mitlabs.ru/">МИТЛабс</a>.
</p>

<p>По вопросам распространения обращайтесь:</p>

<table border="0"  style="border:none;margin-top:-20px;">
<tr>
	<td style="border:none;">e-mail: </td>
	<td style="border:none;"><a href="mailto:info@mitlabs.ru">info@mitlabs.ru</a></td>
</tr>
<tr>
	<td style="border:none;">тел: </td>
	<td style="border:none;">+7 (495) 646 02 30</td>
</tr>

</table>


<p>
<strong class="helper" title="Доменное имя, по которому открывается ваш сайт,  например: YANDEX.RU, AYACO.RU">Ваш домен:</strong><a href="http://www.nic.ru/whois/?query=<?php echo $this->_tpl_vars['HTTP_HOST']; ?>
"><?php echo $this->_tpl_vars['HTTP_HOST']; ?>
</a><br />
<strong class="helper" title="IP адрес компьютера, на котором размещён ваш сайт">IP адрес вашего сервера:</strong> <a href="http://www.nic.ru/whois/?query=<?php echo $this->_tpl_vars['SERVER_ADDR']; ?>
"><?php echo $this->_tpl_vars['SERVER_ADDR']; ?>
</a>
</p>