<?php /* Smarty version 2.6.20, created on 2019-10-14 10:12:50
         compiled from structure/recycled_content.tpl */ ?>
			<h1>Корзина</h1>
			
			<?php if (! empty ( $this->_tpl_vars['DELETED_PAGES'] )): ?>
			<div>
				Удаленные страницы:
				
				<form action="" method="post" id="recycled_form">
					<ol id="recycledPagesList" style="margin: 20px 0">
						<?php $_from = $this->_tpl_vars['DELETED_PAGES']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['I'] => $this->_tpl_vars['deletedPage']):
?>
						<li>
							<input type="checkbox" name="toEdit[]" value="<?php echo $this->_tpl_vars['deletedPage']['pageID']; ?>
" id="recycled_<?php echo $this->_tpl_vars['I']; ?>
" />
							<label for="recycled_<?php echo $this->_tpl_vars['I']; ?>
"><?php echo $this->_tpl_vars['deletedPage']['caption']; ?>
</label>
						</li>
						<?php endforeach; endif; unset($_from); ?>
					</ol>

					<div><a id="checkUncheckAll" style="display:none;cursor: pointer;" href="#">Отметить все</a></div>

					<div style="margin: 20px 0 20px 0px;float:left">
						<input type="submit" class="handyButton" name="doRestore" value=" Восстановить " style="width: 200px; height:30px; margin-right:100px; font-size: 1.3em" />
						<input type="submit" class="handyButton" name="doWipe" value=" Удалить навсегда " style="width: 200px; height:30px; font-size: 1.3em"/>
					</div>
				</form>
			</div>
			<?php else: ?>
			<div style="font-size: 1.3em ">Корзина пуста</div>
			<?php endif; ?>