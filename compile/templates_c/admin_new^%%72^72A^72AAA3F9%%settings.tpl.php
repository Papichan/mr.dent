<?php /* Smarty version 2.6.20, created on 2019-10-03 17:17:45
         compiled from settings/settings.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'settings/settings.tpl', 8, false),)), $this); ?>
<h1><?php echo $this->_tpl_vars['GROUP']['desc']; ?>
</h1>

<div>
    <div>
        <input type="button" id="goAddSetting" class="goToBtn" value="Добавить настройку" />
        <input type="hidden" id="goAddSetting_link" value="<?php echo $this->_tpl_vars['SECTION_ALIAS']; ?>
setting/add/<?php echo $this->_tpl_vars['GROUP']['groupID']; ?>
/" />
    </div>
    <?php if (count($this->_tpl_vars['EDIT_SETTINGS']) == 0): ?>
    В данной группе настроек нет
    <?php else: ?>
	<form action="" method="post">
		<ul>
			<?php $_from = $this->_tpl_vars['EDIT_SETTINGS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['SETTING']):
?>
			<li>[ <strong><?php echo $this->_tpl_vars['SETTING']['name']; ?>
</strong> ] <?php echo $this->_tpl_vars['SETTING']['desc']; ?>

				<br />
                <input type="text" name="<?php echo $this->_tpl_vars['SETTING']['settingID']; ?>
" size="60" value="<?php echo $this->_tpl_vars['SETTING']['value']; ?>
" <?php if ($this->_tpl_vars['SETTING']['isVisible'] == 0): ?>readonly='readonly'<?php endif; ?> />
            </li>
			<?php endforeach; endif; unset($_from); ?>
		</ul>
		<div><input type="submit" value="Сохранить изменения" name="doSave" /></div>
	</form>
	<?php endif; ?>
</div>