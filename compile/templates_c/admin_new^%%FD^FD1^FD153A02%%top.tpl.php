<?php /* Smarty version 2.6.20, created on 2019-10-03 10:44:13
         compiled from top.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="content-language" content="ru" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php if (! empty ( $this->_tpl_vars['AUTOREFRESH'] )): ?>
	<meta http-equiv="refresh" content="<?php echo $this->_tpl_vars['AUTOREFRESH']; ?>
" >
	<?php endif; ?>
	<title><?php if (isset ( $this->_tpl_vars['PAGE_TITLE'] )): ?><?php echo $this->_tpl_vars['PAGE_TITLE']; ?>
 &mdash; <?php endif; ?> <?php echo $this->_tpl_vars['SETTINGS']['general']['siteName']; ?>
 &mdash; Bitplatform</title>

<link rel="stylesheet" href="<?php echo $this->_tpl_vars['ADMIN_CSS']; ?>
mainStyles.css" type="text/css" media="screen,projection" />
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['ADMIN_CSS']; ?>
commonlist.css" type="text/css" media="screen,projection" />
<!--[if IE]><link rel="stylesheet" type="text/css" href= "<?php echo $this->_tpl_vars['ADMIN_CSS']; ?>
ie.css" /><![endif]-->

<link rel="stylesheet" href="<?php echo $this->_tpl_vars['ADMIN_CSS']; ?>
tables.css" type="text/css" media="screen,projection" />
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['ADMIN_CSS']; ?>
forms.css" type="text/css" media="screen,projection" />
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['ADMIN_CSS']; ?>
menu.css" type="text/css" media="screen,projection" />
<!--<link rel="stylesheet" href="<?php echo $this->_tpl_vars['ADMIN_CSS']; ?>
linkedVideos.css" type="text/css" media="screen,projection" />-->
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/select2/select2.css" type="text/css" media="screen,projection" />


<link rel="stylesheet" href="<?php echo $this->_tpl_vars['ADMIN_CSS']; ?>
profile.css" type="text/css" media="screen,projection" />

<link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jgrowl/jquery.jgrowl.css" type="text/css" media="screen,projection" />
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/impromptu/impromptu.css" type="text/css" media="screen,projection" />
<?php if (isset ( $this->_tpl_vars['STYLE_FILE'] )): ?>
<?php $_from = $this->_tpl_vars['STYLE_FILE']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['FILE']):
?>
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['FILE']; ?>
" type="text/css" />
<?php endforeach; endif; unset($_from); ?>
<?php endif; ?>

<link rel="shortcut icon" href="/adminfavicon.ico" />

<?php if (isset ( $this->_tpl_vars['JS_VARS'] )): ?>
<script type="text/javascript">
<?php $_from = $this->_tpl_vars['JS_VARS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['JS_VAR']):
?><?php $_from = $this->_tpl_vars['JS_VAR']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['JS_VAR_NAME'] => $this->_tpl_vars['JS_VAR_VALUE']):
?>
var <?php echo $this->_tpl_vars['JS_VAR_NAME']; ?>
 = <?php if (is_numeric ( $this->_tpl_vars['JS_VAR_VALUE'] )): ?><?php echo $this->_tpl_vars['JS_VAR_VALUE']; ?>
<?php else: ?>"<?php echo $this->_tpl_vars['JS_VAR_VALUE']; ?>
"<?php endif; ?>;
<?php endforeach; endif; unset($_from); ?><?php endforeach; endif; unset($_from); ?>
</script>
<?php endif; ?>

<?php if (! empty ( $this->_tpl_vars['OLD_JQUERY'] )): ?>
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/old/jquery.js"></script>
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/old/form.js"></script><!--  AJAX forms-->
<?php else: ?>
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jquery.js"></script>
			<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jquery.form.js"></script><!--  AJAX forms-->
<?php endif; ?>
<?php if (! empty ( $this->_tpl_vars['NEW_UI'] )): ?>
	<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jquery-ui/jquery-ui.min.js"></script>
<?php endif; ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/select2/select2.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/validator/validate.js"></script><!-- Валидатор форм -->
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/validator/validate.ext.js"></script><!-- Локализатор валидатора форм -->
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/tooltip/jquery.tooltip.js"></script><!--  Tooltip-->
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/dropDownMenu/ddm.js"></script><!--  DropDownMenu-->
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/impromptu/jquery.impromptu.js"></script><!--  Impromptu -->

<?php if (( isset ( $this->_tpl_vars['INCLUDE_JQUERY_UI'] ) || isset ( $this->_tpl_vars['INCLUDE_JQUERY_UI_EFFECTS'] ) || isset ( $this->_tpl_vars['INCLUDE_JQUERY_UI_DATEPICKER'] ) || isset ( $this->_tpl_vars['INCLUDE_JQUERY_UI_SLIDER'] ) || isset ( $this->_tpl_vars['INCLUDE_JQUERY_UI_SORTABLE'] ) || isset ( $this->_tpl_vars['INCLUDE_JQUERY_UI_RESIZABLE'] ) || isset ( $this->_tpl_vars['INCLUDE_JQUERY_UI_DRAGGABLE'] ) )): ?>
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/UI/jquery-ui-1.7.2.custom.css" type="text/css" media="screen,projection" />
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/UI/ui.core.js"></script><!--  UI core-->
<?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JQUERY_UI_EFFECTS'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/UI/effects.core.js"></script><!--  UI effects core--><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JQUERY_UI_DATEPICKER'] )): ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/UI/ui.datepicker.js"></script><!--  Datepicker-->
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/UI/ui.datepicker-ru.js"></script><!--  Datepicker localisation-->
<?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JQUERY_UI_SLIDER'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/UI/ui.slider.js"></script><!--  Slider--><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JQUERY_UI_SORTABLE'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/UI/ui.sortable.js"></script><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JQUERY_UI_RESIZABLE'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/UI/ui.draggable.js"></script><!--  Slider--><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JQUERY_UI_DIALOG'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/UI/ui.dialog.js"></script><!--  Dialog--><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JQUERY_UI_TABS'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/UI/ui.tabs.js"></script><!--  Tabs--><?php endif; ?>

<?php if (isset ( $this->_tpl_vars['INCLUDE_JQUERY_METADATA'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jquery.metadata.js"></script><!--  Metadata--><?php endif; ?>

<?php if (isset ( $this->_tpl_vars['INCLUDE_FILE_UPLOADER'] )): ?><!-- File Uploader -->
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jqUploader/jquery.flash.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jqUploader/jquery.jqUploader.js"></script><!-- File Uploader -->
<?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JQUERY_MULTISELECT'] )): ?> <!-- Needs dimensions --><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jqueryMultiSelect/jqueryMultiSelect.js"></script><!--  Multiselect--><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_SWFOBJECT'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
swfobject/swfobject.js"></script><!--  swf object --><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_BLOCKUI'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jquery.blockUI.js"></script><!--  block UI --><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_AUTOCOMPLETE'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/autocomplete/jquery.autocomplete.js"></script><!-- Autocomplete --><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_PAGINATOR'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/pagination/jquery.pagination.js"></script><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JQMODAL'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jqModal/jqModal.js"></script><!-- jqModal --><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_FCK'] )): ?>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
fckeditor/fckeditor.js"></script>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
fckeditor/jquery.FCKEditor.js"></script>
<?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JWYSIWYG'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jwysiwyg/jquery.wysiwyg.js"></script><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JTRUNCATE'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jquery.jtruncate.js"></script><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JMASK'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/maskedInput/jquery.maskedinput.min.js"></script><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_FANCYBOX'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/fancybox/fancybox.js"></script><!-- fancyBox --><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_PNGFIX'] ) || isset ( $this->_tpl_vars['INCLUDE_FANCYBOX'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/pngFix.js"></script><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_TREEVIEW'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/treeview/treeview.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/treeview/treeview.async.js"></script>
<?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_INLINE_MULTI_SELECT'] )): ?><script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/inlinemultiselect/inlinemultiselect1.1.js"></script><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JCROP'] )): ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jcrop/jquery.Jcrop.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
dialogs/CropThumbnail.js"></script>
<?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_SWFUPLOAD'] )): ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
SWFUpload/swfupload.js"></script><!--  swf upload -->
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
SWFUpload/plugins/swfupload.queue.js"></script><!--  swf upload -->
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
SWFUpload/plugins/swfupload.cookies.js"></script><!--  swf upload -->
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
SWFUpload/plugins/swfupload.swfobject.js"></script><!--  swf upload -->
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
SWFUpload/fileprogress.js"></script><!--  swf upload -->
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
SWFUpload/handlers.js"></script><!--  swf upload -->
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
SWFUpload/swfupload.css" type="text/css" media="screen,projection" />
<?php endif; ?>

<?php if (isset ( $this->_tpl_vars['INCLUDE_DAMNUPLOAD'] )): ?>
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
DamnUpload/damnupload.css" type="text/css" media="screen,projection" />
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
DamnUpload/jquery.damnUploader.js"></script>
<?php endif; ?>


<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
prettyprint/prettyprint.js"></script>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jgrowl/jquery.jgrowl.js"></script><!-- jGrowl -->
<script type="text/javascript" src="<?php echo $this->_tpl_vars['ADMIN_SCRIPTS']; ?>
script.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['ADMIN_SCRIPTS']; ?>
tables.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['ADMIN_SCRIPTS']; ?>
iesucks.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/nicEdit/nicEdit.js"></script>
<?php if (isset ( $this->_tpl_vars['SCRIPT_FILE'] )): ?>
<?php $_from = $this->_tpl_vars['SCRIPT_FILE']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['FILE']):
?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['FILE']; ?>
"></script>
<?php endforeach; endif; unset($_from); ?>
<?php endif; ?>


<!-- Tooltip -->
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/tooltip/jquery.tooltip.css" type="text/css" media="screen,projection" />

<!--  DropDownMenu-->
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/dropDownMenu/ddm.css" type="text/css" media="screen,projection" />
<?php if (isset ( $this->_tpl_vars['INCLUDE_JQUERY_MULTISELECT'] )): ?><link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jqueryMultiSelect/jqueryMultiSelect.css" type="text/css" media="screen,projection" /><!--  Multiselect--><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_AUTOCOMPLETE'] )): ?><link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/autocomplete/jquery.autocomplete.css" type="text/css" media="screen,projection" /><!--  Autocomplete--><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_PAGINATOR'] )): ?><link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/pagination/pagination.css" type="text/css" media="screen,projection" /><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JQMODAL'] )): ?><link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jqModal/jqModal.css" type="text/css" media="screen,projection" /><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JWYSIWYG'] )): ?><link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jwysiwyg/jquery.wysiwyg.css" type="text/css" media="screen,projection" /><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_FANCYBOX'] )): ?><link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/fancybox/fancybox.css" type="text/css" media="screen,projection" /><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_TREEVIEW'] )): ?><link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/treeview/treeview.css" type="text/css" media="screen,projection" /><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_INLINE_MULTI_SELECT'] )): ?><link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/inlinemultiselect/inlinemultiselect.css" type="text/css" media="screen,projection" /><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JCROP'] )): ?><link rel="stylesheet" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jcrop/jquery.Jcrop.css" type="text/css" media="screen,projection" /><?php endif; ?>
<?php if (isset ( $this->_tpl_vars['INCLUDE_JQGRID'] )): ?>
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jqGrid/jquery-ui-1.7.1.custom.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jqGrid/ui.jqgrid.css" />
	<script src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jqGrid/grid.locale-ru.js" type="text/javascript"></script>
	<script src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jqGrid/jquery.jqGrid.min.js" type="text/javascript"></script>
	<script src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jqGrid/jqDnR.js" type="text/javascript"></script>
<?php endif; ?>

<?php if (isset ( $this->_tpl_vars['INCLUDE_FANCYBOX'] )): ?>
<?php echo '
<script type="text/javascript">
    $(document).ready(function(){
        $(\'a.fancy\').fancybox();
    });
</script>
'; ?>

<?php endif; ?>

</head>
<body>