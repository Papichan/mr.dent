<?php /* Smarty version 2.6.20, created on 2019-10-04 10:56:23
         compiled from auth.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <title>Авторизация</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['ADMIN_CSS']; ?>
form_bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo $this->_tpl_vars['ADMIN_CSS']; ?>
authorize.css" type="text/css" />
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jquery.js"></script>
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/jquery.form.js"></script><!--  AJAX forms-->
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['LIBS_ALIAS']; ?>
jQuery/validator/validate.min.js"></script><!-- Валидатор форм -->
    <script type="text/javascript" src="<?php echo $this->_tpl_vars['ADMIN_SCRIPTS']; ?>
authorize.js"></script>
</head>
<body>
    <div id="container">
        <h1>Авторизация</h1>
        
        <div id="message"></div>
        
        <form method="post" id="loginForm">
                <div id="formContainer">
                        <div class="formElement">
                                <input type="text" name="username" id="username" value="" data-required />
                                <label class="formElementLabel" for="username">Логин</label>
                        </div>
                        <div class="formElement">
                                <input type="password" name="userpass" id="userpass" data-required />
                                <label class="formElementLabel" for="userpass">Пароль</label>
                        </div>
                        <div id="remember_pass_container">
                                <input type="checkbox" name="remember_pass" id="remember_pass"/>
                                <label for="remember_pass">Запомнить меня</label>
                        </div>
                        <div id="submitContainer">
                                <input type="submit" name="doLogin" class="handyButton" value="Войти" id="submit"/>
                        </div>
                        <div id="pwdRecoveryContainer">
                                <a href="#" id="restorePwd">Забыли пароль?</a>
                        </div>
                        <?php if (! isset ( $this->_tpl_vars['PANEL_ADMIN'] ) && $this->_tpl_vars['FROTN_LOGIN']): ?>
                        <div id="registrationContainer">
                                <a href="/registration/" id="register">Регистрация</a>
                        </div>
                        <?php endif; ?>
                </div>
        </form>
    </div>
    <?php echo '
    <script>
    $(\'#loginForm\').validate({
            eachValidField : function() {
                    $(this).closest(\'div\').removeClass(\'error\').addClass(\'success\');
            },
            eachInvalidField : function() {
                    $(this).closest(\'div\').removeClass(\'success\').addClass(\'error\');
            }
    });
    </script>
    '; ?>

</body>
</html>