<?php /* Smarty version 2.6.20, created on 2019-10-17 13:32:04
         compiled from scripts.tpl */ ?>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SCRIPTS_ALIAS']; ?>
jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SCRIPTS_ALIAS']; ?>
script.js"></script>

<script type="text/javascript" src="<?php echo $this->_tpl_vars['SCRIPTS_ALIAS']; ?>
jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SCRIPTS_ALIAS']; ?>
jquery-migrate-3.0.1.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SCRIPTS_ALIAS']; ?>
slick.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SCRIPTS_ALIAS']; ?>
jquery.simplebox.min.js"></script>
<script type="text/javascript" src="<?php echo $this->_tpl_vars['SCRIPTS_ALIAS']; ?>
main.js"></script>

<?php if (isset ( $this->_tpl_vars['SCRIPT_FILE'] )): ?>
    <?php $_from = $this->_tpl_vars['SCRIPT_FILE']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['FILE']):
?>
        <script type="text/javascript" src="<?php echo $this->_tpl_vars['FILE']; ?>
"></script>
    <?php endforeach; endif; unset($_from); ?>
<?php endif; ?>
