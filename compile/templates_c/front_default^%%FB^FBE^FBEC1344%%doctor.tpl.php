<?php /* Smarty version 2.6.20, created on 2019-10-17 12:46:53
         compiled from doctors/doctor.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'debug', 'doctors/doctor.tpl', 1, false),)), $this); ?>
<?php echo smarty_function_debug(array(), $this);?>


<main>

<!-- ВЕРХ -->
    <section class="up card-doctor">
        <div class="doctors-wrapper">
            <div class="up__back">
                <div class="up__img">
                    <img src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
team.png" alt="">
                </div>
                <div class="up__wrapper content-wrapper">
                    <ul class="breadcrumbs up__breadcrubms">
                        <li><a href="/">Главная <span style="color: #42c6d1;">></span></a></li>
                        <li><a href="<?php echo $this->_tpl_vars['BREADCRUMBS'][0]['alias']; ?>
">Врачи<span style="color: #42c6d1;">></span></a></li>
                        <li><a><?php echo $this->_tpl_vars['DOCTOR'][0]['name']; ?>
</a></li>

                    </ul>
                    <div class="up__text">
                        <div class="up__text1">Наша лучшая  </div><br>
                        <div class="up__text2">Команда <br>специалистов</div>

                    </div>
                    <div class="up__button">
                        <a href="#" class="button button--blue">Записаться на прием <span>>></span></a>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <!-- СТАТИСТИКА ДОКТОРА -->
    <section class="statistic card-doctor">

        <div class="statistic__wrapper content-wrapper">
            <div class="statistic__block">
                <div class="statistic__img">
                    <img src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
icon-tooth1.png" alt="">
                </div>
                <div class="statistic__block-wrap">
                    <p class="statistic__number"><?php echo $this->_tpl_vars['DOCTOR'][0]['experience']; ?>
 лет</p>
                    <p class="statistic__text">Стаж работы</p>
                </div>
            </div>
            <div class="statistic__block">
                <div class="statistic__img">
                    <img src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
icon-tooth2.png" alt="">
                </div>
                <div class="statistic__block-wrap">
                    <p class="statistic__number"><?php echo $this->_tpl_vars['DOCTOR'][0]['certificate']; ?>
</p>
                    <p class="statistic__text">Сертификаты</p>
                </div>
            </div>
            <div class="statistic__block">
                <div class="statistic__img">
                    <img src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
icon-tooth3.png" alt="">
                </div>
                <div class="statistic__block-wrap">
                    <p class="statistic__number"><?php echo $this->_tpl_vars['DOCTOR'][0]['reviews']; ?>
</p>
                    <p class="statistic__text">Отзывы</p>
                </div>
            </div>
    </section>


    <!-- КАРТОЧКА ДОКТОРА -->
    <?php if (! empty ( $this->_tpl_vars['DOCTOR'] )): ?>
    <section  class="column2 card-doctor">
        <h2 class="column2__title"><?php echo $this->_tpl_vars['DOCTOR'][0]['name']; ?>
</h2>
        <div class="content-wrapper  column2__wrapper">
            <div class="column2__left">
                <img  src="<?php echo $this->_tpl_vars['DOCTOR'][0]['imageFull']; ?>
" alt="">
            </div>

            <div class="column2__right">
                <p class="column2__name"><?php echo $this->_tpl_vars['DOCTOR'][0]['name']; ?>
</p>
                <p>Должность: <?php echo $this->_tpl_vars['DOCTOR'][0]['position']; ?>
<br>
                    Специализация: <?php echo $this->_tpl_vars['DOCTOR'][0]['profession']; ?>
<br>
                    Категория: <?php echo $this->_tpl_vars['DOCTOR'][0]['category']; ?>
<br>
                    <?php echo $this->_tpl_vars['DOCTOR'][0]['description']; ?>

                </p>
            </div>
        </div>
    </section>
    <?php endif; ?>


    <!-- ФОРМА НИЗ -->
    <section id="appointment" class="form  form__chair">
        <div class=" content-wrapper">
            <img src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
form-image4.png" alt="">
            <div class="form-wrapper">
                <!-- Загрузка формы отправки "Записаться на прием" -->
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-appointment.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            </div>
        </div>
    </section>

</main>