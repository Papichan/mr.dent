<?php /* Smarty version 2.6.20, created on 2019-10-03 11:40:47
         compiled from structure/pages_level.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'math', 'structure/pages_level.tpl', 10, false),)), $this); ?>
<?php if (! empty ( $this->_tpl_vars['PAGES_TREE'] )): ?>
<ul class="listContainer parent_<?php echo $this->_tpl_vars['PAGES_TREE'][0]['parentID']; ?>
 level_<?php echo $this->_tpl_vars['LEVEL']; ?>
">
<?php $_from = $this->_tpl_vars['PAGES_TREE']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['PAGE_ITEM']):
?>
    <li class="row pageID_<?php echo $this->_tpl_vars['PAGE_ITEM']['pageID']; ?>
">
        <div class="leftControlsContainer">
            <span class="item checkbox"><input type="checkbox" name="checked[]" value="<?php echo $this->_tpl_vars['PAGE_ITEM']['pageID']; ?>
" /></span>
            <span class="item arrows"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
icons/arrows_updown.png" alt="Переместить" /></span>
        </div>
        <div class="captionContainer">
            <span class="item caption" style="margin-left: <?php echo smarty_function_math(array('equation' => "lev * step",'lev' => $this->_tpl_vars['LEVEL'],'step' => 15), $this);?>
px;"><span class="toggleArrow<?php if ($this->_tpl_vars['PAGE_ITEM']['isSection']): ?> <?php if (! empty ( $this->_tpl_vars['PAGE_ITEM']['children'] )): ?>opened<?php else: ?>unloaded<?php endif; ?><?php endif; ?>"><?php if ($this->_tpl_vars['PAGE_ITEM']['isSection']): ?><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
icons/<?php if (! empty ( $this->_tpl_vars['PAGE_ITEM']['children'] )): ?>arrow_down.png<?php else: ?>arrow_right.png<?php endif; ?>" align="absmiddle" /><?php endif; ?></span><?php echo $this->_tpl_vars['PAGE_ITEM']['caption']; ?>
</span>
        </div>
        <div class="rightControlsContainer">
            <span class="item delete"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
icons/cross_bg.png" title="Удалить" alt="Удалить" /></span>
            <span class="item clone"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
icons/page_copy_bg.png" title="Клонировать" alt="Клонировать" /></span>
            <span class="item edit"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
icons/pencil_bg.png" title="Редактировать" alt="Редактировать" /></span>
        </div>
    </li>
    <?php if (! empty ( $this->_tpl_vars['PAGE_ITEM']['children'] )): ?>

    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'structure/pages_level.tpl', 'smarty_include_vars' => array('PAGES_TREE' => $this->_tpl_vars['PAGE_ITEM']['children'],'LEVEL' => $this->_tpl_vars['LEVEL']+1)));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

    <?php endif; ?>
<?php endforeach; endif; unset($_from); ?>
</ul>
<?php endif; ?>