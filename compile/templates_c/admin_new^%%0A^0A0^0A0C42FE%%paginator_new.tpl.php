<?php /* Smarty version 2.6.20, created on 2019-10-04 18:05:24
         compiled from paginator_new.tpl */ ?>
<?php if (isset ( $this->_tpl_vars['paginator'] )): ?>
	<?php $this->assign('page_delta', 3); ?>
	<?php if (! empty ( $this->_tpl_vars['searchWord'] )): ?>
		<?php $this->assign('fl', "&q=".($this->_tpl_vars['searchWord'])); ?>
	<?php else: ?>
		<?php $this->assign('fl', ""); ?>
	<?php endif; ?>
	<?php if (! empty ( $this->_tpl_vars['searchCategory'] )): ?>
		<?php $this->assign('cat', "&category=".($this->_tpl_vars['searchCategory'])); ?>
	<?php else: ?>
		<?php $this->assign('cat', ""); ?>
	<?php endif; ?>

	<div class="paginator">
	<ul class="number-page">
		<?php if ($this->_tpl_vars['paginator']['pageCurrent'] < $this->_tpl_vars['page_delta']): ?><?php $this->assign('page_delta', $this->_tpl_vars['page_delta']*2-$this->_tpl_vars['paginator']['pageCurrent']); ?><?php endif; ?>
		<?php if ($this->_tpl_vars['paginator']['pageCount']-$this->_tpl_vars['paginator']['pageCurrent'] < $this->_tpl_vars['page_delta']): ?><?php $this->assign('page_delta', $this->_tpl_vars['page_delta']*2-$this->_tpl_vars['paginator']['pageCount']+$this->_tpl_vars['paginator']['pageCurrent']); ?><?php endif; ?>
		<?php if ($this->_tpl_vars['paginator']['pageCurrent'] > 1): ?>
			<li>
				<a class="link-prev" href="?<?php echo $this->_tpl_vars['cat']; ?>
<?php echo $this->_tpl_vars['fl']; ?>
&page=<?php echo $this->_tpl_vars['paginator']['pageCurrent']-1; ?>
">prev</a>
			</li>
		<?php else: ?>
			<li>
				<a class="link-prev" href="#">prev</a>
			</li>
		<?php endif; ?>

		<?php unset($this->_sections['pages']);
$this->_sections['pages']['name'] = 'pages';
$this->_sections['pages']['loop'] = is_array($_loop=$this->_tpl_vars['paginator']['pageCount']+1) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['pages']['start'] = (int)1;
$this->_sections['pages']['show'] = true;
$this->_sections['pages']['max'] = $this->_sections['pages']['loop'];
$this->_sections['pages']['step'] = 1;
if ($this->_sections['pages']['start'] < 0)
    $this->_sections['pages']['start'] = max($this->_sections['pages']['step'] > 0 ? 0 : -1, $this->_sections['pages']['loop'] + $this->_sections['pages']['start']);
else
    $this->_sections['pages']['start'] = min($this->_sections['pages']['start'], $this->_sections['pages']['step'] > 0 ? $this->_sections['pages']['loop'] : $this->_sections['pages']['loop']-1);
if ($this->_sections['pages']['show']) {
    $this->_sections['pages']['total'] = min(ceil(($this->_sections['pages']['step'] > 0 ? $this->_sections['pages']['loop'] - $this->_sections['pages']['start'] : $this->_sections['pages']['start']+1)/abs($this->_sections['pages']['step'])), $this->_sections['pages']['max']);
    if ($this->_sections['pages']['total'] == 0)
        $this->_sections['pages']['show'] = false;
} else
    $this->_sections['pages']['total'] = 0;
if ($this->_sections['pages']['show']):

            for ($this->_sections['pages']['index'] = $this->_sections['pages']['start'], $this->_sections['pages']['iteration'] = 1;
                 $this->_sections['pages']['iteration'] <= $this->_sections['pages']['total'];
                 $this->_sections['pages']['index'] += $this->_sections['pages']['step'], $this->_sections['pages']['iteration']++):
$this->_sections['pages']['rownum'] = $this->_sections['pages']['iteration'];
$this->_sections['pages']['index_prev'] = $this->_sections['pages']['index'] - $this->_sections['pages']['step'];
$this->_sections['pages']['index_next'] = $this->_sections['pages']['index'] + $this->_sections['pages']['step'];
$this->_sections['pages']['first']      = ($this->_sections['pages']['iteration'] == 1);
$this->_sections['pages']['last']       = ($this->_sections['pages']['iteration'] == $this->_sections['pages']['total']);
?>
			<?php if ($this->_sections['pages']['index'] > $this->_tpl_vars['paginator']['pageCurrent']-$this->_tpl_vars['page_delta'] && $this->_sections['pages']['index'] < $this->_tpl_vars['paginator']['pageCurrent']+$this->_tpl_vars['page_delta']): ?>
				<?php if ($this->_sections['pages']['index'] == $this->_tpl_vars['paginator']['pageCurrent']): ?>
					<li class="active"><a href="#"><?php echo $this->_sections['pages']['index']; ?>
</a></li>
				<?php else: ?>
				<li><a href="?<?php echo $this->_tpl_vars['cat']; ?>
<?php echo $this->_tpl_vars['fl']; ?>
&page=<?php echo $this->_sections['pages']['index']; ?>
"><?php echo $this->_sections['pages']['index']; ?>
</a></li>
				<?php endif; ?>
			<?php elseif ($this->_sections['pages']['index'] == $this->_tpl_vars['paginator']['pageCurrent']-$this->_tpl_vars['page_delta']): ?>
				<!--a href="">...</a-->
				<?php if ($this->_tpl_vars['paginator']['pageCurrent'] > $this->_tpl_vars['page_delta']): ?>
					<li><a href="?<?php echo $this->_tpl_vars['cat']; ?>
<?php echo $this->_tpl_vars['fl']; ?>
&page=1">1</a></li>
					<?php if ($this->_tpl_vars['paginator']['pageCurrent'] > $this->_tpl_vars['page_delta'] + 1): ?>
					<li><a href="?<?php echo $this->_tpl_vars['cat']; ?>
<?php echo $this->_tpl_vars['fl']; ?>
&page=2">2</a></li>
					<?php endif; ?>
					<?php if ($this->_tpl_vars['paginator']['pageCurrent'] > $this->_tpl_vars['page_delta'] + 2): ?>
						<li><span>...</span></li>
					<?php endif; ?>
				<?php endif; ?>
			<?php elseif ($this->_sections['pages']['index'] == $this->_tpl_vars['paginator']['pageCurrent']+$this->_tpl_vars['page_delta']): ?>
				<?php if ($this->_tpl_vars['paginator']['pageCount'] - $this->_tpl_vars['paginator']['pageCurrent'] > $this->_tpl_vars['page_delta']): ?>
					<li><span>...</span></li>
				<?php endif; ?>
				<?php if ($this->_tpl_vars['paginator']['pageCurrent'] < $this->_tpl_vars['paginator']['pageCount'] - $this->_tpl_vars['page_delta']): ?>
				<li><a href="?<?php echo $this->_tpl_vars['cat']; ?>
<?php echo $this->_tpl_vars['fl']; ?>
&page=<?php echo $this->_tpl_vars['paginator']['pageCount']-1; ?>
"><?php echo $this->_tpl_vars['paginator']['pageCount']-1; ?>
</a></li>
			<?php endif; ?>
				<?php if ($this->_tpl_vars['paginator']['pageCurrent'] <= $this->_tpl_vars['paginator']['pageCount']): ?>
				<li><a href="?<?php echo $this->_tpl_vars['cat']; ?>
<?php echo $this->_tpl_vars['fl']; ?>
&page=<?php echo $this->_tpl_vars['paginator']['pageCount']; ?>
"><?php echo $this->_tpl_vars['paginator']['pageCount']; ?>
</a></li>
			<?php endif; ?>
			<?php endif; ?>
		<?php endfor; endif; ?>
		<?php if ($this->_tpl_vars['paginator']['pageCurrent'] < $this->_tpl_vars['paginator']['pageCount']): ?>
			<li >
				<a class="link-next" href="?<?php echo $this->_tpl_vars['cat']; ?>
<?php echo $this->_tpl_vars['fl']; ?>
&page=<?php echo $this->_tpl_vars['paginator']['pageCurrent']+1; ?>
">next</a>
			</li>
		<?php else: ?>
			<li>
				<a class="link-next" href="#">next</a>
			</li>

		<?php endif; ?>
			</ul>
</div>
<?php endif; ?>