<?php /* Smarty version 2.6.20, created on 2019-10-03 10:44:13
         compiled from navigation.tpl */ ?>
<div class="menuBlock">
    <?php if (isset ( $this->_tpl_vars['USER'] )): ?>
        <ul>
            <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'structure'): ?> class="active"<?php endif; ?>>
                <a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
structure/" title="Управления структурой сайта">Страницы</a>
                <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'structure'): ?>
                    <?php if (isset ( $this->_tpl_vars['MENU_TPL'] )): ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['MENU_TPL']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    <?php endif; ?>
                <?php endif; ?>
            </li>
            <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'anyeditor'): ?>class='active'<?php endif; ?>>
                <a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
AnyEditor/" title="">Данные</a>
                <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'anyeditor'): ?>
                    <?php if (isset ( $this->_tpl_vars['MENU_TPL'] )): ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['MENU_TPL']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    <?php endif; ?>
                <?php endif; ?>
            </li>
            <?php if ($this->_tpl_vars['USER']['role'] == 'admin'): ?>
                <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'custommenu'): ?>class='active'<?php endif; ?>>
                    <a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
custommenu/" title="Управление меню">Меню</a>
                </li>
            <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'reviews'): ?>class='active'<?php endif; ?>><a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
reviews/" title="Отзывы">Отзывы</a>
                <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'reviews'): ?>
                    <?php if (isset ( $this->_tpl_vars['MENU_TPL'] )): ?>
                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['MENU_TPL']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                    <?php endif; ?>
                <?php endif; ?>
            </li>
                <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'store'): ?>class='active'<?php endif; ?>>
                    <a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
store/" title="Управление меню">Магазин</a>
                    <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'store'): ?>
                        <?php if (isset ( $this->_tpl_vars['MENU_TPL'] )): ?>
                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['MENU_TPL']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </li>

                <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'profile'): ?>class='active'<?php endif; ?>>
                    <a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
profile/" title="Управление учетной записью">Профиль</a>
                    <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'profile'): ?>
                        <?php if (isset ( $this->_tpl_vars['MENU_TPL'] )): ?>
                            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['MENU_TPL']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </li>
                <?php if ($this->_tpl_vars['USER']['role'] == 'admin'): ?>
                    <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'users'): ?>class='active'<?php endif; ?>>
                        <a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
users/" title="Управление учётными записями">Пользователи</a>
                        <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'users'): ?>
                            <?php if (isset ( $this->_tpl_vars['MENU_TPL'] )): ?>
                                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['MENU_TPL']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </li>
                    <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'settings'): ?>class='active'<?php endif; ?>>
                        <a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
settings/" title="Настройки сайта">Настройки</a>
                        <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'settings'): ?>
                            <?php if (isset ( $this->_tpl_vars['MENU_TPL'] )): ?>
                                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['MENU_TPL']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </li>
                    <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'routing'): ?>class='active'<?php endif; ?>><a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
routing/" title="Роутинг">Роутинг</a></li>

                    <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'fieldsregistry'): ?>class='active'<?php endif; ?>><a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
fieldsregistry/" title="Регистратор полей">Fieldregistry</a></li>
                    <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'maintenance'): ?>class='active'<?php endif; ?>><a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
maintenance/" title="Обслуживание сайта">Обслуживание</a></li>
                    <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'modules'): ?>class='active'<?php endif; ?>><a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
modules/" title="Управление учётными записями">Модули</a>
                        <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'modules'): ?>
                            <?php if (isset ( $this->_tpl_vars['MENU_TPL'] )): ?>
                                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['MENU_TPL']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </li>
                    <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'editor'): ?>class='active'<?php endif; ?>><a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
editor/" title="Управление учётными записями">Редактор</a>
                        <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'editor'): ?>
                            <?php if (isset ( $this->_tpl_vars['MENU_TPL'] )): ?>
                                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['MENU_TPL']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </li>
                    <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == 'cache'): ?>class='active'<?php endif; ?>><a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
cache/">Очистка кэша</a></li>

                    <?php if (isset ( $this->_tpl_vars['MODULES'] )): ?>
                        <?php $_from = $this->_tpl_vars['MODULES']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['MODULE']):
?>
                            <li <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == $this->_tpl_vars['MODULE']['rootAlias']): ?>class='active'<?php endif; ?>><a href="<?php echo $this->_tpl_vars['ADMIN_ALIAS']; ?>
<?php echo $this->_tpl_vars['MODULE']['rootAlias']; ?>
/" title="<?php echo $this->_tpl_vars['MODULE']['description']; ?>
"><?php echo $this->_tpl_vars['MODULE']['description']; ?>
</a>
                                <?php if ($this->_tpl_vars['CURRENT_SECTION_ALIAS'] == $this->_tpl_vars['MODULE']['rootAlias']): ?>
                                    <?php if (isset ( $this->_tpl_vars['MENU_TPL'] )): ?>
                                        <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ($this->_tpl_vars['MENU_TPL']), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </li>
                        <?php endforeach; endif; unset($_from); ?>
                    <?php endif; ?>

                <?php endif; ?>
            <?php endif; ?>
        </ul>
    <?php endif; ?>
</div>