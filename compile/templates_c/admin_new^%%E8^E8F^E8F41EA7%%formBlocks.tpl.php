<?php /* Smarty version 2.6.20, created on 2019-10-03 12:32:22
         compiled from formBlocks.tpl */ ?>
<?php if (isset ( $this->_tpl_vars['FORM_BLOCKS'] )): ?>
<script type="text/javascript">
<?php echo '
$(document).ready(function()
{
'; ?>

    <?php $_from = $this->_tpl_vars['FORM_BLOCKS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['BLOCK']):
?>
        <?php $_from = $this->_tpl_vars['BLOCK']['controls']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['CONTROL']):
?>

            if (typeof <?php echo $this->_tpl_vars['CONTROL']['type']; ?>
Init != "undefined")
            {
                var <?php echo $this->_tpl_vars['CONTROL']['type']; ?>
<?php echo $this->_tpl_vars['CONTROL']['name']; ?>
InitObj = <?php echo $this->_tpl_vars['CONTROL']['controlJSVarsJSON']; ?>
;
                <?php echo $this->_tpl_vars['CONTROL']['type']; ?>
Init(<?php echo $this->_tpl_vars['CONTROL']['type']; ?>
<?php echo $this->_tpl_vars['CONTROL']['name']; ?>
InitObj);
            }
        <?php endforeach; endif; unset($_from); ?>
    <?php endforeach; endif; unset($_from); ?>

<?php echo '
})
'; ?>

</script>

<?php $_from = $this->_tpl_vars['FORM_BLOCKS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['BLOCK']):
?>
<div class="formBlockHeader"><?php if (! empty ( $this->_tpl_vars['BLOCK']['blockName'] )): ?><?php echo $this->_tpl_vars['BLOCK']['blockName']; ?>
<?php endif; ?></div>
<div class="formBlock">

	<?php $_from = $this->_tpl_vars['BLOCK']['controls']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['CONTROL']):
?>
		<div class="formElement<?php if (isset ( $this->_tpl_vars['CONTROL']['CSSClass'] )): ?> <?php echo $this->_tpl_vars['CONTROL']['CSSClass']; ?>
<?php endif; ?><?php if (isset ( $this->_tpl_vars['CONTROL']['type'] )): ?> <?php echo $this->_tpl_vars['CONTROL']['type']; ?>
<?php endif; ?>">
			<label for="<?php echo $this->_tpl_vars['CONTROL']['name']; ?>
<?php if (isset ( $this->_tpl_vars['CONTROL']['formIndex'] )): ?>_<?php echo $this->_tpl_vars['CONTROL']['formIndex']; ?>
<?php endif; ?>" class="formElementLabel">
				<?php echo $this->_tpl_vars['CONTROL']['description']; ?>

				<?php if (! empty ( $this->_tpl_vars['CONTROL']['tip'] )): ?><span class="helper" title="<?php echo $this->_tpl_vars['CONTROL']['tip']; ?>
">?</span><?php endif; ?>
			</label>
			<?php if (isset ( $this->_tpl_vars['CONTROL']['controlHTML'] )): ?>
				<?php echo $this->_tpl_vars['CONTROL']['controlHTML']; ?>

			<?php else: ?>
				<?php if (isset ( $this->_tpl_vars['CONTROL']['controlTplPath'] )): ?>
					<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => $this->_tpl_vars['CONTROL']['controlTplPath'], 'smarty_include_vars' => array('localVars' => $this->_tpl_vars['CONTROL']['controlTplVars'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				<?php endif; ?>
			<?php endif; ?>
					</div>
	<?php endforeach; endif; unset($_from); ?>
</div>
<div style="clear:both"></div>
<?php endforeach; endif; unset($_from); ?>
<?php endif; ?>