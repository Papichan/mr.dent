<?php /* Smarty version 2.6.20, created on 2019-10-15 15:07:21
         compiled from /var/www/dent/modules/Feedback/templates/admin/message.tpl */ ?>
<h1>Полученное сообщение</h1>

<form action="<?php echo $this->_tpl_vars['MODULE_ALIAS']; ?>
" method="post">
    <ul>
            <li>Заголовок сообщения
                    <br />
                    <div class="feedback_messageItem"><?php echo $this->_tpl_vars['MESSAGE']['title']; ?>
</div>
            </li>

            <?php if (( $this->_tpl_vars['MESSAGE']['doctor'] != 'none' )): ?>
            <li>Доктор
                <br />
                <div class="feedback_messageItem"><?php echo $this->_tpl_vars['MESSAGE']['doctor']; ?>
</div>
            </li>
            <?php endif; ?>

            <li>Пользователь
                    <br />
                    <div class="feedback_messageItem">
                            <strong>Имя</strong>: <?php echo $this->_tpl_vars['MESSAGE']['userName']; ?>
 
                            <br />
                            <strong>E-mail</strong>: <a href="mailto:<?php echo $this->_tpl_vars['MESSAGE']['userEmail']; ?>
"><?php echo $this->_tpl_vars['MESSAGE']['userEmail']; ?>
</a>
                    </div>
            </li>
            
            <?php if (( $this->_tpl_vars['MESSAGE']['userPhone'] != 'none' )): ?>
            <li>Телефон
                <br />
                <div class="feedback_messageItem"><?php echo $this->_tpl_vars['MESSAGE']['userPhone']; ?>
</div>
            </li>
            <?php endif; ?>

            <li>Дата отправки
                    <br />
                    <div class="feedback_messageItem"><?php echo $this->_tpl_vars['MESSAGE']['sendDate']; ?>
</div>
            </li>

            <li>Текст сообщения
                    <br />
                    <div class="feedback_messageItem"><?php echo $this->_tpl_vars['MESSAGE']['text']; ?>
</div>
            </li>
    </ul>
    <input type="checkbox" name="isModeration" value="1" style="cursor:pointer" alt="" <?php if ($this->_tpl_vars['MESSAGE']['isModeration']): ?>checked<?php endif; ?>>
    <input type="hidden" name="messageID" value="<?php echo $this->_tpl_vars['MESSAGE']['messageID']; ?>
" alt="">
    <label title="Установите, если хотите опубликовать отзыв" for="page_isActive">Публиковать</label>
    <div><br/></div>
    <input class="btn" type="submit" name="do" value="Сохранить" alt="">
</form>