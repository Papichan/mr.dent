<?php /* Smarty version 2.6.20, created on 2019-10-04 11:07:02
         compiled from custommenu/index.tpl */ ?>
<h1>Управление меню</h1>

<div style="text-align: right; margin: -30px 0 5px 0;">
    <input type="button" id="goAddTheme" class="goToBtn" value="добавить меню" />
    <input type="hidden" id="goAddTheme_link" value="<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
add/" />
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'paginator.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
<table class="highlightable" id="faqTable">
    <thead>
        <tr>
            <th>№</th>
            <th>Название</th>
            <th>Комментарий</th>
            <th style="width: 10px;"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
icons/pencil.png" /></th>
            <th style="width: 10px;"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
icons/delete_icon.png" /></th>
        </tr>
    </thead>
    <tbody>
<?php if (! empty ( $this->_tpl_vars['LIST_MENU'] )): ?>
    <?php $_from = $this->_tpl_vars['LIST_MENU']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['I'] => $this->_tpl_vars['MENU']):
?>
        <tr>
            <td style="display: none;"><a href="<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
showinner/<?php echo $this->_tpl_vars['MENU']['menuListID']; ?>
/" /></td>
            <td style="width: 20px;"><?php echo $this->_tpl_vars['I']+1; ?>
</td>
            <td>
                <?php echo $this->_tpl_vars['MENU']['menuListName']; ?>

            </td>
            <td>
                <?php echo $this->_tpl_vars['MENU']['menuListComment']; ?>

            </td>
            <td>
                <a href="<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
edit/<?php echo $this->_tpl_vars['MENU']['menuListID']; ?>
/" title="Редактировать"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
edit.png" alt="Редактировать" style="border: 0;" /></a>
            </td>
            <td class="deleteEntry">
                <a href="<?php echo $this->_tpl_vars['CONTROLLER_ALIAS']; ?>
del/<?php echo $this->_tpl_vars['MENU']['menuListID']; ?>
/" title="Удалить"><img src="<?php echo $this->_tpl_vars['ADMIN_IMAGES']; ?>
delete_icon.png" alt="Удалить" style="border: 0;" /></a>
            </td>
        </tr>
    <?php endforeach; endif; unset($_from); ?>
    </tbody>
</table>
<?php else: ?>
    </tbody>
</table>
    <div class="error">Записей не найдено</div>
<?php endif; ?>