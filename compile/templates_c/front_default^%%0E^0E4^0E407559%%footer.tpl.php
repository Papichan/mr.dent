<?php /* Smarty version 2.6.20, created on 2019-10-21 11:21:47
         compiled from footer.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'footer.tpl', 8, false),array('modifier', 'date_format', 'footer.tpl', 64, false),)), $this); ?>
<footer class="footer">
    <div class="footer__up">
        <div class="content-wrapper ">

            <!-- Вывод основных разделов сайта -->
            <?php if (! empty ( $this->_tpl_vars['MAIN_MENU'] )): ?>
                <ul class="footer__menu">   
                    <?php $this->assign('count', count($this->_tpl_vars['MAIN_MENU'])); ?>

                    <?php unset($this->_sections['mySection']);
$this->_sections['mySection']['name'] = 'mySection';
$this->_sections['mySection']['start'] = (int)0;
$this->_sections['mySection']['loop'] = is_array($_loop=$this->_tpl_vars['count']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['mySection']['step'] = ((int)1) == 0 ? 1 : (int)1;
$this->_sections['mySection']['show'] = true;
$this->_sections['mySection']['max'] = $this->_sections['mySection']['loop'];
if ($this->_sections['mySection']['start'] < 0)
    $this->_sections['mySection']['start'] = max($this->_sections['mySection']['step'] > 0 ? 0 : -1, $this->_sections['mySection']['loop'] + $this->_sections['mySection']['start']);
else
    $this->_sections['mySection']['start'] = min($this->_sections['mySection']['start'], $this->_sections['mySection']['step'] > 0 ? $this->_sections['mySection']['loop'] : $this->_sections['mySection']['loop']-1);
if ($this->_sections['mySection']['show']) {
    $this->_sections['mySection']['total'] = min(ceil(($this->_sections['mySection']['step'] > 0 ? $this->_sections['mySection']['loop'] - $this->_sections['mySection']['start'] : $this->_sections['mySection']['start']+1)/abs($this->_sections['mySection']['step'])), $this->_sections['mySection']['max']);
    if ($this->_sections['mySection']['total'] == 0)
        $this->_sections['mySection']['show'] = false;
} else
    $this->_sections['mySection']['total'] = 0;
if ($this->_sections['mySection']['show']):

            for ($this->_sections['mySection']['index'] = $this->_sections['mySection']['start'], $this->_sections['mySection']['iteration'] = 1;
                 $this->_sections['mySection']['iteration'] <= $this->_sections['mySection']['total'];
                 $this->_sections['mySection']['index'] += $this->_sections['mySection']['step'], $this->_sections['mySection']['iteration']++):
$this->_sections['mySection']['rownum'] = $this->_sections['mySection']['iteration'];
$this->_sections['mySection']['index_prev'] = $this->_sections['mySection']['index'] - $this->_sections['mySection']['step'];
$this->_sections['mySection']['index_next'] = $this->_sections['mySection']['index'] + $this->_sections['mySection']['step'];
$this->_sections['mySection']['first']      = ($this->_sections['mySection']['iteration'] == 1);
$this->_sections['mySection']['last']       = ($this->_sections['mySection']['iteration'] == $this->_sections['mySection']['total']);
?>
                        <li><a href="<?php echo $this->_tpl_vars['MAIN_MENU'][$this->_sections['mySection']['index']]['fullAlias']; ?>
">
                                <?php echo $this->_tpl_vars['MAIN_MENU'][$this->_sections['mySection']['index']]['caption']; ?>

                        </a></li>
                    <?php endfor; endif; ?>
                </ul>
            <?php endif; ?>

            <!-- Вывод всехразделов для категории Сервиса -->
            <div class="footer__services">
                <?php if (! empty ( $this->_tpl_vars['SLICE_SERVICES_FOOTER_MENU'] )): ?>
                    <?php $_from = $this->_tpl_vars['SLICE_SERVICES_FOOTER_MENU']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['se']):
?>
                        <div class="footer__block">
                    <?php $_from = $this->_tpl_vars['se']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['services']):
?>
                            <span class="footer__block-title"><?php echo $this->_tpl_vars['services']['name']; ?>
</span>
                            <ul class="footer__block-list">
                                    <?php $_from = $this->_tpl_vars['services']['children']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['serv']):
?>
                                        <li><a href="/services/<?php echo $this->_tpl_vars['services']['alias']; ?>
/<?php echo $this->_tpl_vars['serv']['alias']; ?>
"><?php echo $this->_tpl_vars['serv']['name']; ?>
</a></li>
                                    <?php endforeach; endif; unset($_from); ?>
                            </ul>
                    <?php endforeach; endif; unset($_from); ?>
                        </div>
                    <?php endforeach; endif; unset($_from); ?>
                <?php endif; ?>
            </div>
        </div>
    </div>


    <!-- СОЦ СЕТИ И ПРОЧЕЕ  -->
    <div class="footer__down">
        <div class="footer__social content-wrapper">
            <div class="footer__social-wrapper footer__left">
                <div class=" footer__item footer__address"><?php echo $this->_tpl_vars['SETTINGS']['general']['addressTown']; ?>
 <br><?php echo $this->_tpl_vars['SETTINGS']['general']['addressStreet']; ?>
</div>
                <div class="footer__item footer__number"><a href="tel:<?php echo $this->_tpl_vars['SETTINGS']['general']['telephone']; ?>
"><?php echo $this->_tpl_vars['SETTINGS']['general']['telephone']; ?>
</a></div>
                <div class="footer__item  footer__mail"><a href="mailto:<?php echo $this->_tpl_vars['SETTINGS']['general']['email']; ?>
"><?php echo $this->_tpl_vars['SETTINGS']['general']['email']; ?>
</a></div>
            </div>
            <div class="footer__social-wrapper footer__right">
                <a href="<?php echo $this->_tpl_vars['SETTINGS']['front']['vkontakte']; ?>
"><img width="33" height="33" src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
icon_vk.png" alt="Vkontakte"></a>
                <a href="<?php echo $this->_tpl_vars['SETTINGS']['front']['facebook']; ?>
"><img width="33" height="33" src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
icon_fb.png" alt="Facebook"></a>
                <a href="<?php echo $this->_tpl_vars['SETTINGS']['front']['twitter']; ?>
"><img width="33" height="33" src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
icon_twitter.png" alt="Twitter"></a>
            </div>
        </div>


    </div>
    </div>
</footer>



<!--  СТАНДАРТНЫЙ ФУТЕР МИТЛАБСА И ПОДКЛЮЧЕНИЕ СТИЛЕЙ -->
<!--
<div id="footer">
    <?php echo $this->_tpl_vars['SETTINGS']['general']['siteName']; ?>
 &copy; <?php echo ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y") : smarty_modifier_date_format($_tmp, "%Y")); ?>
 <a href="http://mitlabs.ru/" title="Компания МИТЛабс">МИТЛабс</a>
</div>
-->

<!-- Подключение скриптов и jQuery -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "scripts.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>