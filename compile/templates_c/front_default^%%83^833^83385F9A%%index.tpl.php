<?php /* Smarty version 2.6.20, created on 2019-10-18 15:20:11
         compiled from index/index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('function', 'debug', 'index/index.tpl', 1, false),array('modifier', 'date_format', 'index/index.tpl', 124, false),)), $this); ?>
<?php echo smarty_function_debug(array(), $this);?>


<main>
    <h1 class="visually-hidden"> Центр стоматологии «Mr.Dent»</h1>

    
    <!-- СЛАЙДЕР -->
    <section class="main-block">
        <div class="main-block__wrapper content-wrapper">
            <div class="sl-mb  main-block__slider">
                <?php if (isset ( $this->_tpl_vars['PAGE_CONTENT']['slider'] )): ?>
                    <?php $_from = $this->_tpl_vars['PAGE_CONTENT']['slider']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['slid']):
?>
                    <div>
                        <div class="main-block__slide-img">
                            <img src="<?php echo $this->_tpl_vars['slid']['image']; ?>
" alt="slide1">
                        </div>
                        <div class="main-block__slide main-block__slide1">
                            <p><?php echo $this->_tpl_vars['slid']['text']; ?>
</p>
                            <div class="main-block__button"><a href="<?php echo $this->_tpl_vars['slid']['link']; ?>
" class="button button--blue"><?php echo $this->_tpl_vars['slid']['buttonTitle']; ?>
<span>>></span></a></div>
                        </div>
                    </div>
                    <?php endforeach; endif; unset($_from); ?>
                <?php endif; ?>
            </div>
            <div class="sl-count__main"><span class="sl-count__num__main">1</span> / <span class="sl-count__total__main"></span></div>
        </div>
    </section>

            
            

    <!-- СТАТИСТИКА (Клиника в цифрах) -->
    <?php if (isset ( $this->_tpl_vars['STATISTIC'] )): ?>
    <section class="statistic">
        <h2 class="statistic__title content-wrapper">Клиника в цифрах</h2>
        <div class="statistic__wrapper content-wrapper">
            <?php $_from = $this->_tpl_vars['STATISTIC']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['stat']):
?>
            <div class="statistic__block">
                <div class="statistic__img">
                    <img src="<?php echo $this->_tpl_vars['stat']['img']; ?>
" alt="">
                </div>
                <div class="statistic__block-wrap">
                    <p class="statistic__number"><?php echo $this->_tpl_vars['stat']['nubmer']; ?>
</p>
                    <p class="statistic__text"><?php echo $this->_tpl_vars['stat']['title']; ?>
</p>
                </div>
            </div>
            <?php endforeach; endif; unset($_from); ?>
        </div>
    </section>
    <?php endif; ?>
    
    

    <!-- О КЛИНИКЕ -->
    <section class="aboutblock">
        <h2 class="aboutblock__title aboutblock__title--tablet"> О клинике Mr.Dent</h2>
        <div class="aboutblock__wrapper content-wrapper">
            <div class="aboutblock__left">
                <a class="simplebox" href="#modal01">
                    <div class="video__mask"><img src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['videoMask']; ?>
" alt=""></div>
                </a>
            </div>
            <div class="aboutblock__right">
                <h2 class="aboutblock__title  aboutblock__title--desktop"><?php echo $this->_tpl_vars['PAGE_CONTENT']['titleAboutClinic']; ?>
</h2>
                <p class="aboutblock__text">
                    <?php echo $this->_tpl_vars['PAGE_CONTENT']['textAboutClinic']; ?>

                </p>
                <a class="button button--pink aboutblock__button" href="/about_company/">Подробнее <span>>></span></a>
            </div>
        </div>
        <div id="modal01" class="modal">
            <iframe id="if"  style="border-radius: 5px" src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['linkVideo']; ?>
" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        </div>
    </section>



        



    <!-- УСЛУГИ КОМПАНИИ -->
    <section class="services">
        <img class="services__img" src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['serviceImage']; ?>
" alt="">
        <h2  class="services__title"> Услуги компании</h2>
        <div class=" content-wrapper services__height">
            <div class="services__wrapper ">
                <!-- У каждого блока уникальный css стиль поэтому используется итератор -->
                <?php $this->assign('itterator', 1); ?>
                <?php $_from = $this->_tpl_vars['SERVICE_IMAGES']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['service']):
?>
                <div class="services__item  services__<?php echo $this->_tpl_vars['itterator']++; ?>
">
                    <a  href="<?php echo $this->_tpl_vars['service']['alias']; ?>
">
                        <?php echo $this->_tpl_vars['service']['name']; ?>
 <img src="<?php echo $this->_tpl_vars['service']['smallImage']; ?>
" alt="">
                    </a>
                </div>
                <?php endforeach; endif; unset($_from); ?>
            </div>
            <div class="services__button">
                <p class="services__button-text">Беспокоит боль или хотите узнать
                    о состоянии ваших зубов!</p>
                <div class="services__button-wrapper">
                    <a href="#" class="button button--blue">Бесплатная<br> консультация  </a>
                </div>
            </div>
        </div>
    </section>

            


    <!-- ОТЗЫВЫ КЛИЕНТОВ  -->
    <?php if (isset ( $this->_tpl_vars['REVIEWS'] )): ?>
    <section id="review" class="review">
        <h2 class="review__title">Отзывы клиентов</h2>
        <div class="review__wrapper review__slider content-wrapper">
            <?php $_from = $this->_tpl_vars['REVIEWS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['review']):
?>
            <div>
                <div class="review__item">
                    <div class="prev"></div>
                    <div class="review__av">
                        <img width="172" height="172" src="<?php echo $this->_tpl_vars['review']['image']; ?>
" alt="">
                    </div>
                    <div class="review__text">
                        <div class="review__data"><?php echo ((is_array($_tmp=$this->_tpl_vars['review']['dateAdd'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e.%m.%Y") : smarty_modifier_date_format($_tmp, "%e.%m.%Y")); ?>
</div>
                        <div class="review__name"><?php echo $this->_tpl_vars['review']['name']; ?>
</div>
                        <div class="review__text"><?php echo $this->_tpl_vars['review']['review']; ?>
 </div>
                    </div>
                    <div class="next"></div>
                </div>
            </div>
            <?php endforeach; endif; unset($_from); ?>
        </div>
        <div class="review__button">
            <a href="#" class="button button--pink">Все отзывы <span>>></span></a>
        </div>
    </section>
    <?php endif; ?>



    
    <!-- Наши специалисты -->
    <?php if (isset ( $this->_tpl_vars['PERSONAL'] )): ?>
    <section id="personal" class="personal">
        <h2 class="personal__title">Наши специалисты</h2>
        <div class="sl-fb personal__wrapper personal__slider ">

            <?php $_from = $this->_tpl_vars['PERSONAL']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['person']):
?>
            <div>
                <div class="personal__item">
                    <div class="personal__av">
                        <img width="77" height="77" src="<?php echo $this->_tpl_vars['person']['imageAva']; ?>
" alt="">
                    </div>
                    <div class="personal__text-wrapper">
                        <p class="personal__name"><?php echo $this->_tpl_vars['person']['name']; ?>
 <br> <span class="personal__spec"><?php echo $this->_tpl_vars['person']['profession']; ?>
</span></p>
                        <div class="personal__slide-block">
                            <div class="personal__button">
                                <a href="###" class="button button--blue">Записаться <span>>></span></a>
                            </div>
                            <div class="personal__link">
                                <a href="/doctors/card-doctor/<?php echo $this->_tpl_vars['person']['personalID']; ?>
">Информация о враче <span>>></span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; endif; unset($_from); ?>

        </div>
        <div class="sl-count"><span class="sl-count__num">1</span> / <span class="sl-count__total"></span></div>
    </section>
    <?php endif; ?>




    <!-- НОВОСТИ -->
    <?php if (isset ( $this->_tpl_vars['TEN_NEWS'] )): ?>
    <section class="news">
        <h2 class="news__title">Новости</h2>
        <div  class="news__wrapper news__slider content-wrapper">

            <?php $_from = $this->_tpl_vars['TEN_NEWS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['new']):
?>
            <div>
                <div class="news__item">
                    <a href="/newsline/<?php echo $this->_tpl_vars['new']['alias']; ?>
/<?php echo $this->_tpl_vars['new']['newsID']; ?>
">
                        <div class="news__item-img">
                            <img src="<?php echo $this->_tpl_vars['new']['srcSmall']; ?>
" alt="">
                        </div>
                        <div class="news__item-title">
                            <a href="/newsline/<?php echo $this->_tpl_vars['new']['alias']; ?>
/<?php echo $this->_tpl_vars['new']['newsID']; ?>
"><?php echo $this->_tpl_vars['new']['summary']; ?>
</a></div>
                        <div class="news__item-data"><?php echo ((is_array($_tmp=$this->_tpl_vars['new']['publicationDate'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%e.%m.%Y") : smarty_modifier_date_format($_tmp, "%e.%m.%Y")); ?>
</div>
                    </a>
                </div>
            </div>
            <?php endforeach; endif; unset($_from); ?>

        </div>
        <div class="news__button">
            <a href="/newsline/" class="button button--pink">Все новости <span>>></span></a>
        </div>
    </section>
    <?php endif; ?>



    <!-- Текст для продвижения -->
    <section class="promotion">
        <h2 class="visually-hidden"><?php echo $this->_tpl_vars['PAGE_CONTENT']['promotionTitle']; ?>
</h2>
        <div class="content-wrapper promotion__wrapper">
            <h2 class="promotion__title"><?php echo $this->_tpl_vars['PAGE_CONTENT']['promotionTitle']; ?>
</h2>
            <p class="promotion__text">
                <?php echo $this->_tpl_vars['PAGE_CONTENT']['promotionText']; ?>

            </p>
        </div>
    </section>



    <!-- Yandex MAP -->
    <!-- https://yandex.ua/map-widget/v1/?um=constructor%3Acba5c5b98d71d61f9a1f93de3f46fdfd168eefd035f476d9b3540980e95e312d&amp;source=constructor -->
    <section class="map">
        <div class="map__wrapper ">

            <iframe src="<?php echo $this->_tpl_vars['SETTINGS']['front']['YandexMap']; ?>
" width="100%" height="611" frameborder="0"></iframe>

            <div class="map__address-wrapper">
                <div class="map__address">
                    <div class="map__logo">
                        <img  src="<?php echo $this->_tpl_vars['IMAGES_ALIAS']; ?>
logo.svg" alt="Логотип">
                    </div>
                    <div class="map__text">
                        <p class="map__street"><?php echo $this->_tpl_vars['SETTINGS']['general']['addressTown']; ?>
 <br><?php echo $this->_tpl_vars['SETTINGS']['general']['addressStreet']; ?>
</p>
                        <p class="map__tel"><a href="tel:<?php echo $this->_tpl_vars['SETTINGS']['general']['telephone']; ?>
"><?php echo $this->_tpl_vars['SETTINGS']['general']['telephone']; ?>
</a></p>
                        <p class="map__mail"><a href="mailto:<?php echo $this->_tpl_vars['SETTINGS']['general']['email']; ?>
"><?php echo $this->_tpl_vars['SETTINGS']['general']['email']; ?>
</a></p>
                        <p class="map__work"><?php echo $this->_tpl_vars['SETTINGS']['general']['officeTitle']; ?>
 <br><?php echo $this->_tpl_vars['SETTINGS']['general']['officeHourse']; ?>
</p>
                    </div>
                </div>
            </div>
        </div>
    </section>




    <!-- ФОРМА ОТПРАВКИ НИЗ -->
    <section id="appointment" class="form">
        <div class=" content-wrapper">
            <img src="<?php echo $this->_tpl_vars['PAGE_CONTENT']['imageBottom']; ?>
" alt="">
            <div class="form-wrapper">
                <!-- Форма отправки "Записаться на прием" -->
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'form/form-appointment.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            </div>
        </div>
    </section>

</main>