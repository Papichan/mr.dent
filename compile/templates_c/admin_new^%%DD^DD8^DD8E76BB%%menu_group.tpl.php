<?php /* Smarty version 2.6.20, created on 2019-10-03 17:17:45
         compiled from settings/menu_group.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'settings/menu_group.tpl', 6, false),)), $this); ?>
<ul>
    <?php if (isset ( $this->_tpl_vars['GROUPS'] )): ?>
    <?php $_from = $this->_tpl_vars['GROUPS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['GROUP']):
?>
		<li <?php if (( $this->_tpl_vars['GROUP']['groupID'] == $this->_tpl_vars['GROUP_ID'] )): ?>class="active"<?php endif; ?>>
			<a href="<?php echo $this->_tpl_vars['SECTION_ALIAS']; ?>
<?php echo $this->_tpl_vars['GROUP']['groupID']; ?>
/"><?php echo $this->_tpl_vars['GROUP']['desc']; ?>
</a>
        <?php if (isset ( $this->_tpl_vars['GROUP']['groups'] ) && count($this->_tpl_vars['GROUP']['groups']) > 0): ?>
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'settings/menu_group.tpl', 'smarty_include_vars' => array('GROUPS' => $this->_tpl_vars['GROUP']['groups'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
        <?php endif; ?>
    </li>
    <?php endforeach; endif; unset($_from); ?>
    <?php endif; ?>
</ul>