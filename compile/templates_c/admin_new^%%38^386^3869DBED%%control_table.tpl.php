<?php /* Smarty version 2.6.20, created on 2019-10-04 15:39:53
         compiled from /var/www/dent/libs/ContentManager/Controls/DependendTable/control_table.tpl */ ?>
<input type="hidden" name="<?php if (! empty ( $this->_tpl_vars['localVars']['tableName'] )): ?><?php echo $this->_tpl_vars['localVars']['tableName']; ?>
[<?php if (isset ( $this->_tpl_vars['localVars']['formIndex'] )): ?><?php echo $this->_tpl_vars['localVars']['formIndex']; ?>
][<?php endif; ?><?php echo $this->_tpl_vars['localVars']['NAME']; ?>
]<?php else: ?><?php echo $this->_tpl_vars['localVars']['NAME']; ?>
<?php endif; ?>[]" value="0"/>
<div class="feedback_messageItem <?php echo $this->_tpl_vars['localVars']['NAME']; ?>
">
	<table class="highlightable " style="width: 1020px;">
		<thead>
		<tr>
			<?php $_from = $this->_tpl_vars['localVars']['FIELDS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['name'] => $this->_tpl_vars['field']):
?>
				<th>
					<?php echo $this->_tpl_vars['field']['name']; ?>

				</th>
			<?php endforeach; endif; unset($_from); ?>
			<th width="20%" style="text-align: center;">Добавить поле <a href="#" id="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
_add">+</a></th>
		</tr>
		</thead>
		<tbody>
		<tr style="display:none" class="default">
			<?php $_from = $this->_tpl_vars['localVars']['FIELDS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['name'] => $this->_tpl_vars['field']):
?>
				<td>
					<?php if ($this->_tpl_vars['field']['type'] == 'select'): ?>
						<select name="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['name']; ?>
[]">
							<option value=""> --- </option>
							<?php $_from = $this->_tpl_vars['field']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['OPTION']):
?>
								<option value="<?php echo $this->_tpl_vars['OPTION']['VALUE']; ?>
"><?php echo $this->_tpl_vars['OPTION']['TEXT']; ?>
</option>
							<?php endforeach; endif; unset($_from); ?>
						</select>
					<?php elseif ($this->_tpl_vars['field']['type'] == 'hidden'): ?>
						<input type="hidden" name="<?php if (! empty ( $this->_tpl_vars['localVars']['tableName'] )): ?><?php echo $this->_tpl_vars['localVars']['tableName']; ?>
[<?php if (isset ( $this->_tpl_vars['localVars']['formIndex'] )): ?><?php echo $this->_tpl_vars['localVars']['formIndex']; ?>
][<?php endif; ?><?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['name']; ?>
]<?php else: ?><?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['name']; ?>
<?php endif; ?>[]" value=""  />
					<?php elseif ($this->_tpl_vars['field']['type'] == 'textarea'): ?>
						<textarea name="<?php if (! empty ( $this->_tpl_vars['localVars']['tableName'] )): ?><?php echo $this->_tpl_vars['localVars']['tableName']; ?>
[<?php if (isset ( $this->_tpl_vars['localVars']['formIndex'] )): ?><?php echo $this->_tpl_vars['localVars']['formIndex']; ?>
][<?php endif; ?><?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['name']; ?>
]<?php else: ?><?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['name']; ?>
<?php endif; ?>[]"></textarea>
					<?php else: ?>
						<input type="text" name="<?php if (! empty ( $this->_tpl_vars['localVars']['tableName'] )): ?><?php echo $this->_tpl_vars['localVars']['tableName']; ?>
[<?php if (isset ( $this->_tpl_vars['localVars']['formIndex'] )): ?><?php echo $this->_tpl_vars['localVars']['formIndex']; ?>
][<?php endif; ?><?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['name']; ?>
]<?php else: ?><?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['name']; ?>
<?php endif; ?>[]" value=""  />
					<?php endif; ?>
				</td>
			<?php endforeach; endif; unset($_from); ?>
			<td style="text-align: center;">
				<input class="btn" value="-" type="button" alt="" onclick="<?php echo '$(this).parents(\'tr\').remove();'; ?>
"/>
				<input class="btn" value="&uparrow;" type="button" alt=""/>
				<input class="btn" value="&downarrow;" type="button" alt=""/>
			</td>
		</tr>
		<?php $_from = $this->_tpl_vars['localVars']['OPTIONS']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['ind'] => $this->_tpl_vars['item']):
?>
			<tr id="itms_<?php echo $this->_tpl_vars['ind']; ?>
" title="ololol">
				<?php $_from = $this->_tpl_vars['item']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['field'] => $this->_tpl_vars['val']):
?>
					<td>
						<?php if ($this->_tpl_vars['localVars']['FIELDS'][$this->_tpl_vars['field']]['type'] == 'select'): ?>
							<select name="<?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['field']; ?>
[]">
								<option value=""> --- </option>
								<?php $_from = $this->_tpl_vars['localVars']['FIELDS'][$this->_tpl_vars['field']]['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['OPTION']):
?>
									<option value="<?php echo $this->_tpl_vars['OPTION']['VALUE']; ?>
" <?php if ($this->_tpl_vars['val'] == $this->_tpl_vars['OPTION']['VALUE']): ?> selected="selected"<?php endif; ?>><?php echo $this->_tpl_vars['OPTION']['TEXT']; ?>
</option>
								<?php endforeach; endif; unset($_from); ?>
							</select>
						<?php elseif ($this->_tpl_vars['localVars']['FIELDS'][$this->_tpl_vars['field']]['type'] == 'hidden'): ?>
							<input type="hidden" name="<?php if (! empty ( $this->_tpl_vars['localVars']['tableName'] )): ?><?php echo $this->_tpl_vars['localVars']['tableName']; ?>
[<?php if (isset ( $this->_tpl_vars['localVars']['formIndex'] )): ?><?php echo $this->_tpl_vars['localVars']['formIndex']; ?>
][<?php endif; ?><?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['field']; ?>
]<?php else: ?><?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['field']; ?>
<?php endif; ?>[]" value="<?php echo $this->_tpl_vars['val']; ?>
"  />
						<?php elseif ($this->_tpl_vars['localVars']['FIELDS'][$this->_tpl_vars['field']]['type'] == 'textarea'): ?>
							<textarea name="<?php if (! empty ( $this->_tpl_vars['localVars']['tableName'] )): ?><?php echo $this->_tpl_vars['localVars']['tableName']; ?>
[<?php if (isset ( $this->_tpl_vars['localVars']['formIndex'] )): ?><?php echo $this->_tpl_vars['localVars']['formIndex']; ?>
][<?php endif; ?><?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['field']; ?>
]<?php else: ?><?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['field']; ?>
<?php endif; ?>[]"><?php echo $this->_tpl_vars['val']; ?>
</textarea>
						<?php else: ?>
							<input type="text" name="<?php if (! empty ( $this->_tpl_vars['localVars']['tableName'] )): ?><?php echo $this->_tpl_vars['localVars']['tableName']; ?>
[<?php if (isset ( $this->_tpl_vars['localVars']['formIndex'] )): ?><?php echo $this->_tpl_vars['localVars']['formIndex']; ?>
][<?php endif; ?><?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['field']; ?>
]<?php else: ?><?php echo $this->_tpl_vars['localVars']['NAME']; ?>
__<?php echo $this->_tpl_vars['field']; ?>
<?php endif; ?>[]"  value="<?php echo $this->_tpl_vars['val']; ?>
"  />
						<?php endif; ?>
					</td>
				<?php endforeach; endif; unset($_from); ?>
				<td style="text-align: center;">
					<input class="btn" value="-" type="button" alt="" onclick="<?php echo '$(this).parents(\'tr\').remove();'; ?>
"/>
					<input class="btn up" value="&uparrow;" type="button" alt=""/>
					<input class="btn down" value="&downarrow;" type="button" alt=""/>
				</td>
			</tr>
		<?php endforeach; endif; unset($_from); ?>
		</tbody>
	</table>
</div>